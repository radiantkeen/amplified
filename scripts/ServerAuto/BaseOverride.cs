function RepairKit::onUse(%data,%obj)
{
   //----------------------------------------------------------------------------
   // z0dd - ZOD, 8/10/02. Let players use repair kit regardless of health status
   // if they choose so via client $pref::
   if (%obj.client.wasteRepKit == 1)
   {
      %obj.decInventory(%data,1);
      messageClient(%obj.client, 'MsgRepairKitUsed', '\c2Repair Kit Used.');
      if (%obj.getDamageLevel() != 0)
      {
         %obj.applyRepair(1.0);
         %obj.playAudio(0, RepairPatchSound);
         StatusEffect.stopAllEffects(%obj);
      }
   }
   else
   {
      // Don't use the kit unless we're damaged
      if (%obj.getDamageLevel() != 0)
      {
         %obj.applyRepair(1.0);
         StatusEffect.stopAllEffects(%obj);
         %obj.playAudio(0, RepairPatchSound);
         %obj.decInventory(%data,1);
         messageClient(%obj.client, 'MsgRepairKitUsed', '\c2Repair Kit Used.');
      }
   }
}

function RepairPatch::onCollision(%data,%obj,%col)
{
   if(%col.isPlayer() && %col.getDamageLevel() != 0 && %col.getState() !$= "Dead" && !%col.isMounted())
   {
      %col.playAudio(0, RepairPatchSound);
      %col.applyRepair(0.75);
      StatusEffect.stopAllEffects(%obj);
      
      if(%obj.isTemporary)
         %obj.delete();
      else
         %obj.respawn();

      if(%col.client > 0)
         messageClient(%col.client, 'MsgItemPickup', '\c0You picked up %1.', %data.pickUpName);
   }
}

function Item::respawn(%this)
{
   %this.startFade(0, 0, true);
   %this.schedule($ItemRespawnTime + 100, "startFade", 1000, 0, false);
   %this.hide(true);
   %this.schedule($ItemRespawnTime, "hide", false);
}

function StaticShapeData::damageObject(%data, %targetObject, %sourceObject, %position, %amount, %damageType)
{
   // if this is a non-team mission type and the object is "protected", don't damage it
   if(%data.noIndividualDamage && Game.allowsProtectedStatics())
      return;

   // if this is a Siege mission and this object shouldn't take damage (e.g. vehicle stations)
   if(%data.noDamageInSiege && Game.class $= "SiegeGame")
      return;

   if(%sourceObject && %targetObject.isEnabled())
   {
      if(%sourceObject.client)
      {
        %targetObject.lastDamagedBy = %sourceObject.client;
        %targetObject.lastDamagedByTeam = %sourceObject.client.team;
        %targetObject.damageTimeMS = GetSimTime();
      }
      else
      {
        %targetObject.lastDamagedBy = %sourceObject;
        %targetObject.lastDamagedByTeam = %sourceObject.team;
        %targetObject.damageTimeMS = GetSimTime();
      }
   }

   // Scale damage type & include shield calculations...
   if (%data.isShielded && (%targetObject.lastHitFlags & $Projectile::IgnoreShields) == 0)
      %amount = %data.checkShields(%targetObject, %position, %amount, %damageType);

   %damageScale = %data.damageScale[%damageType];
   if(%damageScale !$= "")
      %amount *= %damageScale;

   %amount *= %targetObject.damageReduceFactor;
   
    //if team damage is off, cap the amount of damage so as not to disable the object...
    if (!$TeamDamage && !%targetObject.getDataBlock().deployedObject)
    {
       // -------------------------------------
       // z0dd - ZOD, 6/24/02. Console spam fix
       if(isObject(%sourceObject))
       {
          //see if the object is being shot by a friendly
          if(%sourceObject.getDataBlock().catagory $= "Vehicles")
             %attackerTeam = getVehicleAttackerTeam(%sourceObject);
          else
             %attackerTeam = %sourceObject.team;
      }
      if ((%targetObject.getTarget() != -1) && isTargetFriendly(%targetObject.getTarget(), %attackerTeam))
      {
         %curDamage = %targetObject.getDamageLevel();
         %availableDamage = %targetObject.getDataBlock().disabledLevel - %curDamage - 0.05;
         if (%amount > %availableDamage)
            %amount = %availableDamage;
      }
    }

   // if there's still damage to apply
   if (%amount > 0)
      %targetObject.applyDamage(%amount);
      
    // reset so we don't get other damage bleedover
    %targetObject.lastHitFlags = 0;
}
function serverCmdUse(%client,%data)
{
   // Item names from the client must converted
   // into DataBlocks
   // %data = ItemDataBlock[%item];
   if(isObject(%client.player)) // z0dd - ZOD, 5/18/03. Console spam fix
      %client.getControlObject().use(%data);
}

function ShapeBase::use(%this, %data)
{
   //if(%data.class $= "Weapon") {
   //error("ShapeBase::use " @ %data);
   //}
   if(isObject(%this) && %data !$= "") // z0dd - ZOD, 5/18/03. Console spam fix
   {
      if(%data $= Grenade)
      {
         // figure out which grenade type you're using
         for(%x = 0; $InvGrenade[%x] !$= ""; %x++) {
            if(%this.inv[$NameToInv[$InvGrenade[%x]]] > 0)
            {
               %data = $NameToInv[$InvGrenade[%x]];
               break;
            }
         }
      }
      else if(%data $= Mine) // z0dd - ZOD, 5/18/03. For more mine types
      {
         // figure out which mine type you're using
         for(%m = 0; $InvMine[%m] !$= ""; %m++)
         {
//            error("finding mine" SPC $NameToInv[$InvMine[%m]] SPC %this.inv[$NameToInv[$InvMine[%m]]]);
            
            if(%this.inv[$NameToInv[$InvMine[%m]]] > 0)
            {
               %data = $NameToInv[$InvMine[%m]];
               break;
            }
         }
      }
      else if(%data $= "Backpack") {
         %pack = %this.getMountedImage($BackpackSlot);
         // if you don't have a pack but have placed a satchel charge, detonate it
         if(!%pack && (%this.thrownChargeId > 0) && %this.thrownChargeId.armed )
         {
            %this.playAudio( 0, SatchelChargeExplosionSound );
            schedule( 600, %this, "detonateSatchelCharge", %this ); // z0dd - ZOD, 8/24/02. Time after pressing fire that satchel blows. Was 800
            return true;
         }
         return false;
      }
      else if(%data $= Beacon)
      {
         %data.onUse(%this);
         if (%this.inv[%data.getName()] > 0)
            return true;
      }

      // default case
      if (%this.inv[%data.getName()] > 0) {
         %data.onUse(%this);
         return true;
      }
      return false;
   }
}

function serverCmdThrow(%client,%data)
{
   // Item names from the client must converted
   // into DataBlocks
   // %data = ItemDataBlock[%item];

//   error("throw" SPC %client SPC %client.player SPC %data.getName());
   //-----------------------------------------------------------------------
   // z0dd - ZOD, 4/18/02. Let one keybind handle all grenade types.
   if(isObject(%client.player)) // z0dd - ZOD, 5/18/03. Console spam fix
   {
      if(%data $= Grenade)
      {
         // figure out which grenade type you're using
         for(%x = 0; $InvGrenade[%x] !$= ""; %x++)
         {
            if(%client.getControlObject().inv[$NameToInv[$InvGrenade[%x]]] > 0)
            {
               %data = $NameToInv[$InvGrenade[%x]];
               break;
            }
         }
         %client.getControlObject().throw(%data);
      }
      else if(%data $= Mine)
      {
         for(%x = 0; $InvMine[%x] !$= ""; %x++)
         {
            if(%client.getControlObject().inv[$NameToInv[$InvMine[%x]]] > 0)
            {
               %data = $NameToInv[$InvMine[%x]];
               break;
            }
         }
         %client.getControlObject().throw(%data);
      }
      else if(%data $= "Ammo")
      {
         %weapon = %client.getControlObject().getMountedImage($WeaponSlot);
         if(%weapon !$= "")
         {
            if(%weapon.ammo !$= "")
               %client.getControlObject().throw(%weapon.ammo);
            else
               return;
         }
      }
      else
         %client.getControlObject().throw(%data);
   }
}

function ShapeBase::throw(%this,%data)
{
   if(!isObject(%data))
      return false;

   if (%this.inv[%data.getName()] > 0) {

      // save off the ammo count on this item
      if( %this.getInventory( %data ) < $AmmoIncrement[%data.getName()] )
         %data.ammoStore = %this.getInventory( %data );
      else
         %data.ammoStore = $AmmoIncrement[%data.getName()];

      // Throw item first...
      %this.throwItem(%data);
      if($AmmoIncrement[%data.getName()] !$= "")
         %this.decInventory(%data,$AmmoIncrement[%data.getName()]);
      else
         %this.decInventory(%data,1);
      return true;
   }
   return false;
}

function ShapeBase::throwItem(%this,%data)
{
   %item = new Item() {
      dataBlock = %data;
      rotation = "0 0 1 " @ (getRandom() * 360);
   };

   %item.ammoStore = %data.ammoStore;
   MissionCleanup.add(%item);
   %this.throwObject(%item);
}

function ShapeBase::throwObject(%this,%obj)
{
   //------------------------------------------------------------------
   // z0dd - ZOD, 4/15/02. Allow respawn switching during tourney wait.
   if(!$MatchStarted)
      return;
   //------------------------------------------------------------------

   // z0dd - ZOD, 5/26/02. Remove anti-hover so flag can be thrown properly
   if(%obj.getDataBlock().getName() $= "Flag")
   {
      %obj.static = false;
      // z0dd - ZOD - SquirrelOfDeath, 10/02/02. Hack for flag collision bug.
      if(Game.Class $= CTFGame || Game.Class $= PracticeCTFGame)
         %obj.searchSchedule = Game.schedule(10, "startFlagCollisionSearch", %obj);
   }
   //------------------------------------------------------------------

   %srcCorpse = (%this.getState() $= "Dead"); // z0dd - ZOD, 4/14/02. Flag tossed from corpse
   //if the object is being thrown by a corpse, use a random vector
   if (%srcCorpse && %obj.getDataBlock().getName() !$= "Flag") // z0dd - ZOD, 4/14/02. Except for flags..
   {
      %vec = (-1.0 + getRandom() * 2.0) SPC (-1.0 + getRandom() * 2.0) SPC getRandom();
      %vec = vectorScale(%vec, 10);
   }
   else // else Initial vel based on the dir the player is looking
   {
      %eye = %this.getEyeVector();
      %vec = vectorScale(%eye, 20);
   }

   // Add a vertical component to give the item a better arc
   %dot = vectorDot("0 0 1",%eye);
   if (%dot < 0)
      %dot = -%dot;
   %vec = vectorAdd(%vec,vectorScale("0 0 12",1 - %dot)); // z0dd - ZOD, 9/10/02. 10 was 8

   // Add player's velocity
   %vec = vectorAdd(%vec,%this.getVelocity());
   %pos = getBoxCenter(%this.getWorldBox());

   //since flags have a huge mass (so when you shoot them, they don't bounce too far)
   //we need to up the %vec so that you can still throw them...
   if (%obj.getDataBlock().getName() $= "Flag")
   {
      %vec = vectorScale(%vec, (%srcCorpse ? 40 : 75)); // z0dd - ZOD, 4/14/02. Throw flag force. Value was 40
      // ------------------------------------------------------------
      // z0dd - ZOD, 9/27/02. Delay on grabbing flag after tossing it
      %this.flagTossWait = true;
      %this.schedule(1000, resetFlagTossWait);
      // ------------------------------------------------------------
   }

   //
   %obj.setTransform(%pos);
   %obj.applyImpulse(%pos,%vec);
   %obj.setCollisionTimeout(%this);
   %data = %obj.getDatablock();

   %data.onThrow(%obj,%this);

   //call the AI hook
   AIThrowObject(%obj);
}

function Pack::onInventory(%data,%obj,%amount)
{
	//only do this for players
	if(%obj.getClassName() !$= "Player")
		return;

   // Auto-mount the packs on players
   if((%oldPack = %obj.getMountedImage($BackpackSlot)) != 0)
      %obj.setInventory(%oldPack.item, 0);
   if (%amount && %obj.getDatablock().className $= Armor)
   {
		// if you picked up another pack after you placed a satchel charge but
		// before you detonated it, delete the charge
		if(%obj.thrownChargeId > 0)
		{
			%obj.thrownChargeId.delete();
			%obj.thrownChargeId = 0;
		}
      %obj.mountImage(%data.image,$BackpackSlot);
	   %obj.client.setBackpackHudItem(%data.getName(), 1);
	}
	if(%amount == 0 )
   {
      if ( %data.getName() $= "SatchelCharge" )
         %obj.client.setBackpackHudItem( "SatchelUnarmed", 1 );
      else
	      %obj.client.setBackpackHudItem(%data.getName(), 0);
   }
   ItemData::onInventory(%data,%obj,%amount);
   
   // Do bottomprint for pack info
   %packid = $BackpackListID[%data.getName()];
   
   if(%packid !$= "" && %amount > 0)
      commandToClient(%obj.client, 'BottomPrint', "<color:ffd589>"@$BackpackListData[%packid, "name"]@"<color:ffffff>" NL $BackpackListData[%packid, "desc"], 30, 2); // NL $BackpackListData[%packid, "stats"]
}

function checkTurretMount(%data, %obj, %slot)
{
   // search for a turret base in player's LOS
   %eyeVec = VectorNormalize(%obj.getEyeVector());
   %srchRange = VectorScale(%eyeVec, 5.0); // look 5m for a turret base
   %plTm = %obj.getEyeTransform();
   %plyrLoc = firstWord(%plTm) @ " " @ getWord(%plTm, 1) @ " " @ getWord(%plTm, 2);
   %srchEnd = VectorAdd(%plyrLoc, %srchRange);
   %potTurret = ContainerRayCast(%obj.getEyeTransform(), %srchEnd, $TypeMasks::TurretObjectType);
   if(%potTurret != 0)
   {
      %otherMountObj = "foo";

      %name = %potTurret.getDatablock().getName(); // z0dd - ZOD, 4/20/02. modular MPB turret
      if(%name $= "TurretBaseLarge" || %name $= "MobileTurretBase" || %name $= "GunshipTurret") // z0dd - ZOD, 4/20/02. modular MPB turret
      {
         // found a turret base, what team is it on?
         if(%potTurret.team == %obj.client.team)
         {
            if(%potTurret.getDamageState() !$= "Enabled")
	    {
               // the base is destroyed
               messageClient(%obj.client, 'MsgBaseDestroyed', "\c2Turret is disabled, cannot mount barrel."); // z0dd - ZOD, 4/20/02. modular MPB turret
               %obj.setImageTrigger($BackpackSlot, false);
            }
            else
            {
               // it's a functional turret base on our team! stick the barrel on it
               messageClient(%obj.client, 'MsgTurretMount', "\c2Mounting barrel pack on turret."); // z0dd - ZOD, 4/20/02. modular MPB turret
               serverPlay3D(TurretPackActivateSound, %potTurret.getTransform());
               %potTurret.initiateBarrelSwap(%obj);
            }
         }
         else
         {
            // whoops, wrong team
            messageClient(%obj.client, 'MsgTryEnemyTurretMount', "\c2Cannot mount a barrel on an enemy turret base!");
            %obj.setImageTrigger($BackpackSlot, false);
         }
      }
      else
      {
         // tried to mount barrel on some other turret type
         messageClient(%obj.client, 'MsgNotTurretBase', "\c2Can only mount a barrel on a turret base.");
         %obj.setImageTrigger($BackpackSlot, false);
      }
   }
   else
   {
      // I don't see any turret
      messageClient(%obj.client, 'MsgNoTurretBase', "\c2No turret within range.");
      %obj.setImageTrigger($BackpackSlot, false);
   }
}

function serverCmdControlObject(%client, %targetId)
{
   // match started:
   if(!$MatchStarted)
   {
      commandToClient(%client, 'ControlObjectResponse', false, "mission has not started.");
      return;
   }

   // z0dd - ZOD, 8/9/03. No turret control in Spawn CTF.
   if(isObject(Game))
   {
      if(Game.class $= SCtFGame)
      {
         commandToClient(%client, 'ControlObjectResponse', false, "turret disabled in this gametype.");
         return;
      }
   }
   // object:
   %obj = getTargetObject(%targetId);
   if(%obj == -1)
   {
      commandToClient(%client, 'ControlObjectResponse', false, "failed to find target object.");
      return;
   }

   // shapebase:
   if(!(%obj.getType() & $TypeMasks::ShapeBaseObjectType))
   {
      commandToClient(%client, 'ControlObjectResponse', false, "object cannot be controlled.");
      return;
   }

   // can control:
   if(!%obj.getDataBlock().canControl || %obj.getMountedImage(0).getName() $= "MissileBarrelLarge") // z0dd - ZOD 4/18/02. Prevent missile barrels from being controlled
   {
      commandToClient(%client, 'ControlObjectResponse', false, "object cannot be controlled.");
      return;
   }

   // check damage:
   if(%obj.getDamageState() !$= "Enabled")
   {
      commandToClient(%client, 'ControlObjectResponse', false, "object is " @ %obj.getDamageState());
      return;
   }

   // powered:
   if(!%obj.isPowered())
   {
      commandToClient(%client, 'ControlObjectResponse', false, "object is not powered.");
      return;
   }

   // controlled already:
   %control = %obj.getControllingClient();
   if(%control)
   {
      if(%control == %client)
         commandToClient(%client, 'ControlObjectResponse', false, "you are already controlling that object.");
      else
         commandToClient(%client, 'ControlObjectResponse', false, "someone is already controlling that object.");
      return;
   }

   // same team?
   if(getTargetSensorGroup(%targetId) != %client.getSensorGroup())
   {
      commandToClient(%client, 'ControlObjectResonse', false, "cannot control enemy objects.");
      return;
   }

   // dead?
   if(%client.player == 0)
   {
      commandToClient(%client, 'ControlObjectResponse', false, "dead people cannot control objects.");
      return;
   }

   //mounted in a vehicle?
   if (%client.player.isMounted() || isObject(%client.walker))
   {
      commandToClient(%client, 'ControlObjectResponse', false, "can't control objects while mounted in a vehicle.");
      return;
   }

   %client.setControlObject(%obj);
   commandToClient(%client, 'ControlObjectResponse', true, getControlObjectType(%obj));

   // --------------------------------------------------------------------------------------------------------
   // z0dd - ZOD, 5/12/02. Change turrets name to controllers name.
   if((%obj.getType() & $TypeMasks::TurretObjectType) && (!%client.isAIControlled()))
   {
      // Set this varible on the client so we can reset turret nameTag when client is done.
      %client.TurretControl = %obj;

      if(%obj.nameTag !$= "") // Get the name tag for storage, this is created in the *.mis file.
      {
         %obj.oldTag = getTaggedString(%obj.nameTag); // Store this nameTag in a var on the turret.
         removeTaggedString(%obj.nameTag); // Reset the turrets nameTag.
         %obj.nameTag = "";
      }
      else // This is either a deployed turret or the *.mis file has no nameTag for it.
      {
         %obj.oldTag = ""; // No nameTag to store on turret (paranoia).

         // Reset the turrets targetNameTag. This may cause problems - ZOD
         //removeTaggedString(%obj.getDataBlock().targetNameTag);
         //%obj.getDataBlock().targetNameTag = "";
      }

      // Reset the turrets target
      freeTarget(%obj.getTarget());

      // Set the turrets target and new nameTag.
      %obj.nameTag = addTaggedString(%client.nameBase @ " controlling ");
      %obj.target = createTarget(%obj, %obj.nameTag, "", "", %obj.getDatablock().targetTypeTag, %obj.team, 0);
      setTargetSensorGroup(%obj.target, %obj.team);
   }
   // --------------------------------------------------------------------------------------------------------
}

function resetControlObject(%client)
{
   if( isObject( %client.comCam ) )
      %client.comCam.delete();

   if(isObject(%client.player) && !%client.player.isDestroyed() && $MatchStarted)
   {
      if(isObject(%client.walker))
         %client.setControlObject(%client.walker);
      else
         %client.setControlObject(%client.player);
   }
   else
      %client.setControlObject(%client.camera);

   // -----------------------------------------------------------------------------------------------------------------------
   // z0dd - ZOD, 5/12/02. Reset the turrets nameTag back to its original.
   if(%client.TurretControl !$= "")
      %turret = %client.TurretControl;
   else
      return;

   if(isObject(%turret))
   {
      // Reset the turrets target and nameTag
      removeTaggedString(%turret.nameTag);
      %turret.nameTag = "";
      freeTarget(%turret.getTarget());

      // Set the turrets target and new nameTag
      if(%turret.oldTag !$= "")
         %turret.nameTag = addTaggedString(%turret.oldTag);
      else
         //%turret.nameTag = addTaggedString(getTaggedString(%turret.getDataBlock().targetNameTag));
         %turret.nameTag = %turret.getDataBlock().targetNameTag; // This should allready be a tagged string

      %turret.target = createTarget(%turret, %turret.nameTag, "", "", %turret.getDatablock().targetTypeTag, %turret.team, 0);
      setTargetSensorGroup(%turret.target, %turret.team);

      // Reset the varible set on the client and turret
      %turret.oldTag = "";
      %client.TurretControl = "";
   }
   // -----------------------------------------------------------------------------------------------------------------------
}

function serverCmdResetControlObject(%client)
{
   resetControlObject(%client);
   commandToClient(%client, 'ControlObjectReset');
   // z0dd - ZOD 6/04/02. Vehicle reticle fix.
   if(isObject(%client.player))
   {
      if(%client.player.isPilot() || %client.player.isWeaponOperator())
      {
         return;
      }
      else
      {
         commandToClient(%client, 'RemoveReticle');
         %weapon = %client.player.getMountedImage($WeaponSlot);
         %client.setWeaponsHudActive(%weapon.item);
      }
   }
}

//----------------------------------------------------------------------------
function ShapeBase::cycleWeapon( %this, %data )
{
   if(%this.isWalker)
   {
        %this.walkerCycleWeapon(%data);
        return;
   }

   if ( %this.weaponSlotCount == 0 )
      return;

   %slot = -1;
   if ( %this.getMountedImage($WeaponSlot) != 0 )
   {
      %curWeapon = %this.getMountedImage($WeaponSlot).item.getName();
      for ( %i = 0; %i < %this.weaponSlotCount; %i++ )
      {
         //error("curWeaponName == " @ %curWeaponName);
         if ( %curWeapon $= %this.weaponSlot[%i] )
         {
            %slot = %i;
            break;
         }
      }
   }

   if ( %data $= "prev" )
   {
      // Previous weapon...
      if ( %slot == 0 || %slot == -1 )
      {
         %i = %this.weaponSlotCount - 1;
         %slot = 0;
      }
      else
         %i = %slot - 1;
   }
   else
   {
      // Next weapon...
      if ( %slot == ( %this.weaponSlotCount - 1 ) || %slot == -1 )
      {
         %i = 0;
         %slot = ( %this.weaponSlotCount - 1 );
      }
      else
         %i = %slot + 1;
   }

   %newSlot = -1;
   while ( %i != %slot )
   {
      if ( %this.weaponSlot[%i] !$= ""
        && %this.hasInventory( %this.weaponSlot[%i] )
        && %this.hasAmmo( %this.weaponSlot[%i] ) )
      {
         // player has this weapon and it has ammo or doesn't need ammo
         %newSlot = %i;
         break;
      }

      if ( %data $= "prev" )
      {
         if ( %i == 0 )
            %i = %this.weaponSlotCount - 1;
         else
            %i--;
      }
      else
      {
         if ( %i == ( %this.weaponSlotCount - 1 ) )
            %i = 0;
         else
            %i++;
      }
   }

   if ( %newSlot != -1 )
      %this.use( %this.weaponSlot[%newSlot] );
}

//----------------------------------------------------------------------------
function ShapeBase::selectWeaponSlot( %this, %data )
{
   if(%this.isWalker)
   {
        %this.walkerSelectKey(%data);
        return;
   }

   if ( %data < 0 || %data > %this.weaponSlotCount
     || %this.weaponSlot[%data] $= "" || %this.weaponSlot[%data] $= "TargetingLaser" )
      return;

   %this.use( %this.weaponSlot[%data] );
}
