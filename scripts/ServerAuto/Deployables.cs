// Deployables Core Library

// keen: from turret code, perhaps use to apply a sliding limit to all deployables based on server population?
function calculateTurretMax()
{
     %count = 0;

     for(%i = 0; %i < ClientGroup.getCount(); %i++)
     {
          if(!ClientGroup.getObject(%i).isAIControlled())
               %count++;
     }

     $TeamDeployableMax[TurretBasePack] = 2 + mFloor(%count / 8);

//     schedule(30000, 0, calculateTurretMax);
}

function DefaultGame::clearDeployableMaxes(%game)
{
   for(%i = 0; %i <= %game.numTeams; %i++)
   {
      $TeamDeployedCount[%i, TurretIndoorDeployable] = 0;
      $TeamDeployedCount[%i, TurretOutdoorDeployable] = 0;
      $TeamDeployedCount[%i, PulseSensorDeployable] = 0;
      $TeamDeployedCount[%i, MotionSensorDeployable] = 0;
      $TeamDeployedCount[%i, InventoryDeployable] = 0;
      $TeamDeployedCount[%i, DeployedCamera] = 0;
      $TeamDeployedCount[%i, MineDeployed] = 0;
      $TeamDeployedCount[%i, TargetBeacon] = 0;
      $TeamDeployedCount[%i, MarkerBeacon] = 0;

      // Amplified items stuff
      $TeamDeployedCount[%i, BlastWallPack] = 0;
      $TeamDeployedCount[%i, JammerBeaconPack] = 0;
      $TeamDeployedCount[%i, CoreNodePack] = 0;
      $TeamDeployedCount[%i, NodeSpawnFavsPack] = 0;
      $TeamDeployedCount[%i, NodeRegeneratorPack] = 0;
      $TeamDeployedCount[%i, NodeCloakPack] = 0;
      $TeamDeployedCount[%i, NodePortalPack] = 0;
   }
}

$NotDeployableReason::EnemyFlagTooClose         =  20;
$NotDeployableReason::FriendlyFlagTooClose      =  21;
$NotDeployableReason::RequiresBaseNode          =  22;
$NotDeployableReason::OutOfBounds               =  23;
$NotDeployableReason::EnemySpawnTooClose        =  24;
$NotDeployableReason::FriendlySpawnTooFar       =  25;
$NotDeployableReason::BaseNodeTooFar            =  26;
$NotDeployableReason::ExtendedDeployFail        =  27;

// Deployable system overrides
function ShapeBaseImageData::onDeploy(%item, %plyr, %slot)
{
   if(%item.item $= "MotionSensorDeployable" || %item.item $= "PulseSensorDeployable")
   {
      %plyr.deploySensors--;
      %plyr.client.updateSensorPackText(%plyr.deploySensors);
      if(%plyr.deploySensors <= 0)
      {
         // take the deployable off the player's back and out of inventory
         %plyr.unmountImage(%slot);
         %plyr.decInventory(%item.item, 1);
      }
   }
   else
   {
      // take the deployable off the player's back and out of inventory
      %plyr.unmountImage(%slot);
      %plyr.decInventory(%item.item, 1);
   }

   // create the actual deployable
   %rot = %item.getInitialRotation(%plyr);
   if(%item.deployed.className $= "DeployedTurret")
      %className = "Turret";
   else
      %className = "StaticShape";

   %deplObj = new (%className)() {
      dataBlock = %item.deployed;
   };


   // set orientation
   if(%className $= "Turret")
      %deplObj.setDeployRotation(%item.surfacePt, %item.surfaceNrm);
   else
      %deplObj.setTransform(%item.surfacePt SPC %rot);

   // set the recharge rate right away
   if(%deplObj.getDatablock().rechargeRate)
      %deplObj.setRechargeRate(%deplObj.getDatablock().rechargeRate);

   // set team, owner, and handle
   %deplObj.team = %plyr.client.Team;
   %deplObj.owner = %plyr.client;

   // set the sensor group if it needs one
   if(%deplObj.getTarget() != -1)
      setTargetSensorGroup(%deplObj.getTarget(), %plyr.client.team);

   // place the deployable in the MissionCleanup/Deployables group (AI reasons)
   addToDeployGroup(%deplObj);

   //let the AI know as well...
   AIDeployObject(%plyr.client, %deplObj);

   // play the deploy sound
   serverPlay3D(%item.deploySound, %deplObj.getTransform());

   // increment the team count for this deployed object
   $TeamDeployedCount[%plyr.team, %item.item]++;

   // z0dd - ZOD, 6/01/03. Set beacons with deployed inv's so they are easy to find by teammates
   if(%item.item $= InventoryDeployable && !%plyr.client.isAIControlled()) {
      %b = new BeaconObject() {
         dataBlock = "DeployedBeacon";
         position = %deplObj.position;
         rotation = %deplObj.rotation;
         team = %deplObj.team;
         scale = "0.5 0.5 0.5";
      };
      MissionCleanup.add(%b);
      %b.setBeaconType(friend);
      %b.setTarget(%deplObj.team);
      %deplObj.beacon = %b;
   }

   %deplObj.deploy();
   %deplObj.deployedItem = %item.item; // so deployed base objects can be undeployed

   return %deplObj;
}

function ShapeBaseImageData::testInvalidDeployConditions(%item, %plyr, %slot)
{
   cancel(%plyr.deployCheckThread);
   %disqualified = $NotDeployableReason::None;  //default
   $MaxDeployDistance = %item.maxDeployDis;
   $MinDeployDistance = %item.minDeployDis;

   %surface = Deployables::searchView(%plyr,
                                      $MaxDeployDistance,
                                      ($TypeMasks::TerrainObjectType |
                                       $TypeMasks::InteriorObjectType));
   if (%surface)
   {
      %surfacePt  = posFromRaycast(%surface);
      %surfaceNrm = normalFromRaycast(%surface);

      // Check that point to see if anything is objstructing it...
      %eyeTrans = %plyr.getEyeTransform();
      %eyePos   = posFromTransform(%eyeTrans);

      %searchResult = containerRayCast(%eyePos, %surfacePt, -1, %plyr);
      if (!%searchResult)
      {
         %item.surface = %surface;
         %item.surfacePt = %surfacePt;
         %item.surfaceNrm = %surfaceNrm;
      }
      else
      {
         if(checkPositions(%surfacePT, posFromRaycast(%searchResult)))
         {
            %item.surface = %surface;
            %item.surfacePt = %surfacePt;
            %item.surfaceNrm = %surfaceNrm;
         }
         else
         {
            // Don't set the item
            %disqualified = $NotDeployableReason::ObjectTooClose;
         }
      }
      if(!getTerrainAngle(%surfaceNrm) && %item.flatMaxDeployDis !$= "")
      {
         $MaxDeployDistance = %item.flatMaxDeployDis;
         $MinDeployDistance = %item.flatMinDeployDis;
      }
   }

   if (%item.testMaxDeployed(%plyr))
   {
      %disqualified = $NotDeployableReason::MaxDeployed;
   }
   else if (%item.testNoSurfaceInRange(%plyr))
   {
      %disqualified = $NotDeployableReason::NoSurfaceFound;
   }
   else if (%item.testNoTerrainFound(%surface))
   {
      %disqualified = $NotDeployableReason::NoTerrainFound;
   }
   else if (%item.testNoInteriorFound())
   {
      %disqualified = $NotDeployableReason::NoInteriorFound;
   }
   else if (%item.testSlopeTooGreat(%surface, %surfaceNrm))
   {
      %disqualified = $NotDeployableReason::SlopeTooGreat;
   }
   else if (%item.testSelfTooClose(%plyr, %surfacePt))
   {
      %disqualified = $NotDeployableReason::SelfTooClose;
   }
   else if (%item.testObjectTooClose(%surfacePt))
   {
      %disqualified = $NotDeployableReason::ObjectTooClose;
   }
   else if (%item.testTurretTooClose(%plyr))
   {
      %disqualified = $NotDeployableReason::TurretTooClose;
   }
   else if (%item.testInventoryTooClose(%plyr))
   {
      %disqualified = $NotDeployableReason::InventoryTooClose;
   }
   else if (%item.testTurretSaturation())
   {
      %disqualified = $NotDeployableReason::TurretSaturation;
   }
   else if(%item.extendedDeployChecks(%plyr))
   {
      %disqualified = $NotDeployableReason::ExtendedDeployFail;
   }
   else if(%item.testEnemyFlagTooClose(%plyr))
   {
      %disqualified = $NotDeployableReason::EnemyFlagTooClose;
   }
   else if(%item.testFriendlyFlagTooClose(%plyr))
   {
      %disqualified = $NotDeployableReason::FriendlyFlagTooClose;
   }
   else if(%item.testEnemySpawnTooClose(%plyr))
   {
      %disqualified = $NotDeployableReason::EnemySpawnTooClose;
   }
   else if(%item.testFriendlySpawnTooFar(%plyr))
   {
      %disqualified = $NotDeployableReason::FriendlySpawnTooFar;
   }
   else if(%item.testOutOfBounds(%plyr))
   {
      %disqualified = $NotDeployableReason::OutOfBounds;
   }
   //---------------------------------------------------------------------------------------
   // z0dd - ZOD, 4/18/02. Addresses the exploit of deploying objects inside other objects.
//   else if (%item.testOrganicTooClose(%plyr))
//   {
//      %disqualified = $NotDeployableReason::OrganicTooClose;
//   }
   //---------------------------------------------------------------------------------------
   else if (%disqualified == $NotDeployableReason::None)
   {
      // Test that there are no objstructing objects that this object
      //  will intersect with
      //
      %rot = %item.getInitialRotation(%plyr);
      if(%item.deployed.className $= "DeployedTurret")
      {
         %xform = %item.deployed.getDeployTransform(%item.surfacePt, %item.surfaceNrm);
      }
      else
      {
         %xform = %surfacePt SPC %rot;
      }

      if (!%item.deployed.checkDeployPos(%xform))
      {
         %disqualified = $NotDeployableReason::ObjectTooClose;
      }
      else if (!%item.testHavePurchase(%xform))
      {
         %disqualified = $NotDeployableReason::SurfaceTooNarrow;
      }
   }

   if (%plyr.getMountedImage($BackpackSlot) == %item)  //player still have the item?
   {
      if (%disqualified)
         activateDeploySensorRed(%plyr);
      else
         activateDeploySensorGrn(%plyr);

      if (%plyr.client.deployPack == true)
         %item.attemptDeploy(%plyr, %slot, %disqualified);
      else
      {
         %plyr.deployCheckThread = %item.schedule(25, "testInvalidDeployConditions", %plyr, %slot); //update checks every 50 milliseconds
      }
   }
   else
       deactivateDeploySensor(%plyr);
}

function Deployables::displayErrorMsg(%item, %plyr, %slot, %error)
{
   deactivateDeploySensor(%plyr);

   %errorSnd = '~wfx/misc/misc.error.wav';
   switch (%error)
   {
      case $NotDeployableReason::None:
         %item.onDeploy(%plyr, %slot);
         messageClient(%plyr.client, 'MsgTeamDeploySuccess', "");
         return;

      case $NotDeployableReason::NoSurfaceFound:
         %msg = '\c2Item must be placed within reach.%1';

      case $NotDeployableReason::MaxDeployed:
         %msg = '\c2Your team\'s control network has reached its capacity for this item.%1';

      case $NotDeployableReason::SlopeTooGreat:
         %msg = '\c2Surface is too steep to place this item on.%1';

      case $NotDeployableReason::SelfTooClose:
         %msg = '\c2You are too close to the surface you are trying to place the item on.%1';

      case $NotDeployableReason::ObjectTooClose:
         %msg = '\c2You cannot place this item so close to another object.%1';

      case $NotDeployableReason::NoTerrainFound:
         %msg = '\c2You must place this on outdoor terrain.%1';

      case $NotDeployableReason::NoInteriorFound:
         %msg = '\c2You must place this on a solid surface.%1';

      case $NotDeployableReason::TurretTooClose:
         %msg = '\c2Interference from a nearby turret prevents placement here.%1';

      case $NotDeployableReason::TurretSaturation:
         %msg = '\c2There are too many turrets nearby.%1';

      case $NotDeployableReason::SurfaceTooNarrow:
         %msg = '\c2There is not adequate surface to clamp to here.%1';

      case $NotDeployableReason::InventoryTooClose:
         %msg = '\c2Interference from a nearby inventory prevents placement here.%1';

      case $NotDeployableReason::EnemyFlagTooClose:
         %msg = '\c2Cannot place here, enemy flag is too close.%1';

      case $NotDeployableReason::FriendlyFlagTooClose:
         %msg = '\c2Cannot place here, friendly flag is too close.%1';
         
      case $NotDeployableReason::EnemySpawnTooClose:
         %msg = '\c2Cannot place here, enemy defense in this area is too strong.%1';
         
      case $NotDeployableReason::FriendlySpawnTooFar:
         %msg = '\c2Cannot place here, you are too far from your base\'s network.%1';
         
      case $NotDeployableReason::OutOfBounds:
         %msg = '\c2No, you can\'t deploy outside the mission area.%1';
         
      case $NotDeployableReason::ExtendedDeployFail:
         %msg = "";
         
      // --------------------------------------------------------------------------------------
      // z0dd - ZOD, 4/18/02. Addresses the exploit of deploying objects inside other objects.
//      case $NotDeployableReason::OrganicTooClose:
//         %msg = '\c2You cannot place this item so close to an organic object.%1';
      // --------------------------------------------------------------------------------------

      default:
         %msg = '\c2Deploy failed.';
   }
   
   if(%error == $NotDeployableReason::ExtendedDeployFail)
        messageClient(%plyr.client, 'MsgDeployFailed', '\c2%2%1', %errorSnd, $NotDeployableReason::ExtendedDeployFailReason);
   else
        messageClient(%plyr.client, 'MsgDeployFailed', %msg, %errorSnd);
}

function getTeamSpawnSphereCount(%team)
{
   %teamDropsGroup = "MissionCleanup/TeamDrops" @ %team;

   %group = nameToID(%teamDropsGroup);

   if(%group != -1)
      return %group.getCount();
   else
      return 0;
}

function getTeamSpawnSphere(%team, %index)
{
   %teamDropsGroup = "MissionCleanup/TeamDrops" @ %team;

   %group = nameToID(%teamDropsGroup);

   if(%group != -1)
   {
      %count = %group.getCount();

      if(%index > %count)
          return 0;

      %sphere = %group.getObject(%index);

      if(isObject(%sphere))
           return %sphere;

      return 0; // no spawnsphere found, nuts
   }
}

function getTeamSpawnSpheres(%team)
{
   %teamDropsGroup = "MissionCleanup/TeamDrops" @ %team;

   %group = nameToID(%teamDropsGroup);

   if(%group != -1)
   {
      %count = %group.getCount();
      %spheres = %count;

      for(%i = 0; %i < %count; %i++)
      {
          %sphere = %group.getObject(%i);

          if(isObject(%sphere))
              %spheres = %spheres SPC %sphere;
      }

      return %spheres; // no spawnsphere found, nuts
   }
}

function getTeamNearestSpawnSphere(%team, %pos)
{
   %teamDropsGroup = "MissionCleanup/TeamDrops"@%team;
   %sphereIndex = 0;
   %group = nameToID(%teamDropsGroup);
   %lowestDist = 0;
   %lowestDistID = 0;

   if(%group != -1)
   {
      %count = %group.getCount();

      if(%count < 1)
           return isObject(%group.getObject(0)) ? %group.getObject(0) : 0;

      for(%i = 0; %i < %count; %i++)
      {
          %sphere = %group.getObject(%i);

          if(isObject(%sphere))
          {
              %sphereArray[%sphereIndex] = %sphere;
              %dist = VectorDist(%pos, %sphere.position);

              if(%dist < %lowestDist)
              {
                   %lowestDist = %dist;
                   %lowestDistID = %i;
              }

              %sphereIndex++;
          }
      }

      return %sphereArray[%lowestDistID];
   }

   return 0; // No sphere
}

function getEnemyNearestSpawnSphere(%team, %pos)
{
     %team = %team == 2 ? 1 : 2;
     return getTeamNearestSpawnSphere(%team, %pos);
}

function ShapeBase::isNearbyFriendlyBase(%obj, %rad)
{
   %team = %obj.client.team;
   %pos = %obj.getWorldBoxCenter();

   %friendlyBase = getTeamNearestSpawnSphere(%team, %pos);

   if(isObject(%friendlyBase))
   {
      %dist = VectorDist(%friendlyBase.position, %pos);

      if(%dist <= %rad)
         return true;
   }

   return false;
}

function ShapeBase::isNearbyEnemyBase(%obj, %rad)
{
   %team = %obj.client.team == 2 ? 1 : 2;
   %pos = %obj.getWorldBoxCenter();

   %enemyBase = getTeamNearestSpawnSphere(%team, %pos);

   if(isObject(%enemyBase))
   {
      %dist = VectorDist(%enemyBase.position, %pos);

      if(%dist <= %rad)
         return true;
   }

   return false;
}

function ShapeBaseImageData::testOutOfBounds(%item, %plyr)
{
    return %plyr.client.outOfBounds;
}

function ShapeBaseImageData::extendedDeployChecks(%item, %plyr)
{
    $NotDeployableReason::ExtendedDeployFailReason = "";

    return false;
}

function ShapeBaseImageData::testEnemyFlagTooClose(%item, %plyr)
{
    if(%item.maxEnemyFlagDist $= "" || %item.maxEnemyFlagDist < 1)
        return false;

    %team = %plyr.client.team == 1 ? 2 : 1;
    %enemyFlag = $TeamFlag[%team];

    if(isObject(%enemyFlag))
    {
        %dist = VectorDist(%enemyFlag.originalPosition, %plyr.position);
        
        if(%dist <= %item.maxEnemyFlagDist)
            return true;
    }

    return false;
}

function ShapeBaseImageData::testFriendlyFlagTooClose(%item, %plyr)
{
    if(%item.minFriendlyFlagDist $= "" || %item.minFriendlyFlagDist < 1)
        return false;

    %team = %plyr.client.team;
    %friendlyFlag = $TeamFlag[%team];

    if(isObject(%friendlyFlag))
    {
        %dist = VectorDist(%friendlyFlag.originalPosition, %plyr.position);

        if(%dist <= %item.minFriendlyFlagDist)
            return true;
    }

    return false;
}

function ShapeBaseImageData::testEnemySpawnTooClose(%item, %plyr)
{
    if(%item.maxEnemySpawnDist $= "" || %item.maxEnemySpawnDist < 1)
        return false;

    %team = %plyr.client.team == 1 ? 2 : 1;
    %pos = %plyr.getWorldBoxCenter();

    %enemySpawn = getTeamNearestSpawnSphere(%team, %pos);

    if(isObject(%enemySpawn))
    {
        %dist = VectorDist(%enemySpawn.position, %pos);

        if(%dist <= %item.maxEnemySpawnDist)
            return true;
    }

    return false;
}

function ShapeBaseImageData::testFriendlySpawnTooFar(%item, %plyr)
{
    if(%item.maxFriendlySpawnDist $= "" || %item.maxFriendlySpawnDist < 1)
        return false;

    %team = %plyr.client.team;
    %pos = %plyr.getWorldBoxCenter();

    %friendlySpawn = getTeamNearestSpawnSphere(%team, %pos);

    if(isObject(%friendlySpawn))
    {
        %dist = VectorDist(%friendlySpawn.position, %pos);

        if(%dist >= %item.maxFriendlySpawnDist)
            return true;
    }

    return false;
}

//------------------------------------------------------------------------------
// Linked Component System
// Used for coalesced multi-object deployables in MD3 - may not get used here

// Flags
$LC::Node                       = 1 << 0;
$LC::Invulnerable               = 1 << 1;
$LC::DeleteOnly                 = 1 << 2;
$LC::DestroysParent             = 1 << 3;
$LC::RandomChildDeath           = 1 << 4;
$LC::PrimaryNode                = 1 << 5;
$LC::RootNode                   = 1 << 6;
$LC::IndependantDamage          = 1 << 7;
$LC::DeleteOnDestroy            = 1 << 8;
$LC::Forcefield                 = 1 << 9;

datablock StaticShapeData(GenericRootNode)
{
   shapeFile = "turret_muzzlepoint.dts";
   explosion      = HandGrenadeExplosion; //ShapeExplosion;

   maxDamage      = 1.0;
   destroyedLevel = 1.0;
   disabledLevel  = 1.0;

   expDmgRadius = 5.0;
   expDamage = 0.5;
   expImpulse = 750.0;

   debrisShapeName = "debris_generic.dts";
   debris = StaticShapeDebris;

   // Tag objects based on whatever they would be called
//   targetTypeTag = 'Bombardment Base';
};

function GenericRootNode::onDestroyed(%data, %obj, %prevState)
{
    Parent::onDestroyed(%data, %obj, %prevState);

    if(%obj.deployedItem !$= "")
        $TeamDeployedCount[%obj.team, %obj.deployedItem]--;
}

function createRootNode(%plyr, %trans, %type)
{
    if(%type $= "")
        %type = "GenericRootNode";

    %root = createTeamObject(%plyr, "StaticShape", %type, %trans, false, "1 1 1");
    %root.createNode();
    %root.setRootNode();

    return %root;
}

function ShapeBase::setRootNode(%obj)
{
    if(%obj.flags & $LC::RootNode)
        return;

    %obj.flags |= $LC::DeleteOnly | $LC::RootNode;
    %obj.isLinkedNode = true;
    %obj.parentNode = %obj;
    %obj.childNodes = new SimSet();
}

function ShapeBase::getRootNode(%obj)
{
    if(%obj.isLinkedNode)
    {
        if(!isObject(%obj.parentNode))
            return 0;

        if(%obj.flags & $LC::RootNode)
            return %obj;
        else
            return %obj.parentNode;
    }
    else
        return 0;
}

function ShapeBase::isRootNode(%obj)
{
    return %obj.flags & $LC::RootNode;
}

function ShapeBase::propagateDamageLevel(%obj, %damage)
{
    %obj.setDamageLevel(%damage);
//    echo("propagatedamagelevel" SPC %obj SPC %obj.getDatablock().getName() SPC %damage);

    for(%idx = 0; %idx < %obj.childNodes.getCount(); %idx++)
    {
        %child = %obj.childNodes.getObject(%idx);

        if(!isObject(%child))
            continue;

        if(%child.flags & ($LC::IndependantDamage | $LC::Invulnerable))
            continue;

        %obj.setDamageLevel(%damage);
    }
}

function GameBase::createNode(%obj, %flags)
{
    if(%obj.parentNode !$= "")
        return;

    %obj.isLinkedNode = false;
    %obj.parentNode = 0;
    %obj.flags = %flags;
}

function GameBase::linkNode(%obj, %parent)
{
    if(%obj.isLinkedNode == true)
        %obj.unlinkNode();

    %obj.isLinkedNode = true;
    %obj.parentNode = %parent;
    %parent.childNodes.add(%obj);
}

function GameBase::addNode(%obj, %node, %flags)
{
    if(%obj.parentNode $= "")
        %node.createNode(%flags);
    else
        %node.unlinkNode();

    %node.flags = %flags;
    %node.isLinkedNode = true;
    %node.parentNode = %obj;

    %obj.childNodes.add(%node);
}

function GameBase::unlinkNode(%obj)
{
    if(%obj.parentNode != %obj)
    {
        if(isObject(%obj.parentNode))
            %obj.parentNode.childNodes.remove(%obj);

        %obj.parentNode = 0;
        %obj.isLinkedNode = false;
    }
}

function ShapeBase::destroyNode(%obj)
{
//    echo("DestroyNode called!" SPC %obj SPC %obj.getDatablock().getName());

    for(%idx = 0; %idx < %obj.childNodes.getCount(); %idx++)
    {
        %child = %obj.childNodes.getObject(%idx);

        if(!isObject(%child))
            continue;

        if(%child.flags & $LC::DeleteOnly)
            %child.deleteNode();
        else if(%child.flags & $LC::Forcefield)
        {
            %pos = "-10000 -10000 -10000";
            %child.setPosition(%pos);

            if(isObject(%child.pz))
            {
                %child.pz.setPosition(%pos);
                %child.getDatablock().losePower(%child);
                %child.pz.schedule(500, "delete");
            }

            %child.delete();
        }
        else
        {
            if(%child.getDamageState() !$= "Destroyed")
            {
                if(%child.flags & $LC::RandomChildDeath)
                {
                    %t = getRandom(500, 3000);
                    %child.schedule(%t, "setDamageState", "Destroyed");

                    if(%child.flags & $LC::DeleteOnDestroy)
                    {
                        if(isObject(%child.trigger))
                            %child.trigger.delete();

                        %child.schedule(%t, "deleteNode");
                    }
                }
                else
                {
                    %child.setDamageState("Destroyed");

                    if(%child.flags & $LC::DeleteOnDestroy)
                    {
                        if(isObject(%child.trigger))
                            %child.trigger.delete();

                        %child.deleteNode();
                    }
                }
            }
            else
                %child.deleteNode();
        }
    }

    %obj.deleteNode(4000);
}

function GameBase::deleteNode(%obj, %time)
{
    if(%time $= "")
        %time = 500;

    %obj.schedule(400, "setPosition", ((getRandom() * 10000) SPC (getRandom() * 10000) SPC "-10000"));
    %obj.schedule(%time, "delete");
}

function GameBase::setOffsetPosition(%obj, %newPos)
{
    %obj.setPosition(vectorAdd(%obj.position, %newPos));
}

function ShapeBase::initializeBasePiece(%obj, %plyr, %newPos, %parent, %flags)
{
    %obj.initializeTeamObject(%obj, %plyr);
    %obj.setOffsetPosition(%newPos);
    %parent.addNode(%obj, %flags);
}

function ShapeBase::initializeTeamObject(%obj, %plyr)
{
    MissionCleanup.add(%obj);
    addToDeployGroup(%obj);
    AIDeployObject(%plyr.client, %obj);

    %obj.team = %plyr.client.team;
    %obj.owner = %plyr.client;

    if(%obj.getTarget() != -1)
        setTargetSensorGroup(%obj.getTarget(), %plyr.client.team);
}

function createTeamObject(%plyr, %type, %data, %trans, %selfPower, %scale)
{
    if(%scale $= "")
        %scale = "1 1 1";

    if(%selfPower $= "")
        %selfPower = false;
        
    %obj = new (%type)()
    {
        dataBlock = %data;
//        team = %plyr.client.team;
        scale = %scale;
    };

    %obj.initializeTeamObject(%plyr);
    %obj.setTransform(%trans);

    if(%selfPower)
    {
        %obj.setSelfPowered();
        %obj.setRechargeRate(%data.rechargeRate);
    }
    
    return %obj;
}

function createTeamObjectV(%plyr, %type, %data, %pos, %vec, %selfPower, %scale)
{
    if(%scale $= "")
        %scale = "1 1 1";

    if(%selfPower $= "")
        %selfPower = false;

    %obj = new (%type)()
    {
        dataBlock = %data;
//        team = %plyr.client.team;
        scale = %scale;
    };

    %obj.initializeTeamObject(%plyr);
    %obj.setPosition(%pos);
    %obj.setDeployRotation(%pos, %vec);

    if(%selfPower)
    {
        %obj.setSelfPowered();
        %obj.setRechargeRate(%data.rechargeRate);
    }

    return %obj;
}

function addStation(%plyr, %trans, %bSelfPowered, %parent)
{
    %obj = createTeamObject(%plyr, "StaticShape", "StationInventory", %trans, %bSelfPowered, "1 1 1");
    %obj.createNode($LC::PrimaryNode | $LC::IndependantDamage);

    if(isObject(%parent))
        %obj.linkNode(%parent);
    
    %trigger = new Trigger()
    {
        dataBlock = stationTrigger;
        polyhedron = "-0.75 0.75 0.1 1.5 0.0 0.0 0.0 -1.5 0.0 0.0 0.0 2.3";
    };
    MissionCleanup.add(%obj);
    
    %trigger.setTransform(%obj.getTransform());
    %trigger.station = %obj;
    %trigger.mainObj = %obj;
    %trigger.disableObj = %obj;

    %obj.trigger = %trigger;

    return %obj;
}

function addTurret(%plyr, %trans, %bSelfPowered, %parent, %barrel)
{
    if(%barrel $= "")
        %barrel = "PlasmaBarrelLarge";

    %turret = new Turret()
    {
        dataBlock = "FreeBaseTurret";
        initialBarrel = %barrel;
    };

    %turret.initializeTeamObject(%plyr);
    %turret.setTransform(%trans);
    %turret.createNode($LC::PrimaryNode | $LC::IndependantDamage);

    if(%bSelfPowered)
    {
        %turret.setSelfPowered();
        %turret.setRechargeRate(%turret.getDatablock().rechargeRate);
    }

    if(isObject(%parent))
        %turret.linkNode(%parent);

    return %turret;
}

function addForcefield(%plyr, %data, %pos, %rot, %scale, %parent)
{
   %ff = new ForceFieldBare()
   {
      position = %pos;
      rotation = %rot;
      scale = %scale;
      dataBlock = %data;
      team = %plyr.client.team;
   };

   %ff.getDatablock().gainPower(%ff);
   %ff.target = createTarget(%ff, "Force Field", "", "", "", 0, 0);
   setTargetSensorGroup(%ff.getTarget(), %plyr.client.team);
   MissionCleanup.add(%ff);

   if(isObject(%parent))
      %parent.addNode(%ff, $LC::Invulnerable | $LC::DeleteOnly | $LC::Forcefield);

   return %ff;
}

function addForcefieldV(%plyr, %data, %pos, %vec, %scale, %parent)
{
   %ff = new forceFieldBare()
   {
      position = %pos;
//      rotation = "";
      scale = %scale;
      dataBlock = %data;
      team = %plyr.client.team;
   };
   %ff.setDeployRotation(%pos, %vec);

   %ff.getDatablock().gainPower(%ff);
   %ff.target = createTarget(%ff, "Force Field", "", "", "", 0, 0);
   setTargetSensorGroup(%ff.getTarget(), %plyr.client.team);
   MissionCleanup.add(%ff);

   if(isObject(%parent))
      %parent.addNode(%ff, $LC::Invulnerable | $LC::DeleteOnly | $LC::Forcefield);

   return %ff;
}

// legacy MD2 function - refactored for new node system
// keen: this seems to be returning true not finding anything? wat - see commented out code
function glueToRemoteBase(%deplObj, %pos)
{
    %found = false;
    
    InitContainerRadiusSearch(%pos, 2, $TypeMasks::StaticShapeObjectType);

    while((%int = ContainerSearchNext()) != 0)
    {
        if(%int.isLinkedNode == true && %int.getClassName() !$= "TerrainBlock" && %int.getClassName() !$= "InteriorInstance") // && %int.getClassName() !$= "StaticTSObject" && !%int.isPlayer() && !%int.isVehicle())
        {
            %deplObj.linkNode(%int.parentNode);
//            %deplObj.setSelfPowered();
//            %deplObj.setRechargeRate(%deplObj.getDatablock().rechargeRate);
            break; // keen: return here seemed to continue executing code? gave me a syntax error
        }
    }
    
    if(%found)
        return true;
    else
        return false;
}

// Functions
function addTrigger(%plyr, %data, %pos, %rot, %vec, %scale, %parent)
{
   %Tg = new Trigger()
   {
      position = %pos;
      rotation = %rot;
      scale = %scale;
      dataBlock = %data;
      team = %plyr.client.team;
   };
   %Tg.setDeployRotation(%pos, %vec);

   %ff.target = createTarget(%TG, "Trigger", "", "", "", 0, 0);
   MissionCleanup.add(%Tg);
   //%Tg.parent = %parent;

   //if(!%parent.attachedFF)
   //   %parent.attachedFF = 0;

   //%parent.shield[%parent.attachedFF] = %Tg;
   //%parent.attachedFF++;

   //return %Tg;
}

function ForceFieldBareData::onAdd(%data, %obj)
{
   Parent::onAdd(%data, %obj);

   %pz = new PhysicalZone() {
      position = %obj.position;
      rotation = %obj.rotation;
      scale    = %obj.scale;
      polyhedron = "0.000000 1.0000000 0.0000000 1.0000000 0.0000000 0.0000000 0.0000000 -1.0000000 0.0000000 0.0000000 0.0000000 1.0000000";
      velocityMod  = 1.0;
      gravityMod   = 1.0;
      appliedForce = "0 0 0";
		ffield = %obj;
   };
	%pzGroup = nameToID("MissionCleanup/PZones");
	if(%pzGroup <= 0) {
		%pzGroup = new SimGroup("PZones");
		MissionCleanup.add(%pzGroup);
	}
	%pzGroup.add(%pz);
   %obj.pz = %pz;
   //MissionCleanupGroup.add(%pz);
}

function ShapeBase::addToTeamPowerNetwork(%obj)
{
    InitContainerRadiusSearch(%obj.position, 10000, $TypeMasks::StaticShapeObjectType);

    while((%gen = ContainerSearchNext()) != 0)
    {
        if(%gen.getDatablock().className $= "Generator")
        {
            if(%gen.team == %obj.team)
            {
                %pg = %gen.getGroup();
                %pg.add(%obj);
                %pg.updatePowerState(%obj);

                return;
            }
        }
    }

    // Fail state - set self powered, no gens
    %obj.setSelfPowered();
    %obj.setRechargeRate(%obj.getDatablock().rechargeRate);
}

function SimGroup::updatePowerState(%this, %obj)
{
    if(%this.powerCount > 0)
        %obj.getDatablock().gainPower(%obj);
    else
        %obj.getDatablock().losePower(%obj);
}

// Deployables
//====================================== Deployable Portal Pad
$TeamDeployableMax[NodePortalPack] = 2;

datablock StaticShapeData(TelePadBase) : StaticShapeDamageProfile
{
   shapeFile = "station_teleport.dts";
   maxDamage = 10;
   destroyedLevel = 10;
   disabledLevel = 10;
   explosion = DeployablesExplosion;
   dynamicType = $TypeMasks::StaticShapeObjectType;

   deployedObject = true;

//   targetNameTag = '';
   targetTypeTag = 'Portal Pad';

   debrisShapeName = "debris_generic_small.dts";
   debris = DeployableDebris;
   heatSignature = 0;
};

datablock ShapeBaseImageData(NodePortalPackImage)
{
   mass = 10;
   emap = true;

   shapeFile = "pack_deploy_inventory.dts";
   item = NodePortalPack;
   mountPoint = 1;
   offset = "0 0 0";
   deployed = TelePadBase;

   stateName[0] = "Idle";
   stateTransitionOnTriggerDown[0] = "Activate";

   stateName[1] = "Activate";
   stateScript[1] = "onActivate";
   stateTransitionOnTriggerUp[1] = "Idle";

   maxDepSlope = 360;
   deploySound = StationDeploySound;

   minDeployDis                       =  0.5;
   maxDeployDis                       =  5.0;  //meters from body
   
    maxEnemyFlagDist = 100;
	minFriendlyFlagDist = 50;
	requiresBaseNode = false;
	maxBaseNodeDist = 0;
	maxEnemySpawnDist = 100;
	maxFriendlySpawnDist = 0;
};

datablock ItemData(NodePortalPack)
{
   className = Pack;
   catagory = "Deployables";
   shapeFile = "pack_deploy_inventory.dts";
   mass = 3.0;
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 1;
   rotate = false;
   image = "NodePortalPackImage";
   pickUpName = "a deployable portal pad";

   emap = true;
};

$g_WormholeTimeout = 5000;

function NodePortalPackImage::onDeploy(%item, %plyr, %slot)
{
   %plyr.unmountImage(%slot);
   %plyr.decInventory(%item.item, 1);
   %rot = %item.getInitialRotation(%plyr);

   if(!isObject($PortalExits[%plyr.team]))
       $PortalExits[%plyr.team] = new SimSet();

   %trans = %item.surfacePt SPC "0 1 0 0";
   %teleBase = createTeamObjectV(%plyr, "StaticShape", "TelePadBase", vectorAdd(%item.surfacePt, "0 0 0.1"), "0 0 1", true, "1 1 1");
   %teleBase.deployedItem = "NodePortalPack";
   %teleBase.play3d(%item.deploySound);
   
   %teleBase.wormholePoint = vectorAdd(%item.surfacePt, "0 0 2.5");
   $TeamDeployedCount[%plyr.team, "NodePortalPack"]++;     // just fine

   $PortalExits[%plyr.team].add(%teleBase);

   %plyr.nextWormholeTime = getSimTime() + $g_WormholeTimeout;
    
   // Wormhole aura magic
   %aura = Aura.create("WormholeAura");
   %aura.attachToObject(%teleBase);
   %teleBase.wormhole = %aura;
   %teleBase.padState = false;

   %teleBase.setThreadDir($DeployThread, false);
   %teleBase.getDatablock().checkWormhole(%teleBase);
}

function TelePadBase::checkWormhole(%data, %obj)
{
    if(!isObject(%obj))
        return;

    %count = $PortalExits[%obj.team].getCount();
    
    if(%count > 1 && !%obj.padState)
    {
        %obj.padState = true;

        %obj.wormhole.start();
        %obj.setThreadDir($DeployThread, true);
        %obj.playThread($DeployThread, "activate");
    }
    else if(%count < 2 && %obj.padState)
    {
        %obj.padState = false;

        %obj.wormhole.stop();
        %obj.setThreadDir($DeployThread, false);
    }

    %data.schedule(1000, "checkWormhole", %obj);
}

function StaticShape::getOtherPortalPad(%obj)
{
    if(%obj.padState)
    {
        %setCount = $PortalExits[%obj.team].getCount();

        for(%i = 0; %i < %setCount; %i++)
        {
            %exit = $PortalExits[%obj.team].getObject(%i);

            if(%exit == %obj)
                continue;

            return %exit;
        }
    }
    
    return 0;
}

function TelePadBase::onDestroyed(%this, %obj, %prevState)
{
   %obj.wormhole.stop();
   %obj.wormhole.destroy();
   $PortalExits[%obj.team].remove(%obj);
   
   Parent::onDestroyed(%this, %obj, %prevState);

   $TeamDeployedCount[%obj.team, "NodePortalPack"]--;
   
   %obj.schedule(256, "delete");
}

function TelePadBase::onPickup(%this, %obj, %shape, %amount)
{
   // created to prevent console errors
}

//====================================== Blast Wall
$TeamDeployableMax[BlastWallPack] = 6;

datablock StaticShapeData(BlastWall) : StaticShapeDamageProfile
{
   shapeFile      = "bmiscf.dts";
   explosion      = ShapeExplosion;

   maxDamage      = 50;
   destroyedLevel = 50;
   disabledLevel  = 50;

   expDmgRadius = 5.0;
   expDamage = 0.1;
   expImpulse = 1500.0;

   isShielded = false;
   energyPerDamagePoint = 0;
   maxEnergy = 0;
   rechargeRate = 0.0;

   targetTypeTag = 'Blast Wall';

   debrisShapeName = "debris_generic.dts";
   debris = StaticShapeDebris;
};

datablock ShapeBaseImageData(BlastWallPackImage)
{
   mass = 10;

   shapeFile = "pack_upgrade_shield.dts";
   item = BlastWallPack;
   mountPoint = 1;
   offset = "0 0 0";
   deployed = BlastWall;

   stateName[0] = "Idle";
   stateTransitionOnTriggerDown[0] = "Activate";

   stateName[1] = "Activate";
   stateScript[1] = "onActivate";
   stateTransitionOnTriggerUp[1] = "Idle";

   isLarge = true;
   emap = true;

   maxDepSlope = 60;
   deploySound = StationDeploySound;

   minDeployDis                       =  0.5;
   maxDeployDis                       =  5.0;  //meters from body
   
    maxEnemyFlagDist = 250;
	minFriendlyFlagDist = 20;
	requiresBaseNode = false;
	maxBaseNodeDist = 0;
	maxEnemySpawnDist = 200;
	maxFriendlySpawnDist = 0;
};

datablock ItemData(BlastWallPack)
{
   className = Pack;
   catagory = "Deployables";
   shapeFile = "pack_upgrade_shield.dts";
   mass = 3.0;
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 1;
   rotate = false;
   image = "BlastWallPackImage";
   pickUpName = "a blast wall";

   emap = true;
};

function BlastWallPack::onPickup(%this, %obj, %shape, %amount)
{
   // created to prevent console errors
}

function BlastWallPackImage::onDeploy(%item, %plyr, %slot)
{
   %plyr.unmountImage(%slot);
   %plyr.decInventory(%item.item, 1);

   %rot = %item.getInitialRotation(%plyr);
   %deplObj = new StaticShape()
   {
      dataBlock = %item.deployed;
      team = %plyr.client.team;
   };

   %data = %deplObj.getDatablock();

   if(getTerrainAngle(%item.surfaceNrm) > 45)
      %deplObj.setDeployRotation(%item.surfacePt, %item.surfaceNrm);
   else
      %deplObj.setTransform(%item.surfacePt SPC %rot);

// Tried to get walls that stretch to fit, then I realized my math was terrible, so meh
//   %rotx = 1.5708;
//   %zoffset = 1.5;
//   %hscale = 0.25;
//   %wscale = 0.2;
//   %up = 2;
//   %side = 2.5;
//   %depth = 0.75;
   
//   %ray1 = castRay(%item.surfacePt, "0 0 1", 10, $TypeMasks::InteriorObjectType | $TypeMasks::StaticShapeObjectType);
   
//   if(%ray1)
//       %up = %ray1.hitDist * %hscale;

//   %ray2 = castRay(vectorAdd(%item.surfacePt, "0 0" SPC %up), "1 0 0", 15, $TypeMasks::InteriorObjectType | $TypeMasks::StaticShapeObjectType);

//   if(%ray2)
//       %side = %ray2.hitDist * %wscale;
   
//   %nscale = %side SPC %up SPC %depth;
//   echo("blastwall" SPC %deplObj SPC %up SPC %side);
   
   %deplObj.setScale("2.75 0.25 20");
   
   %deplObj.team = %plyr.client.Team;
   %deplObj.owner = %plyr.client;

   if(%deplObj.getTarget() != -1)
         setTargetSensorGroup(%deplObj.getTarget(), %plyr.client.team);

   addToDeployGroup(%deplObj);
   AIDeployObject(%plyr.client, %deplObj);
   serverPlay3D(%item.deploySound, %deplObj.getTransform());
   $TeamDeployedCount[%plyr.team, "BlastWallPack"]++;
   MissionCleanup.add(%deplObj);

   %deplObj.deployedItem = "BlastWallPack";
}

function BlastWallPackImage::testSlopeTooGreat(%item)
{
//   if(%item.surface)
//      return getTerrainAngle(%item.surfaceNrm) > 90;
   return false; // deplpyable at all angles
}

function BlastWall::onDestroyed(%this, %obj, %prevState)
{
   $TeamDeployedCount[%obj.team, "BlastWallPack"]--;
   %obj.schedule(250, "delete");
   Parent::onDestroyed(%this, %obj, %prevState);
}

//====================================== Spawn Refit Node
$TeamDeployableMax[NodeSpawnFavsPack] = 3;

datablock StaticShapeData(NodeSpawnFavs) : StaticShapeDamageProfile
{
   shapeFile      = "ammo_mine.dts";
   explosion      = MortarExplosion;

   maxDamage      = 5.0;
   destroyedLevel = 5.0;
   disabledLevel  = 5.0;

   expDmgRadius = 10.0;
   expDamage = 1.0;
   expImpulse = 3000.0;

   isShielded = false;
   energyPerDamagePoint = 0;
   maxEnergy = 0;
   rechargeRate = 0;

   targetTypeTag = 'Spawn Refit Node';

   debrisShapeName = "debris_generic.dts";
   debris = StaticShapeDebris;

   cmdCategory = "DSupport";
   cmdIcon = CMDSensorIcon;
   cmdMiniIconName = "commander/MiniIcons/com_deploymotionsensor";
};

datablock ShapeBaseImageData(NodeSpawnFavsPackImage)
{
   mass = 10;

   shapeFile = "ammo_mine.dts";
   item = NodeSpawnFavsPack;
   mountPoint = 1;
   offset = "0 0 0";
   deployed = NodeSpawnFavs;

   stateName[0] = "Idle";
   stateTransitionOnTriggerDown[0] = "Activate";

   stateName[1] = "Activate";
   stateScript[1] = "onActivate";
   stateTransitionOnTriggerUp[1] = "Idle";

   isLarge = true;
   emap = true;

   maxDepSlope = 30;
   deploySound = StationDeploySound;

   minDeployDis                       =  0.5;
   maxDeployDis                       =  5.0;  //meters from body
   
   maxEnemyFlagDist = 100;
   minFriendlyFlagDist = 10;
   requiresBaseNode = false;
   maxBaseNodeDist = 0;
   maxEnemySpawnDist = 100;
   maxFriendlySpawnDist = 300;
};

datablock ItemData(NodeSpawnFavsPack)
{
   className = Pack;
   catagory = "Deployables";
   shapeFile = "ammo_mine.dts";
   mass = 2.0;
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 1;
   rotate = false;
   image = "NodeSpawnFavsPackImage";
   pickUpName = "a spawn refit node";

   emap = true;
};

function NodeSpawnFavsPack::onPickup(%this, %obj, %shape, %amount)
{
   // created to prevent console errors
}

function NodeSpawnFavsPackImage::onDeploy(%item, %plyr, %slot)
{
   %plyr.unmountImage(%slot);
   %plyr.decInventory(%item.item, 1);

   %rot = %item.getInitialRotation(%plyr);
   %deplObj = new StaticShape()
   {
      dataBlock = %item.deployed;
      team = %plyr.client.team;
   };

   %data = %deplObj.getDatablock();

   if(getTerrainAngle(%item.surfaceNrm) > 45)
      %deplObj.setDeployRotation(%item.surfacePt, %item.surfaceNrm);
   else
      %deplObj.setTransform(%item.surfacePt SPC %rot);
   
   %deplObj.setScale("3 3 3");
   %deplObj.team = %plyr.client.Team;
   %deplObj.owner = %plyr.client;

   if(%deplObj.getTarget() != -1)
         setTargetSensorGroup(%deplObj.getTarget(), %plyr.client.team);

   addToDeployGroup(%deplObj);
   AIDeployObject(%plyr.client, %deplObj);
   serverPlay3D(%item.deploySound, %deplObj.getTransform());
   $TeamDeployedCount[%plyr.team, "NodeSpawnFavsPack"]++;
   MissionCleanup.add(%deplObj);

   %deplObj.deployedItem = "NodeSpawnFavsPack";
   %deplObj.addToTeamPowerNetwork();
}

function NodeSpawnFavs::onGainPowerEnabled(%data, %obj)
{
    %obj.spawnRefit = true;
    
    Parent::onGainPowerEnabled(%data, %obj);
}

function NodeSpawnFavs::onLosePowerDisabled(%data, %obj)
{
    %obj.spawnRefit = false;

    Parent::onLosePowerDisabled(%data, %obj);
}

function NodeSpawnFavs::onDestroyed(%this, %obj, %prevState)
{
    $TeamDeployedCount[%obj.team, "NodeSpawnFavsPack"]--;
    %obj.schedule(250, "delete");
    Parent::onDestroyed(%this, %obj, %prevState);
}

//====================================== Cloaking Emitter Node
$TeamDeployableMax[NodeCloakPack] = 2;

datablock SensorData(CloakingJammerSensor)
{
   detects = true;
   detectsUsingLOS = true;
   detectsPassiveJammed = true;
   detectRadius = 30;
   detectionPings = false;
   detectsFOVOnly = true;
   detectFOVPercent = 1.3;
   useObjectFOV = true;

   jams = true;
   jamsOnlyGroup = true;
   jamsUsingLOS = true;
   jamRadius = 30;
};

datablock StaticShapeData(NodeCloakingEmitter) : StaticShapeDamageProfile
{
   shapeFile      = "deploy_sensor_motion.dts";
   explosion      = MortarExplosion;

   maxDamage      = 10.0;
   destroyedLevel = 10.0;
   disabledLevel  = 10.0;

   expDmgRadius = 6.0;
   expDamage = 1.0;
   expImpulse = 1500.0;

   isShielded = false;
   energyPerDamagePoint = 0;
   maxEnergy = 0;
   rechargeRate = 0;

   targetTypeTag = 'Cloaking Emitter Node';

   debrisShapeName = "debris_generic.dts";
   debris = StaticShapeDebris;

   cmdCategory = "DSupport";
   cmdIcon = CMDSensorIcon;
   cmdMiniIconName = "commander/MiniIcons/com_deploymotionsensor";
   
   sensorData = CloakingJammerSensor;
   sensorRadius = CloakingJammerSensor.detectRadius;
   sensorColor = "255 255 255";
   deployAmbientThread = true;
};

datablock ShapeBaseImageData(NodeCloakPackImage)
{
   mass = 10;

   shapeFile = "pack_deploy_sensor_motion.dts";
   item = NodeCloakPack;
   mountPoint = 1;
   offset = "0 0 0";
   deployed = NodeCloakingEmitter;

   stateName[0] = "Idle";
   stateTransitionOnTriggerDown[0] = "Activate";

   stateName[1] = "Activate";
   stateScript[1] = "onActivate";
   stateTransitionOnTriggerUp[1] = "Idle";

   isLarge = true;
   emap = true;

   maxDepSlope = 30;
   deploySound = MotionSensorDeploySound;

   minDeployDis                       =  0.5;
   maxDeployDis                       =  5.0;  //meters from body
   
   maxEnemyFlagDist = 300;
   minFriendlyFlagDist = 10;
   requiresBaseNode = false;
   maxBaseNodeDist = 0;
   maxEnemySpawnDist = 300;
   maxFriendlySpawnDist = 0;
};

datablock ItemData(NodeCloakPack)
{
   className = Pack;
   catagory = "Deployables";
   shapeFile = "pack_deploy_sensor_motion.dts";
   mass = 2.0;
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 1;
   rotate = false;
   image = "NodeCloakPackImage";
   pickUpName = "a cloaking emitter node";

   emap = true;
};

function NodeCloakPack::onPickup(%this, %obj, %shape, %amount)
{
   // created to prevent console errors
}

function NodeCloakPackImage::onDeploy(%item, %plyr, %slot)
{
   %plyr.unmountImage(%slot);
   %plyr.decInventory(%item.item, 1);

   %rot = %item.getInitialRotation(%plyr);
   %deplObj = new StaticShape()
   {
      dataBlock = %item.deployed;
      team = %plyr.client.team;
   };

   %data = %deplObj.getDatablock();

   if(getTerrainAngle(%item.surfaceNrm) > 45)
      %deplObj.setDeployRotation(%item.surfacePt, %item.surfaceNrm);
   else
      %deplObj.setTransform(%item.surfacePt SPC %rot);

   %deplObj.setScale("3 3 3");
   %deplObj.setSelfPowered();
   %deplObj.team = %plyr.client.Team;
   %deplObj.owner = %plyr.client;
   %deplObj.playthread(0, "Deploy");

   if(%deplObj.getTarget() != -1)
         setTargetSensorGroup(%deplObj.getTarget(), %plyr.client.team);

   addToDeployGroup(%deplObj);
   AIDeployObject(%plyr.client, %deplObj);
   serverPlay3D(%item.deploySound, %deplObj.getTransform());
   $TeamDeployedCount[%plyr.team, "NodeCloakPack"]++;
   MissionCleanup.add(%deplObj);

   %deplObj.deployedItem = "NodeCloakPack";

   // Cloaking Aura Magic
   %aura = Aura.create("CloakingBeaconField");
   %aura.attachToObject(%deplObj);
   %aura.start();
}

function NodeCloakingEmitter::onDestroyed(%this, %obj, %prevState)
{
    $TeamDeployedCount[%obj.team, "NodeCloakPack"]--;
    %obj.schedule(250, "delete");
    Parent::onDestroyed(%this, %obj, %prevState);
}

//====================================== Jammer Emitter Node
$TeamDeployableMax[JammerBeaconPack] = 2;

datablock SensorData(JammerBeaconSensorObj)
{
   detects = true;
   detectsUsingLOS = true;
   detectsPassiveJammed = true;
   detectRadius = 30;
   detectionPings = false;
   detectsFOVOnly = true;
   detectFOVPercent = 1.3;
   useObjectFOV = true;

   jams = true;
   jamsOnlyGroup = true;
   jamsUsingLOS = true;
   jamRadius = 30;
};

datablock StaticShapeData(NodeJammerEmitter) : StaticShapeDamageProfile
{
   shapeFile      = "deploy_sensor_motion.dts";
   explosion      = MortarExplosion;

   maxDamage      = 10.0;
   destroyedLevel = 10.0;
   disabledLevel  = 10.0;

   expDmgRadius = 6.0;
   expDamage = 1.0;
   expImpulse = 1500.0;

   isShielded = false;
   energyPerDamagePoint = 0;
   maxEnergy = 0;
   rechargeRate = 0;

   targetTypeTag = 'Jammer Beacon';

   debrisShapeName = "debris_generic.dts";
   debris = StaticShapeDebris;

   cmdCategory = "DSupport";
   cmdIcon = CMDSensorIcon;
   cmdMiniIconName = "commander/MiniIcons/com_deploypulsesensor";

   sensorData = JammerBeaconSensorObj;
   sensorRadius = JammerBeaconSensorObj.detectRadius;
   sensorColor = "255 71 9";
   deployAmbientThread = true;
};

datablock ShapeBaseImageData(JammerBeaconPackImage)
{
   mass = 10;

   shapeFile = "pack_deploy_sensor_pulse.dts";
   item = JammerBeaconPack;
   mountPoint = 1;
   offset = "0 0 0";
   deployed = NodeJammerEmitter;

   stateName[0] = "Idle";
   stateTransitionOnTriggerDown[0] = "Activate";

   stateName[1] = "Activate";
   stateScript[1] = "onActivate";
   stateTransitionOnTriggerUp[1] = "Idle";

   isLarge = true;
   emap = true;

   maxDepSlope = 30;
   deploySound = SensorDeploySound;

   minDeployDis                       =  0.5;
   maxDeployDis                       =  5.0;  //meters from body

   maxEnemyFlagDist = 200;
   minFriendlyFlagDist = 10;
   requiresBaseNode = false;
   maxBaseNodeDist = 0;
   maxEnemySpawnDist = 200;
   maxFriendlySpawnDist = 0;
};

datablock ItemData(JammerBeaconPack)
{
   className = Pack;
   catagory = "Deployables";
   shapeFile = "pack_deploy_sensor_pulse.dts";
   mass = 2.0;
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 1;
   rotate = false;
   image = "JammerBeaconPackImage";
   pickUpName = "a sensor jammer beacon";

   emap = true;
};

function JammerBeaconPack::onPickup(%this, %obj, %shape, %amount)
{
   // created to prevent console errors
}

function JammerBeaconPackImage::onDeploy(%item, %plyr, %slot)
{
   %plyr.unmountImage(%slot);
   %plyr.decInventory(%item.item, 1);

   %rot = %item.getInitialRotation(%plyr);
   %deplObj = new StaticShape()
   {
      dataBlock = %item.deployed;
      team = %plyr.client.team;
   };

   %data = %deplObj.getDatablock();

   if(getTerrainAngle(%item.surfaceNrm) > 45)
      %deplObj.setDeployRotation(%item.surfacePt, %item.surfaceNrm);
   else
      %deplObj.setTransform(%item.surfacePt SPC %rot);

   %deplObj.setScale("1 1 1");
   %deplObj.setSelfPowered();
   %deplObj.team = %plyr.client.Team;
   %deplObj.owner = %plyr.client;
   %deplObj.playthread(0, "Deploy");

   if(%deplObj.getTarget() != -1)
         setTargetSensorGroup(%deplObj.getTarget(), %plyr.client.team);

   addToDeployGroup(%deplObj);
   AIDeployObject(%plyr.client, %deplObj);
   serverPlay3D(%item.deploySound, %deplObj.getTransform());
   $TeamDeployedCount[%plyr.team, "JammerBeaconPack"]++;
   MissionCleanup.add(%deplObj);

   %deplObj.deployedItem = "JammerBeaconPack";
}

function NodeJammerEmitter::onDestroyed(%this, %obj, %prevState)
{
    $TeamDeployedCount[%obj.team, "JammerBeaconPack"]--;
    %obj.schedule(250, "delete");
    Parent::onDestroyed(%this, %obj, %prevState);
}

//====================================== Regenerator Node
$TeamDeployableMax[NodeRegeneratorPack] = 1;

datablock StaticShapeData(NodeRegeneratorEmitter) : StaticShapeDamageProfile
{
   shapeFile      = "deploy_sensor_motion.dts";
   explosion      = MortarExplosion;

   maxDamage      = 10.0;
   destroyedLevel = 10.0;
   disabledLevel  = 10.0;

   expDmgRadius = 6.0;
   expDamage = 1.0;
   expImpulse = 1500.0;

   isShielded = false;
   energyPerDamagePoint = 0;
   maxEnergy = 0;
   rechargeRate = 0;

   targetTypeTag = 'Regenerator Node';

   debrisShapeName = "debris_generic.dts";
   debris = StaticShapeDebris;

   cmdCategory = "DSupport";
   cmdIcon = CMDSensorIcon;
   cmdMiniIconName = "commander/MiniIcons/com_deploymotionsensor";

   deployAmbientThread = true;
};

datablock ShapeBaseImageData(NodeRegeneratorPackImage)
{
   mass = 10;

   shapeFile = "pack_deploy_sensor_motion.dts";
   item = NodeRegeneratorPack;
   mountPoint = 1;
   offset = "0 0 0";
   deployed = NodeRegeneratorEmitter;

   stateName[0] = "Idle";
   stateTransitionOnTriggerDown[0] = "Activate";

   stateName[1] = "Activate";
   stateScript[1] = "onActivate";
   stateTransitionOnTriggerUp[1] = "Idle";

   isLarge = true;
   emap = true;

   maxDepSlope = 30;
   deploySound = MotionSensorDeploySound;

   minDeployDis                       =  0.5;
   maxDeployDis                       =  5.0;  //meters from body

   maxEnemyFlagDist = 200;
   minFriendlyFlagDist = 10;
   requiresBaseNode = false;
   maxBaseNodeDist = 0;
   maxEnemySpawnDist = 200;
   maxFriendlySpawnDist = 200;
};

datablock ItemData(NodeRegeneratorPack)
{
   className = Pack;
   catagory = "Deployables";
   shapeFile = "pack_deploy_sensor_motion.dts";
   mass = 2.0;
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 1;
   rotate = false;
   image = "NodeRegeneratorPackImage";
   pickUpName = "a regenerator node";

   emap = true;
};

function NodeRegeneratorPack::onPickup(%this, %obj, %shape, %amount)
{
   // created to prevent console errors
}

function NodeRegeneratorPackImage::onDeploy(%item, %plyr, %slot)
{
   %plyr.unmountImage(%slot);
   %plyr.decInventory(%item.item, 1);

   %rot = %item.getInitialRotation(%plyr);
   %deplObj = new StaticShape()
   {
      dataBlock = %item.deployed;
      team = %plyr.client.team;
   };

   %data = %deplObj.getDatablock();

   if(getTerrainAngle(%item.surfaceNrm) > 45)
      %deplObj.setDeployRotation(%item.surfacePt, %item.surfaceNrm);
   else
      %deplObj.setTransform(%item.surfacePt SPC %rot);

   %deplObj.setScale("1 1 1");
   %deplObj.team = %plyr.client.Team;
   %deplObj.owner = %plyr.client;
   %deplObj.playthread(0, "Deploy");

   if(%deplObj.getTarget() != -1)
         setTargetSensorGroup(%deplObj.getTarget(), %plyr.client.team);

   addToDeployGroup(%deplObj);
   AIDeployObject(%plyr.client, %deplObj);
   serverPlay3D(%item.deploySound, %deplObj.getTransform());
   $TeamDeployedCount[%plyr.team, "NodeRegeneratorPack"]++;
   MissionCleanup.add(%deplObj);

   %deplObj.deployedItem = "NodeRegeneratorPack";

   // Regenerator Aura
   %aura = Aura.create("SubspaceAura");
   %aura.attachToObject(%deplObj);
   %deplObj.regenField = %aura;
   %deplObj.addToTeamPowerNetwork();
}

function NodeRegeneratorEmitter::onGainPowerEnabled(%data, %obj)
{
    %obj.regenField.start();
    Parent::onGainPowerEnabled(%data, %obj);
}

function NodeRegeneratorEmitter::onLosePowerDisabled(%data, %obj)
{
    %obj.regenField.stop();
    Parent::onLosePowerDisabled(%data, %obj);
}

function NodeRegeneratorEmitter::onDestroyed(%this, %obj, %prevState)
{
    $TeamDeployedCount[%obj.team, "NodeRegeneratorPack"]--;
    %obj.schedule(250, "delete");
    
    Parent::onDestroyed(%this, %obj, %prevState);
}
