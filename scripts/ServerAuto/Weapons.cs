// Weapons Class Extension

$WeaponInfo::DisplayTime = 5;

function processSpawnCloakInvuln(%data, %obj)
{
   if( %obj.station $= "" && %obj.isCloaked() )
   {
      if( %obj.respawnCloakThread !$= "" )
      {
         Cancel(%obj.respawnCloakThread);
         %obj.setCloaked( false );
         %obj.respawnCloakThread = "";
      }
      else
      {
         if( %obj.getEnergyLevel() > 20 )
         {
            %obj.setCloaked( false );
            %obj.reCloak = %obj.schedule( 500, "setCloaked", true );
         }
      }
   }

   if( %obj.client > 0 )
   {
      %obj.setInvincibleMode(0 ,0.00);
      %obj.setInvincible( false ); // fire your weapon and your invincibility goes away.
   }
}

function ShapeBase::hasAmmo(%this, %weapon)
{
    if(%this.disableSwitch)
        return false;
     
    if(%weapon.dontCycleTo || %weapon.image.dontCycleTo)
        return false;
     
    if(%weapon.image.usesEnergy)
        return true;
    else
        return %weapon.image.hasAmmo(%this);
}

function ShapeBaseImageData::hasAmmo(%data, %obj)
{
    return %obj.getInventory(%data.ammo) > 0;
}

function ShapeBaseImageData::validateFire(%data, %obj, %vehicleID)
{
   // ---------------------------------------------------------------------------
    // determine draw source
    %bCap = false;
    %energyUse = 0;
    %ammoUse = 0;

    if(%obj.isPlayer())
    {
       // ---------------------------------------------------------------------------
       // z0dd - ZOD, 9/3/02. Anti rapid fire mortar/missile fix.
       // keen: Optimization for these two weapons only
//       if(%obj.cantFire !$= "")
//          return false;

//       if(%data.rapidFireGlitch !$= "")
//       {
//          %obj.cantFire = 1;
//          %preventTime = %data.stateTimeoutValue[4];
//          %obj.reloadSchedule = schedule(%preventTime * 1000, %obj, resetFire, %obj);
//          return false;
//       }
       
        if(%obj.cloakjammed)
            return false;
            
        %drawSource = %obj;
        %modeStats = %data.validateFireMode(%obj);

        if(%modeStats $= "f")
            return false;

        %energyUse = getWord(%modeStats, 0);
        %ammoUse = getWord(%modeStats, 1);
    }
    else
    {
        %drawSource = %obj;
        
        if(%data.usesEnergy)
        {
            if(%data.useMountEnergy && %drawSource.getEnergyLevel() < %data.fireEnergy)
                return false;
            else if(%data.useCapacitor && %data.usesEnergy && %vehicleID.turretObject.getCapacitorLevel() > %data.fireEnergy)
            {
                %drawSource = %vehicleID.turretObject;
                %bCap = true;
            }
            else if(%drawSource.getEnergyLevel() < %data.fireEnergy)
                return false;

            %energyUse = %data.fireEnergy;
        }
        
        if(%data.ammo !$= "")
        {
            if(%data.hasAmmo(%obj) > 0)
                %ammoUse = 1;
            else
                return false;
        }
    }

    // use draw resources
    if(%energyUse)
    {
       %energyUse *= %drawSource.energyEfficiencyFactor; // + %drawSource.energyEfficiency[%data.item];
       
       if(%bCap)
           %drawSource.useCapEnergy(%energyUse);
       else
           %drawSource.useEnergy(%energyUse);
    }

    if(%ammoUse)
        %obj.decInventory(%data.ammo, %ammoUse);
        
    return true;
}

// Default fire mode
function ShapeBaseImageData::validateFireMode(%data, %obj)
{
    %ammoUse = 0;
    
    if(%data.usesEnergy)
        if(%obj.getEnergyLevel() < %data.fireEnergy)
            return "f";

    if(%data.ammo !$= "")
        if(%data.hasAmmo(%obj) > 0)
            %ammoUse = 1;
        else
            return "f";

    %energyUse = %data.fireEnergy > 0 ? %data.fireEnergy : 0;

    return %energyUse SPC %ammoUse;
}

//  %thegreatcircle = %obj.getControllingClient().getControlObject();
//  vehicle|turret.getControllingClient()->clientconnection.getControlObject()
function ShapeBaseImageData::spawnProjectile(%data, %obj, %slot)
{
    if(%data.useForwardVector == true)
        %vector = %data.projectileSpread > 0 ? vectorSpread(%obj.getEyeVector(), %data.projectileSpread) : %obj.getEyeVector();
    else if(%data.pilotHeadTracking == true)
    {
        %vect = %obj.isVehicle() ? %obj.getMountNodeObject(0).getEyeVector() : %obj.getMuzzleVector(%slot);
        %vector = %data.projectileSpread > 0 ? vectorSpread(%vect, %data.projectileSpread * %obj.spreadFactorBase) : %vect;
    }
    else if(%data.scorpionHeadTracking == true)
    {
        %vect = %obj.vehicle.getMountNodeObject(0).getEyeVector();
        %vector = %data.projectileSpread > 0 ? vectorSpread(%vect, %data.projectileSpread * %obj.spreadFactorBase) : %vect;
    }
    else
        %vector = %data.projectileSpread > 0 ? vectorSpread(%obj.getMuzzleVector(%slot), %data.projectileSpread * %obj.spreadFactorBase) : %obj.getMuzzleVector(%slot);

    %point = %obj.getMuzzlePoint(%slot);
    
    if(%data.projectileOffset > 0)
        %point = vectorAdd(%point, (%data.projectileOffset * getRandomT()) SPC "0" SPC (%data.projectileOffset * getRandomT()));

    if(%obj.isWalker)
        %proj = createProjectile(%data.projectileType, %data.projectile, %vector, %point, %obj, %slot, %obj.pilotObject);
    else
        %proj = createProjectile(%data.projectileType, %data.projectile, %vector, %point, %obj, %slot, %obj);
        
    %proj.damageBuffFactor = %obj.damageBuffFactor;

    if(%data.deleteLastProjectile)
    {
        if(isObject(%obj.lastProjectile))
            %obj.lastProjectile.delete();

        %obj.deleteLastProjectile = %data.deleteLastProjectile;
    }

    %obj.lastProjectile = %proj;

    // AI hook
    if(%obj.client)
        %obj.client.projectile = %proj;

    if(%data.updatePilotAmmo == true && %data.staggerCount $= "")
    {
        if(%data.scorpionHeadTracking == true)
            %obj.getDatablock().updateAmmoCount(%obj, %obj.vehicle.getMountNodeObject(0).client, %data.ammo);
        else
            %obj.getDatablock().updateAmmoCount(%obj, %obj.getControllingClient(), %data.ammo);
    }

    if(%data.projectileType $= "SniperProjectile")
        %proj.setEnergyPercentage(0.85);
        
    return %proj;
}

function ShapeBaseImageData::onFire(%data, %obj, %slot)
{
   // ---------------------------------------------------------------------------
   // z0dd - ZOD, 9/3/02. Anti rapid fire mortar/missile fix.
   if(%obj.cantFire > 0)
   {
      %data.misFire(%obj, %slot);
      return 0;
   }

   %wpnName = %data.getName();
   if((%wpnName $= "MortarImage") || (%wpnName $= "MissileLauncherImage"))
   {
      %obj.cantFire = 1;
      %preventTime = %data.stateTimeoutValue[4];
      %obj.reloadSchedule = schedule(%preventTime * 1000, %obj, resetFire, %obj);
   }
   // ---------------------------------------------------------------------------
   
    if(%obj.isCloaked())
    {
        %data.misFire(%obj, %slot);
        return;
    }
        
    if(%data.sharedResourcePool == true)
        return VehicleImage::onFire(%data, %obj, %slot);

    %mount = %obj.getObjectMount();
    %turret = isObject(%mount) ? %mount : 0;
    
    if(!%data.validateFire(%obj, %turret))
    {
        %data.misFire(%obj, %slot);
        return;
    }

    if(%data.staggerCount !$= "")
        return %data.staggerFire(%obj, %slot, %data.staggerCount, 1);
    else
    {
        %p = %data.spawnProjectile(%obj, %slot, %mode);

        %p.currentFireMode = %obj.selectedFireMode[%data.item];
        %fireSound = isObject(%mode) && %mode.fireSound !$= "" ? %mode.fireSound : %data.defaultModeFireSound;

        if(%fireSound !$= "" || %fireSound !$= "n")
            %obj.play3D(%fireSound);
            
        if(%data.isLaser)
            %p.setEnergyPercentage(%data.laserOpacity);
    }
    
    return %p;
}

function ShapeBaseImageData::staggerFire(%data, %obj, %slot, %total, %iteration)
{
    if(%iteration > %total)
    {
        if(%data.ammo !$= "" && %data.updatePilotAmmo == true && !%obj.isPlayer())
        {
            %obj.getDatablock().updateAmmoCount(%obj, %obj.getControllingClient(), %data.ammo);
            
            %vtest = %obj.getObjectMount();
            %drawSource = isObject(%vtest) ? %vtest : %obj;

            if(%drawSource.ammoCache[%data.ammo] < 1)
            {
                %drawSource.ammoCache[%data.ammo] = 0;
                %obj.setImageAmmo(%slot, false);
                return;
            }
        }

        return;
    }
        
    %p = %data.spawnProjectile(%obj, %slot);
    %fireSound = %data.defaultModeFireSound;

    if(%fireSound !$= "" || %fireSound !$= "n")
        %obj.play3D(%fireSound);

    if(%data.isLaser)
        %p.setEnergyPercentage(%data.laserOpacity);
            
    %data.schedule(%data.staggerDelay, "staggerFire", %obj, %slot, %total, %iteration++);
    
    return %p;
}

function ShapeBaseImageData::misFire(%data, %obj, %slot)
{
    if(%data.defaultModeFailSound !$= "")
        %obj.play3D(%data.defaultModeFailSound);
}

function ShapeBaseImageData::playSoundIfAmmo(%data, %obj, %sound)
{
    if(isObject(%obj))
        if(%obj.getMountedImage(0) == %data)
            if(%obj.getInventory(%data.ammo) > 0)
                %obj.play3d(%sound);
}

// Functions for vehicle-specific weaponry - not sure if a new script class
// _needs_ to be created for it, so borrowing here
function VehicleImage::onFire(%data, %obj, %slot)
{
    %vtest = %obj.getObjectMount();
    %vehicle = isObject(%vtest) ? %vtest : %obj;
    
    if(!VehicleImage::validateFire(%data, %vehicle, %obj, %slot))
    {
        %data.misFire(%obj, %slot);
        return;
    }

    if(%data.staggerCount !$= "")
        return %data.staggerFire(%obj, %slot, %data.staggerCount, 1);
    else
        %p = %data.spawnProjectile(%obj, %slot, %mode);

    return %p;
}

function VehicleImage::validateFire(%data, %drawSource, %obj, %slot)
{
    %energyUse = 0;
    %ammoUse = 0;
    
    if(%drawSource.isCloaked())
        return false;

    if(%data.usesEnergy)
    {
        if(%drawSource.getEnergyLevel() < %data.fireEnergy)
            return false;

        %energyUse = %data.fireEnergy;
    }

    if(%data.ammo !$= "")
    {
        if(%drawSource.ammoCache[%data.ammo] > 0)
            %ammoUse = %data.staggerCount > 0 ? %data.staggerCount : 1;
        else
            return false;
    }

    // DarkDragonDX: Prevent pilot head tracking weapons from hitting their own vehicles
    // disabling for now until more testing can be done as this was preventing some weapons from firing at all
//    if(%data.pilotHeadTracking)
//    {
//        %ray = ContainerRayCast(%drawSource.getMuzzlePoint(%slot), vectorProject(%drawSource.getEyeVector(), 10), -1);

//        if(getWord(%ray, 0) == %drawSource)
//            return false;
//    }
    
    // use draw resources
    if(%energyUse)
    {
       %energyUse *= %drawSource.energyEfficiencyFactor;
       %drawSource.useEnergy(%energyUse);
    }

    if(%ammoUse)
    {
        %drawSource.ammoCache[%data.ammo]--; // -= %data.staggerCount > 0 ? %data.staggerCount : 1;
        
        if(%drawSource.ammoCache[%data.ammo] < 1)
        {
            %drawSource.ammoCache[%data.ammo] = 0;
            %obj.setImageAmmo(%slot, false);
        }
    }
    
    return true;
}

// Charging weapons
function ShapeBaseImageData::onFired(%data, %obj, %slot)
{
//    commandToClient(%obj.getControllingClient(), 'ClearDeploySensor');
    %time = getSimTime();

    if(%time < %obj.lastFired[%slot])
    {
        %obj.setImageTrigger(%slot, false);
        return;
    }
    
    %obj.weaponCharging[%slot] = true;
    %obj.weaponChargeState[%slot] = 0;
    %obj.chargingTicks[%slot] = 0;
    %obj.weaponChargeStart[%slot] = %time;
    %obj.chargeReadyTime[%slot] = %time + %data.chargingTime;
    %obj.overchargeTime[%slot] = %time + %data.chargingTime + %data.overchargeTime;
    
    commandToClient(%obj.getControllingClient(), 'ShowDeploySensor', %data.chargingBeginColor, 0);
    %data.schedule($g_TickTime, "ChargeTick", %obj, %slot);
}

function ShapeBaseImageData::ChargeTick(%data, %obj, %slot)
{
    if(!%obj.weaponCharging[%slot])
        return;
        
    %obj.chargingTicks[%slot]++;
    %time = getSimTime();
    
    if(%time >= %obj.chargeReadyTime[%slot] && %obj.weaponChargeState[%slot] == 0)
    {
        %obj.weaponChargeState[%slot]++;
        commandToClient(%obj.getControllingClient(), 'ShowDeploySensor', %data.chargingReadyColor, 0);
    }
    
    if(%time >= %obj.overchargeTime[%slot] && %obj.weaponChargeState[%slot] == 1)
        %obj.setImageTrigger(%slot, false);
    
    %data.schedule($g_TickTime, "ChargeTick", %obj, %slot);
}

function ShapeBaseImageData::onReleased(%data, %obj, %slot)
{
    %time = getSimTime();
    %obj.weaponCharging[%slot] = false;
    %obj.chargingTicks[%slot] = 0;
    
    commandToClient(%obj.getControllingClient(), 'ClearDeploySensor');
    
    if(%time >= %obj.chargeReadyTime[%slot] && %time <= %obj.overchargeTime[%slot])
    {
        %p = %data.onFire(%obj, %slot);
        
        if(!%p)
        {
            %obj.lastFired[%slot] = %time + %data.chargeFailRecycleTime;
            return;
        }

//        %obj.decInventory(%data.ammo, 1);
//        %obj.applyKick(500);
        %obj.play3D(%data.chargeFireSound);
        %obj.schedule(%data.chargeReloadSoundTime, "play3d", %data.chargeReloadSound);
        %obj.lastFired[%slot] = %time + %data.chargeReloadTime;
        
        return %p;
    }
    else
    {
        %obj.lastFired[%slot] = %time + %data.chargeFailRecycleTime;
        %obj.play3D(%data.chargeFailSound);
    }
}

// Grenade Launcher/RPG functionality
function HandInventory::onUse(%data, %obj)
{
   // %obj = player  %data = datablock of what's being thrown
   if(Game.handInvOnUse(%data, %obj))
   {
      //AI HOOK - If you change the %throwStren, tell Tinman!!!
      //Or edit aiInventory.cs and search for: use(%grenadeType);

      // z0dd - ZOD, 6/11/02. Toss grenades and your invincibility and cloaking goes away.
      if(%obj.station $= "" && %obj.isCloaked())
      {
         if( %obj.respawnCloakThread !$= "" )
         {
            Cancel(%obj.respawnCloakThread);
            %obj.setCloaked( false );
            %obj.respawnCloakThread = "";
         }
      }
      if( %obj.client > 0 )
      {
         %obj.setInvincibleMode(0, 0.00);
         %obj.setInvincible( false );
      }

      %tossTimeout = getSimTime() - %obj.lastThrowTime[%data];
      if(%tossTimeout < $HandInvThrowTimeout)
         return;

      %throwStren = %obj.throwStrength;

      if(%obj.isDreadnought && %data.isGrenade == true)
      {
         %obj.arpgGrenade = %data;
         %obj.lastThrowTime[%data] = getSimTime();

         %obj.setImageTrigger($ShoulderSlot, true);

         %obj.throwStrength = 0; // could this be used?

         return;
      }

      %obj.decInventory(%data, 1);
//      %obj.setInventory("GrenadeLauncherAmmo", %data);

      %thrownItem = new Item()
      {
         dataBlock = %data.thrownItem;
         sourceObject = %obj;
      };
      MissionCleanup.add(%thrownItem);

      // throw it
      %eye = %obj.getEyeVector();
      %vec = vectorScale(%eye, (%throwStren * 20.0));

      // add a vertical component to give it a better arc
      %dot = vectorDot("0 0 1", %eye);
      if(%dot < 0)
         %dot = -%dot;
      %vec = vectorAdd(%vec, vectorScale("0 0 10", 1 - %dot)); // z0dd - ZOD, 8/4/02. Add a higher arc to the toss. was 0 0 4

      // add player's velocity
      %vec = vectorAdd(%vec, vectorScale(%obj.getVelocity(), 0.65)); // z0dd - ZOD, 8/4/02. Add more of the players vel to the toss. was 0.4
      %pos = getBoxCenter(%obj.getWorldBox());

      %thrownItem.sourceObject = %obj;
      %thrownItem.instigator = %obj;
      %thrownItem.team = %obj.team;
      %thrownItem.setTransform(%pos);

      %thrownItem.applyImpulse(%pos, %vec);
      %thrownItem.setCollisionTimeout(%obj);
      serverPlay3D(GrenadeThrowSound, %pos);
      %obj.lastThrowTime[%data] = getSimTime();

      %thrownItem.getDataBlock().onThrow(%thrownItem, %obj);
      %obj.throwStrength = 0;
   }
}

function Item::onGrenadeExplode(%this, %instigator, %position)
{
    %this.setPosition(%position);
    detonateGrenade(%this); // detonategrenade should be set to schedule(0) maybe? seems to work a lot better when not in the middle of script execution
}

// keen: provide facilities for alternate weapon images (ex. +/- Main RoF images)
function Weapon::onUse(%data, %obj)
{
    if(Game.weaponOnUse(%data, %obj))
        if(%obj.isPlayer())
            %obj.mountImage(%data.image, $WeaponSlot);
}

// probably not needed
function Player::updateReticleAmmo(%obj, %weapon, %targetSlot)
{
    if(%targetSlot $= "")
        %targetSlot = 0;
        
    if(%weapon $= "")
        %weapon = %obj.getMountedImage(%targetSlot);

    if(%weapon.ammo !$= "")
        %obj.client.setAmmoHudCount(%obj.getInventory(%weapon.ammo));
    else
        %obj.client.setAmmoHudCount("");
}

function WeaponImage::onMount(%this, %obj, %slot)
{
    if(!%obj.isPlayer())
        return;

    if(%this.subImage)
    {
        %obj.setImageAmmo(%slot, true);
        return;
    }
        
    //messageClient(%obj.client, 'MsgWeaponMount', "", %this, %obj, %slot);
    // Looks arm position
    if(%this.armthread $= "")
       %obj.setArmThread(look);
    else
       %obj.setArmThread(%this.armThread);

    %tps = %data.triggerProxySlot !$= "" ? %data.triggerProxySlot : 0;
    %targetSlot = %tps ? %tps : $WeaponSlot;
    
    // Initial ammo state
    if(%obj.getMountedImage(%targetSlot).ammo !$= "")
        if(%obj.getInventory(%this.ammo))
            %obj.setImageAmmo(%slot, true);
    
    %obj.client.setWeaponsHudActive(%this.item);
    
    if(!%obj.client.isAIControlled())
        %obj.updateReticleAmmo(%this, %targetSlot);
        
//    %obj.updateWeaponMode(%slot, true);
        
    // check for sub-images
    if(%this.subImage1 !$= "")
        %obj.mountImage(%this.subImage1, $WeaponSubImage1);

    if(%this.subImage2 !$= "")
        %obj.mountImage(%this.subImage2, $WeaponSubImage2);

    if(%this.subImage3 !$= "")
        %obj.mountImage(%this.subImage3, $WeaponSubImage3);
        
    commandToClient(%obj.client, 'BottomPrint', "<color:ffd589>"@$WeaponsListData[$WeaponsListID[%this.item], "name"]@"<color:ffffff>" NL $WeaponsListData[$WeaponsListID[%this.item], "desc"], 5, 2); // NL $WeaponsListData[$WeaponsListID[%this.item], "stats"]
}

function updateReticle(%client, %item, %guiTex)
{
    %client.setWeaponsHudActive(%item, %guiTex, true);
}

function WeaponImage::onUnmount(%this,%obj,%slot)
{
   if(%obj.isVehicle())
   {
       Parent::onUnmount(%this, %obj, %slot);
       return;
   }
   
   %obj.client.setWeaponsHudActive(%this.item, 1);
   %obj.client.setAmmoHudCount(-1);
   
   commandToClient(%obj.client,'removeReticle');
   
   // try to avoid running around with sniper/missile arm thread and no weapon
   %obj.setArmThread(look);
   
    // check for sub-images
    if(%this.subImage1 !$= "")
        %obj.unmountImage($WeaponSubImage1);

    if(%this.subImage2 !$= "")
        %obj.unmountImage($WeaponSubImage2);

    if(%this.subImage3 !$= "")
        %obj.unmountImage($WeaponSubImage3);
        
   Parent::onUnmount(%this, %obj, %slot);
}

// Add exception for Engineer here
function TargetingLaserImage::onMount(%this,%obj,%slot)
{
    %eng = %obj.getDatablock().flags & $ArmorMask::Engineer;
   
    if(%eng)
    {
        %obj.unmountImage(%slot);
        %obj.mountImage(DeconstructorImage, %slot);
//        commandToClient(%obj.client, 'setRepairReticle');
    }
    else
        Parent::onMount(%this,%obj,%slot);
}

datablock AudioProfile(UndeploySound)
{
   filename    = "fx/misc/static.wav";
   description = AudioDefault3d;
   preload = true;
};

datablock ItemData(Deconstructor)
{
   className    = Weapon;
   catagory     = "Spawn Items";
   shapeFile    = "weapon_shocklance.dts";
   image        = DeconstructorImage;
   mass         = 1;
   elasticity   = 0.2;
   friction     = 0.6;
   pickupRadius = 2;
	pickUpName   = "a deconstructor";

   emap = true;
};

datablock ShapeBaseImageData(DeconstructorImage)
{
   classname = WeaponImage;
   shapeFile = "weapon_shocklance.dts";
   item = Deconstructor;
   offset = "0 0 0";
   emap = true;
   tool = true;

   usesEnergy = true;
   missEnergy = 0;
   hitEnergy  = 15;
   minEnergy  = 15;       // needs to change to be datablock's energy drain for a hit

   stateName[0] = "Activate";
   stateTransitionOnTimeout[0] = "ActivateReady";
   stateSound[0] = ShockLanceSwitchSound;
   stateTimeoutValue[0] = 0.5;
   stateSequence[0] = "Activate";

   stateName[1] = "ActivateReady";
   stateTransitionOnLoaded[1] = "Ready";
   stateTransitionOnNoAmmo[1] = "NoAmmo";

   stateName[2] = "Ready";
   stateTransitionOnNoAmmo[2] = "NoAmmo";
   stateTransitionOnTriggerDown[2] = "Fire";

   stateName[3] = "Fire";
   stateTransitionOnTimeout[3] = "Reload";
   stateTimeoutValue[3] = 0.5;
   stateFire[3] = true;
   stateAllowImageChange[3] = false;
   stateSequence[3] = "Fire";
   stateScript[3] = "onFire";
   stateSound[3] = "";

   stateName[4] = "Reload";
   stateTransitionOnNoAmmo[4] = "NoAmmo";
   stateTransitionOnTimeout[4] = "Ready";
   stateTimeoutValue[4] = 1.0;
   stateAllowImageChange[4] = false;
   stateSequence[4] = "Reload";
   stateSound[4] = ShockLanceReloadSound;

   stateName[5] = "NoAmmo";
   stateTransitionOnAmmo[5] = "Ready";

   stateName[6]                  = "DryFire";
   stateSound[6]                 = ShockLanceDryFireSound;
   stateTimeoutValue[6]          = 1.0;
   stateTransitionOnTimeout[6]   = "Ready";
};

function DeconstructorImage::onFire(%this, %obj, %slot)
{
    %hit = castRay(%obj.getMuzzlePoint(%slot), %obj.getMuzzleVector(%slot), 10, $TypeMasks::TerrainObjectType | $TypeMasks::InteriorObjectType | $TypeMasks::VehicleObjectType | $TypeMasks::StaticShapeObjectType | $TypeMasks::ItemObjectType, %obj);        if(%point)

    if(!%hit || %hit.hitObj.team != %obj.team || %hit.hitObj.deployedItem $= "")
    {
        %obj.playAudio(0, ShockLanceMissSound);
        return;
    }
    
    %hitObj = %hit.hitObj;
    %hitPos = %hit.hitPos;

    %hitObj.play3D(UndeploySound);
    %hitObj.setCloaked(true);
    %hitObj.schedule(448, "setPosition", "0 0 -10000");
    %hitObj.schedule(512, "setDamageLevel", %hitObj.getDatablock().maxDamage);
    %hitObj.schedule(512, "setDamageState", "Destroyed");

    %item = new Item()
    {
        dataBlock = %hitObj.deployedItem;
        static = true;
        rotate = false;
    };

    MissionCleanup.add(%item);
    %item.setCloaked(true);
    %item.schedule(640, "setPosition", %hitPos);
    %item.schedule(1024, "setCloaked", false);
    %item.schedulePop();
}
