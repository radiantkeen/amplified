// AI Core Script

// Name generation
// -----------------------------------------------------------------------------
// Variable defs
$AINameTypeBase = 1;
$AINameTypePrefix = 2;
$AINameTypeSuffix = 3;
$AINameTypeVehicle = 4;
$AINameTypeRegistry = 5;

$AINameCount[$AINameTypeBase] = 0;
$AINameCount[$AINameTypePrefix] = 0;
$AINameCount[$AINameTypeSuffix] = 0;
$AINameCount[$AINameTypeVehicle] = 0;
$AINameCount[$AINameTypeRegistry] = 0;

$AINamePrefixChance = 50;
$AINameSuffixChance = 50;

// Code
function AIRegisterBotName(%type, %name)
{
    $AIBotNamePool[%type, $AINameCount[%type]] = %name;
    $AINameCount[%type]++;
}

function AIResetNameTable(%type)
{
    for(%i = 0; %i < $AINameCount[%type]; %i++)
        $AIBotNameUsed[$AIBotNamePool[%type, %i]] = false;
}

function AIGenerateName(%type)
{
    %idx = 0;

    while(%idx < $AINameCount[%type])
    {
        %name = $AIBotNamePool[%type, getRandom($AINameCount[%type] -1)];

        if(!$AIBotNameUsed[%name])
        {
            $AIBotNameUsed[%name] = true;

            return %name;
        }

        %idx++;
    }

    AIResetNameTable(%type);

    return AIGenerateName(%type);
}

function AIGenerateRandomName()
{
    %prefix = (getRandom(100) >= $AINamePrefixChance);
    %suffix = (getRandom(100) >= $AINameSuffixChance);
    %name = "";

    if(%prefix)
        %name = AIGenerateName($AINameTypePrefix);

    %name = %name $= "" ? AIGenerateName($AINameTypeBase) : %name SPC AIGenerateName($AINameTypeBase);

    if(%suffix)
        %name = %name SPC AIGenerateName($AINameTypeSuffix);

     // DarkDragonDX: Hopefully fix weird prepended spaces in bot names
     return trim(%name);
}

function AIGenerateRandomShipName()
{
    %registry = (getRandom(100) >= $AINameRegistryChance);
    %name = "";

    if(%registry)
    {
        %registryName = AIGenerateName($AINameTypeRegistry);
        %name = %registryName@"-"@mFloor(getRandom() * 10000);
    }
    else
        %name = AIGenerateName($AINameTypeRegistry); //"The";

    %name = %name SPC AIGenerateName($AINameTypeVehicle);

    return trim(%name);
}

function convertOldBotNamesToConsole(%section)
{
    if(%section == 0)
    {
        $RandomBotGenNameCount++;

        for(%i = 0; %i < $RandomBotGenNameCount; %i++)
            echo("AIRegisterBotName($AINameTypeBase, \""@$RandomBotGenName[%i]@"\");");

        schedule(0, 0, "convertOldBotNamesToConsole", 1);
    }
    else if(%section == 1)
    {
        $RandomBotGenPrefixCount++;

        for(%i = 0; %i < $RandomBotGenPrefixCount; %i++)
            echo("AIRegisterBotName($AINameTypePrefix, \""@$RandomBotGenPrefix[%i]@"\");");

        schedule(0, 0, "convertOldBotNamesToConsole", 2);
    }
    else if(%section == 2)
    {
        $RandomBotGenSuffixCount++;

        for(%i = 0; %i < $RandomBotGenSuffixCount; %i++)
            echo("AIRegisterBotName($AINameTypeSuffix, \""@$RandomBotGenSuffix[%i]@"\");");
    }
}

// Hook into base T2 to use the name
// -----------------------------------------------------------------------------
function AIConnection::onAIConnect(%client, %name, %team, %skill, %offense, %voice, %voicePitch)
{
   if(%skill $= "")
      %skill = getRandom();

   %name = AIGenerateRandomName();

   // Sex/Race defaults
   %client.race = getRandom(1) ? "Human" : "Bioderm";

   if(%client.race $= "Human")
        %client.sex = getRandom(1) ? "Male" : "Female";
   else
        %client.sex = "Male";

   %client.armor = getRandom(1) ? "Light" : "Medium";

   if(%client.race $= "Human" && %client.sex $= "Male")
        %voice = getRandom(1) ? "Bot1" : "Male"@getRandom(1,5);
   else if(%client.race $= "Human" && %client.sex $= "Female")
        %voice = getRandom(1) ? "Bot1" : "Fem"@getRandom(1,5);
   else
        %voice = getRandom(1) ? "Bot1" : "Derm"@getRandom(1,3);

   %client.voice = %voice;
   %client.voiceTag = addTaggedString(%voice);

//   if (%voicePitch $= "" || %voicePitch < 0.5 || %voicePitch > 2.0)
//      %voicePitch = 1.0;
   %voicePitch = 1 - ((getRandom(20) - 10)/100);
   %client.voicePitch = %voicePitch;

   %client.name = addTaggedString( "\cp\c9" @ %name @ "\co" );
   %client.nameBase = %name;

   echo("BOTADD:" SPC %client.name SPC %client SPC %client.getAddress());
   $HostGamePlayerCount++;

   //set the initial team - Game.assignClientTeam() should be called later on...
   %client.team = %team;

   if(%voice $= "Bot1")
        %skin = getRandom(1) ? "basebot" : "basebbot";
   else if(%client.race $= "Human")
        %skin = getRandom(1) ? "beagle" : "swolf";
   else
        %skin = getRandom(1) ? "base" : "baseb";

   %client.skin = addTaggedString(%skin);

	//setup the target for use with the sensor net, etc...
   %client.target = allocClientTarget(%client, %client.name, %client.skin, %client.voiceTag, '_ClientConnection', 0, 0, %client.voicePitch);

   if($currentMissionType $= "SinglePlayer")
		messageAllExcept(%client, -1, 'MsgClientJoin', "", %name, %client, %client.target, true);
   else
	   messageAllExcept(%client, -1, 'MsgClientJoin', '\c1%1 joined the game.', %name, %client, %client.target, true);

	//assign the skill
	%client.setSkillLevel(%skill);

	//assign the affinity
   %client.offense = getRandom(1);

   //clear any flags
   %client.stop(); // this will clear the players move state
   %client.clearStep();
   %client.lastDamageClient = -1;
   %client.lastDamageTurret = -1;
   %client.setEngageTarget(-1);
   %client.setTargetObject(-1);
   %client.objective = "";

	//clear the defaulttasks flag
	%client.defaultTasksAdded = false;

	//if the mission is already running, spawn the bot
   if ($missionRunning)
      %client.startMission();

   schedule(0, 0, "updatePlayerCounts");
}

if($AIRegisteredNames != true)
{
    exec("scripts/Server/AINameTable.cs");
    $AIRegisteredNames = true;
}
