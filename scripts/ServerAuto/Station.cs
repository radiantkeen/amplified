//------------------------------------------------------------------------------

function displayArmorStatistics(%obj)
{
    %client = %obj.client;
    %data = %obj.getDataBlock();
    %rRate = %obj.getRechargeRate() * ($g_tickDelta * 1000);
    
    if(%obj.maxHitPoints < %obj.baseHitPoints)
         %colorFlag = $HudTextColors::Red;
    else if(%obj.maxHitPoints > %obj.baseHitPoints)
         %colorFlag = $HudTextColors::Green;
    else
         %colorFlag = $HudTextColors::White;

    bottomPrint(%client, $HudTextColors::Default@""@$ArmorsListData[$ArmorsListID[%client.armor], "name"] SPC "Slots:" SPC %data.enhancementSlots NL "Armor:"@%colorFlag SPC %obj.maxHitPoints SPC "<color:42dbea>HP | Energy:<color:FFFFFF>" SPC %data.maxEnergy SPC "<color:42dbea>KW Rate:" SPC %rRate SPC "KW\\s | Run Speed:" SPC convertMS(%data.maxForwardSpeed, "kph") SPC "KPH" NL $ArmorsListData[$ArmorsListID[%client.armor], "desc"], 300, 3);
//    bottomPrint(%client, $HudTextColors::Default@""@$ArmorsListData[$ArmorsListID[%client.armor], "name"] SPC "Slots:" SPC %data.enhancementSlots SPC "Installed Module:" SPC %obj.currentArmorMod.displayName NL "Armor:"@%colorFlag SPC %obj.maxHitPoints SPC "<color:42dbea>HP | Energy:<color:FFFFFF>" SPC %data.maxEnergy SPC "<color:42dbea>KW | Charge Rate:" SPC %rRate SPC "KW\s | Run Speed:" SPC convertMS(%data.maxForwardSpeed, "kph") SPC "KPH" NL $ArmorsListData[$ArmorsListID[%client.armor], "desc"], 300, 3);
}

// Inventory functions
function ShapeBase::maxInventory(%this, %data)
{
    return %this.getDatablock().max[%data.getName()];
}

function ShapeBase::incInventory(%this,%data,%amount)
{
   %max = %this.maxInventory(%data);
   %cv = %this.inv[%data.getName()];
   if (%cv < %max) {
      if (%cv + %amount > %max)
         %amount = %max - %cv;
      %this.setInventory(%data,%cv + %amount);
      %data.incCatagory(%this); // Inc the players weapon count
      return %amount;
   }
   return 0;
}

function ShapeBase::decInventory(%this,%data,%amount)
{
   %name = %data.getName();
   %cv = %this.inv[%name];
   if (%cv > 0) {
      if (%cv < %amount)
         %amount = %cv;
      %this.setInventory(%data,%cv - %amount, true);
      %data.decCatagory(%this); // Dec the players weapon count
      return %amount;
   }
   return 0;
}

// Repair kit overrides
//function RepairPatch::onCollision(%data,%obj,%col) // come back to this later?
//function RepairKit::onUse(%data, %obj)
