//-----------------------------------------------------------------------------
// ShapeBase Extension
//--------------------------------------

function ShapeBase::setPowerLevel(%obj, %amount)
{
    %energyPct = %amount / %obj.maxPower;
    %obj.setEnergyLevel(%healthPct * %obj.getDatablock().maxEnergy);
}

function ShapeBase::getPowerLevel(%obj)
{
    return mCeil(%obj.getEnergyLevel() * 100);
}

function GameConnection::playHitIndicator(%client, %sameteam)
{
    if(%client.hitDisplay > 0)
        cancel(%client.hitDisplay);

    if(%sameteam)
        activateDeploySensorRed(%client.player);
    else
        activateDeploySensorGrn(%client.player);

    %client.hitDisplay = %client.schedule(256, "clearHitIndicator");
}

function GameConnection::clearHitIndicator(%client)
{
    %client.hitDisplay = 0;
    deactivateDeploySensor(%client.player);
}

function ShapeBaseData::onAdd(%data, %obj)
{
   Parent::onAdd(%data, %obj);
	// if it's a deployed object, schedule the ambient thread to play in a little while
   if(%data.deployAmbientThread)
	   %obj.schedule(750, "playThread", $AmbientThread, "ambient");
	// check for ambient animation that should always be played
	if(%data.alwaysAmbient)
		%obj.playThread($AmbientThread, "ambient");

    // keen: Amplified initial stuff
    %obj.isWet = false;
    %obj.isFragmented = false;
    
    %obj.damageReduceFactor = 1.0;
    %obj.shieldAbsorbFactor = 1.0;
    %obj.damageBuffFactor = 1.0;
    %obj.energyEfficiencyFactor = 1.0;
    %obj.spreadFactorBase = 1.0;
    %obj.maxAmmoCapacityFactor = 1.0;
    %obj.rechargeRateFactor = 1.0;
    %obj.damageReduction = 0;
}

function GameBaseData::onAdd(%data, %obj)
{
   if(%data.targetTypeTag !$= "")
   {
      // use the name given to the object in the mission file
      if(%obj.nameTag !$= "")
      {
         %obj.nameTag = addTaggedString(%obj.nameTag);
         %nameTag = %obj.nameTag;
      }
      else
         %nameTag = %data.targetNameTag;

   	%obj.target = createTarget(%obj, %nameTag, "", "", %data.targetTypeTag, 0, 0);
   }
   else
      %obj.target = -1;
}

function ShapeBase::getObjectSlot(%this, %obj)
{
    for(%i = 0; %i < 16; %i++)
    {
        if(%this.getMountNodeObject(%i) == %obj)
            return %i;
    }
    
    return -1;
}
