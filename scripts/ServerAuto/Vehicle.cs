//------------------------------------------------------------------------------
// Special - 0-1
$VehicleTargeterSlot = 0;
$VehicleWeightSlot = 1;

datablock AudioProfile(DebrisFXExplosionSound)
{
   filename = "fx/explosions/deployables_explosion.wav";
   description = AudioExplosion3d;
   preload = true;
};

datablock AudioProfile(DebrisFXSparksSound)
{
   filename = "fx/weapons/ELF_underwater.wav";
   description = AudioExplosion3d;
   preload = true;
};

datablock DebrisData(HitDebrisFX)
{
   explodeOnMaxBounce = false;

   elasticity = 0.35;
   friction = 0.5;

   lifetime = 12.0;
   lifetimeVariance = 4.0;

   minSpinSpeed = 20;
   maxSpinSpeed = 800;

   numBounces = 5;
   bounceVariance = 3;

   staticOnMaxBounce = true;
   gravModifier = 1.0;

   useRadiusMass = true;
   baseRadius = 1;

   velocity = 14.0;
   velocityVariance = 7.0;
};

datablock ExplosionData(DebrisFXExplosion)
{
   soundProfile = DebrisFXExplosionSound;
   faceViewer = true;

   explosionShape = "effect_plasma_explosion.dts";
   sizes[0] = "0.2 0.2 0.2";
   sizes[1] = "0.3 0.3 0.3";
};

datablock ParticleData(DebrisEnergySparks)
{
   dragCoefficient      = 1;
   gravityCoefficient   = 0.0;
   inheritedVelFactor   = 0.0;
   constantAcceleration = 0.0;
   lifetimeMS           = 350;
   lifetimeVarianceMS   = 50;
   textureName          = "special/blueSpark";
   colors[0]     = "1.0 1.0 1.0 1.0";
   colors[1]     = "0.4 0.4 1.0 1.0";
   colors[2]     = "0.2 0.1 0.5 0.0";
   sizes[0]      = 2.0;
   sizes[1]      = 1.5;
   sizes[2]      = 1.0;
   times[0]      = 0.0;
   times[1]      = 0.7;
   times[2]      = 1.0;
};

datablock ParticleEmitterData(DebrisEnergySparkEmitter)
{
   ejectionPeriodMS = 5;
   periodVarianceMS = 0;
   ejectionVelocity = 32;
   velocityVariance = 8.0;
   ejectionOffset   = 0.0;
   thetaMin         = 0;
   thetaMax         = 180;
   phiReferenceVel  = 0;
   phiVariance      = 360;
   overrideAdvances = false;
   orientParticles  = true;
   lifetimeMS       = 250;
   particles = "DebrisEnergySparks";
};

datablock ExplosionData(DebrisSparkExplosion)
{
//   explosionShape = "energy_explosion.dts";
   soundProfile   = DebrisFXSparksSound;

   emitter[0] = DebrisEnergySparkEmitter;

   faceViewer           = false;
};

datablock StaticShapeData(DebrisExplosive) : StaticShapeDamageProfile
{
   shapeFile      = "turret_muzzlepoint.dts";
   explosion      = DebrisFXExplosion;

   maxDamage      = 1;
   destroyedLevel = 1;
   disabledLevel  = 1;

   isShielded = false;
   energyPerDamagePoint = 0;
   maxEnergy = 0;
   rechargeRate = 0.0;

   debrisShapeName = "debris_generic.dts";
   debris = HitDebrisFX;
};

datablock StaticShapeData(SparkDebrisExplosive) : StaticShapeDamageProfile
{
   shapeFile      = "turret_muzzlepoint.dts";
   explosion      = DebrisSparkExplosion;

   maxDamage      = 1;
   destroyedLevel = 1;
   disabledLevel  = 1;

   isShielded = false;
   energyPerDamagePoint = 0;
   maxEnergy = 0;
   rechargeRate = 0.0;

//   debrisShapeName = "debris_generic_small.dts";
//   debris = HitDebrisFX;
};

function createHitDebris(%pos)
{
   %debris = getRandom(1) ? "DebrisExplosive" : "SparkDebrisExplosive";

   %d = new StaticShape()
   {
      dataBlock = %debris;
   };

   MissionCleanup.add(%d);

   %d.setPosition(%pos);
   %d.schedule(32, "setDamageState", "Destroyed");
   %d.schedule(512, "delete");
}

datablock AudioProfile(EngineAlertSound)
{
   filename    = "gui/vote_nopass.WAV";
   description = AudioExplosion3d;
   preload = true;
};

datablock TurretImageData(SeekingParamImage) // ShapeBaseImageData
{
   mountPoint = 4;
   offset = "0 0.1 -0.3";
   rotation = "0 1 0 180";
   shapeFile = "turret_muzzlepoint.dts";

   isSeeker     = true;
   seekRadius   = 300;
   maxSeekAngle = 30;
   seekTime     = 0.85;
   minSeekHeat  = 0.8;
   useTargetAudio = false;
   minTargetingDistance = 15;

   // Turret parameters
   activationMS      = 1000;
   deactivateDelayMS = 1500;
   thinkTimeMS       = 200;
   degPerSecTheta    = 720;
   degPerSecPhi      = 720;
};

datablock TurretImageData(GenericParamImage)
{
   mountPoint = 2;
   shapeFile = "turret_muzzlepoint.dts";

   projectile = AssaultChaingunBullet;
   projectileType = TracerProjectile;

   useCapacitor = true;
   usesEnergy = true;

   // Turret parameters
   activationMS      = 750;
   deactivateDelayMS = 1500;
   thinkTimeMS       = 100;
   degPerSecTheta    = 600;
   degPerSecPhi      = 600;

   attackRadius      = 200;
};

datablock ShapeBaseImageData(VehicleMissileLocker)
{
   shapeFile = "turret_muzzlepoint.dts";
   mountPoint = 0;
   offset = "0 0 0";
   rotation = "0 1 0 0";

   isSeeker     = true;
   seekRadius   = 400;
   maxSeekAngle = 40;
   seekTime     = 0.85;
   minSeekHeat  = 0.8;
   useTargetAudio = false;
   minTargetingDistance = 10;
};

datablock ShapeBaseImageData(VehicleMissileInactive) : VehicleMissileLocker
{
   isSeeker = false;
};

function testVehicleForMount(%player, %obj)
{
   if(isObject(%obj) && %obj.getDamageState() !$= "Destroyed" && !%player.teleporting && !%player.client.inInv)
      %player.getDataBlock().onCollision(%player, %obj, 0);
}

//--------------------------------------------------------------
// NUMBER OF PURCHASEABLE VEHICLES PER TEAM
//--------------------------------------------------------------

$VehicleRespawnTime        = 15000;

// todo: switch to bitmask method
function findEmptySeat(%vehicle, %player, %forceNode)
{
   %minNode = 1;
   %node = -1;
   %dataBlock = %vehicle.getDataBlock();
   %dis = %dataBlock.minMountDist;
   %playerPos = getWords(%player.getTransform(), 0, 2);
   %message = "";
   
   if(%player.getDatablock().flags & $ArmorMask::Pilot)
       %minNode = 0;
   else
      %minNode = findFirstHeavyNode(%dataBlock);
      
   if(%forceNode !$= "")
      %node = %forceNode;
   else
   {
      for(%i = 0; %i < %dataBlock.numMountPoints; %i++)
         if(!%vehicle.getMountNodeObject(%i))
         {
            if(%dataBlock.noSitPoint[%i] == true)
                continue;
                
            %seatPos = getWords(%vehicle.getSlotTransform(%i), 0, 2);
            %disTemp = VectorLen(VectorSub(%seatPos, %playerPos));
            if(%disTemp <= %dis)
            {
               %node = %i;
               %dis = %disTemp;
            }
         }
    }
   if(%node != -1 && %node < %minNode)
   {
      if(%message $= "")
      {
         if(%node == 0)
            %message = '\c2Only Scout and Engineer Armors can pilot this vehicle.~wfx/misc/misc.error.wav';
         else
            %message = '\c2Only Scout and Engineer Armors can use that position.~wfx/misc/misc.error.wav';
      }

      if(!%player.noSitMessage)
      {
         %player.noSitMessage = true;
         %player.schedule(2000, "resetSitMessage");
         messageClient(%player.client, 'MsgArmorCantMountVehicle', %message);
      }
      %node = -1;
   }
   return %node;
}

function findAIEmptySeat(%vehicle, %player)
{
    %dataBlock = %vehicle.getDataBlock();
    %num = 1;
	%node = -1;
 
	for(%i = %num; %i < %dataBlock.numMountPoints; %i++)
	{
	   if (!%vehicle.getMountNodeObject(%i))
	   {
            if(%dataBlock.noSitPoint[%i] == true)
                continue;
	      //cheap hack - for now, AI's will mount the next available node regardless of where they collided
	      %node = %i;
	      break;
	   }
	}

	//return the empty seat
	return %node;
}

function findFirstHeavyNode(%data)
{
   for(%i = 0; %i < %data.numMountPoints; %i++)
   
      if(%data.mountPose[%i] $= "")
         return %i;
         
   return %data.numMountPoints;
}

/// Vehicle HUD

function vehicleAbandonTimeOut(%vehicle)
{
   if(%vehicle.getDatablock().cantAbandon $= "" && %vehicle.lastPilot $= "")
   {
      if(%vehicle.isWalker)
      {
        // don't fade if someone else captured it
        if(isObject(%vehicle.pilotObject))
            return;
            
        $VehicleTotalCount[%vehicle.originalteam, %vehicle.shortName]--;
        %vehicle.mountable = 0;
        %vehicle.startFade(1000, 0, true);
        %vehicle.schedule(1001, "delete");
        return;
      }

      for(%i = 0; %i < %vehicle.getDatablock().numMountPoints; %i++)
         if((%passenger = %vehicle.getMountNodeObject(%i)) && %passenger.isPlayer())
         {
//            %passenger = %vehicle.getMountNodeObject(%i);
            if(%passenger.lastVehicle !$= "")
               schedule(15000, %passenger.lastVehicle,"vehicleAbandonTimeOut", %passenger.lastVehicle);
            %passenger.lastVehicle = %vehicle;
            %vehicle.lastPilot = %passenger;
            return;
         }

      if(%vehicle.respawnTime !$= "")
         %vehicle.marker.schedule = %vehicle.marker.data.schedule(%vehicle.respawnTime, "respawn", %vehicle.marker);
      %vehicle.mountable = 0;
      %vehicle.startFade(1000, 0, true);
      %vehicle.schedule(1001, "delete");
      
      for(%i = 0; %i < 16; %i++)
      {
         if(isObject((%pt = %vehicle.getMountNodeObject(%i))))
         {
            %pt.startFade(1000, 0, true);
            %pt.schedule(1001, "delete");
         }
      }
   }
}

// Used on maps that alter the count of vehicles - default vehicles only


// Vehicle function overrides
// -----------------------------------------------------------------

function VehicleData::onAdd(%data, %obj)
{
   Parent::onAdd(%data, %obj);

   if(%obj.getTarget() != -1)
   {
       if(%data.sensorData !$= "")
          setTargetSensorData(%obj.getTarget(), %data.sensorData);
   }

   if(%obj.disableMove)
      %obj.immobilized = true;

   if(%obj.deployed)
   {
      if($countDownStarted)
      {
         // spawning on mission load
         %data.installParts(%obj, 0);
         %data.schedule(($Host::WarmupTime * 1000) / 2, "vehicleDeploy", %obj, 0, 1);
      }
      else
      {
         $VehiclesDeploy[$NumVehiclesDeploy] = %obj;
         $NumVehiclesDeploy++;
      }
   }

   if(%obj.mountable || %obj.mountable $= "")
      %data.isMountable(%obj, true);
   else
      %data.isMountable(%obj, false);

    %obj.setSelfPowered();
    %obj.setRechargeRate(%data.rechargeRate);
    %obj.setEnergyLevel(%data.maxEnergy);

    %data.onTick(%obj);
}

function VehicleData::onRemove(%this, %obj)
{
   // if there are passengers/driver, kick them out
   for(%i = 0; %i < %obj.getDatablock().numMountPoints; %i++)
   {
      %pl = %obj.getMountNodeObject(%i);

      if(isObject(%pl) && %pl.isPlayer())
         %pl.unmount();
   }

   if(isObject(%obj.frame))
      %obj.frame.delete();

    for(%i = 0; %i < 16; %i++)
    {
        %something = %obj.getMountNodeObject(%i);

        if(isObject(%something))
            %something.schedule(1000, "delete");
    }

   %this.deleteAllMounted(%obj);

   vehicleListRemove(%obj.getDataBlock(), %obj);

   if(%obj.lastPilot.lastVehicle == %obj)
      %obj.lastPilot.lastVehicle = "";

   Parent::onRemove(%this, %obj);
}

function VehicleData::onTrigger(%data, %obj, %trigger, %state)
{
    if(%trigger == 0)
    {
        // Nothing for non-override vehs
    }
    else if(%trigger == 3)
        %obj.bJetState = %state;
    else if(%trigger == 4 && %state)
        %data.triggerGrenade(%obj, %obj.getMountNodeObject(0));
    else if(%trigger == 5)
        %data.triggerMine(%obj, %obj.getMountNodeObject(0), 0, %state);
}

function createVehicle(%client, %station, %blockName, %team , %pos, %rot, %angle)
{
   if(%blockName $= "Walker")
   {
      %station.ready = false;

      if(%client.player.lastVehicle)
      {
         %client.player.lastVehicle.lastPilot = "";
         vehicleAbandonTimeOut(%client.player.lastVehicle);
         %client.player.lastVehicle = "";
      }

      spawnWalker(%client, %pos SPC %rot SPC %angle);

      %station.playThread($ActivateThread,"activate2");
      %station.playAudio($ActivateSound, ActivateVehiclePadSound);

      // play the FX
      %fx = new StationFXVehicle()
      {
         dataBlock = VehicleInvFX;
         stationObject = %station;
      };
      
      return;
   }
   
   %obj = %blockName.create(%team);
   if(%obj)
   {
      //-----------------------------------------------
      // z0dd - ZOD, 4/25/02. MPB Teleporter.
      if ( %blockName $= "MobileBaseVehicle" )
      {
         %station.station.teleporter.MPB = %obj;
         %obj.teleporter = %station.station.teleporter;
      }
      //-----------------------------------------------
      %station.ready = false;
      %obj.team = %team;
      %obj.useCreateHeight(true);
      %obj.schedule(5500, "useCreateHeight", false);
      %obj.getDataBlock().isMountable(%obj, false);
      %obj.getDataBlock().schedule(6500, "isMountable", %obj, true);

      %station.playThread($ActivateThread,"activate2");
      %station.playAudio($ActivateSound, ActivateVehiclePadSound);

      vehicleListAdd(%blockName, %obj);
      MissionCleanup.add(%obj);

      %turret = %obj.getMountNodeObject(10);
      if(%turret > 0)
      {
         %turret.setCloaked(true);
         %turret.schedule(4800, "setCloaked", false);
      }

      %obj.setCloaked(true);
      %obj.setTransform(%pos @ " " @ %rot @ " " @ %angle);

      %obj.schedule(3700, "playAudio", 0, VehicleAppearSound);
      %obj.schedule(4800, "setCloaked", false);

      if(%client.player.lastVehicle)
      {
         %client.player.lastVehicle.lastPilot = "";
         vehicleAbandonTimeOut(%client.player.lastVehicle);
         %client.player.lastVehicle = "";
      }
      %client.player.lastVehicle = %obj;
      %obj.lastPilot = %client.player;

      // play the FX
      %fx = new StationFXVehicle()
      {
         dataBlock = VehicleInvFX;
         stationObject = %station;
      };

      if ( %client.isVehicleTeleportEnabled() )
         %obj.getDataBlock().schedule(5000, "mountDriver", %obj, %client.player);
   }
   if(%obj.getTarget() != -1)
      setTargetSensorGroup(%obj.getTarget(), %client.getSensorGroup());
   // We are now closing the vehicle hud when you buy a vehicle, making the following call
   // unnecessary (and it breaks stuff, too!)
   //VehicleHud.updateHud(%client, 'vehicleHud');
}

function VehicleHud::updateHud( %obj, %client, %tag )
{
   %station = %client.player.station;
   %team = %client.getSensorGroup();
   %count = 0;
   %none = "-";

   %vehmsg = %station.vehicle[ScoutVehicle] ? "[1] Wildcat" : %none;
   %numleft = %station.vehicle[ScoutVehicle] ? ($VehicleMax[ScoutVehicle] - $VehicleTotalCount[%team, ScoutVehicle]) : 0;
   messageClient( %client, 'SetLineHud', "", %tag, %count, %vehmsg, "", ScoutVehicle, %numleft );
   %count++;

   %vehmsg = %station.vehicle[AssaultVehicle] ? "[2] Tank" : %none;
   %numleft = %station.vehicle[AssaultVehicle] ? ($VehicleMax[AssaultVehicle] - $VehicleTotalCount[%team, AssaultVehicle]) : 0;
   messageClient( %client, 'SetLineHud', "", %tag, %count, %vehmsg, "", AssaultVehicle, %numleft );
   %count++;

   %vehmsg = %station.vehicle[mobileBaseVehicle] ? "[3] Mobile Point Base" : %none;
   %numleft = %station.vehicle[MobileBaseVehicle] ? ($VehicleMax[MobileBaseVehicle] - $VehicleTotalCount[%team, MobileBaseVehicle]) : 0;
   messageClient( %client, 'SetLineHud', "", %tag, %count, %vehmsg, "", MobileBaseVehicle, %numleft );
   %count++;

   %vehmsg = %station.vehicle[scoutFlyer] ? "[4] Shrike" : %none;
   %numleft = %station.vehicle[ScoutFlyer] ? ($VehicleMax[ScoutFlyer] - $VehicleTotalCount[%team, ScoutFlyer]) : 0;
   messageClient( %client, 'SetLineHud', "", %tag, %count, %vehmsg, "", ScoutFlyer, %numleft );
   %count++;

   %vehmsg = %station.vehicle[bomberFlyer] ? "[5] Thundersword" : %none;
   %numleft = %station.vehicle[BomberFlyer] ? ($VehicleMax[BomberFlyer] - $VehicleTotalCount[%team, BomberFlyer]) : 0;
   messageClient( %client, 'SetLineHud', "", %tag, %count, %vehmsg, "", BomberFlyer, %numleft );
   %count++;

   %vehmsg = %station.vehicle[hapcFlyer] ? "[6] Havoc Transport" : %none;
   %numleft = %station.vehicle[HAPCFlyer] ? ($VehicleMax[HAPCFlyer] - $VehicleTotalCount[%team, HAPCFlyer]) : 0;
   messageClient( %client, 'SetLineHud', "", %tag, %count, %vehmsg, "", HAPCFlyer, %numleft );
   %count++;
   
   %vehmsg = %station.vehicle[Gunship] ? "[7] Gunship" : %none;
   %numleft = %station.vehicle[Gunship] ? ($VehicleMax[Gunship] - $VehicleTotalCount[%team, Gunship]) : 0;
   messageClient( %client, 'SetLineHud', "", %tag, %count, %vehmsg, "", Gunship, %numleft );
   %count++;
   
   %vehmsg = %station.vehicle[Walker] ? "[8] Walker" : %none;
   %numleft = %station.vehicle[Walker] ? ($VehicleMax[Walker] - $VehicleTotalCount[%team, Walker]) : 0;
   messageClient( %client, 'SetLineHud', "", %tag, %count, %vehmsg, "", Walker, %numleft );
   %count++;
}

function StationVehiclePad::createStationVehicle(%data, %obj)
{
   // z0dd - ZOD - Founder(founder@mechina.com), 4/20/02
   // This code used to be called from StationVehiclePad::onAdd
   // This was changed so we can add the station to the mission group
   // so it gets powered properly and auto cleaned up at mission end

   // Get the v-pads mission group so we can place the station in it.
   %group = %obj.getGroup();

   // Set the default transform based on the vehicle pads slot
   %xform = %obj.getSlotTransform(0);
   %position = getWords(%xform, 0, 2);
   %rotation = getWords(%xform, 3, 5);
   %angle = (getWord(%xform, 6) * 180) / 3.14159;

   // Place these parameter's in the v-pad datablock located in mis file.
   // If the mapper doesn't move the station, use the default location.
   if(%obj.stationPos $= "" || %obj.stationRot $= "")
   {
      %pos = %position;
      %rot = %rotation @ " " @ %angle;
   }
   else
   {
      %pos = %obj.stationPos;
      %rot = %obj.stationRot;
   }

   %sv = new StaticShape() {
	scale = "1 1 1";
      dataBlock = "StationVehicle";
	lockCount = "0";
	homingCount = "0";
	team = %obj.team;
      position = %pos;
      rotation = %rot;
   };

   // Add the station to the v-pads mission group for cleanup and power.
   %group.add(%sv);
   %sv.setPersistent(false); // set the station to not save.

   // Apparently called to early on mission load done, call it now.
   %sv.getDataBlock().gainPower(%sv);

   // Create the trigger
   %sv.getDataBlock().createTrigger(%sv);
   %sv.pad = %obj;
   %obj.station = %sv;
   %sv.trigger.mainObj = %obj;
   %sv.trigger.disableObj = %sv;

   // Set the sensor group.
   if(%sv.getTarget() != -1)
      setTargetSensorGroup(%sv.getTarget(), %obj.team);

   //Remove unwanted vehicles
   if(%obj.scoutVehicle !$= "Removed")
	   %sv.vehicle[scoutvehicle] = true;
   if(%obj.assaultVehicle !$= "Removed")
	   %sv.vehicle[assaultVehicle] = true;
   if(%obj.mobileBaseVehicle !$= "Removed")
   {
      %sv.vehicle[mobileBasevehicle] = true;
      // z0dd - ZOD, 4/20/02. Enable MPB Teleporter.
      %sv.getDataBlock().createTeleporter(%sv, %group);
   }
   if(%obj.scoutFlyer !$= "Removed")
	   %sv.vehicle[scoutFlyer] = true;
   if(%obj.bomberFlyer !$= "Removed")
	   %sv.vehicle[bomberFlyer] = true;
   if(%obj.hapcFlyer !$= "Removed")
   	%sv.vehicle[hapcFlyer] = true;
   if(%obj.Gunship !$= "Removed")
   	%sv.vehicle[Gunship] = true;
   if(%obj.Walker !$= "Removed")
   	%sv.vehicle[Walker] = true;
}

//--------------------------------------------------------------
// NUMBER OF PURCHASEABLE VEHICLES PER TEAM
//--------------------------------------------------------------

$VehicleRespawnTime        = 15000;
$Vehiclemax[ScoutVehicle]     = 4;
$VehicleMax[AssaultVehicle]   = 3;
$VehicleMax[MobileBaseVehicle]  = 1;
$VehicleMax[ScoutFlyer]       = 4;
$VehicleMax[BomberFlyer]      = 2;
$VehicleMax[HAPCFlyer]        = 2;
$VehicleMax[Gunship]          = 2;
$VehicleMax[Walker]           = 2;

function clearVehicleCount(%team)
{
   $VehicleTotalCount[%team, ScoutVehicle]      = 0;
   $VehicleTotalCount[%team, AssaultVehicle]    = 0;
   $VehicleTotalCount[%team, MobileBaseVehicle] = 0;
   $VehicleTotalCount[%team, ScoutFlyer]        = 0;
   $VehicleTotalCount[%team, BomberFlyer]       = 0;
   $VehicleTotalCount[%team, HAPCFlyer]         = 0;
   $VehicleTotalCount[%team, Gunship]           = 0;
   $VehicleTotalCount[%team, Walker]            = 0;
}

function vehicleListRemove(%data, %obj)
{
   %blockName = %data.getName();
   for($i = 0; %i < $VehicleMax[%blockName]; %i++)
      if($VehicleInField[%obj.team, %blockName, %i] == %obj)
      {
         $VehicleInField[%obj.team, %blockName, %i] = 0;
         $VehicleTotalCount[%obj.team, %blockName]--;
         break;
      }
}

function vehicleListAdd(%blockName, %obj)
{
   for($i = 0; %i < $VehicleMax[%blockName]; %i++)
   {
      if($VehicleInField[%obj.team, %blockName, %i] $= "" || $VehicleInField[%obj.team, %blockName, %i] == 0)
      {
         $VehicleInField[%obj.team, %blockName, %i] = %obj;
         $VehicleTotalCount[%obj.team, %blockName]++;
         break;
      }
   }
}

function VehicleData::mountDriver(%data, %obj, %player)
{
   if(isObject(%obj) && %obj.getDamageState() !$= "Destroyed")
   {
      %player.setCloaked(true);
      schedule(1000, 0, "testVehicleForMount", %player, %obj);
      %player.schedule(1500,"setCloaked", false);
   }
}

function VehicleData::onDestroyed(%data, %obj, %prevState)
{
    if(%obj.lastDamagedBy)
    {
        %destroyer = %obj.lastDamagedBy;
        game.vehicleDestroyed(%obj, %destroyer);
    }

    %tp = %obj.getMountNodeObject(0);

//   if(%obj.turretObject > 0)
//      if(%obj.turretObject.getControllingClient())
//         %obj.turretObject.getDataBlock().playerDismount(%obj.turretObject);

   // todo: combine above code with this code so that if they are controlling a turret, they get playerDismount() called too
   for(%i = 0; %i < %obj.getDatablock().numMountPoints; %i++)
   {
      if ((%flingee = %obj.getMountNodeObject(%i)) && %flingee.isPlayer())
      {
         %flingee.getDataBlock().doDismount(%flingee, true);
         %xVel = 250.0 - (getRandom() * 500.0);
         %yVel = 250.0 - (getRandom() * 500.0);
         %zVel = (getRandom() * 100.0) + 50.0;
         %flingVel = %xVel @ " " @ %yVel @ " " @ %zVel;
         %flingee.applyImpulse(%flingee.getTransform(), %flingVel);
//         echo("got player..." @ %flingee.getClassName());
         %flingee.damage(0, %obj.getPosition(), 1, $DamageType::Crash, 0, $DamageGroupMask::Kinetic);
      }
   }

	radiusVehicleExplosion(%data, %obj, %tp);

   %data.deleteAllMounted(%obj);

   // keen: anti-vehicle run over fix
   %obj.setFrozenState(true);
   %obj.getDatablock().schedule(256, "hideAndSeek", %obj);
}

function VehicleData::hideAndSeek(%data, %obj)
{
    %obj.setPosition(VectorScale(vectorRand(), -10000));
    %obj.schedule(2048, "delete");

    // delete the other objects that might be hanging around on the vehicle
    for(%i = 0; %i < 16; %i++)
    {
        %something = %obj.getMountNodeObject(%i);

        if(isObject(%something))
            %something.schedule(1000, "delete");
    }
}

function VehicleData::deleteAllMounted(%obj)
{

}

function TurretData::onDamage(%data, %obj)
{
    if(%obj.mountedVehicleTurret != true)
    {
        Parent::onDamage(%data, %obj);
        return;
    }

   %newDamageVal = %obj.getDamageLevel();

   if(%obj.lastDamageVal !$= "")
      if(isObject(%obj.getObjectMount()) && %obj.lastDamageVal > %newDamageVal)
         %obj.getObjectMount().setDamageLevel(%newDamageVal);

   %obj.lastDamageVal = %newDamageVal;
}

function TurretData::damageObject(%this, %targetObject, %sourceObject, %position, %amount, %damageType ,%vec, %client, %projectile)
{
    if(%targetObject.mountedVehicleTurret != true)
    {
        Parent::damageObject(%this, %targetObject, %sourceObject, %position, %amount, %damageType ,%vec, %client, %projectile);
        return;
    }

   //If vehicle turret is hit then apply damage to the vehicle
   %vehicle = %targetObject.getObjectMount();

   if(%vehicle)
      %vehicle.getDataBlock().damageObject(%vehicle, %sourceObject, %position, %amount, %damageType, %vec, %client, %projectile);
}

function TurretData::playerDismount(%data, %obj)
{
   //Passenger Exiting
   %obj.fireTrigger = 0;

   for(%s = 0; %s < 8; %s++)
       %obj.setImageTrigger(%s, false);

   %client = %obj.getControllingClient();
// %client.setControlObject(%client.player);
   %client.player.mountImage(%client.player.lastWeapon, $WeaponSlot);
   %client.player.mountVehicle = false;
//   setTargetSensorGroup(%obj.getTarget(), 0);
//   setTargetNeverVisMask(%obj.getTarget(), 0xffffffff);
//   %client.player.getDataBlock().doDismount(%client.player);
}

function TurretData::doDismount(%data, %obj)
{
    // Do nothing
}

function VehicleData::triggerGrenade(%data, %vehicle, %player)
{
    // Placeholder
}

function VehicleData::triggerMine(%data, %obj, %player, %slot, %state)
{
    if(%state)
        return;

    if(%slot == 0)
    {
        if(%data.restockVehicle(%obj, %player))
            return;
    }
    else if(!%player.isWeaponOperator() && %data.numMountPoints > 3) // only allowed on multi-person carrier type craft
    {
        %time = getSimTime();

        if(%time < %obj.slotInvTimeout[%slot])
        {
            %cdTimeSec = mCeil((%obj.slotInvTimeout[%slot] - %time) / 1000);

            messageClient(%player.client, 'MsgVMResupplyFail', '\c2Inventory station recently used, try again in %1 seconds.', %cdTimeSec);
            %player.playAudio(0, "StationAccessDeniedSound");
            return;
        }

        %obj.slotInvTimeout[%slot] = %time + 120000;
        %player.lastWeapon = ( %player.getMountedImage($WeaponSlot) == 0 ) ? "" : %player.getMountedImage($WeaponSlot).item;

        %player.unmountImage($WeaponSlot);
        %player.playAudio(0, "MobileBaseInventoryActivateSound");
        %player.setCloaked(true);
        %player.schedule(500, "setCloaked", false);

        buyFavorites(%player.client, 0);
        messageClient(%player.client, 'MsgVehInvActivate', '\c2Resupply function activated, 2 minute cooldown.');
    }
}

function VehicleData::restockVehicle(%data, %vehicle, %player)
{
    InitContainerRadiusSearch(%vehicle.getWorldBoxCenter(), 25, $TypeMasks::StaticShapeObjectType);

    while((%hit = containerSearchNext()) != 0)
    {
        if(%hit.team == %vehicle.team && %hit.getDatablock().shapeFile $= "vehicle_pad.dts")
        {
            %time = getSimTime();

            if(%time < %vehicle.nextRestockTime)
            {
                %cdTimeSec = mCeil((%vehicle.nextRestockTime - %time) / 1000);

                messageClient(%player.client, 'MsgVehNoReloadCD', '\c2Vehicle has recently restocked, come back in %1 seconds.', %cdTimeSec);
                return true;
            }

            if(vectorLen(%vehicle.getVelocity()) > 5)
            {
                messageClient(%player.client, 'MsgVehRTooFast', '\c2Vehicle is moving too fast to restock, please land before attempting again.');
                return true;
            }

            // Reload vehicle
            %vehicle.play3d(MissileReloadSound);
            messageClient(%player.client, 'MsgVehReload', '\c2Vehicle landed on pad; ammo restocked and hull repaired.');
            
            %data.doVehicleRestock(%vehicle);

            // Repair vehicle fully
            zapVehicle(%vehicle, FXHealGreen);
            %vehicle.setDamageLevel(0);
            %vehicle.setEnergyLevel(%vehicle.getMaxEnergy());
            %vehicle.nextRestockTime = %time + 120000;
        }
    }

    return;
}

function VehicleData::doVehicleRestock(%data, %vehicle)
{
    // Used for child overrides
}

function VehicleData::onTick(%data, %obj)
{
    if(!isObject(%obj))
        return false;
        
    if(%obj.updateFrames == true && (%obj.tickCount % 96 == 0)) // 3 seconds
        %data.processFrameUpdate(%obj);
    
    %pilot = %obj.getMountNodeObject(0);

    if(%obj.tickCount % 8 == 0)
    {
        if(isObject(%pilot))
            %data.updatePilotStatus(%obj, %pilot);
        
        if(%obj.thrusterCount > 0)
            %data.processThrusterUpdate(%obj);
    }

    %data.tickPhysics(%obj);

    %obj.tickCount++;
    %data.schedule($g_TickTime, "onTick", %obj);

    return true;
}

function VehicleData::processThrusterUpdate(%data, %obj)
{
    %speed = vectorLen(%obj.getVelocity());
    
    for(%i = 0; %i < %obj.thrusterCount; %i++)
    {
        %bUpdate = false;

        if(%obj.thrusterData[%i, "speed"] == 0)
            %obj.thrusterData[%i, "state"] = (%obj.bJetState == true);
        else //if(%speed >= %obj.thrusterData[%i, "speed"])
           %obj.thrusterData[%i, "state"] = (%speed >= %obj.thrusterData[%i, "speed"]);
        
        %src = %obj.thrusterData[%i, "bFrame"] ? %obj.frame : %obj;
        %src.setImageTrigger(%obj.thrusterData[%i, "slot"], %obj.thrusterData[%i, "state"]);
    }
}

function VehicleData::registerThruster(%data, %obj, %img, %slot, %speed, %bFrame) // speed 0 == jet thruster
{
    if(%obj.thrusterCount $= "")
        %obj.thrusterCount = 0;
        
    %obj.thrusterData[%obj.thrusterCount, "slot"] = %slot;
    %obj.thrusterData[%obj.thrusterCount, "speed"] = %speed;
    %obj.thrusterData[%obj.thrusterCount, "bFrame"] = %bFrame;
    %obj.thrusterData[%obj.thrusterCount, "state"] = false;

    %obj.thrusterCount++;
    
    %src = %bFrame ? %obj.frame : %obj;
    %src.mountImage(%img, %slot);
}

function VehicleData::processFrameUpdate(%data, %obj)
{
   if(isObject(%obj.frame))
   {
      %obj.mountObject(%obj.frame, %data.frameMountPoint);
      %obj.frame.setDamageLevel(%obj.getDamageLevel());
   }
}

function VehicleData::tickPhysics(%data, %obj)
{
    %pct = %obj.getDamagePct();
    %dangerLevel = %data.damageLevelTolerance[1] > 0 ? %data.damageLevelTolerance[1] : 0.8;

    if(%pct >= %dangerLevel)
    {
        if(!%obj.onFire)
        {
            %obj.onFire = true;
//            %obj.playAudio(0, VehicleOnFireSound);
        }
        
        if(%obj.tickCount % 12 == 0)
        {
            if(%obj.tickCount % 48 == 0)
                %obj.playAudio(3, EngineAlertSound);

            %vel = vectorLen(%obj.getVelocity());
            %force = %vel + (%pct * 500);
            %stVec = VectorRand();
            %nVec = vectorScale(%stVec, %force);
            %obj.applyImpulse(%obj.getTransform(), %nVec);

            %dmgFXPos = vectorProject(%obj.position, %stVec, getRandom() * %data.cameraMaxDist * 0.5);
            createHitDebris(%dmgFXPos);
        }
    }
    else if(%obj.onFire)
    {
        %obj.onFire = false;
//        %obj.stopAudio(0);
    }
    
    // Don't run physics calculations for bot pilots
    %bp = %obj.getMountNodeObject(0);
    
    if(%bp > 0 && %bp.client.isAIControlled())
        return;
    
    %data.tickForwardsOnly(%obj);
}

// Physics 1 - Amplified constant motion/anti backwards physics
// ------------------------------------------------------------
function VehicleData::findGround(%data, %obj)
{
   %dist = %data.checkMinHeight * -1;
   %mask = $TypeMasks::TerrainObjectType | $TypeMasks::InteriorObjectType | $TypeMasks::StaticObjectType | $TypeMasks::WaterObjectType;
   %pos = %obj.position;
   %end = VectorAdd(%pos, "0 0" SPC %dist);
   %result = containerRayCast(%pos, %end, %mask, %obj);
   %target = getWord(%result, 0);

   if(isObject(%target))
      return true;
   else
      return false;
}

function VehicleData::isMovingBackwards(%data, %obj)
{
    %forwardVec = vectorNormalize(%obj.getVelocity());
//    %objPos = %obj.position;
    %dif = %obj.getForwardVector();
    %dot = VectorDot(%dif, %forwardVec);

    if(%dot >= mCos(1.05))
        return false;
    else
        return true;
}

function VehicleData::tickForwardsOnly(%data, %obj)
{
    if(%data.checkMinVelocity > 0)
    {
        %velVec = %obj.getVelocity();
        %vel = vectorLen(%velVec);
        %fvel = vectorLen(getWords(%velVec, 0, 1) SPC "0");
        %nVec = "0 0 0";
        %doImpulse = false;

        if(%data.isMovingBackwards(%obj))
        {
            if(%vel > %data.checkMinReverseSpeed)
            {
                %force = %vel + (%data.mass * 3.5); // totalWeight
                %nVec = vectorScale(%obj.getForwardVector(), %force);
                %doImpulse = true;
            }
        }

        if(%doImpulse)
            %obj.applyImpulse(%obj.getTransform(), %nVec);
    }
}

function VehicleData::onPilotSeated(%data, %obj, %player)
{
   commandToClient(%player.client, 'SetWeaponryVehicleKeys', true);

   setTargetSensorGroup(%obj.getTarget(), %obj.team);

   // if there is a turret, set its team as well.
   for(%i = 0; %i < 16; %i++)
   {
      %turret = %obj.getMountNodeObject(%i);

      if(isObject(%turret) && %turret.isTurret())
      {
          setTargetSensorGroup(%turret.getTarget(), %obj.team);
          setTargetNeverVisMask(%turret.getTarget(), 0xffffffff);
      }
   }
   
   // update observers who are following this guy...
   if( %player.client.observeCount > 0 )
      resetObserveFollow( %player.client, false );
      
    if(%obj.selectedWeapon $= "")
        %obj.selectedWeapon = 1;
}

function VehicleData::updatePilotStatus(%data, %obj, %pilot)
{
//        commandToClient(%pilot.client, 'BottomPrint', "Hull: "@%hpGraph@" ("@mCeil(%hpPct * 100)@"%) | Shield: "@%shGraph@" ("@mCeil(%shieldPct * 100)@"%)\nSpeed: "@mFloor(vectorLen(%obj.getVelocity()) * 3.6)@" KPH | Missiles: "@%obj.missileCount@" | Weapon: "@%weapon NL "Module: "@%module, 1, 3);
}

function VehicleData::onGunnerSeated(%data, %obj, %player, %turretNode) // Only called to update turret status, turret is implied here - if not, there's bigger problems to worry about
{
    %turret = %obj.getMountNodeObject(%turretNode);
    
    if(%obj.applyTurretOnSit == true)
    {
        %pack = %player.getMountedImage($BackpackSlot);
        
        if(%pack.turretBarrel !$= "")
        {
            messageClient(%player.client, 'MsgTurretMount', "\c2Mounting barrel pack on turret."); // z0dd - ZOD, 4/20/02. modular MPB turret
            serverPlay3D(TurretPackActivateSound, %turret.getTransform());
            %turret.initiateBarrelSwap(%player);
//            %player.setInventory(%pack.item, 0);
        }
    }

    %player.vehicleTurret = %turret;
    %player.setTransform("0 0 0 0 0 1 0");
    %player.lastWeapon = %player.getMountedImage($WeaponSlot);
    %player.unmountImage($WeaponSlot);

    if(!%player.client.isAIControlled())
    {
        %player.setControlObject(%turret);
        %player.client.setObjectActiveImage(%turret, 0);
    }

    %turret.turreteer = %player;
    %turret.owner = %player.client;
    %turret.onNode = %turretNode;

    commandToClient(%player.client, 'SetWeaponryVehicleKeys', true);
    //commandToClient(%player.client, 'setHudMode', 'Pilot', "HAPC", %node);
    commandToClient(%player.client, 'setHudMode', 'Pilot', "HAPC", %node);
//    commandToClient(%player.client, 'setRepairReticle');

//    %turret = %turrObj > 0 ? %turrObj : %obj.getMountNodeObject(%turretNode);
//    %turrObj = %turrObj > 0 ? %turrObj : %turret;

//    serverCmdSetVehicleWeapon(%player.client, %turret.selectedWeapon);

//    %wep = %turret.weapon[%turret.selectedWeapon];
//    %ammocount = %obj.ammoCache[%wep.ammo] $= "" ? 0 : %obj.ammoCache[%wep.ammo];

//    if(isObject(%wep) && %wep.name !$= "")
//    {
//        if(%wep.ammo !$= "")
//            %turret.getDatablock().updateAmmoCount(%turret, %player.client, %wep.ammo);
//    }

//    %data.updateGunnerStatus(%obj, %player);
//    schedule(32, 0, "commandToClient", %player.client, 'ShowAmmoCounter'); // won't show unless it's scheduled... I don't even
}

function VehicleData::updateGunnerStatus(%data, %obj, %player)
{
    if(%player.getObjectMount() != %obj)
        return;
        
//    %wep = %player.vehicleTurret.weapon[%player.vehicleTurret.selectedWeapon];
//    %weapon = "None";

//    if(isObject(%wep) && %wep.name !$= "")
//        %weapon = %wep.name;

//    %hpPct = %obj.getDamageLeftPct();
//    %shieldPct = %obj.shieldSource.strength / %obj.shieldSource.maxStrength;
//    %ammocount = %obj.ammoCache[%wep.ammo] $= "" ? 0 : %obj.ammoCache[%wep.ammo];
    
//    commandToClient(%player.client, 'BottomPrint', "Hull: "@mCeil(%hpPct * 100)@"% | Shield: "@mCeil(%shieldPct * 100)@"% | Missiles: "@%obj.missileCount@"\nCurrent Weapon: "@%weapon, 1, 2);
    
//    %module = %obj.installedVehiclePart[$VehiclePartType::Module].name;
//    commandToClient(%player.client, 'GraphGunner', %hpPct, %shieldPct, %obj.missileCount, %weapon, %module);
    
    %data.schedule(256, "updateGunnerStatus", %obj, %player);
}

function VehicleData::playerDismounted(%data, %obj, %player)
{
   for(%i = 0; %i < 8; %i++)
      %obj.setImageTrigger(%i, false);

   if( %player.client.observeCount > 0 )
      resetObserveFollow( %player.client, true );
}

function VehicleData::triggerPack(%data, %obj, %player)
{
    messageClient(%player.client, 'MsgCantUsePack', '\c2You can\'t use your pack while piloting.~wfx/misc/misc.error.wav');
}

function serverCmdSetVehicleWeapon(%client, %num)
{
   %turret = %client.player.getControlObject();
   if(%turret.getDataBlock().numWeapons < %num)
      return;
   %turret.selectedWeapon = %num;

   //%hudNum = %turret.getDataBlock().getHudNum(%num);
   //%client.setVWeaponsHudActive(%hudNum);
   %client.setVWeaponsHudActive(%num);
   
   // set the active image on the client's obj
   if(%num == 1)
      %client.setObjectActiveImage(%turret, 2);
   else if(%num == 2)
      %client.setObjectActiveImage(%turret, 4);
   else
      %client.setObjectActiveImage(%turret, 6);

   commandToClient(%client, 'setRepairReticle');
   
   // if firing then set the proper image trigger
   if(%turret.fireTrigger)
   {
      if(%num == 1)
      {
         %turret.setImageTrigger(4, false);
         if(%turret.getImageTrigger(6))
         {
            %turret.setImageTrigger(6, false);
            ShapeBaseImageData::deconstruct(%turret.getMountedImage(6), %turret);
         }
         %turret.setImageTrigger(2, true);
      }
      else if( %num == 2)
      {
         %turret.setImageTrigger(2, false);
         if(%turret.getImageTrigger(6))
         {
            %turret.setImageTrigger(6, false);
            ShapeBaseImageData::deconstruct(%turret.getMountedImage(6), %turret);
         }
         %turret.setImageTrigger(4, true);
      }
      else
      {
         %turret.setImageTrigger(2, false);
         %turret.setImageTrigger(4, false);
      }
   }
}

function VehicleData::installTurret(%data, %obj, %turretData, %slot)
{
   %turret = TurretData::create(%turretData);
   %turret.selectedWeapon = 0;
   %turret.team = %obj.teamBought;
   %turret.mountedVehicleTurret = true;
   %turret.setSelfPowered();
   %turret.setCapacitorRechargeRate(1); //  %turret.getDataBlock().capacitorRechargeRate // turrets draw from main power
   %turret.setAutoFire(false);
   %turret.setCloaked(true);
   %turret.schedule(5500, "setCloaked", false);
   %turret.vehicle = %obj;
   MissionCleanup.add(%turret);

   setTargetSensorGroup(%turret.getTarget(), %turret.team);
   setTargetNeverVisMask(%turret.getTarget(), 0xffffffff);
   %obj.mountObject(%turret, %slot);

   if(%obj.turretCount $= "")
      %obj.turretCount = 0;
      
   %obj.turrets[%obj.turretCount] = %turret;
   %turret.vTurretID = %obj.turretCount;
   %obj.turretCount++;
   
   // necessary because stupidity with T2
   %turret.getDatablock().abandonCheck(%turret);
   
   return %turret;
}

function TurretData::abandonCheck(%data, %obj)
{
    if(isObject(%obj.vehicle))
        %data.schedule(30000, "abandonCheck", %obj);
    else
        %obj.delete();
}

// Base overrides
function ScoutFlyer::onAdd(%this, %obj)
{
   Parent::onAdd(%this, %obj);
   
   %obj.mountImage(ShrikeDefaultParam, 0);
   %obj.mountImage(ScoutChaingunImage, 2);
   %obj.mountImage(ScoutChaingunPairImage, 3);
   %obj.mountImage(MissileLauncherLImage, 4);
   %obj.mountImage(MissileLauncherRImage, 5);
   %obj.selectedWeapon = 1;
   %obj.nextWeaponFire = 2;

   %this.doVehicleRestock(%obj);
   %obj.schedule(5500, "playThread", $ActivateThread, "activate");
}

function ScoutFlyer::doVehicleRestock(%data, %vehicle)
{
   %vehicle.setInventory("MissileLauncherAmmo", 20);
}

function ScoutFlyer::triggerPack(%data, %obj, %player)
{
    %obj.missileTargeting = !%obj.missileTargeting;
    
    if(%obj.missileTargeting)
        %obj.mountImage("ShrikeMissileParam", 0);
    else
        %obj.mountImage("ShrikeDefaultParam", 0);
}

function ScoutFlyer::updatePilotStatus(%data, %obj, %pilot)
{
    %wep = %obj.selectedWeapon == 1 ? "Shrike Blasters" : "Missile Launchers";
    %ammo = %obj.selectedWeapon == 1 ? "inf" : %obj.inv["MissileLauncherAmmo"];
    %target = %obj.missileTargeting ? "Online" : "Offline";
    
    commandToClient(%pilot.client, 'BottomPrint', "Weapon:" SPC %wep SPC " | Ammo:" SPC %ammo SPC " | Missile Targeter (pack):" SPC %target NL "Restock at a vehicle pad by landing and pressing (mine)", 1, 2);
}

function ScoutFlyer::onTrigger(%data, %obj, %trigger, %state)
{
     if(%trigger == 0)
     {
        if(%obj.lastFireTime + %data.fireTimeout > getSimTime()) // double fire cheat
             return;

          if(%obj.selectedWeapon == 1)
          {
               %obj.setImageTrigger(4, false);
               %obj.setImageTrigger(5, false);

               switch(%state)
               {
                   case 0:
                   %obj.fireWeapon = false;
                   %obj.setImageTrigger(2, false);
                   %obj.setImageTrigger(3, false);
                   case 1:
                   %obj.fireWeapon = true;
                   if(%obj.nextWeaponFire == 2)
                   {
                         %obj.setImageTrigger(2, true);
                         %obj.setImageTrigger(3, true);
                   }
                   else
                   {
                         %obj.setImageTrigger(2, false);
                         %obj.setImageTrigger(3, true);
                   }
               }
          }
          else
          {
               %obj.setImageTrigger(2, false);
               %obj.setImageTrigger(3, false);

               switch(%state)
               {
                   case 0:
                   %obj.fireWeapon = false;
                   %obj.setImageTrigger(4, false);
                   %obj.setImageTrigger(5, false);
                   case 1:
                   %obj.fireWeapon = true;
                   if(%obj.nextWeaponFire == 4)
                   {
                         %obj.setImageTrigger(4, true);
                         %obj.setImageTrigger(5, false);
                   }
                   else
                   {
                         %obj.setImageTrigger(4, false);
                         %obj.setImageTrigger(5, true);
                   }
               }
          }
     }
    else if(%trigger == 5)
        %data.triggerMine(%obj, %obj.getMountNodeObject(0), 0, %state);
}

function ScoutFlyer::playerMounted(%data, %obj, %player, %node)
{
   // scout flyer == SUV (single-user vehicle)
   commandToClient(%player.client, 'setHudMode', 'Pilot', "Shrike", %node);
   commandToClient(%player.client, 'SetWeaponryVehicleKeys', true);
   commandToClient(%player.client, 'setRepairReticle');
   
//   %data.onPilotSeated(%obj, %player);
}

function ScoutFlyer::playerDismounted(%data, %obj, %player)
{
    Parent::playerDismounted(%data, %obj, %player);
}

function ScoutVehicle::onAdd(%this, %obj)
{
    Parent::onAdd(%this, %obj);
   
   %obj.schedule(5500, "playThread", $ActivateThread, "activate");
}

function ScoutVehicle::onTrigger(%data, %obj, %trigger, %state)
{
    Parent::onTrigger(%data, %obj, %trigger, %state);
}

function ScoutVehicle::playerMounted(%data, %obj, %player, %node)
{
   // scout vehicle == SUV (single-user vehicle)
   commandToClient(%player.client, 'setHudMode', 'Pilot', "Hoverbike", %node);

   %data.onPilotSeated(%obj, %player);
}

function ScoutVehicle::playerDismounted(%data, %obj, %player)
{
    Parent::playerDismounted(%data, %obj, %player);
}

function BomberFlyer::onAdd(%this, %obj)
{
   Parent::onAdd(%this, %obj);

   %turret = TurretData::create(BomberTurret);
   MissionCleanup.add(%turret);
   %turret.team = %obj.teamBought;
   %turret.selectedWeapon = 1;
   %turret.setSelfPowered();
   %obj.mountObject(%turret, 10);
   %turret.mountImage(BomberTurretBarrel,2);
   %turret.mountImage(BomberTurretBarrelPair,3);
   %turret.mountImage(BomberBombImage, 4);
   %turret.mountImage(BomberBombPairImage, 5);
   %turret.mountImage(BomberTargetingImage, 6);
   %obj.turretObject = %turret;
   %turret.setCapacitorRechargeRate( %turret.getDataBlock().capacitorRechargeRate );
   %turret.vehicleMounted = %obj;

   //vehicle turrets should not auto fire at targets
   %turret.setAutoFire(false);

   //for this particular weapon - a non-firing datablock used only so the AI can aim the turret
   //Also needed so we can set the turret parameters..
   %turret.mountImage(AIAimingTurretBarrel, 0);

   %this.doVehicleRestock(%obj);
   
   // setup the turret's target info
   setTargetSensorGroup(%turret.getTarget(), %turret.team);
   setTargetNeverVisMask(%turret.getTarget(), 0xffffffff);
}

function BomberFlyer::playerMounted(%data, %obj, %player, %node)
{
   if(%node == 0)
   {
      // pilot position
      %player.setPilot(true);
	   commandToClient(%player.client, 'setHudMode', 'Pilot', "Bomber", %node);
   }
   else if(%node == 1)
   {
      // bombardier position
      %turret = %obj.getMountNodeObject(10);
      %player.vehicleTurret = %turret;
      %player.setTransform("0 0 0 0 0 1 0");
      %player.lastWeapon = %player.getMountedImage($WeaponSlot);
      %player.unmountImage($WeaponSlot);
      if(!%player.client.isAIControlled())
      {
         %player.setControlObject(%turret);
         %player.client.setObjectActiveImage(%turret, 2);
      }
      commandToClient(%player.client, 'startBomberSight');
      %turret.bomber = %player;
      $bWeaponActive = 0;
      %obj.getMountNodeObject(10).selectedWeapon = 1;
      commandToClient(%player.client,'SetWeaponryVehicleKeys', true);

	   commandToClient(%player.client, 'setHudMode', 'Pilot', "Bomber", %node);
      %player.isBomber = true;
   }
   else
   {
      // tail gunner position
	   commandToClient(%player.client, 'setHudMode', 'Passenger', "Bomber", %node);
   }
   // build a space-separated string representing passengers
   // 0 = no passenger; 1 = passenger (e.g. "1 0 0 ")
   %passString = buildPassengerString(%obj);
	// send the string of passengers to all mounted players
	for(%i = 0; %i < %data.numMountPoints; %i++)
		if(%obj.getMountNodeObject(%i) > 0)
		   commandToClient(%obj.getMountNodeObject(%i).client, 'checkPassengers', %passString);

   // update observers who are following this guy...
   if( %player.client.observeCount > 0 )
      resetObserveFollow( %player.client, false );
}

function BomberFlyer::doVehicleRestock(%data, %vehicle)
{
    %turret = %vehicle.getMountNodeObject(10);
    %turret.setInventory("BombAmmo", 50);
    bottomprint(%turret.getControllingClient(), %turret.getInventory("BombAmmo") SPC "Bombs remaining", 1, 1);
}

//----------------------------
// BEOWULF ASSAULT VEHICLE
//----------------------------

function AssaultVehicle::onAdd(%this, %obj)
{
   Parent::onAdd(%this, %obj);

   %turret = TurretData::create(AssaultPlasmaTurret);
   %turret.selectedWeapon = 1;
   MissionCleanup.add(%turret);
   %turret.team = %obj.teamBought;
   %turret.setSelfPowered();
   %obj.mountObject(%turret, 10);
   %turret.mountImage(AssaultPlasmaTurretBarrel, 2);
   %turret.mountImage(AssaultMortarTurretBarrel, 4);
   %turret.setCapacitorRechargeRate( %turret.getDataBlock().capacitorRechargeRate );
   %obj.turretObject = %turret;

   //vehicle turrets should not auto fire at targets
   %turret.setAutoFire(false);

   //Needed so we can set the turret parameters..
   %turret.mountImage(AssaultTurretParam, 0);
   %obj.schedule(6000, "playThread", $ActivateThread, "activate");

   // set the turret's target info
   setTargetSensorGroup(%turret.getTarget(), %turret.team);
   setTargetNeverVisMask(%turret.getTarget(), 0xffffffff);
}

// Vehicle overrides over
//--------------------------------------------------------------------------------

function ScoutFlyer::playerDismounted(%data, %obj, %player)
{
   %obj.fireWeapon = false;
   %obj.setImageTrigger(2, false);
   %obj.setImageTrigger(3, false);
   %obj.setImageTrigger(4, false);
   %obj.setImageTrigger(5, false);
   
   setTargetSensorGroup(%obj.getTarget(), %obj.team);

   if( %player.client.observeCount > 0 )
      resetObserveFollow( %player.client, true );
}

function ScoutChaingunImage::onFire(%data,%obj,%slot)
{
   // obj = ScoutFlyer object number
   // slot = 2

   Parent::onFire(%data,%obj,%slot);
   %obj.nextWeaponFire = 3;
   schedule(%data.fireTimeout, 0, "fireNextGun", %obj);
}

function ScoutChaingunPairImage::onFire(%data,%obj,%slot)
{
   // obj = ScoutFlyer object number
   // slot = 3

   Parent::onFire(%data,%obj,%slot);
   %obj.nextWeaponFire = 2;
   schedule(%data.fireTimeout, 0, "fireNextGun", %obj);
}

function fireNextGun(%obj)
{
   if(%obj.fireWeapon)
   {
      if(%obj.nextWeaponFire == 2)
      {
         %obj.setImageTrigger(2, true);
         %obj.setImageTrigger(3, false);
      }
      else
      {
         %obj.setImageTrigger(2, false);
         %obj.setImageTrigger(3, true);
      }
   }
   else
   {
      %obj.setImageTrigger(2, false);
      %obj.setImageTrigger(3, false);
   }
}

function fireNextSecondary(%obj)
{
   if(%obj.fireWeapon)
   {
      if(%obj.nextWeaponFire == 4)
      {
         %obj.setImageTrigger(4, true);
         %obj.setImageTrigger(5, false);
      }
      else
      {
         %obj.setImageTrigger(4, false);
         %obj.setImageTrigger(5, true);
      }
   }
   else
   {
      %obj.setImageTrigger(4, false);
      %obj.setImageTrigger(5, false);
   }
}

function ScoutChaingunImage::onTriggerDown(%this, %obj, %slot)
{
}

function ScoutChaingunImage::onTriggerUp(%this, %obj, %slot)
{
}

function ScoutChaingunImage::onMount(%this, %obj, %slot)
{
//   %obj.setImageAmmo(%slot,true);
}

function ScoutChaingunPairImage::onMount(%this, %obj, %slot)
{
//   %obj.setImageAmmo(%slot,true);
}

function ScoutChaingunImage::onUnmount(%this,%obj,%slot)
{
}

function ScoutChaingunPairImage::onUnmount(%this,%obj,%slot)
{
}


function BomberTurret::onDamage(%data, %obj)
{
   %newDamageVal = %obj.getDamageLevel();
   if(%obj.lastDamageVal !$= "")
      if(isObject(%obj.getObjectMount()) && %obj.lastDamageVal > %newDamageVal)
         %obj.getObjectMount().setDamageLevel(%newDamageVal);
   %obj.lastDamageVal = %newDamageVal;
}

function BomberTurret::damageObject(%this, %targetObject, %sourceObject, %position, %amount, %damageType ,%vec, %client, %projectile)
{
   //If vehicle turret is hit then apply damage to the vehicle
   %vehicle = %targetObject.getObjectMount();
   if(%vehicle)
      %vehicle.getDataBlock().damageObject(%vehicle, %sourceObject, %position, %amount, %damageType, %vec, %client, %projectile);
}

function VehicleTurret::onEndSequence(%data, %obj, %thread)
{
   if($DeployThread == %thread)
      %obj.stopThread($DeployThread);
}

function BomberTurret::onTrigger(%data, %obj, %trigger, %state)
{
   //error("onTrigger: trigger = " @ %trigger @ ", state = " @ %state);
   //error("obj = " @ %obj @ ", class " @ %obj.getClassName());
   switch (%trigger)
   {
      case 0:
         %obj.fireTrigger = %state;
         if(%obj.selectedWeapon == 1)
         {
            %obj.setImageTrigger(4, false);
            if(%obj.getImageTrigger(6))
            {
               %obj.setImageTrigger(6, false);
               ShapeBaseImageData::deconstruct(%obj.getMountedImage(6), %obj);
            }
            if(%state)
               %obj.setImageTrigger(2, true);
            else
               %obj.setImageTrigger(2, false);
         }
         else if(%obj.selectedWeapon == 2)
         {
            %obj.setImageTrigger(2, false);
            if(%obj.getImageTrigger(6))
            {
               %obj.setImageTrigger(6, false);
               ShapeBaseImageData::deconstruct(%obj.getMountedImage(6), %obj);
            }
            if(%state)
               %obj.setImageTrigger(4, true);
            else
               %obj.setImageTrigger(4, false);
         }
         else
         {
            %obj.setImageTrigger(2, false);
            %obj.setImageTrigger(4, false);
            if(%state)
               %obj.setImageTrigger(6, true);
            else
            {
               %obj.setImageTrigger(6, false);
               BomberTargetingImage::deconstruct(%obj.getMountedImage(6), %obj);
            }
         }

      case 2:
         if(%state)
         {
            %obj.getDataBlock().playerDismount(%obj);
         }
   }
}

function BomberTurret::playerDismount(%data, %obj)
{
   //Passenger Exiting
   %obj.fireTrigger = 0;
   %obj.setImageTrigger(2, false);
   %obj.setImageTrigger(4, false);
   if(%obj.getImageTrigger(6))
   {
      %obj.setImageTrigger(6, false);
      ShapeBaseImageData::deconstruct(%obj.getMountedImage(6), %obj);
   }
   %client = %obj.getControllingClient();
   %client.player.isBomber = false;
   commandToClient(%client, 'endBomberSight');
//   %client.player.setControlObject(%client.player);
   %client.player.mountVehicle = false;
//   %client.player.getDataBlock().doDismount(%client.player);

   //turret auto fire if ai mounted, if ai, %client = -1 - Lagg...
   if (%client > 0)
   {
      if(%client.player.getState() !$= "Dead")
         %client.player.mountImage(%client.player.lastWeapon, $WeaponSlot);
   }
   setTargetSensorGroup(%obj.getTarget(), 0);
   setTargetNeverVisMask(%obj.getTarget(), 0xffffffff);
}

//function BomberTurret::getHudNum(%data, %num)
//{
//   if(%num == 1)
//      return 0;
//   else
//      return 4;
//}

function AIAimingTurretBarrel::onFire(%this,%obj,%slot)
{
}

function BomberBombImage::onUnmount(%this,%obj,%slot)
{
}

function BomberBombPairImage::onUnmount(%this,%obj,%slot)
{
}

function BomberTurretBarrel::firePair(%this, %obj, %slot)
{
   %obj.setImageTrigger( 3, true);
}

function BomberTurretBarrelPair::stopFire(%this, %obj, %slot)
{
   %obj.setImageTrigger( 3, false);
}

function BomberTurretBarrelPair::onMount(%this, %obj, %slot)
{
//   %obj.setImageAmmo(%slot,true);
}

function BomberTurretBarrel::onMount(%this, %obj, %slot)
{
//   %obj.setImageAmmo(%slot,true);
}

function BomberBombImage::firePair(%this, %obj, %slot)
{
   %obj.setImageTrigger( 5, true);
}

function BomberBombPairImage::stopFire(%this, %obj, %slot)
{
   %obj.setImageTrigger( 5, false);
}

function BomberBombPairImage::onMount(%this, %obj, %slot)
{
//   %obj.setImageAmmo(%slot,true);
}

function BomberBombImage::onMount(%this, %obj, %slot)
{
}

function BomberBombImage::onUnmount(%this,%obj,%slot)
{
}

function BomberBombPairImage::onUnmount(%this,%obj,%slot)
{
}

function MobileTurretBase::onAdd(%this, %obj)
{
   Parent::onAdd(%this, %obj);
   setTargetSensorGroup(%obj.target, %obj.team);
//   setTargetNeverVisMask(%obj.target, 0xffffffff); // z0dd - ZOD, 4/17/02. Causes mpb sensor to be shown in middle of map on cmd screen instead of MPB as origin. no idea why
}

function MobileTurretBase::onDamage(%data, %obj)
{
   %newDamageVal = %obj.getDamageLevel();
   if(%obj.lastDamageVal !$= "")
      if(isObject(%obj.getObjectMount()) && %obj.lastDamageVal > %newDamageVal)
         %obj.getObjectMount().setDamageLevel(%newDamageVal);
   %obj.lastDamageVal = %newDamageVal;
}

function MobileTurretBase::damageObject(%this, %targetObject, %sourceObject, %position, %amount, %damageType ,%vec, %client, %projectile)
{
   //If vehicle turret is hit then apply damage to the vehicle
   %vehicle = %targetObject.getObjectMount();
   if(%vehicle)
      %vehicle.getDataBlock().damageObject(%vehicle, %sourceObject, %position, %amount, %damageType, %vec, %client, %projectile);
}

function MobileTurretBase::onEndSequence(%data, %obj, %thread)
{
   //Used so that the parent wont be called..
}

function AssaultPlasmaTurret::onDamage(%data, %obj)
{
   %newDamageVal = %obj.getDamageLevel();
   if(%obj.lastDamageVal !$= "")
      if(isObject(%obj.getObjectMount()) && %obj.lastDamageVal > %newDamageVal)
         %obj.getObjectMount().setDamageLevel(%newDamageVal);
   %obj.lastDamageVal = %newDamageVal;
}

function AssaultPlasmaTurret::damageObject(%this, %targetObject, %sourceObject, %position, %amount, %damageType ,%vec, %client, %projectile)
{
   //If vehicle turret is hit then apply damage to the vehicle
   %vehicle = %targetObject.getObjectMount();
   if(%vehicle)
      %vehicle.getDataBlock().damageObject(%vehicle, %sourceObject, %position, %amount, %damageType, %vec, %client, %projectile);
}

function AssaultPlasmaTurret::onTrigger(%data, %obj, %trigger, %state)
{
   switch (%trigger) {
      case 0:
         %obj.fireTrigger = %state;
         if(%obj.selectedWeapon == 1)
         {
            %obj.setImageTrigger(4, false);
            if(%state)
               %obj.setImageTrigger(2, true);
            else
               %obj.setImageTrigger(2, false);
         }
         else
         {
            %obj.setImageTrigger(2, false);
            if(%state)
               %obj.setImageTrigger(4, true);
            else
               %obj.setImageTrigger(4, false);
         }
      case 2:
         if(%state)
         {
            %obj.getDataBlock().playerDismount(%obj);
         }
   }
}

function AssaultPlasmaTurret::playerDismount(%data, %obj)
{
   //Passenger Exiting
   %obj.fireTrigger = 0;
   %obj.setImageTrigger(2, false);
   %obj.setImageTrigger(4, false);
   %client = %obj.getControllingClient();
// %client.setControlObject(%client.player);
   //turret auto fire if ai mounted, if ai, %client = -1 - Lagg...
   if (%client > 0)
   {
      %client.player.mountImage(%client.player.lastWeapon, $WeaponSlot);
   }
   %client.player.mountVehicle = false;
   setTargetSensorGroup(%obj.getTarget(), 0);
   setTargetNeverVisMask(%obj.getTarget(), 0xffffffff);
//   %client.player.getDataBlock().doDismount(%client.player);
}

function HAPCFlyer::onAdd(%this, %obj)
{
   Parent::onAdd(%this, %obj);
   %obj.schedule(6000, "playThread", $ActivateThread, "activate");
}

function VehicleData::damageObject(%data, %targetObject, %sourceObject, %position, %amount, %damageType, %momVec, %theClient, %proj)
{
   if(%proj !$= "")
   {
      if(%amount > 0 && %targetObject.lastDamageProj !$= %proj)
      {
         %targetObject.lastDamageProj = %proj;
         %targetObject.lastDamageAmount = %amount;
      }
      else if(%targetObject.lastDamageAmount < %amount)
         %amount = %amount - %targetObject.lastDamageAmount;
      else
         return;
   }

   // check for team damage
   %sourceClient = %sourceObject ? %sourceObject.getOwnerClient() : 0;
   %targetTeam = getTargetSensorGroup(%targetObject.getTarget());

   if(%sourceClient)
      %sourceTeam = %sourceClient.getSensorGroup();
   else if(isObject(%sourceObject) && %sourceObject.getClassName() $= "Turret")
   {
      %sourceTeam = getTargetSensorGroup(%sourceObject.getTarget());
      %sourceClient = %sourceObject.getControllingClient(); // z0dd - ZOD, 6/10/02. Play a sound to client when they hit a vehicle with a controlled turret
   }
   else
   {
      %sourceTeam = %sourceObject ? getTargetSensorGroup(%sourceObject.getTarget()) : -1;
      // Client is allready defined and this spams console - ZOD
      //%sourceClient = %sourceObject.getControllingClient(); // z0dd - ZOD, 6/10/02. Play a sound to client when they hit a vehicle from a vehicle
   }

    // vehicles no longer obey team damage -JR
//    if(!$teamDamage && (%targetTeam == %sourceTeam) && %targetObject.getDamagePercent() > 0.5)
//       return;
    //but we do want to track the destroyer
    if(%sourceObject)
    {
        %targetObject.lastDamagedBy = %sourceObject;
        %targetObject.lastDamageType = %damageType;
    }
    else
        %targetObject.lastDamagedBy = 0;
        
   if(%sourceClient && %sourceClient.vehicleHitSound && (%targetObject.lastHitFlags & $Projectile::PlaysHitSound))
   {
      %sameteam = (%sourceClient.team == %targetObject.team);

//      if(%sameteam)
//         messageClient(%sourceClient, 'MsgClientHit', "~wfx/misc/whistle.wav");
//      else
         messageClient(%sourceClient, 'MsgClientHit', %sourceClient.vehicleHitWav);

      %sourceClient.playHitIndicator(%sameteam);
   }

   // Scale damage type & include shield calculations...
   if (%data.isShielded && (%targetObject.lastHitFlags & $Projectile::IgnoreShields) == 0)
      %amount = %data.checkShields(%targetObject, %position, %amount, %damageType);

   %damageScale = %data.damageScale[%damageType];
   if(%damageScale !$= "")
      %amount *= %damageScale;
      
   %amount *= %targetObject.damageReduceFactor;

   if(%amount != 0)
      %targetObject.applyDamage(%amount);

   if(%targetObject.getDamageState() $= "Destroyed" )
   {
      if( %momVec !$= "")
         %targetObject.setMomentumVector(%momVec);
   }
   
    // reset so we don't get other damage bleedover
    %targetObject.lastHitFlags = 0;
}
