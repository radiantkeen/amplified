//-----------------------------------------------------------------------------
// *Base Class Extensions
//--------------------------------------

//==============================================================================
// SimObject
function SimObject::getPosition(%obj)
{
     return getWords(%obj.getTransform(), 0, 2);
}

function SimObject::getRotation(%obj)
{
     return getWords(%obj.getTransform(), 3, 6);
}

function SimObject::getSlotRotation(%obj, %slot)
{
     return getWords(%obj.getSlotTransform(%slot), 3, 6);
}

function SimObject::getSlotPosition(%obj, %slot)
{
     return getWords(%obj.getSlotTransform(%slot), 0, 2);
}

function SimObject::setPosition(%obj, %pos)
{
    %obj.setTransform(%pos SPC getWords(%obj.getTransform(), 3, 6));
}

function SimObject::setRotation(%obj, %rot)
{
    %obj.setTransform(getWords(%obj.getTransform(), 0, 2) SPC %rot);
}

function SimObject::isVehicle(%obj)
{
    if(%obj.isWalker == true)
        return true;
    else
        return %obj.getType() & $TypeMasks::VehicleObjectType;
}

function SimObject::isPlayer(%obj)
{
     return %obj.getType() & $TypeMasks::PlayerObjectType;
}

function SimObject::isTurret(%obj)
{
     return %obj.getType() & $TypeMasks::TurretObjectType;
}

function SimObject::isWet(%obj)
{
    InitContainerRadiusSearch(%obj.getPosition(), 0.1, $TypeMasks::WaterObjectType);

    return ContainerSearchNext() ? true : false;
}

function SimObject::play3D(%obj, %sound)
{
     serverPlay3D(%sound, %obj.getTransform());
}

//==============================================================================
// ShapeBase
function ShapeBase::applyKick(%obj, %force)
{
    %obj.applyImpulse(%obj.getTransform(), VectorScale(%obj.getMuzzleVector(0), %force));
}

function ShapeBase::useEnergy(%obj, %amount)
{
    %obj.setEnergyLevel(%obj.getEnergyLevel() - (%amount * %obj.energyEfficiencyFactor));
}

function ShapeBase::takeDamage(%obj, %amount)
{
    %obj.setDamageLevel(%obj.getDamageLevel() + %amount);
}

function ShapeBase::getEyePos(%obj)
{
     return getWords(%obj.getEyeTransform(), 0, 2);
}

function ShapeBase::getMass(%obj)
{
     return %obj.getDatablock().mass;
}

function ShapeBase::getMaxEnergy(%obj)
{
     return %obj.getDatablock().maxEnergy;
}

function ShapeBase::getMaxDamage(%obj)
{
     return %obj.getDatablock().maxDamage;
}

function ShapeBase::getDamageLeft(%obj)
{
     return %obj.getMaxDamage() - %obj.getDamageLevel();
}

function ShapeBase::getDamageLeftPct(%obj)
{
     return %obj.getDamageLeft() / %obj.getMaxDamage();
}

function ShapeBase::getEnergyPct(%obj)
{
     return %obj.getEnergyLevel() / %obj.getMaxEnergy();
}

function ShapeBase::getCapEnergyPct(%obj)
{
     return %obj.getCapacitorLevel() / %obj.getDatablock().maxCapacitorEnergy;
}

function ShapeBase::useCapEnergy(%obj, %amount)
{
    %obj.setCapacitorLevel(%obj.getCapacitorLevel() - %amount);
}

function ShapeBase::getDamagePct(%obj)
{
     return %obj.getDamageLevel() / %obj.getMaxDamage();
}

function ShapeBase::getMuzzleRaycastPt(%obj, %slot, %dist)
{
     getMuzzleRaycastPt(%obj, %slot, %dist);
}

function ShapeBase::getEyeRaycastPt(%obj, %dist)
{
     getEyeRaycastPt(%obj, %dist);
}

function ShapeBase::getForwardRaycastPt(%obj, %dist)
{
     getForwardRaycastPt(%obj, %dist);
}

// StaticShape missing functions
function StaticShape::getTargetPoint(%obj)
{
    return %obj.position;
}
