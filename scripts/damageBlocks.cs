//--------------------------------------------------------------------------
// Damage Types v2.0
//--------------------------------------------------------------------------
// Universal Damage Datablocks

datablock SimDataBlock(UniversalDamageProfile)
{
   shieldDamageScale[$DamageType::Blaster] 			= 1.0;
   shieldDamageScale[$DamageType::Bullet] 			= 1.0;
   shieldDamageScale[$DamageType::ELF] 				= 1.0;
   shieldDamageScale[$DamageType::ShockLance] 		= 1.0;
   shieldDamageScale[$DamageType::Laser] 			= 1.0;
   shieldDamageScale[$DamageType::ShrikeBlaster] 	= 1.0;
   shieldDamageScale[$DamageType::BellyTurret] 		= 1.0;
   shieldDamageScale[$DamageType::AATurret] 		= 1.0;
   shieldDamageScale[$DamageType::IndoorDepTurret] 	= 1.0;
   shieldDamageScale[$DamageType::OutdoorDepTurret] = 1.0;
   shieldDamageScale[$DamageType::SentryTurret] 	= 1.0;
   shieldDamageScale[$DamageType::Disc] 			= 1.0;
   shieldDamageScale[$DamageType::Grenade] 			= 1.0;
   shieldDamageScale[$DamageType::Mine] 			= 1.0;
   shieldDamageScale[$DamageType::Missile] 			= 1.0;
   shieldDamageScale[$DamageType::Mortar] 			= 1.0;
   shieldDamageScale[$DamageType::Plasma] 			= 1.0;
   shieldDamageScale[$DamageType::BomberBombs] 		= 1.0;
   shieldDamageScale[$DamageType::TankChaingun] 	= 1.0;
   shieldDamageScale[$DamageType::TankMortar] 		= 1.0;
   shieldDamageScale[$DamageType::MissileTurret] 	= 1.0;
   shieldDamageScale[$DamageType::MortarTurret] 	= 1.0;
   shieldDamageScale[$DamageType::PlasmaTurret] 	= 1.0;
   shieldDamageScale[$DamageType::SatchelCharge] 	= 1.0;
   shieldDamageScale[$DamageType::Default] 			= 1.0;
   shieldDamageScale[$DamageType::Impact] 			= 1.0;
   shieldDamageScale[$DamageType::Ground] 			= 1.0;
   shieldDamageScale[$DamageType::Explosion] 		= 1.0;
   shieldDamageScale[$DamageType::Lightning] 		= 5.0;

   damageScale[$DamageType::Blaster] 				= 1.0;
   damageScale[$DamageType::Bullet] 				= 1.0;
   damageScale[$DamageType::ELF] 					= 1.0;
   damageScale[$DamageType::ShockLance] 			= 1.0;
   damageScale[$DamageType::Laser] 					= 1.0;
   damageScale[$DamageType::ShrikeBlaster] 			= 1.0;
   damageScale[$DamageType::BellyTurret] 			= 1.0;
   damageScale[$DamageType::AATurret] 				= 1.0;
   damageScale[$DamageType::IndoorDepTurret] 		= 1.0;
   damageScale[$DamageType::OutdoorDepTurret] 		= 1.0;
   damageScale[$DamageType::SentryTurret] 			= 1.0;
   damageScale[$DamageType::Disc] 					= 1.0;
   damageScale[$DamageType::Grenade] 				= 1.0;
   damageScale[$DamageType::Mine] 					= 1.0;
   damageScale[$DamageType::Missile] 				= 1.0;
   damageScale[$DamageType::Mortar] 				= 1.0;
   damageScale[$DamageType::Plasma] 				= 1.0;
   damageScale[$DamageType::BomberBombs] 			= 1.0;
   damageScale[$DamageType::TankChaingun] 			= 1.0;
   damageScale[$DamageType::TankMortar] 			= 1.0;
   damageScale[$DamageType::MissileTurret] 			= 1.0;
   damageScale[$DamageType::MortarTurret] 			= 1.0;
   damageScale[$DamageType::PlasmaTurret] 			= 1.0;
   damageScale[$DamageType::SatchelCharge] 			= 1.0;
   damageScale[$DamageType::Default] 				= 1.0;
   damageScale[$DamageType::Impact] 				= 1.0;
   damageScale[$DamageType::Ground] 				= 1.0;
   damageScale[$DamageType::Explosion] 				= 1.0;
   damageScale[$DamageType::Lightning] 				= 5.0;

   damageScale[$DamageType::EMP] 			        = 1.0;
   shieldDamageScale[$DamageType::EMP]		        = 1.0;
};

//**** SHRIKE SCOUT FIGHTER ****
datablock SimDataBlock(ShrikeDamageProfile) : UniversalDamageProfile
{
   damageScale[$DamageType::Lightning]	=		5.0;
};

//**** THUNDERSWORD BOMBER ****
datablock SimDataBlock(BomberDamageProfile) : UniversalDamageProfile
{
   damageScale[$DamageType::Lightning]	=		5.0;
};

//**** HAVOC TRANSPORT ****
datablock SimDataBlock(HavocDamageProfile) : UniversalDamageProfile
{
   damageScale[$DamageType::Lightning]	=		5.0;
};

//**** WILDCAT GRAV CYCLE ****
datablock SimDataBlock(WildcatDamageProfile) : UniversalDamageProfile
{
   damageScale[$DamageType::Lightning]	=		5.0;
};

//**** BEOWULF TANK ****
datablock SimDataBlock(TankDamageProfile) : UniversalDamageProfile
{
   damageScale[$DamageType::Lightning]	=		5.0;
};

//**** JERICHO MPB ****
datablock SimDataBlock(MPBDamageProfile) : UniversalDamageProfile
{
   damageScale[$DamageType::Lightning]	=		5.0;
};

//----------------------------------------------------------------------------
// TURRET DAMAGE PROFILES
//----------------------------------------------------------------------------

datablock SimDataBlock(TurretDamageProfile) : UniversalDamageProfile
{
   damageScale[$DamageType::Lightning]	=		5.0;
};

//----------------------------------------------------------------------------
// STATIC SHAPE DAMAGE PROFILES
//----------------------------------------------------------------------------

datablock SimDataBlock(StaticShapeDamageProfile) : UniversalDamageProfile
{
   damageScale[$DamageType::Lightning]	=		5.0;
};

//----------------------------------------------------------------------------
// PLAYER DAMAGE PROFILES
//----------------------------------------------------------------------------

datablock SimDataBlock(LightPlayerDamageProfile) : UniversalDamageProfile
{
   damageScale[$DamageType::Lightning]	=		5.0;
};

datablock SimDataBlock(MediumPlayerDamageProfile) : UniversalDamageProfile
{
   damageScale[$DamageType::Lightning]	=		5.0;
};

datablock SimDataBlock(HeavyPlayerDamageProfile) : UniversalDamageProfile
{
   damageScale[$DamageType::Lightning]	=		5.0;
};
