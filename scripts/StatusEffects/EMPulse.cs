// EM Pulse Effect
datablock AudioProfile(EMPEffectSound)
{
   filename    = "fx/powered/nexus_deny.wav";
   description = AudioDefault3d;
   preload = true;
};

datablock ParticleData(EMPSparksParticle)
{
    dragCoefficient = 0.6;
    gravityCoefficient = 0;
    windCoefficient = 0;
    inheritedVelFactor = 0.2;
    constantAcceleration = 0;
    lifetimeMS = 650;
    lifetimeVarianceMS = 325;
    useInvAlpha = 0;
    spinRandomMin = 0;
    spinRandomMax = 0;
    textureName = "special/bigspark";
    times[0] = 0;
    times[1] = 0.5;
    times[2] = 1;
    colors[0] = "0.100000 0.100000 1.000000 1.000000";
    colors[1] = "0.050000 0.050000 1.000000 1.000000";
    colors[2] = "0.000000 0.000000 1.000000 0.000000";
    sizes[0] = 2.31452;
    sizes[1] = 0.25;
    sizes[2] = 0.25;
};

datablock ParticleEmitterData(EMPSparksEmitter)
{
    ejectionPeriodMS = 3;
    periodVarianceMS = 0;
    ejectionVelocity = 18;
    velocityVariance = 6.75;
    ejectionOffset =   0;
    thetaMin = 0;
    thetaMax = 180;
    phiReferenceVel = 0;
    phiVariance = 360;
    overrideAdvances = 0;
    lifeTimeMS = 200;
    orientParticles= 1;
    orientOnVelocity = 1;
    particles = "EMPSparksParticle";
};

function EMPulseEffect::setup(%this)
{
    %this.duration = 7500;
    %this.stackable = false;
    %this.stackableDuration = 0;
    %this.ticking = true;
    %this.tickTimeMultiplier = 1.0;
    %this.effectInvulnPeriod = 10000;
}

function EMPulseEffect::validateEffect(%this, %obj, %instigator)
{
    if(%obj.hasEnergyPack || %obj.isShielded || %obj.isMounted())
        return false;

    return true;
}

function EMPulseEffect::startEffect(%this, %obj, %instigator)
{
    Parent::startEffect(%this, %obj, %instigator);

    %obj.preEMPRR = %obj.getRechargeRate();
    
    %obj.playAudio(0, "EMPEffectSound");
    %obj.setRechargeRate(0);
    %obj.useEnergy(25);
    
    if(%obj.isPlayer())
        messageClient(%obj.client, 'MsgStartEMPEffect', '\c2Warning! Electromagnetic pulse knocked fusion reactor offline, restarting fusion reaction...');
}

function EMPulseEffect::tickEffect(%this, %obj, %instigator)
{
    if(Parent::tickEffect(%this, %obj, %instigator))
    {
        if(getRandom() > 0.4)
            createLifeEmitter(%obj.position, "EMPSparksEmitter", 200);
    }
}

function EMPulseEffect::endEffect(%this, %obj)
{
    if(Parent::endEffect(%this, %obj))
    {
        %obj.setRechargeRate(%obj.preEMPRR);

        if(%obj.isPlayer())
            messageClient(%obj.client, 'MsgEndEMPEffect', '\c2Fusion reactor online.');
    }
}

StatusEffect.registerEffect("EMPulseEffect");
