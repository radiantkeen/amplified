// Poison - ignores armor
// poison cloud? - green smoke
function PoisonEffect::setup(%this)
{
    %this.duration = 15000;
    %this.stackable = true;
    %this.stackableDuration = 5000;
    %this.ticking = true;
    %this.tickTimeMultiplier = 1.0;
    %this.effectInvulnPeriod = 10000;

    %this.poisonSpikeDamage = 0.5;
}

function PoisonEffect::validateEffect(%this, %obj, %instigator)
{
    if(%obj.isShielded || %obj.isMounted() || %obj.isWalker)
        return false;
    else if(%obj.isPlayer())
        return true;
        
    return false;
}

function PoisonEffect::startEffect(%this, %obj, %instigator)
{
    Parent::startEffect(%this, %obj, %instigator);

//    %obj.play3D("PosonCoughSound");

    messageClient(%obj.client , 'MsgStartPoisonEffect' , '\c2Warning! Toxins detected, applying ');
}

function PoisonEffect::onStackEffect(%this, %obj, %instigator)
{
    Parent::onStackEffect(%this, %obj, %instigator);

    messageClient(%obj.client , 'MsgStackPoisonEffect' , '\c2Warning! Additional toxins applied!');
}

function PoisonEffect::tickEffect(%this, %obj, %instigator)
{
    if(%obj.isDead)
        return;
        
    if(Parent::tickEffect(%this, %obj, %instigator))
    {
        %self = %this.getName();
        
        if(%obj.tickCount[%self] % 32 == 0 && getRandom() >= 0.6)
        {
            %obj.getDataBlock().damageObject(%obj, %instigator, %obj.getWorldBoxCenter(), %this.poisonSpikeDamage, $DamageType::Poison);
            playPain(%obj);
        }

//        if(getRandom() > 0.9)
//        {
            // cough and bellow green smoke
//            createLifeEmitter(%obj.position, "PoisonBreathEmitter", 400);
//        }
    }
}

function PoisonEffect::endEffect(%this, %obj)
{
    if(Parent::endEffect(%this, %obj))
        messageClient(%obj.client , 'MsgEndPoisonEffect' , '\c2Toxins have been reduced to non-lethal levels.');
}

StatusEffect.registerEffect("PoisonEffect");
