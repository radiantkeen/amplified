function BasicVortex::init(%this)
{
    %this.detectMask = $TypeMasks::PlayerObjectType | $TypeMasks::VehicleObjectType;
    %this.blockMask = $TypeMasks::TerrainObjectType | $TypeMasks::InteriorObjectType | $TypeMasks::StaticObjectType;
    %this.radius = 45;
}

function BasicVortex::forEachInAura(%this, %obj)
{
    %pos = %this.getPosition();
    %tgtPos = %obj.getWorldBoxCenter();
    %dist = vectorDist(%tgtPos, %pos);
    %vec = VectorFromPoints(%pos, %tgtPos);
    %strength = %obj.isPlayer() ? 13 : 21;
    %force = (%strength * %dist) + (1 / %dist);

    %obj.applyImpulse(%tgtPos, vectorScale(%vec, %force));
}

Aura.registerAura("BasicVortex", $AuraType::Instance);
