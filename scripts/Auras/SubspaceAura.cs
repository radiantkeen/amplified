function SubspaceAura::init(%this)
{
    %this.detectMask = $TypeMasks::PlayerObjectType | $TypeMasks::VehicleObjectType | $TypeMasks::StaticShapeObjectType;
    %this.blockMask = $TypeMasks::TerrainObjectType;
    %this.radius = 150;
    %this.repairPerTick = 0.02;
    
    %this.setTickRate(1000);
}

function SubspaceAura::validateAuraTick(%this)
{
    if(!isObject(%this.source))
    {
        %this.destroy();
        return false;
    }

    return true;
}

function SubspaceAura::forEachInAura(%this, %obj)
{
    if(%obj.isDead == true || %obj == %this.source)
        return;

    %dmg = %obj.getDamageLevel();

    if(%dmg > 0)
        %obj.setDamageLevel(%dmg - %this.repairPerTick);
}

Aura.registerAura("SubspaceAura", $AuraType::Instance);
