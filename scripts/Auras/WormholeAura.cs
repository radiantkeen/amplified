datablock AudioProfile(WormholeTravelSound)
{
   filename    = "fx/Bonuses/down_perppass3_bunnybump.wav";
   description = AudioExplosion3d;
   preload = true;
};

datablock ParticleData(Teleporter1Particle)
{
    dragCoefficient = 0;
    gravityCoefficient = -0;
    windCoefficient = 1;
    inheritedVelFactor = 0.025;
    constantAcceleration = -1.0;
    lifetimeMS = 1612;
    lifetimeVarianceMS = 0;
    useInvAlpha = 0;
    spinRandomMin = -200;
    spinRandomMax = 314.516;
    textureName = "special/cloudflash7.png";
    times[0] = 0;
    times[1] = 0.532258;
    times[2] = 0.75;
    times[3] = 1;
    colors[0] = "0.125984 0.112000 0.920000 1.000000";
    colors[1] = "0.456693 0.208000 0.200000 0.774194";
    colors[2] = "1.000000 1.000000 1.000000 0.500000";
    colors[3] = "1.000000 1.000000 1.000000 0.000000";
    sizes[0] = 3.10484;
    sizes[1] = 4.50484;
    sizes[2] = 4.10484;
    sizes[3] = 3.10484;
};

datablock ParticleEmitterData(Teleporter1Emitter)
{
    ejectionPeriodMS = 6;
    periodVarianceMS = 0;
    ejectionVelocity = 10.25;
    velocityVariance = 0.25;
    ejectionOffset =   0;
    thetaMin = 0;
    thetaMax = 180;
    phiReferenceVel = 0;
    phiVariance = 360;
    overrideAdvances = 0;
    orientParticles= 0;
    orientOnVelocity = 1;
    particles = "Teleporter1Particle";
};

function WormholeAura::init(%this)
{
    %this.detectMask = $TypeMasks::PlayerObjectType | $TypeMasks::ProjectileObjectType | $TypeMasks::ItemObjectType;
    %this.blockMask = $TypeMasks::InteriorObjectType;
    %this.radius = 8;
}

function WormholeAura::onTick(%this)
{
    Parent::onTick(%this);
    
    if(%this.tickCount % 32 == 0)
        createLifeEmitter(%this.source.wormholePoint, "Teleporter1Emitter", 1500);
}

function WormholeAura::forEachInAura(%this, %obj)
{
    if(%obj.isPlayer())
    {
        %time = getSimTime();

        if(%obj.nextWormholeTime > %time)
            return;
            
        %obj.nextWormholeTime = %time + $g_WormholeTimeout;
        
        %obj.setPosition(%this.source.getOtherPortalPad().wormholePoint);
        %obj.playAudio(0, WormholeTravelSound);
        %obj.setWhiteOut(0.75);
    }
    else
    {
        if(%obj.lastWormhole == %this.source)
            return;
            
        if(!%obj.getDatablock().canReflect(%obj))
            return;

        %dest = %this.source.getOtherPortalPad();
        %p = cloneProjectile(%obj, %dest.wormholePoint, %obj.initialDirection);

        %p.lastWormhole = %dest;
    }
}

Aura.registerAura("WormholeAura", $AuraType::Instance);
