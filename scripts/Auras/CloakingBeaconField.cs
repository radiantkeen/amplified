function CloakingBeaconField::init(%this)
{
    %this.detectMask = $TypeMasks::PlayerObjectType | $TypeMasks::VehicleObjectType | $TypeMasks::StaticShapeObjectType;
    %this.blockMask = $TypeMasks::TerrainObjectType | $TypeMasks::InteiorObjectType;
    %this.radius = 30;
}

function CloakingBeaconField::validateAuraTick(%this)
{
    if(!isObject(%this.source))
    {
        %this.destroy();
        return false;
    }

    return true;
}

function CloakingBeaconField::onEnter(%this, %obj)
{
    if(%obj.getType() & %this.blockMask)
    {
        %this.removeFromAura(%obj);
        return;
    }
        
    if(%obj.isDead || %obj.team != %this.source.team || %obj == %this.source)
    {
        %this.removeFromAura(%obj);
        return;
    }

    %obj.cloakjammed = true;
    %obj.setCloaked(true);
}

function CloakingBeaconField::forEachInAura(%this, %obj)
{
    if(!%obj.isCloaked())
        %obj.setCloaked(true);
}

function CloakingBeaconField::onLeave(%this, %obj)
{
    %obj.cloakjammed = false;
    %obj.schedule(32, "setCloaked", false); // to prevent forevercloak
}

Aura.registerAura("CloakingBeaconField", $AuraType::Persistent);
