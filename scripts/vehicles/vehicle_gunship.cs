//**************************************************************
// Gunship
//**************************************************************

//**************************************************************
// VEHICLE CHARACTERISTICS
//**************************************************************

datablock FlyingVehicleData(Gunship) : HavocDamageProfile
{
   spawnOffset = "0 0 6";
   renderWhenDestroyed = false;

   catagory = "Vehicles";
   shapeFile = "vehicle_air_hapc.dts";
   multipassenger = true;
   computeCRC = true;


   debrisShapeName = "vehicle_air_hapc_debris.dts";
   debris = ShapeDebris;

   drag = 0.2;
   density = 1.0;

   mountPose[0] = sitting;
//   mountPose[1] = sitting;
   numMountPoints = 6;
   isProtectedMountPoint[0] = true;
   isProtectedMountPoint[1] = true;
   isProtectedMountPoint[2] = true;
   isProtectedMountPoint[3] = true;
   isProtectedMountPoint[4] = true;
   isProtectedMountPoint[5] = true;

   cameraMaxDist = 17;
   cameraOffset = 2;
   cameraLag = 8.5;
   explosion = LargeAirVehicleExplosion;
	explosionDamage = 0.5;
	explosionRadius = 5.0;

   maxDamage = 9.1;
   destroyedLevel = 9.1;

   isShielded = true;
   rechargeRate = 1.45; // z0dd - ZOD, 4/16/02. Was 0.8
   energyPerDamagePoint = 100; // z0dd - ZOD, 4/16/02. Was 200
   maxEnergy = 800; // z0dd - ZOD, 4/16/02. Was 550
   minDrag = 100;                // Linear Drag
   rotationalDrag = 2700;        // Anguler Drag

   // Auto stabilize speed
   maxAutoSpeed = 10;
   autoAngularForce = 3000;      // Angular stabilizer force
   autoLinearForce = 450;        // Linear stabilzer force
   autoInputDamping = 0.95;      // 
                                                        
   // Maneuvering
   maxSteeringAngle = 8;
   horizontalSurfaceForce = 9;  // Horizontal center "wing"
   verticalSurfaceForce = 9;    // Vertical center "wing"
   maneuveringForce = 7000;      // Horizontal jets // z0dd - ZOD, 4/25/02. Was 6000
   steeringForce = 1200;          // Steering jets
   steeringRollForce = 400;      // Steering jets
   rollForce = 12;               // Auto-roll
   hoverHeight = 8;         // Height off the ground at rest
   createHoverHeight = 6;   // Height off the ground when created
   maxForwardSpeed = 90;  // speed in which forward thrust force is no longer applied (meters/second) z0dd - ZOD, 4/25/02. Was 71

   // Turbo Jet
   jetForce = 6500; // z0dd - ZOD, 4/25/02. Was 5000
   minJetEnergy = 55;
   jetEnergyDrain = 4.5; // z0dd - ZOD, 4/16/02. Was 3.6
   vertThrustMultiple = 3.0;

   dustEmitter = LargeVehicleLiftoffDustEmitter;
   triggerDustHeight = 4.0;
   dustHeight = 2.0;

   damageEmitter[0] = LightDamageSmoke;
   damageEmitter[1] = HeavyDamageSmoke;
   damageEmitter[2] = DamageBubbles;
   damageEmitterOffset[0] = "3.0 -3.0 0.0 ";
   damageEmitterOffset[1] = "-3.0 -3.0 0.0 ";
   damageLevelTolerance[0] = 0.3;
   damageLevelTolerance[1] = 0.7;
   numDmgEmitterAreas = 2;

   // Rigid body
   mass = 550;
   bodyFriction = 0;
   bodyRestitution = 0.3;
   minRollSpeed = 0;
   softImpactSpeed = 12;       // Sound hooks. This is the soft hit.
   hardImpactSpeed = 15;    // Sound hooks. This is the hard hit.

   // Ground Impact Damage (uses DamageType::Ground)
   minImpactSpeed = 25;      // If hit ground at speed above this then it's an impact. Meters/second
   speedDamageScale = 0.18;

   // Object Impact Damage (uses DamageType::Impact)
   collDamageThresholdVel = 28;
   collDamageMultiplier   = 0.060;

   //
   minTrailSpeed = 500;
   trailEmitter = ContrailEmitter;
   forwardJetEmitter = FlyerJetEmitter;
   downJetEmitter = FlyerJetEmitter;

   //
   jetSound = HAPCFlyerThrustSound;
   engineSound = HAPCFlyerEngineSound;
   softImpactSound = SoftImpactSound;
   hardImpactSound = HardImpactSound;
   //wheelImpactSound = WheelImpactSound;

   //
   softSplashSoundVelocity = 5.0; 
   mediumSplashSoundVelocity = 8.0;   
   hardSplashSoundVelocity = 12.0;   
   exitSplashSoundVelocity = 8.0;
   
   exitingWater      = VehicleExitWaterHardSound;
   impactWaterEasy   = VehicleImpactWaterSoftSound;
   impactWaterMedium = VehicleImpactWaterMediumSound;
   impactWaterHard   = VehicleImpactWaterHardSound;
   waterWakeSound    = VehicleWakeHardSplashSound; 
   
   minMountDist = 4;

   splashEmitter[0] = VehicleFoamDropletsEmitter;
   splashEmitter[1] = VehicleFoamEmitter;

   shieldImpact = VehicleShieldImpact;

   cmdCategory = "Tactical";
   cmdIcon = CMDFlyingHAPCIcon;
   cmdMiniIconName = "commander/MiniIcons/com_hapc_grey";
   targetNameTag = 'Firestorm';
   targetTypeTag = 'Gunship';
   sensorData = AWACPulseSensor;
   sensorRadius = AWACPulseSensor.detectRadius;
   sensorColor = "255 194 9";

   checkRadius = 7.8115;
   observeParameters = "1 15 15";

   stuckTimerTicks = 32;   // If the hapc spends more than 1 sec in contact with something
   stuckTimerAngle = 80;   //  with a > 80 deg. pitch, BOOM!

   shieldEffectScale = "1.0 0.9375 0.45";
};

function Gunship::hasDismountOverrides(%data, %obj)
{
   return true;
}

function Gunship::getDismountOverride(%data, %obj, %mounted)
{
   %node = -1;
   for (%i = 0; %i < %data.numMountPoints; %i++)
   {
      if (%obj.getMountNodeObject(%i) == %mounted)
      {
         %node = %i;
         break;
      }
   }
   if (%node == -1)
   {
      warning("Couldn't find object mount point");
      return "0 0 1";
   }

   if (%node == 3 || %node == 2)
   {
      return "-1 0 1";
   }
   else if (%node == 5 || %node == 4)
   {
      return "1 0 1";
   }
   else
   {
      return "0 0 1";
   }
}

datablock ShapeBaseImageData(HavocPilotSection) : EngineAPEImage
{
   mountPoint = 7;
   offset = "0 0 1";
   shapeFile = "vehicle_air_scout.dts";
};

datablock StaticShapeData(GunshipSensor)
{
   shapeFile = "sensor_pulse_medium.dts";

   dynamicType = $TypeMasks::VehicleObjectType;

   maxDamage = Gunship.maxDamage;
   destroyedLevel = Gunship.destroyedLevel;

   targetNameTag = 'Mayhem';
   targetTypeTag = 'Gunship';
};

function GunshipSensor::onDamage(%data, %obj)
{
   %newDamageVal = %obj.getDamageLevel();
   if(%obj.lastDamageVal !$= "")
      if(isObject(%obj.getObjectMount()) && %obj.lastDamageVal > %newDamageVal)
         %obj.getObjectMount().setDamageLevel(%newDamageVal);
   %obj.lastDamageVal = %newDamageVal;
}

function GunshipSensor::damageObject(%this, %targetObject, %sourceObject, %position, %amount, %damageType ,%vec, %client, %projectile)
{
   //If vehicle turret is hit then apply damage to the vehicle
   %vehicle = %targetObject.getObjectMount();
   if(%vehicle)
      %vehicle.getDataBlock().damageObject(%vehicle, %sourceObject, %position, %amount, %damageType, %vec, %client, %projectile);
}

function GunshipSensor::doDismount(%this)
{
    // prevent console spam for some wacky reason
}

datablock TurretImageData(GunshipTurretParam)
{
   mountPoint = 2;
   shapeFile = "turret_muzzlepoint.dts";

   projectile = AssaultChaingunBullet;
   projectileType = TracerProjectile;

   useCapacitor = true;
   usesEnergy = true;

   // Turret parameters
   activationMS      = 750;
   deactivateDelayMS = 1500;
   thinkTimeMS       = 100;
   degPerSecTheta    = 600;
   degPerSecPhi      = 600;

   attackRadius      = 75;
};

//**************************************************************
// WEAPONS
//**************************************************************

datablock TurretData(GunshipTurret) : TurretDamageProfile
{
   className               = VehicleTurret;
   catagory                = "Turrets";
   shapeFile               = "turret_base_large.dts";
   preload                 = true;

   mass                    = 1.0;  // Not really relevant
   repairRate              = 0;
   maxDamage               = Gunship.maxDamage;
   destroyedLevel          = Gunship.destroyedLevel;

   thetaMin                = 0;
   thetaMax                = 165;

   // capacitor
   maxCapacitorEnergy      = 200;
   capacitorRechargeRate   = 1.171875;

   inheritEnergyFromMount  = true;
   firstPersonOnly         = true;
   useEyePoint             = true;
//   numWeapons              = 2;

   targetNameTag           = 'Mayhem';
   targetTypeTag           = 'Turret';
};

function GunshipTurret::onDamage(%data, %obj)
{
   %newDamageVal = %obj.getDamageLevel();

   if(%obj.lastDamageVal !$= "")
      if(isObject(%obj.getObjectMount()) && %obj.lastDamageVal > %newDamageVal)
         %obj.getObjectMount().setDamageLevel(%newDamageVal);

   %obj.lastDamageVal = %newDamageVal;
}

function GunshipTurret::damageObject(%this, %targetObject, %sourceObject, %position, %amount, %damageType ,%vec, %client, %projectile)
{
   //If vehicle turret is hit then apply damage to the vehicle
   %vehicle = %targetObject.getObjectMount();

   if(%vehicle)
      %vehicle.getDataBlock().damageObject(%vehicle, %sourceObject, %position, %amount, %damageType, %vec, %client, %projectile);
}

function GunshipTurret::playerDismount(%data, %obj)
{
   //Passenger Exiting
   %obj.fireTrigger = 0;
   %obj.setImageTrigger(0, false);
   %client = %obj.getControllingClient();
   %client.player.mountImage(%client.player.lastWeapon, $WeaponSlot);
   %client.player.mountVehicle = false;
//   setTargetSensorGroup(%obj.getTarget(), 0);
   setTargetNeverVisMask(%obj.getTarget(), 0xffffffff);
}

function GunshipTurret::doDismount()
{

}

function Gunship::onAdd(%this, %obj)
{
   Parent::onAdd(%this, %obj);

   %obj.mountImage(HavocPilotSection, 7);
   %obj.turretR = %this.installTurret(%obj, "GunshipTurret", 2);
   %obj.turretL = %this.installTurret(%obj, "GunshipTurret", 5);
   %obj.applyTurretOnSit = true;
   
   %obj.turretR.mountImage(AABarrelLarge, 0);
   %obj.turretL.mountImage(PlasmaBarrelLarge, 0);

   %sensor = new StaticShape()
   {
       dataBlock = GunshipSensor;
       scale = "1 1 1";
   };

   %obj.mountObject(%sensor, 1);
   MissionCleanup.add(%sensor);

   %sensor.parent = %obj;
   %obj.sensor = %sensor;
   %sensor.team = %obj.teamBought;

   setTargetSensorGroup(%sensor.getTarget(), %sensor.team);
   setTargetNeverVisMask(%sensor.getTarget(), 0xffffffff);

   %obj.sensor.setCloaked(true);
   %obj.sensor.schedule(4500, "playThread", $ActivateThread, "ambient");
   %obj.sensor.schedule(5000, setCloaked, false);
}

function Gunship::onTick(%data, %obj)
{
    if(Parent::onTick(%data, %obj))
    {
        %speed = vectorLen(%obj.getVelocity());

        if(%speed > 12)
            %obj.setImageTrigger(7, true);
        else
            %obj.setImageTrigger(7, false);
    }
}

function Gunship::playerMounted(%data, %obj, %player, %node)
{
   if(%node == 0)
   {
      commandToClient(%player.client, 'setHudMode', 'Pilot', "HAPC", %node);
//      commandToClient(%player.client,'SetWeaponryVehicleKeys', true);
      commandToClient(%player.client, 'setRepairReticle');

      setTargetSensorGroup(%obj.getMountNodeObject(2).getTarget(), %player.team);
      setTargetSensorGroup(%obj.getMountNodeObject(5).getTarget(), %player.team);
   }
   else if(%node == 3)
      %data.onGunnerSeated(%obj, %player, 2);
   else if(%node == 4)
      %data.onGunnerSeated(%obj, %player, 5);
   else
	   commandToClient(%player.client, 'setHudMode', 'Passenger', "HAPC", %node);

   // build a space-separated string representing passengers
   // 0 = no passenger; 1 = passenger (e.g. "1 0 0 1 1 0 ")
   %passString = buildPassengerString(%obj);

   // send the string of passengers to all mounted players
   for(%i = 0; %i < %data.numMountPoints; %i++)
   {
      %seat = %obj.getMountNodeObject(%i);

      if(%seat > 0 && %seat.isPlayer())
         commandToClient(%seat.client, 'checkPassengers', %passString);
   }

   // update observers who are following this guy...
   if( %player.client.observeCount > 0 )
      resetObserveFollow( %player.client, false );
}
