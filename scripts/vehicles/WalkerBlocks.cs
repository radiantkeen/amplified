// Walker Datablocks

// Audio
//------------------------------------------------------------------------------
datablock AudioProfile(WalkerPowerOn)
{
   filename    = "fx/powered/base_power_on.wav";
   description = AudioDefault3d;
   preload = true;
};

datablock AudioProfile(WalkerPowerOff)
{
   filename    = "fx/powered/base_power_off.wav";
   description = AudioDefault3d;
   preload = true;
};

// TSShape Definitions
//------------------------------------------------------------------------------
datablock TSShapeConstructor(TR2HeavyMaleDts)
{
   baseShape = "TR2heavy_male.dts";
   sequence0 = "TR2heavy_male_root.dsq root";
   sequence1 = "TR2heavy_male_forward.dsq run";
   sequence2 = "TR2heavy_male_back.dsq back";
   sequence3 = "TR2heavy_male_side.dsq side";
   sequence4 = "heavy_male_lookde.dsq look";
   sequence5 = "heavy_male_head.dsq head";
   sequence6 = "TR2heavy_male_fall.dsq fall";
   sequence7 = "TR2heavy_male_jet.dsq jet";
   sequence8 = "TR2heavy_male_land.dsq land";
   sequence9 = "TR2heavy_male_jump.dsq jump";
   sequence10 = "heavy_male_recoilde.dsq light_recoil";
   sequence11 = "heavy_male_idlepda.dsq pda";
   sequence12 = "heavy_male_headside.dsq headside";
   sequence13 = "heavy_male_lookms.dsq lookms";
   sequence14 = "TR2heavy_male_diehead.dsq death1";
   sequence15 = "TR2heavy_male_diechest.dsq death2";
   sequence16 = "TR2heavy_male_dieback.dsq death3";
   sequence17 = "TR2heavy_male_diesidelf.dsq death4";
   sequence18 = "TR2heavy_male_diesidert.dsq death5";
   sequence19 = "TR2heavy_male_dieforward.dsq death6";      // heavy_male_dieleglf
   sequence20 = "TR2heavy_male_diechest.dsq death7";        // heavy_male_dielegrt
   sequence21 = "TR2heavy_male_dieslump.dsq death8";
   sequence22 = "TR2heavy_male_dieforward.dsq death9";      // heavy_male_dieknees
   sequence23 = "TR2heavy_male_dieforward.dsq death10";
   sequence24 = "TR2heavy_male_diespin.dsq death11";
   sequence25 = "TR2heavy_male_celsalute.dsq cel1";
   sequence26 = "TR2heavy_male_celwave.dsq cel2";
   sequence27 = "TR2heavy_male_tauntbest.dsq cel3";
   sequence28 = "TR2heavy_male_tauntimp.dsq cel4";
   sequence29 = "TR2heavy_male_celdance.dsq cel5";
   sequence30 = "TR2heavy_male_celflex.dsq cel6";
   sequence31 = "TR2heavy_male_celtaunt.dsq cel7";
   sequence32 = "TR2heavy_male_celjump.dsq cel8";
   sequence33 = "TR2heavy_male_ski.dsq ski";
   sequence34 = "TR2heavy_male_standjump.dsq standjump";
   sequence35 = "heavy_male_looknw.dsq looknw";
};

datablock ShapeBaseImageData(WalkerMissileParam)
{
   shapeFile = "turret_muzzlepoint.dts";
   mountPoint = 10;
   offset = "0 0 0";
   rotation = "0 1 0 0";

   isSeeker     = true;
   seekRadius   = 300;
   maxSeekAngle = 30;
   seekTime     = 0.85;
   minSeekHeat  = 0.8;
   useTargetAudio = true;
   minTargetingDistance = 10;
};

datablock ShapeBaseImageData(WalkerDefaultParam) : WalkerMissileParam
{
   isSeeker = false;
};

datablock SimDataBlock(WalkerDamageProfile) : ArmorDamageProfile
{
   damageScale[$DamageType::Impact] 				= 0.75;
   damageScale[$DamageType::Ground] 				= 1.5;
};

datablock SensorData(WalkerBaseSensor)
{
   detects = true;
   detectsUsingLOS = true;
   detectsPassiveJammed = false;
   detectRadius = 400;
   detectionPings = false;
   detectsFOVOnly = true;
   detectFOVPercent = 1.2;
   useObjectFOV = true;
};

datablock DebrisData(WalkerDebris) : DreadDebris
{
   elasticity = 0.15;
   friction = 0.75;
};

datablock ParticleData(WalkerJetParticle)
{
   dragCoefficient      = 1.5;
   gravityCoefficient   = 0;
   inheritedVelFactor   = 0.2;
   constantAcceleration = 0.0;
   lifetimeMS           = 200;
   lifetimeVarianceMS   = 0;
   textureName          = "particleTest";
   colors[0]     = "0.9 0.7 0.3 0.6";
   colors[1]     = "0.3 0.3 0.5 0";
   sizes[0]      = 2;
   sizes[1]      = 6;
};

datablock ParticleEmitterData(WalkerJetEmitter)
{
   ejectionPeriodMS = 7;
   periodVarianceMS = 0;
   ejectionVelocity = 25;
   velocityVariance = 1.0;
   ejectionOffset   = 0.0;
   thetaMin         = 0;
   thetaMax         = 15;
   phiReferenceVel  = 0;
   phiVariance      = 360;
   overrideAdvances = false;
   particles = "WalkerJetParticle";
};

datablock DebrisData(WAutoShellDebris)
{
   shapeName = "weapon_missile_casement.dts";

   lifetime = 5.0;

   minSpinSpeed = 300.0;
   maxSpinSpeed = 400.0;

   elasticity = 0.5;
   friction = 0.2;

   numBounces = 3;

   fade = true;
   staticOnMaxBounce = true;
   snapOnMaxBounce = true;
};

datablock PlayerData(StormguardWalkerBase) : WalkerDamageProfile
{
   emap = true;

   flags = $ArmorMask::Walker;

   className = Armor;
   shapeFile = "TR2heavy_male.dts";
   cameraMaxDist = 12;
   computeCRC = true;

   debrisShapeName = "debris_player.dts";
   debris = WalkerDebris;

   canObserve = true;
   cmdCategory = "Clients";
   cmdIcon = CMDPlayerIcon;
   cmdMiniIconName = "commander/MiniIcons/com_player_grey";

   hudImageNameFriendly[0] = "gui/hud_mortor.png";
   hudImageNameEnemy[0] = "gui/hud_mortor.png";
   hudRenderModulated[0] = true;

   hudImageNameFriendly[1] = "commander/MiniIcons/com_flag_grey";
   hudImageNameEnemy[1] = "commander/MiniIcons/com_flag_grey";
   hudRenderModulated[1] = true;
   hudRenderAlways[1] = true;
   hudRenderCenter[1] = true;
   hudRenderDistance[1] = true;

   hudImageNameFriendly[2] = "commander/MiniIcons/com_flag_grey";
   hudImageNameEnemy[2] = "commander/MiniIcons/com_flag_grey";
   hudRenderModulated[2] = true;
   hudRenderAlways[2] = true;
   hudRenderCenter[2] = true;
   hudRenderDistance[2] = true;

   aiAvoidThis = true;

   minLookAngle = -1.4;
   maxLookAngle = 1.4;
   maxFreelookAngle = 3.0;

   mass = 2500;
   drag = 0.5;
   maxdrag = 0.6;
   density = 10;
   maxDamage = 15.0;
   maxEnergy = 300;
   repairRate = 0.01;
   energyPerDamagePoint = 75.0;

   rechargeRate = 0.256;
   jetForce = (36.5 * 190) * 12.0;
   underwaterJetForce = (42.4 * 190) * 13.0;
   underwaterVertJetFactor = 1.5;
   jetEnergyDrain = (24 / 32) * 1.359;
   underwaterJetEnergyDrain =  (8 / 32) * 1.25;
   minJetEnergy = 6; // 65536
   maxJetHorizontalPercentage = 0.8;

   runForce = (35 * 210) * 20;
   runEnergyDrain = 0;
   minRunEnergy = 0;
   maxForwardSpeed = 18;
   maxBackwardSpeed = 9;
   maxSideSpeed = 5;

   maxUnderwaterForwardSpeed = 10;
   maxUnderwaterBackwardSpeed = 5;
   maxUnderwaterSideSpeed = 2.5;

   recoverDelay = 6;
   recoverRunForceScale = 1.2;

   jumpForce = (9.25 * 215) * 10;
   jumpEnergyDrain = 0;
   minJumpEnergy = 65535;
   jumpDelay = 0;

   // heat inc'ers and dec'ers
   heatDecayPerSec      = 0.01; // takes 3 seconds to clear heat sig.
   heatIncreasePerSec   = 10; // takes 3.0 seconds of constant jet to get full heat sig.

   // Controls over slope of runnable/jumpable surfaces
   runSurfaceAngle  = 55;
   jumpSurfaceAngle = 55;

   minJumpSpeed = 20;
   maxJumpSpeed = 30;

   horizMaxSpeed = 1000;
   horizResistSpeed = 20;
   horizResistFactor = 0.3;
   maxJetForwardSpeed = 16;

   upMaxSpeed = 60;
   upResistSpeed = 35;
   upResistFactor = 0.125;

   minImpactSpeed = 60;
   speedDamageScale = 0.006;

   jetSound = HAPCFlyerThrustSound;
   wetJetSound = AssaultVehicleThrustSound;
   jetEmitter = WalkerJetEmitter;

   boundingBox = "6.2 6.2 9.0";
   pickupRadius = 1.5; //0.75;

   // damage location details
   boxNormalHeadPercentage       = 0.83;
   boxNormalTorsoPercentage      = 0.49;
   boxHeadLeftPercentage         = 0;
   boxHeadRightPercentage        = 1;
   boxHeadBackPercentage         = 0;
   boxHeadFrontPercentage        = 1;

   //Foot Prints
   decalData   = HeavyMaleFootprint;
   decalOffset = 0.4;

   footPuffEmitter = LightPuffEmitter;
   footPuffNumParts = 15;
   footPuffRadius = 0.25;

   dustEmitter = LiftoffDustEmitter;

   splash = PlayerSplash;
   splashVelocity = 4.0;
   splashAngle = 67.0;
   splashFreqMod = 300.0;
   splashVelEpsilon = 0.60;
   bubbleEmitTime = 0.4;
   splashEmitter[0] = PlayerFoamDropletsEmitter;
   splashEmitter[1] = PlayerFoamEmitter;
   splashEmitter[2] = PlayerBubbleEmitter;
   mediumSplashSoundVelocity = 10.0;
   hardSplashSoundVelocity = 20.0;
   exitSplashSoundVelocity = 5.0;

   footstepSplashHeight = 0.35;
   //Footstep Sounds
   LFootSoftSound       = LFootHeavySoftSound;
   RFootSoftSound       = RFootHeavySoftSound;
   LFootHardSound       = LFootHeavyHardSound;
   RFootHardSound       = RFootHeavyHardSound;
   LFootMetalSound      = LFootHeavyMetalSound;
   RFootMetalSound      = RFootHeavyMetalSound;
   LFootSnowSound       = LFootHeavySnowSound;
   RFootSnowSound       = RFootHeavySnowSound;
   LFootShallowSound    = LFootHeavyShallowSplashSound;
   RFootShallowSound    = RFootHeavyShallowSplashSound;
   LFootWadingSound     = LFootHeavyWadingSound;
   RFootWadingSound     = RFootHeavyWadingSound;
   LFootUnderwaterSound = LFootHeavyUnderwaterSound;
   RFootUnderwaterSound = RFootHeavyUnderwaterSound;
   LFootBubblesSound    = LFootHeavyBubblesSound;
   RFootBubblesSound    = RFootHeavyBubblesSound;
   movingBubblesSound   = ArmorMoveBubblesSound;
   waterBreathSound     = WaterBreathMaleSound;

   impactSoftSound      = ImpactHeavySoftSound;
   impactHardSound      = ImpactHeavyHardSound;
   impactMetalSound     = ImpactHeavyMetalSound;
   impactSnowSound      = ImpactHeavySnowSound;

   skiSoftSound         = SkiAllSoftSound;
   skiHardSound         = SkiAllHardSound;
   skiMetalSound        = SkiAllMetalSound;
   skiSnowSound         = SkiAllSnowSound;

   impactWaterEasy      = ImpactHeavyWaterEasySound;
   impactWaterMedium    = ImpactHeavyWaterMediumSound;
   impactWaterHard      = ImpactHeavyWaterHardSound;

   groundImpactMinSpeed    = 20.0;
   groundImpactShakeFreq   = "4.0 4.0 4.0";
   groundImpactShakeAmp    = "1.0 1.0 1.0";
   groundImpactShakeDuration = 0.8;
   groundImpactShakeFalloff = 10.0;

   exitingWater         = ExitingWaterHeavySound;

   maxWeapons = 6;           // Max number of different weapons the mech can have
   maxGrenades = 0;
   maxMines = 0;

   // Inventory restrictions
   max[RepairKit]          = 1000;
   max[Beacon]             = 1000;
   max[Mine]               = 1000;
   max[Grenade]            = 1000;

   observeParameters = "1.0 12.0 12.0";
   shieldEffectScale = "1.0 1.0 1.0";
};
