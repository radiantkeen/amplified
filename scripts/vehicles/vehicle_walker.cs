// Walker system
exec("scripts/vehicles/WalkerBlocks.cs");

$WalkerHardpointPos["Stormguard", 2] = "0 0 0";
$WalkerHardpointPos["Stormguard", 3] = "-3.5 0 0";
$WalkerHardpointPos["Stormguard", 4] = "0 -1 2.5";
$WalkerHardpointPos["Stormguard", 5] = "-3.5 -1 2.5";
$WalkerHardpointPos["Stormguard", 6] = "-1.75 0 0.75";

exec("scripts/vehicles/WalkerWeaponBlocks.cs");

// Walker Firegroups
//----------------------------------------------------------------------------
$WalkerMaxFireGroups = 0;

$WalkerFiregroup::Alpha = 1 << $WalkerMaxFireGroups;
$WalkerFiregroupMask[$WalkerMaxFireGroups] = $WalkerFiregroup::Alpha;
$WalkerFiregroupID[$WalkerFiregroup::Alpha] = $WalkerMaxFireGroups;
$WalkerFiregroupName[$WalkerMaxFireGroups] = "Guns";
$WalkerMaxFireGroups++;

$WalkerFiregroup::Beta = 1 << $WalkerMaxFireGroups;
$WalkerFiregroupMask[$WalkerMaxFireGroups] = $WalkerFiregroup::Beta;
$WalkerFiregroupID[$WalkerFiregroup::Beta] = $WalkerMaxFireGroups;
$WalkerFiregroupName[$WalkerMaxFireGroups] = "Cannon";
$WalkerMaxFireGroups++;

$WalkerFiregroup::Gamma = 1 << $WalkerMaxFireGroups;
$WalkerFiregroupMask[$WalkerMaxFireGroups] = $WalkerFiregroup::Gamma;
$WalkerFiregroupID[$WalkerFiregroup::Gamma] = $WalkerMaxFireGroups;
$WalkerFiregroupName[$WalkerMaxFireGroups] = "Missiles";
$WalkerMaxFireGroups++;

// Walker Virtual Client - places a stub client on the walkers
function VirtualClient::__construct(%this)
{
    VirtualClient.Version = 1.0;
}

function VirtualClient::__destruct(%this)
{
   // Thou shalt not spam
}

if(!isObject(VirtualClient))
   System.addClass(VirtualClient);
   
function VirtualClient::isAIControlled(%this)
{
    return false;
}

function VirtualClient::clientDetected(%this)
{
    // reduce console spam
}

function VirtualClient::isMounted(%this)
{
    // reduce console spam
    return false;
}

function spawnWalker(%client, %trans)
{
    %walkerName = "Stormguard";
    %block = %walkerName@"WalkerBase";

    %walker = new Player()
    {
        dataBlock = %block;
        isWalker = true;
        client = VirtualClient;
    };

    %typeTag = "Assault Walker";
    %nameTag = addTaggedString(%walkerName SPC %typeTag);
    %skinTag = addTaggedString("basebbot");
    %voiceTag = addTaggedString("Bot1");
    %walkerTarget = createTarget(%walker, %nameTag, %skinTag, %voiceTag, "", %client.team, 1.0); // obj, name, skin, voice, type, sensorgroup, pitch
//                  createTarget(%obj, %nameTag, %skinTag, %voiceTag, %typeTag, %sensorGroup, %voicePitch)

    %walker.setCloaked(true);
    %walker.schedule(3700, "playAudio", 0, VehicleAppearSound);
    %walker.schedule(1500, "playAudio", 1, WalkerPowerOn);
    %walker.schedule(4800, "setCloaked", false);
    %walker.setTransform(%trans);
    MissionCleanup.add(%walker);
    $VehicleTotalCount[%client.team, "Walker"]++;

    %walker.mountable = false;
    %walker.team = %client.team;
    %walker.originalteam = %client.team;
    %walker.outOfBounds = false;
    %walker.pilotObject = "";
    %walker.shortName = %walkerName;
    %client.player.lastVehicle = %walker;
    %walker.lastPilot = %client.player;
    %walker.mountedGuns = 0;
    %walker.targeter = false;
    %walker.isShielded = true;
    
    // sets walker target for viewing
    %walker.target = %walkerTarget;
    %walker.setTarget(%walkerTarget);
    setTargetSensorData(%walkerTarget, WalkerBaseSensor);
    
    // used for creating walker
    %walker.spawningClient = %client;
    
    %block.initWalker(%walker);
    %walker.isWalker = true; // Have to set this twice since it gets unset
    %block.equip(%walker, %client.player);
    %walker.mountImage(WalkerDefaultParam, 0);
    %block.schedule($g_TickTime, "onTick", %walker);

    setTargetSensorGroup(%walker.target, %client.team);
    %walker.spawningClient = "";

    schedule(5000, %walker, "setWalkerMountable", %walker, true);
    
    if(%client.isVehicleTeleportEnabled())
       %block.schedule(6000, "mountDriver", %walker, %client.player, true);
}

function StormguardWalkerBase::equip(%data, %walker, %player)
{
    %walker.gunammo = 800;
    %walker.cannonammo = 150;
    %walker.missileammo = 60;
    
    %walker.mountImage(StormguardAC2, 2);
    %walker.mountImage(StormguardAC3, 3);
    %walker.mountImage(StormguardMainCannon4, 4);
    %walker.mountImage(StormguardClusterMissile5, 5);
    %walker.mountImage(StormguardClusterMissile6, 6);
}

function Player::updateWalkerAmmoCount(%obj)
{
    if(!isObject(%obj) || %obj.isDead || !isObject(%obj.pilotObject))
        return;

    switch(%obj.selectedFireGroup)
    {
        case 0:
            %ammo = %obj.gunammo;

        case 1:
            %ammo = %obj.cannonammo;
            
        case 2:
            %ammo = %obj.missileammo;
    }

    commandToClient(%obj.pilotObject.client, 'setAmmoHudCount', $WalkerFiregroupName[%obj.selectedFireGroup]@":" SPC %ammo);
}

// this code may get run multiple times on startup for some reason, so only init
// things that may be set by modules once
function Armor::initWalker(%data, %walker)
{
    %walker.baseRechargeRate = %data.rechargeRate;
    %walker.selectedFireGroup = 0;
    %walker.powerState = true;
    %walker.shutdownTimeout = getSimTime() + 10000;
    %walker.powerTransition = false;
    %walker.fireTrigger = false;
    %walker.jetState = false;
}

function Armor::walkerTick(%this, %obj)
{
    %pilot = %obj.pilotObject;
    %pct = %obj.getDamagePct();
    
    
    if(%obj.tickCount % 8 == 0)
    {
        if(isObject(%pilot))
            %this.updatePilotStatus(%obj, %pilot);
            
        %obj.setHeat(1.0);
    }

    if(%pct >= 0.8)
    {
        if(%obj.tickCount % 12 == 0)
        {
            if(%obj.tickCount % 48 == 0)
                %obj.playAudio(3, EngineAlertSound);

            %dmgFXPos = vectorProject(%obj.getWorldBoxCenter(), %stVec, getRandom() * %data.cameraMaxDist * 0.5);
            createHitDebris(%dmgFXPos);
        }
    }
}

function Player::walkerCycleWeapon(%obj, %dir)
{
    for(%i = 0; %i < 8; %i++)
        %obj.setImageTrigger(%i, false);
                
     if(%dir $= "prev")
     {
          %obj.selectedFireGroup--;

          if(%obj.selectedFireGroup < 0)
               %obj.selectedFireGroup = $WalkerMaxFireGroups - 1;
     }
     else
     {
          %obj.selectedFireGroup++;

          if(%obj.selectedFireGroup >= $WalkerMaxFireGroups)
               %obj.selectedFireGroup = 0;
     }

     %obj.fireTrigger = false;
     
     %obj.updateWalkerAmmoCount();
}

function Player::walkerSelectKey(%obj, %num)
{
//    echo("Walker" SPC %obj SPC "selected key:" SPC %num);
}

function Armor::updatePilotStatus(%data, %obj, %pilot)
{
    if(%obj.powerState && !%pilot.isDead)
    {
        %tcword = %obj.targeter ? "Online" : "Offline";
        
//        commandToClient(%pilot.client, 'BottomPrint', "Speed:" SPC mFloor(vectorLen(%obj.getVelocity()) * 3.6) SPC "KPH - Missile Targeting Computer (pack):" SPC %tcword SPC "\nAmmo - Bullets:" SPC %obj.gunammo SPC "Plasma:" SPC %obj.cannonammo SPC " Missiles:" SPC %obj.missileammo NL "Restock at vehicle pad using (beacon).", 1, 3);
        commandToClient(%pilot.client, 'BottomPrint', "Speed:" SPC mFloor(vectorLen(%obj.getVelocity()) * 3.6) SPC "KPH - Missile Targeting Computer (pack):" SPC %tcword NL "Restock at vehicle pad using (healthkit).", 1, 2);
    }
}

function Armor::onWalkerCollision(%this, %obj, %col)
{
    // Handle walker collisions here
}

function Armor::mountDriver(%data, %obj, %player)
{
    if(isObject(%obj) && !%obj.isDead && %obj.team == %player.team)
    {
        %player.startFade(1000, 0, true);
        walkerMountPlayer(%obj, %player.client, true);
        %player.schedule(1500, "startFade", 1000, 0, false);
    }
}

function setWalkerMountable(%walker, %state)
{
    %walker.mountable = %state;
}

function walkerMountPlayer(%walker, %client, %force)
{
    %player = %client.player;
    %data = %player.getDatablock();
    
    if(%walker.mountable == false)
        return;

    if(%player.isDead)
        return;
        
    if((%data.flags & $ArmorMask::Pilot) == 0)
    {
        messageClient(%player.client, 'MsgArmorCantMountVehicle', '\c2This vehicle can only be used by scout or engineer armors.~wfx/misc/misc.error.wav');
        return;
    }
    
    // Prevent people from jacking your ride while you're in it
    if(isObject(%walker.pilotObject))
    {
        if(%force)
            %walker.getDatablock().unmountPilot(%walker);
        else
            return;
    }
    
    %player.ejectFlag();
    %player.lastWeapon = (%player.getMountedImage($WeaponSlot) == 0 ) ? "" : %player.getMountedImage($WeaponSlot).getName().item;
    %player.unmountImage($WeaponSlot);

    if(%player == %player.lastVehicle.lastPilot && %player.lastVehicle != %walker)
    {
        schedule(15000, %player.lastVehicle, "vehicleAbandonTimeOut", %player.lastVehicle);
          %player.lastVehicle.lastPilot = "";
    }

    if(%player.lastPilot !$= "" && %walker == %player.lastPilot.lastVehicle)
        %walker.lastPilot.lastVehicle = "";

    %walker.abandon = false;
    %walker.lastPilot = %player;
    %player.lastVehicle = %walker;
    %walker.team = %client.team;
    %walker.pilotObject = %player;
    %walker.ownerClient = %client;
    %client.walker = %walker;
//    %skin = %client.team == 1 ? addTaggedString("basebot") : addTaggedString("basebbot");

    setTargetRenderMask(%walker.target, (1 << $TargetInfo::HudRenderStart));
    setTargetSensorGroup(%walker.target, %client.team);
//    setTargetSkin(%walker.target, %skin);

    if(!%walker.powerState)
    {
        commandToClient(%client, 'BottomPrint', "Walker powered down, press JET to re-initialize reactor.", 0, 1);
        
        %client.camera.setTransform(%walker.getTransform());
        %client.camera.getDataBlock().setMode(%client.camera, "mechOrbit", %walker);
        %client.camera.setOrbitMode(%walker, %walker.getTransform(), 0.75, 9.5, 9.5);
        %client.setControlObject(%client.camera);
    }
    else
        %client.setControlObject(%walker);

    %walker.pilotObject.transient = true;
    %walker.pilotObject.initialBuriedPos = vectorAdd(getTerrainHeight(%walker.position), "0 0 -500");
    %walker.pilotObject.setTransform(%obj.initialBuriedPos SPC "0 0 1 0");
    %walker.playAudio(3, CameraGrenadeActivateSound);
    %walker.setTargeterStatus();
    %walker.updateWalkerAmmoCount();
    
   if(%client.observeCount > 0)
      resetObserveFollowWalker(%client, false);
}

function resetObserveFollowWalker(%client, %dismount)
{
   if(%dismount)
   {
      if(!isObject(%client.player))
         return;

      for( %i = 0; %i < %client.observeCount; %i++ )
      {
         %client.observers[%i].camera.setOrbitMode( %client.player, %client.player.getTransform(), 0.5, 4.5, 4.5);
      }
   }
   else
   {
      if(!%client.walker $= "")
         return;

      // grab the vehicle...
      %mount = %client.walker;
      if( %mount.getDataBlock().observeParameters $= "" )
         %params = %client.walker.getTransform();
      else
         %params = %mount.getDataBlock().observeParameters;

      for( %i = 0; %i < %client.observeCount; %i++ )
      {
         %client.observers[%i].camera.setOrbitMode(%mount, %mount.getTransform(), getWord( %params, 0 ), getWord( %params, 1 ), getWord( %params, 2 ));
      }
   }
}

function Armor::damageWalker(%data, %targetObject, %sourceObject, %position, %amount, %damageType, %momVec)
{
   if(%damageType == $DamageType::Impact)
      if(%sourceObject.getDamageState() $= "Destroyed")
         return;

   %targetClient = %targetObject.getOwnerClient();
   if(isObject(%mineSC))
      %sourceClient = %mineSC;
   else
      %sourceClient = isObject(%sourceObject) ? %sourceObject.getOwnerClient() : 0;

   %targetTeam = %targetClient.team;

   //if the source object is a player object, player's don't have sensor groups
   // if it's a turret, get the sensor group of the target
   // if its a vehicle (of any type) use the sensor group
   if (%sourceClient)
      %sourceTeam = %sourceClient.getSensorGroup();
   else if(%damageType == $DamageType::Suicide)
      %sourceTeam = 0;
   //--------------------------------------------------------------------------------------------------------------------
   // z0dd - ZOD, 4/8/02. Check to see if this turret has a valid owner, if not clear the variable.
   else if(isObject(%sourceObject) && %sourceObject.getClassName() $= "Turret")
   {
      %sourceTeam = getTargetSensorGroup(%sourceObject.getTarget());
      if(%sourceObject.owner !$="" && (%sourceObject.owner.team != %sourceObject.team || !isObject(%sourceObject.owner)))
      {
         %sourceObject.owner = "";
      }
   }
   //--------------------------------------------------------------------------------------------------------------------
   else if( isObject(%sourceObject) && %sourceObject.isVehicle())
      %sourceTeam = getTargetSensorGroup(%sourceObject.getTarget());
   else
   {
      if (isObject(%sourceObject) && %sourceObject.getTarget() >= 0 )
      {
         %sourceTeam = getTargetSensorGroup(%sourceObject.getTarget());
      }
      else
      {
         %sourceTeam = -1;
      }
   }

   // if teamdamage is off, and both parties are on the same team
   // (but are not the same person), apply no damage
   if(!$teamDamage && (%targetClient != %sourceClient) && (%targetTeam == %sourceTeam))
      return;

   if(%sourceClient && %sourceClient.vehicleHitSound && (%targetObject.lastHitFlags & $Projectile::PlaysHitSound))
   {
//      %sameteam = (%sourceClient.team == %targetObject.team);

//      if(%sameteam)
//         messageClient(%sourceClient, 'MsgClientHit', "~wfx/misc/whistle.wav");
//      else
         messageClient(%sourceClient, 'MsgClientHit', %sourceClient.vehicleHitWav);

      %sourceClient.playHitIndicator(%sameteam);
   }

    // Convert damage calculation here:
   if(%targetObject.isShielded && (%targetObject.lastHitFlags & $Projectile::IgnoreShields) == 0)
      %amount = %data.checkShields(%targetObject, %position, %amount, %damageType);
   
   %amount *= %targetObject.damageReduceFactor;
   %damageScale = %data.damageScale[%damageType];

   if(%damageScale !$= "")
      %amount *= %damageScale;

   if(%amount == 0)
      return;

   %targetObject.applyDamage(%amount);
   %targetClient.lastDamagedBy = %damagingClient;
   %targetClient.lastDamaged = getSimTime();

   // Mech health exceeded? BLOW THEM UP!
   if(%targetObject.getState() $= "Dead")
   {
      %targetObject.isDead = true;
      setWalkerMountable(%targetObject, false);
      $VehicleTotalCount[%targetObject.team, "Walker"]--; // potentially causes negative gains in vehicles

      %flingee = %targetObject.pilotObject;
      %targetObject.fireTrigger = false;
      
      if(isObject(%flingee))
      {
          %targetObject.getDatablock().unmountPilot(%targetObject);
      
          %flingee.setVelocity(%targetObject.getVelocity());
          %flingee.setPosition(%targetObject.getWorldBoxCenter());
          %flingee.client.setControlObject(%flingee);
          %flingee.getDataBlock().doDismount(%flingee, true);

          if(%flingee.inv[%flingee.lastWeapon])
              %flingee.use(%flingee.lastWeapon);

          if(%flingee.getMountedImage($WeaponSlot) == 0)
              %fingee.selectWeaponSlot(0);

          %xVel = 250.0 - (getRandom() * 500.0);
          %yVel = 250.0 - (getRandom() * 500.0);
          %zVel = (getRandom() * 100.0) + 5000.0;
          %flingVel = %xVel @ " " @ %yVel @ " " @ %zVel;
          %flingee.applyImpulse(%flingee.getTransform(), %flingVel);
//          %flingee.schedule(64, "damage", 0, %flingee.getPosition(), 2.5, $DamageType::Walker);
//          tracefor(96);
        }
         
      %targetObject.setMomentumVector(%momVec);
      %targetObject.schedule(256, "setPosition", "0 0 -10000");
      %targetObject.schedule(2000, "delete");
      createRemoteProjectile("LinearFlareProjectile", "MagIonOLDeathCharge", vectorRand(), %targetObject.getWorldBoxCenter(), 0, %targetObject);
      %targetObject.gib();
   }
}

function Armor::walkerTrigger(%data, %walker, %triggerNum, %val)
{
    switch(%triggerNum)
    {
        case 0:
            %data.walkerFire(%walker, %val);

        case 2:
            %data.walkerJump(%walker, %val);

        case 3:
            %data.walkerJet(%walker, %val);

        case 4:
            %data.walkerGrenade(%walker, %val);

        case 5:
            %data.walkerMine(%walker, %val);

        default:
            return;
    }
}

function Player::walkerUse(%walker, %data)
{
    %block = %walker.getDatablock();

    switch$(%data)
    {
        case "BackPack":
            %block.togglePack(%walker);

        case "Beacon":
            %block.toggleBeacon(%walker);

        case "RepairKit":
            %block.toggleRepairKit(%walker);

        default:
//            echo("Walker" SPC %walker SPC "attempted to used item:" SPC %data);
            return false;
    }
     
    return true;
}

function Armor::walkerFire(%data, %walker, %state)
{
    if(!%walker.powerState)
    {
        %walker.fireTrigger = false;
        return;
    }
        
    %walker.fireTrigger = %state;
    %client = %walker.pilotObject.client;
//    %time = getSimTime();

    switch(%walker.selectedFireGroup)
    {
        case 0:
            %walker.setImageTrigger(2, %state);
            %walker.setImageTrigger(3, %state);

        case 1:
            %walker.setImageTrigger(4, %state);
            
        case 2:
            %walker.setImageTrigger(5, %state);
            %walker.setImageTrigger(6, %state);
    }
}

function Armor::walkerJump(%data, %walker, %state)
{
    if(!%state)
        return;
        
    %data.unmountPilot(%walker);
}

function Armor::unmountPilot(%data, %walker)
{
    %pilot = %walker.pilotObject;
    
    if(!isObject(%pilot))
        return;

    for(%i = 0; %i < 8; %i++)
        %walker.setImageTrigger(%i, false);
        
    %walker.fireTrigger = false;

    %pilot.setVelocity(%walker.getVelocity());
    %pilot.setPosition(vectorProject(%walker.getWorldBoxCenter(), %walker.getForwardVector(), 5));
    %pilot.client.setControlObject(%pilot);
    %pilot.getDataBlock().doDismount(%pilot, true);

    if(%pilot.inv[%pilot.lastWeapon])
        %pilot.use(%pilot.lastWeapon);

    if(%pilot.getMountedImage($WeaponSlot) == 0)
        %pilot.selectWeaponSlot(0);

    %pilot.transient = false;
    %walker.playAudio(3, CameraGrenadeAttachSound);

    %pilot.setInvincibleMode(0 ,0.00);
    %pilot.setInvincible( false );

    %walker.ownerClient = VirtualClient;
    %walker.pilotObject.client.walker = "";
    %walker.pilotObject = "";

   if(%pilot.client.observeCount > 0)
      resetObserveFollowWalker(%pilot.client, true);
      
//    messageClient(%pilot.client, 'MsgEjectMech', '\c2Ejected from mech, use the (beacon) key to re-enter.');
}

function Armor::walkerJet(%data, %walker, %state)
{
    if(%data.hasJumpJets)
        %walker.jetState = %state;
        
    if(!%walker.powerState)
        %walker.toggleShutdown(%data);
}

function Armor::walkerGrenade(%data, %walker, %state)
{
    if(!%state)
        return;
}

function Player::toggleShutdown(%walker, %data)
{
    %time = getSimTime();
    %client = %walker.pilotObject.client;
    
    if(%time < %walker.shutdownTimeout)
    {
        messageClient(%client, 'msgWalkerPowerCycle', '\c2Walker power system cycling, please wait.');
        return false;
    }

    %walker.fireTrigger = false;
    %walker.shutdownTimeout = %time + 10000;
    %walker.powerState = !%walker.powerState;
    %sound = %walker.powerState ? "WalkerPowerOn" : "WalkerPowerOff";
    %walker.setPassiveJammed(true);
    %walker.powerTransition(5000);
    %walker.playAudio(1, %sound);
    
    if(%walker.powerState)
    {
        messageClient(%client, 'MsgWalkerStartup', '\c2%1 initiating startup sequence.', %walker.shortName);
        %client.schedule(5000, "setControlObject", %walker);
    }
    else
    {
        messageClient(%client, 'MsgWalkerShutdown', '\c2%1 initiating shutdown sequence.', %walker.shortName);

        %client.camera.setTransform(%walker.getTransform());
        %client.camera.getDataBlock().setMode(%client.camera, "mechOrbit", %walker);
        %client.camera.setOrbitMode(%walker, %walker.getTransform(), 0.75, 9.5, 9.5);
        %client.setControlObject(%client.camera);
        commandToClient(%client, 'BottomPrint', "Walker powered down, press JET to re-initialize reactor.", 0, 1);
    }

    return true;
}

function Player::powerTransition(%walker, %time)
{
    %walker.powerTransition = !%walker.powerTransition;
    
    if(%walker.powerTransition)
        %walker.schedule(%time, "powerTransition", 0);
}

function Armor::walkerMine(%data, %walker, %state)
{
    if(!%state)
        return;
}

function Player::setTargeterStatus(%walker)
{
    %image = %walker.targeter ? WalkerMissileParam : WalkerDefaultParam;
    %sound = %walker.targeter ? "TurretPackActivateSound" : "BlasterDryFireSound";
    %ret = %walker.targeter ? "MissileLauncher" : "Blaster";
    
    %walker.unmountImage(0);
    %walker.mountImage(%image, 0);
    %walker.playAudio(1, %sound);
    %walker.pilotObject.client.setWeaponsHudActive(%ret);
}

function Armor::togglePack(%data, %obj)
{
    %obj.targeter = !%obj.targeter;

    %obj.setTargeterStatus();
}

function Armor::toggleBeacon(%data, %walker)
{
    // none
}

function Armor::toggleRepairKit(%data, %walker)
{
    %time = getSimTime();

    if(%time > %walker.nextRestockTime)
    {
        InitContainerRadiusSearch(%walker.position, 10, $TypeMasks::StaticShapeObjectType);

        while((%hit = containerSearchNext()) != 0)
        {
            if(%hit.team == %walker.ownerClient.team && %hit.getDatablock().shapeFile $= "vehicle_pad.dts")
            {
                // Reload vehicle
                %walker.play3d(MissileReloadSound);
                messageClient(%walker.ownerClient, 'MsgWalkerReload', '\c2Walker parked on pad, ammo restocked and armor repaired.');

                %walker.gunammo = 800;
                %walker.cannonammo = 150;
                %walker.missileammo = 60;
                %walker.nextRestockTime = %time + 120000;
                
                // Repair vehicle fully
                zapVehicle(%walker, FXHealGreen);
                %walker.setDamageLevel(0);
                %walker.setEnergyLevel(%walker.getMaxEnergy());
                %walker.updateWalkerAmmoCount();
                return;
            }
        }
    }
    else
        messageClient(%walker.ownerClient, 'MsgWalkerRecentReload', '\c2Walker has been recently restocked, come back later.');
}
