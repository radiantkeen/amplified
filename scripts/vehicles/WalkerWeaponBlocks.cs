// Datablock cache for walker weapon slots

// Stormguard
//------------------------------------------------------------------------------
datablock ShapeBaseImageData(StormguardEnergy2)
{
   className = WeaponImage;
   shapeFile = "turret_elf_large.dts";
   offset = $WalkerHardpointPos["Stormguard", 2];
   rotation = "0 1 0 0";
   mountPoint = 0;
   subImage = true;

   usesEnergy = true;
   minEnergy = -1;
   fireEnergy = -1;

   //--------------------------------------
   stateName[0]             = "Activate";
   stateSequence[0]         = "Activate";
   stateTimeoutValue[0]        = 0.01;
   stateTransitionOnTimeout[0] = "Ready";
   //--------------------------------------
   stateName[1]       = "Ready";
   stateSequence[1]         = "Deploy";
   stateTransitionOnTriggerDown[1] = "Fire";
   //--------------------------------------
   stateName[2]             = "Fire";
   stateFire[2]             = true;
   stateSequence[2]         = "Fire";
   stateTimeoutValue[2]          = 0.1;
   stateTransitionOnTriggerUp[2] = "Ready";
   stateTransitionOnTimeout[2] = "Fire";
};

datablock ShapeBaseImageData(StormguardEnergy3) : StormguardEnergy2
{
   offset = $WalkerHardpointPos["Stormguard", 3];
   rotation = "0 1 0 0";
};

datablock ShapeBaseImageData(StormguardEnergy6) : StormguardEnergy2
{
   offset = $WalkerHardpointPos["Stormguard", 6];
   rotation = "0 1 0 0";
};

datablock ShapeBaseImageData(StormguardLaser2) : StormguardEnergy2
{
   shapeFile = "TR2weapon_shocklance.dts";
   offset = $WalkerHardpointPos["Stormguard", 2];
   rotation = "0 1 0 0";
};

datablock ShapeBaseImageData(StormguardLaser3) : StormguardLaser2
{
   offset = $WalkerHardpointPos["Stormguard", 3];
   rotation = "0 1 0 0";
};

datablock ShapeBaseImageData(StormguardLaser6) : StormguardLaser2
{
   offset = $WalkerHardpointPos["Stormguard", 6];
   rotation = "0 1 0 0";
};

datablock ShapeBaseImageData(StormguardCannon2) : StormguardEnergy2
{
   shapeFile = "turret_mortar_large.dts";
   offset = $WalkerHardpointPos["Stormguard", 2];
   rotation = "0 1 0 0";
};

datablock ShapeBaseImageData(StormguardCannon3) : StormguardCannon2
{
   offset = $WalkerHardpointPos["Stormguard", 3];
   rotation = "0 1 0 0";
};

datablock ShapeBaseImageData(StormguardCannon6) : StormguardCannon2
{
   offset = $WalkerHardpointPos["Stormguard", 6];
   rotation = "0 1 0 0";
};

datablock ShapeBaseImageData(StormguardPlasma2) : StormguardEnergy2
{
   shapeFile = "TR2weapon_mortar.dts";
   offset = $WalkerHardpointPos["Stormguard", 2];
   rotation = "0 1 0 0";
};

datablock ShapeBaseImageData(StormguardPlasma3) : StormguardPlasma2
{
   offset = $WalkerHardpointPos["Stormguard", 3];
   rotation = "0 1 0 0";
};

datablock ShapeBaseImageData(StormguardPlasma6) : StormguardPlasma2
{
   offset = $WalkerHardpointPos["Stormguard", 6];
   rotation = "0 1 0 0";
};

datablock TracerProjectileData(WalkerChaingunBullet) : AssaultChaingunBullet
{
   directDamage        = 0.225;
   directDamageType    = $DamageType::TankChaingun;
   dryVelocity       = 425.0;
   wetVelocity       = 100.0;
   velInheritFactor  = 1.0;
   fizzleTimeMS      = 800;
   lifetimeMS        = 1000;
   explodeOnDeath    = false;
   reflectOnWaterImpactAngle = 0.0;
   explodeOnWaterImpact      = false;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 1000;

   activateDelayMS   = -1;
   playRandomChaingunSound = true;
   explosion = ChaingunExplosion;
};

datablock ShapeBaseImageData(StormguardAC2) : StormguardEnergy2
{
   shapeFile = "turret_tank_barrelchain.dts"; // TR2weapon_chaingun
   offset = $WalkerHardpointPos["Stormguard", 2];
   rotation = "0 1 0 0";
   
   projectile = WalkerChaingunBullet;
   projectileType = TracerProjectile;
   projectileSpread = 6.0;

   casing              = WAutoShellDebris;
   shellExitDir        = "1.0 0.3 1.0";
   shellExitOffset     = "0.15 -0.56 -0.1";
   shellExitVariance   = 15.0;
   shellVelocity       = 6.0;
   
//   defaultModeFireSound = "MILFireSound";
   defaultModeFailSound = "ChaingunDryFireSound";
   
   //--------------------------------------
   stateName[0]             = "Activate";
   stateSequence[0]         = "Activate";
   stateTimeoutValue[0]        = 0.01;
   stateTransitionOnTimeout[0] = "Ready";
   //--------------------------------------
   stateName[1]       = "Ready";
   stateSequence[1]         = "Spin";
   stateSpinThread[1]         = Stop;
   stateTransitionOnTriggerDown[1] = "Fire";
   //--------------------------------------
   stateName[2]             = "Fire";
   stateFire[2]             = true;
   stateSpinThread[2]       = FullSpeed;
   stateEjectShell[2]       = true;
   stateSequenceRandomFlash[2] = true;
   stateSequence[2]         = "Fire_Vis";
   stateSound[2]                       = AssaultChaingunFireSound;
   stateScript[2]                      = "onFire";
   stateTimeoutValue[2]          = 0.1;
   stateTransitionOnTriggerUp[2] = "Ready";
   stateTransitionOnTimeout[2] = "Fire";
};

function StormguardAC2::validateFire(%data, %obj, %vehicleID)
{
    if(%obj.cloakjammed)
    {
        %obj.setImageTrigger(2, false);
        return false;
    }

    if(%obj.gunammo < 1)
    {
        %obj.setImageTrigger(2, false);
        return false;
    }
 
    %obj.gunammo--;

    %obj.updateWalkerAmmoCount();
    return true;
}

datablock ShapeBaseImageData(StormguardAC3) : StormguardAC2
{
   offset = $WalkerHardpointPos["Stormguard", 3];
   rotation = "0 1 0 0";
};

function StormguardAC3::validateFire(%data, %obj, %vehicleID)
{
    if(%obj.cloakjammed)
    {
        %obj.setImageTrigger(3, false);
        return false;
    }

    if(%obj.gunammo < 1)
    {
        %obj.setImageTrigger(3, false);
        return false;
    }

    %obj.gunammo--;

    %obj.updateWalkerAmmoCount();
    return true;
}

datablock ShapeBaseImageData(StormguardAC6) : StormguardAC2
{
   shapeFile = "turret_tank_barrelchain.dts";
   offset = $WalkerHardpointPos["Stormguard", 6];
   rotation = "0 1 0 90";
};

datablock ShapeBaseImageData(StormguardMainCannon2) : StormguardEnergy2
{
   shapeFile = "turret_tank_barrelmortar.dts";
   offset = $WalkerHardpointPos["Stormguard", 2];
   rotation = "0 1 0 0";
};

datablock ShapeBaseImageData(StormguardMainCannon3) : StormguardMainCannon2
{
   offset = $WalkerHardpointPos["Stormguard", 3];
   rotation = "0 1 0 0";
};

datablock LinearFlareProjectileData(WalkerPlasmaBolt) : PlasmaBarrelBolt
{
   indirectDamage      = 1.25;
   damageRadius        = 10.0;
   kickBackStrength    = 500;
   radiusDamageType    = $DamageType::PlasmaCannon;
   explosion           = PlasmaBarrelBoltExplosion;
   splash              = PlasmaSplash;

   radialStatusEffect  = "BurnEffect";
   statusEffectChance  = 0.4;
   statusEffectMask    = $TypeMasks::PlayerObjectType;

   baseEmitter         = DiscMistEmitter;

   dryVelocity       = 150;
   wetVelocity       = -1;
   velInheritFactor  = 1.0;
   fizzleTimeMS      = 2000;
   lifetimeMS        = 3000; // z0dd - ZOD, 4/25/02. Was 6000
   explodeOnDeath    = false;
   reflectOnWaterImpactAngle = 0.0;
   explodeOnWaterImpact      = true;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = -1;

   activateDelayMS = -1;
};

datablock ShapeBaseImageData(StormguardMainCannon4) : StormguardMainCannon2
{
   shapeFile = "turret_mortar_large.dts";
   offset = $WalkerHardpointPos["Stormguard", 6];
   rotation = "0 1 0 0";
   
   projectile = WalkerPlasmaBolt;
   projectileType = LinearFlareProjectile;
   projectileSpread = 0.0;

   defaultModeFireSound = "PBLFireSound";
   defaultModeFailSound = "MortarDryFireSound";
   
   //--------------------------------------
   stateName[0]             = "Activate";
//   stateSequence[0]         = "Deploy";
   stateTimeoutValue[0]        = 0.01;
   stateTransitionOnTimeout[0] = "Ready";
   //--------------------------------------
   stateName[1]       = "Ready";
   stateSequence[1]         = "Deploy";
   stateSpinThread[1]         = Stop;
   stateTransitionOnTriggerDown[1] = "Fire";
   //--------------------------------------
   stateName[2]             = "Fire";
   stateFire[2]             = true;
   stateSpinThread[2]       = FullSpeed;
   stateEjectShell[2]       = true;
//   stateSequenceRandomFlash[2] = true;
   stateSequence[2]         = "Fire";
//   stateSound[2]                       = AssaultChaingunFireSound;
   stateScript[2]                      = "onFire";
   stateTimeoutValue[2]          = 2.0;
   stateTransitionOnTriggerUp[2] = "Ready";
   stateTransitionOnTimeout[2] = "Fire";
};

function StormguardMainCannon4::validateFire(%data, %obj, %vehicleID)
{
    if(%obj.cloakjammed)
        return false;

    if(%obj.cannonammo < 1)
        return false;

    %obj.cannonammo--;
    
    %obj.updateWalkerAmmoCount();
    return true;
}

datablock ShapeBaseImageData(StormguardMainCannon6) : StormguardMainCannon2
{
   offset = $WalkerHardpointPos["Stormguard", 6];
   rotation = "0 1 0 0";
};

datablock ShapeBaseImageData(StormguardMissile4) : StormguardEnergy2
{
   shapeFile = "stackable1s.dts";
   offset = $WalkerHardpointPos["Stormguard", 4];
   rotation = "0 0 1 0";
};

datablock ShapeBaseImageData(StormguardMissile5) : StormguardMissile4
{
   offset = $WalkerHardpointPos["Stormguard", 5];
   rotation = "0 0 1 0";
};

datablock ShapeBaseImageData(StormguardMissile6) : StormguardMissile4
{
   offset = $WalkerHardpointPos["Stormguard", 6];
   rotation = "0 0 1 0";
};

datablock ShapeBaseImageData(StormguardLightMissile4) : StormguardEnergy2
{
   shapeFile = "turret_missile_large.dts";
   offset = $WalkerHardpointPos["Stormguard", 4];
   rotation = "0 0 1 0";
};

datablock ShapeBaseImageData(StormguardLightMissile5) : StormguardLightMissile4
{
   offset = $WalkerHardpointPos["Stormguard", 5];
   rotation = "0 0 1 0";
};

datablock ShapeBaseImageData(StormguardLightMissile6) : StormguardLightMissile4
{
   offset = $WalkerHardpointPos["Stormguard", 6];
   rotation = "0 0 1 0";
};

datablock SeekerProjectileData(LRMMissile) : ShoulderMissile
{
   indirectDamage      = 0.75;
   damageRadius        = 5.0;
   radiusDamageType    = $DamageType::Missile;
   kickBackStrength    = 500;

   flags               = $Projectile::PlayerFragment | $Projectile::PlaysHitSound;
   ticking             = false;
   headshotMultiplier  = 1.0;

   explosion           = "MissileExplosion";

   baseEmitter         = MissileSmokeEmitter;
   delayEmitter        = MissileFireEmitter;
   puffEmitter         = MissilePuffEmitter;
   bubbleEmitter       = GrenadeBubbleEmitter;
   bubbleEmitTime      = 1.0;

   lifetimeMS          = 6000;
   muzzleVelocity      = 150.0;
   maxVelocity         = 300.0;
   turningSpeed        = 127.0;
   acceleration        = 50.0;

   proximityRadius = 1;

   terrainAvoidanceSpeed         = 1;
   terrainScanAhead              = 1;
   terrainHeightFail             = 1;
   terrainAvoidanceRadius        = 1;

   flareDistance = 5;
   flareAngle    = 5;
};

datablock ShapeBaseImageData(StormguardClusterMissile4) : StormguardEnergy2
{
   shapeFile = "stackable2m.dts";
   offset = $WalkerHardpointPos["Stormguard", 4];
   rotation = "1 0 0 90";
   
   projectile = LRMMissile;
   projectileType = SeekerProjectile;
   projectileSpread = 12.0;

   staggerCount = 4;
   staggerDelay = 200;
   useForwardVector = true;

   defaultModeFireSound = "MILFireSound";
   defaultModeFailSound = "MortarDryFireSound";

   //--------------------------------------
   stateName[0]             = "Activate";
   stateSequence[0]         = "Activate";
   stateTimeoutValue[0]        = 0.01;
   stateTransitionOnTimeout[0] = "Ready";
   //--------------------------------------
   stateName[1]       = "Ready";
   stateSequence[1]         = "Spin";
   stateSpinThread[1]         = Stop;
   stateTransitionOnTriggerDown[1] = "Fire";
   //--------------------------------------
   stateName[2]             = "Fire";
   stateFire[2]             = true;
   stateSpinThread[2]       = FullSpeed;
   stateEjectShell[2]       = true;
   stateSequenceRandomFlash[2] = true;
//   stateSound[2]                       = MILFireSound;
   stateScript[2]                      = "onFire";
   stateTimeoutValue[2]          = 6.0;
   stateTransitionOnTriggerUp[2] = "Ready";
   stateTransitionOnTimeout[2] = "Fire";
};

datablock ShapeBaseImageData(StormguardClusterMissile5) : StormguardClusterMissile4
{
   offset = $WalkerHardpointPos["Stormguard", 4];
   rotation = "1 0 0 90";
};

function StormguardClusterMissile5::validateFire(%data, %obj, %vehicleID)
{
    if(%obj.cloakjammed)
        return false;

    if(%obj.missileammo < 1)
        return false;

    %obj.missileammo--;
    
    %obj.updateWalkerAmmoCount();
    return true;
}

function StormguardClusterMissile5::spawnProjectile(%data, %obj, %slot)
{
    %proj = Parent::spawnProjectile(%data, %obj, %slot);

    MissileSet.add(%proj);
    %target = %obj.getLockedTarget();
    
    if(%target)
         %proj.setObjectTarget(%target);
    else if(%obj.isLocked())
        %proj.setPositionTarget(%obj.getLockedPosition());
    else
        %proj.setNoTarget();

    return %proj;
}

datablock ShapeBaseImageData(StormguardClusterMissile6) : StormguardClusterMissile4
{
   offset = $WalkerHardpointPos["Stormguard", 5];
   rotation = "1 0 0 90";
};

function StormguardClusterMissile6::validateFire(%data, %obj, %vehicleID)
{
    if(%obj.cloakjammed)
        return false;

    if(%obj.missileammo < 1)
        return false;

    %obj.missileammo--;

    %obj.updateWalkerAmmoCount();
    return true;
}

function StormguardClusterMissile6::spawnProjectile(%data, %obj, %slot)
{
    %proj = Parent::spawnProjectile(%data, %obj, %slot);

    MissileSet.add(%proj);
    %target = %obj.getLockedTarget();
    
    if(%target)
         %proj.setObjectTarget(%target);
    else if(%obj.isLocked())
        %proj.setPositionTarget(%obj.getLockedPosition());
    else
        %proj.setNoTarget();

    return %proj;
}
