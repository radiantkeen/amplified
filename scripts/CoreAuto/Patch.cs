// Patches contributed by
// Ragora
// calvin_balke

// Remove 250ms limit on GrenadeProjectiles
memPatch("6339D0", "81BB4801000000000000");
memPatch("7ACFC6", "306D732900");
memPatch("6339EF", "C7834801000000000000");

// Completely disables CRC checking I believe (otherwise known as the "Stop AMD Radeon and WINE from disconnecting" patch
memPatch("6A3059","EB");
