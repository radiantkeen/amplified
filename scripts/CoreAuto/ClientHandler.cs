// Handles all clientside communications and hooks
//------------------------------------------------------------------------------
// Client registration

function serverCmdRegisterMDClient(%client)
{
   %client.hasMD2Client = true;
   commandToClient(%client, 'ClientEchoText', %client.guid@"@"@%client.getAddress()@": Meltdown 2 v"@EngineBase.ModVersion@" activated client successfully.");
}

function serverCmdTriconRegisterClient(%client)
{
     %client.usesTricon = true;
     echo("Client "@%client@" ("@getTaggedString(%client.name)@") using Tricon 2 Client.");
}

function serverCmdConstructionQueryServer(%client)
{
     commandToClient(%client,'QueryServerReply',"Meltdown 2 Server",EngineBase.ModVersion,"Developed by Keen - http://forums.radiantage.net","2");
}

function serverCmdConstructionRegisterClient(%client, %version)
{
     %client.usesConstruction = true;
     echo("Client "@%client@" ("@getTaggedString(%client.name)@") using Construction Client "@%version@".");
}

function serverCmdCheckendTilt(%client)
{
//     %client.endTiltCount++;
}

function serverCmdCheckHtilt(%client)
{
//     %client.HtiltCount++;
}

function serverCmdpracticeHudInitialize(%client)
{
     // Thou shalt not spam
}

function serverCmdEmote(%client,%anim) {
	%plyr = %client.player;
	if (isObject(%plyr)) {
		switch$ (%anim) {
			case "SitDown":
				%plyr.setActionThread("sitting",true);
			case "Squat":
				%plyr.setActionThread("scoutRoot",true);
			case "Jig":
				%plyr.setActionThread("ski",true);
			case "LieDown":
				%plyr.setActionThread("death9",true);
			case "HeartAttack":
				%plyr.setActionThread("death8",true);
			case "SuckerPunched":
				%plyr.setActionThread("death11",true);
		}
	}
}
