//-----------------------------------------------------------------------------
// Debug Mode

function debugMode(%bool)
{
    System.debugMode = %bool;

    if(%bool)
        activatePackage(EngineDebugMode);
    else
        deactivatePackage(EngineDebugMode);

    showDebugMode();
}

function isDebugMode()
{
    return System.debugMode;
}

function showDebugMode()
{
    %word = System.debugMode ? "ON" : "OFF";
    error("Debug mode is" SPC %word);
}

function decho(%msg)
{
    error("DEBUG:" SPC %msg);
}

// Debug Mode Package
// Put functions in here you want to modify when debug mode is set

package EngineDebugMode {

function echo(%msg)
{
    Parent::echo(timestamp(1) SPC %msg);
}

function error(%msg)
{
    Parent::error(timestamp(1) SPC %msg);
}

};
