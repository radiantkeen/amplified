// Misc functions not fitting a specific role put here
// Any class and/or override functions should go in the Server folder to be executed at server startup
// String table

//------------------------------------------------------------------------------
// Universal Typemasks

$TypeMasks::AllObjectType = $TypeMasks::PlayerObjectType | $TypeMasks::VehicleObjectType | $TypeMasks::StationObjectType | $TypeMasks::GeneratorObjectType | $TypeMasks::SensorObjectType | $TypeMasks::TurretObjectType | $TypeMasks::TerrainObjectType | $TypeMasks::InteriorObjectType | $TypeMasks::ForceFieldObjectType | $TypeMasks::StaticShapeObjectType | $TypeMasks::MoveableObjectType | $TypeMasks::DamagableItemObjectType | $TypeMasks::StaticObjectType;
$TypeMasks::DefaultLOSType = $TypeMasks::TerrainObjectType | $TypeMasks::InteriorObjectType | $TypeMasks::StaticObjectType | $TypeMasks::PlayerObjectType | $TypeMasks::VehicleObjectType;
$TypeMasks::CoreObjectType = $TypeMasks::PlayerObjectType | $TypeMasks::VehicleObjectType | $TypeMasks::StationObjectType | $TypeMasks::GeneratorObjectType | $TypeMasks::SensorObjectType | $TypeMasks::TurretObjectType;
$TypeMasks::NonTransparent = $TypeMasks::PlayerObjectType | $TypeMasks::VehicleObjectType | $TypeMasks::StationObjectType | $TypeMasks::GeneratorObjectType | $TypeMasks::SensorObjectType | $TypeMasks::TurretObjectType | $TypeMasks::TerrainObjectType | $TypeMasks::InteriorObjectType | $TypeMasks::StaticShapeObjectType | $TypeMasks::MoveableObjectType;
$TypeMasks::DamageableObjectType = $TypeMasks::PlayerObjectType | $TypeMasks::VehicleObjectType | $TypeMasks::StationObjectType | $TypeMasks::GeneratorObjectType | $TypeMasks::SensorObjectType | $TypeMasks::TurretObjectType | $TypeMasks::ForceFieldObjectType;
$TypeMasks::EverythingType = -1;

//------------------------------------------------------------------------------
// Color defs

$HudTextColors::White = "<color:FFFFFF>";
$HudTextColors::Black = "<color:FFFFFF>";
$HudTextColors::Red = "<color:FF0000>";
$HudTextColors::Green = "<color:00FF00>";
$HudTextColors::Blue = "<color:0000FF>";
$HudTextColors::Yellow = "<color:FFFF00>";
$HudTextColors::Default = "<color:42DBEA>";

//------------------------------------------------------------------------------
// Functions

function createSlotExtension(%obj, %slot, %shapeName)
{
    if(!%slot) // default slot
      %slot = 0;

    if(%shapeName $= "")
       %shapeName = FXPoint;

    %ext = new StaticShape()
    {
       dataBlock = %shapeName;
    };

    MissionCleanup.add(%ext);
    %ext.isExtension = true;

    %obj.mountObject(%ext, %slot);
    %obj.slotExtension[%slot] = %ext;
    %obj.slotExtension[%slot].srcObj = %obj;
}

function isNearForcefield(%obj, %dist)
{
  	InitContainerRadiusSearch(%obj.getPosition(), %dist, $TypeMasks::ForceFieldObjectType);

	return containerSearchNext() ? true : false;
}

function createColorGraph(%symbol, %maxLines, %pct, %color1, %color2, %color3, %noFillColor, %firstPct, %secondPct)
{
     %color = %pct > %firstPct ? (%pct > %secondPct ? %color3 : %color2) : %color1;

     // Could go higher, but mech uses 53 max
     if(%maxLines > 53)
          %maxLines = 53;

     %displayLines = mCeil(%pct * %maxLines);
     %leftLines = %maxLines - %displayLines;
     %level = %color;

     for(%i = 0; %i < %displayLines; %i++)
          %level = %level@%symbol;

     %level = %level@%noFillColor;

     for(%i = 0; %i < %leftLines; %i++)
          %level = %level@%symbol;

     %graph = "["@%level@"<color:42dbea>]";

     return %graph;
}

function createMonoGraph(%symbol, %maxLines, %pct, %color1, %color2, %color3, %noFillColor, %firstPct, %secondPct)
{
     %color = %pct > %firstPct ? (%pct > %secondPct ? %color3 : %color2) : %color1;

     // Could go higher, but mech uses 53 max
     if(%maxLines > 53)
          %maxLines = 53;

     %displayLines = mCeil(%pct * %maxLines);
     %leftLines = %maxLines - %displayLines;
     %level = %color;

     for(%i = 0; %i < %displayLines; %i++)
          %level = %level@%symbol;

     %level = %level@%noFillColor;

     for(%i = 0; %i < %leftLines; %i++)
          %level = %level@%symbol;

     %graph = "["@%level@"<color:42dbea>]";

     return %graph;
}

function createGraph(%symbol, %maxLines, %pct, %blankSymbol)
{
     // Could go higher, but mech uses 53 max
     if(%maxLines > 53)
          %maxLines = 53;

     %displayLines = mCeil(%pct * %maxLines);
     %leftLines = %maxLines - %displayLines;
     %level = "";

     for(%i = 0; %i < %displayLines; %i++)
          %level = %level@%symbol;

     for(%i = 0; %i < %leftLines; %i++)
          %level = %level@%blankSymbol;

     %graph = "["@%level@"]";

     return %graph;
}

function setFlightCeiling(%val)
{
    %missionArea = nameToID("MissionGroup/MissionArea");

    if(%missionArea != -1)
        %missionArea.FlightCeiling = %val;
}

function changeServerAllowAliases(%bool)
{
   $Host::NoSmurfs = %bool;
}

function changeServerPlayerCount(%num)
{
   $Host::MaxPlayers = %num;
}

function changeAdminPassword(%pass)
{
   if(%pass)
      $Host::AdminPassword = %pass;
   else
      $Host::AdminPassword = "";
}

function changeServerPassword(%pass)
{
   if(%pass)
      $Host::Password = %pass;
   else
      $Host::Password = "";
}

function createRandomSimSet()
{
    %rnd = "SSet"@mFloor(getRandom() * 2147483647);

    if(!isObject(%rnd))
        %set = new SimSet(%rnd);
    else
        %set = createRandomSimSet();

    return %set;
}

function createRandomSimGroup()
{
    %rnd = "SGrp"@mFloor(getRandom() * 2147483647);

    if(!isObject(%rnd))
        %set = new SimGroup(%rnd);
    else
        %set = createRandomSimGroup();

    return %set;
}

// todo: castPiercingRay, which is basically a repeating version of this, adds to set
function castRay(%pos, %vec, %dist, %mask, %ignoreObj)
{
    %end = vectorProject(%pos, %vec, %dist);
    %ignore = %ignoreObj !$= "" ? %ignoreObj : 0;
    %ray = containerRayCast(%pos, %end, %mask, %ignore);
    
    if(%ray)
    {
        %hitInfo = new ScriptObject()
        {
            class = "RayCastInfo";
            hitObj = getWord(%ray, 0);
            hitPos = getWords(%ray, 1, 3);
            hitNormal = getWords(%ray, 4, 6);
        };

        %hitInfo.hitDist = vectorDist(%pos, %hitInfo.hitPos);
        %hitInfo.schedule($g_TickTime, "delete");
        
        return %hitInfo;
    }
    else
        return false;
}

// todo: forEachInSet()
function radiusSearch(%pos, %dist, %mask)
{
    %count = 0;
    %searchObjects = new SimSet();
    
    InitContainerRadiusSearch(%pos, %dist, %mask);
    
    while((%found = containerSearchNext()) != 0)
    {
        %searchObjects.add(%found);
        %count++;
    }
    
    if(%count)
    {
        %searchObjects.schedule($g_TickTime, "delete");
        
        return %searchObjects;
    }
    else
    {
        %searchObjects.delete();
        return false;
    }
}

function radiusSearchExplosion(%pos, %dist, %mask, %shadowMask)
{
    %count = 0;
    %searchObjects = new SimSet();

    InitContainerRadiusSearch(%pos, %dist, %mask);

    while((%found = containerSearchNext()) != 0)
    {
        %coverage = calcExplosionCoverage(%found.getWorldBoxCenter(), %found, %shadowMask);
        
        if(%coverage == 0)
            continue;
    
        %searchObjects.add(%found);
        %count++;
    }

    if(%count)
        return %searchObjects;
    else
    {
        %searchObjects.delete();
        return false;
    }
}

function radiusSearchLOS(%pos, %dist, %mask)
{
    %count = 0;
    %searchObjects = new SimSet();

    InitContainerRadiusSearch(%pos, %dist, %mask);

    while((%found = containerSearchNext()) != 0)
    {
        if(castRay(%pos, VectorNormalize(VectorSub(%found.getWorldBoxCenter(), %pos)), %dist, %mask, 0))
        {
            %searchObjects.add(%found);
            %count++;
        }
    }

    if(%count)
        return %searchObjects;
    else
    {
        %searchObjects.delete();
        return false;
    }
}

// forcefield.cs - fragment player modification
function killAllPlayersWithinZone( %data, %obj, %team )
{
   for( %c = 0; %c < ClientGroup.getCount(); %c++ )
   {
      %client = ClientGroup.getObject(%c);
      if( isObject( %client.player ) )
      {
         if( %forceField = %client.player.isInForceField() )// isInForceField() will return the id of the ff or zero
         {
            if( %forceField == %obj )
            {
               if( %team !$= "" && %team == %client.team )
                  return;
               else
               {
                  %client.player.blowup(); // chunkOrama!
                  %client.player.isFragmented = true;
                  %client.player.scriptkill($DamageType::ForceFieldPowerup);
               }
            }
         }
      }
   }
}

$g_RandStr = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
$g_RandLen = strLen($g_RandStr);

function generateRandomString(%length)
{
    %len = 0;
    %lastchar = "";
    %char = "";
    %str = "";
    
    while(%len < %length)
    {
        %char = getSubStr($g_RandStr, getRandom(0, $g_RandLen), 1);
        
        if(%char $= %lastchar)
            continue;
            
        %str = %str @ %char;
        %lastchar = %char;
        %len++;
    }
    
    return %str;
}
