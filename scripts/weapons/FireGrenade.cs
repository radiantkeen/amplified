// Extended Grenades
// ------------------------------------------------------------------------

datablock AudioProfile(FlametongueExpSound)
{
   filename    = "fx/Bonuses/upward_passback1_bomb.wav";
   description = AudioExplosion3d;
};

datablock AudioProfile(FlametongueFireSound)
{
   filename    = "fx/weapons/plasma_rifle_projectile_hit.wav";
   description = AudioDefault3d;
};

datablock AudioProfile(FlametongueReloadSound)
{
   filename    = "fx/weapons/spinfusor_reload.wav";
   description = AudioDefault3d;
};

datablock ExplosionData(FlametongueExplosion) : FireballAtmosphereBoltExplosion
{
   soundProfile   = FlametongueExpSound;
   shakeCamera = false;
};

datablock ParticleData(FireboltFireParticle)
{
   dragCoeffiecient     = 0.0;
   gravityCoefficient   = -0.2;
   inheritedVelFactor   = 0.0;

   lifetimeMS           = 350;
   lifetimeVarianceMS   = 0;

   textureName          = "particleTest";

   useInvAlpha = false;
   spinRandomMin = -180.0;
   spinRandomMax = 180.0;

   animateTexture = true;
   framesPerSec = 15;

   animTexName[0]       = "special/Explosion/exp_0016";
   animTexName[1]       = "special/Explosion/exp_0018";
   animTexName[2]       = "special/Explosion/exp_0020";
   animTexName[3]       = "special/Explosion/exp_0022";
   animTexName[4]       = "special/Explosion/exp_0024";
   animTexName[5]       = "special/Explosion/exp_0026";
   animTexName[6]       = "special/Explosion/exp_0028";
   animTexName[7]       = "special/Explosion/exp_0030";
   animTexName[8]       = "special/Explosion/exp_0032";

   colors[0]     = "1.0 0.7 0.5 1.0";
   colors[1]     = "1.0 0.5 0.2 1.0";
   colors[2]     = "1.0 0.25 0.1 0.0";
   sizes[0]      = 1.0;
   sizes[1]      = 4.0;
   sizes[2]      = 2.0;
   times[0]      = 0.0;
   times[1]      = 0.2;
   times[2]      = 1.0;
};

datablock ParticleEmitterData(FireboltFireEmitter)
{
   ejectionPeriodMS = 10;
   periodVarianceMS = 2;

   ejectionVelocity = 0.25;
   velocityVariance = 0.0;

   thetaMin         = 0.0;
   thetaMax         = 30.0;

   particles = "FireboltFireParticle";
};

datablock ParticleData(FireboltSmokeParticle)
{
   dragCoeffiecient     = 4.0;
   gravityCoefficient   = -0.00;   // rises slowly
   inheritedVelFactor   = 0.0;

   lifetimeMS           = 1000;
   lifetimeVarianceMS   = 100;   // ...more or less

   textureName          = "particleTest";

   useInvAlpha =     true;

   spinRandomMin = -50.0;
   spinRandomMax = 50.0;

   colors[0]     = "0.3 0.3 0.3 0.0";
   colors[1]     = "0.3 0.3 0.3 1.0";
   colors[2]     = "0.0 0.0 0.0 0.0";
   sizes[0]      = 2;
   sizes[1]      = 3;
   sizes[2]      = 5;
   times[0]      = 0.0;
   times[1]      = 0.7;
   times[2]      = 1.0;
};

datablock ParticleEmitterData(FireboltSmokeEmitter)
{
   ejectionPeriodMS = 25;
   periodVarianceMS = 5;

   ejectionVelocity = 1.0;  // A little oomph at the back end
   velocityVariance = 0.5;

   thetaMin         = 10.0;
   thetaMax         = 30.0;

   useEmitterSizes = false;

   particles = "FireboltSmokeParticle";
};

datablock LinearFlareProjectileData(FlametongueBolt)
{
   projectileShapeName = "plasmabolt.dts";
   scale               = "4.0 4.0 4.0";
   faceViewer          = true;
   directDamage        = 0.0;
   hasDamageRadius     = true;
   indirectDamage      = 0.5;
   damageRadius        = 12.0;
   kickBackStrength    = 100.0;
   radiusDamageType    = $DamageType::Burn;

   explosion           = "FlametongueExplosion";
   splash              = PlasmaSplash;

   flags               = $Projectile::PlaysHitSound;
   ticking             = false;
   headshotMultiplier  = 1.0;

   radialStatusEffect  = "BurnEffect";
   statusEffectChance  = 1.0;
   statusEffectMask    = $TypeMasks::PlayerObjectType;
   
   dryVelocity       = 250.0; // z0dd - ZOD, 7/20/02. Faster plasma projectile. was 55
   wetVelocity       = -1;
   velInheritFactor  = 1.0;
   fizzleTimeMS      = 800;
   lifetimeMS        = 1000;
   explodeOnDeath    = false;
   reflectOnWaterImpactAngle = 0.0;
   explodeOnWaterImpact      = true;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = -1;

   //activateDelayMS = 100;
   activateDelayMS = -1;

   size[0]           = 0.2;
   size[1]           = 0.5;
   size[2]           = 0.1;

   baseEmitter         = FireboltFireEmitter;
   delayEmitter        = FireboltSmokeEmitter;

   numFlares         = 35;
   flareColor        = "1 0.75 0.25";
   flareModTexture   = "flaremod";
   flareBaseTexture  = "flarebase";

	sound        = PlasmaProjectileSound;
   fireSound    = PlasmaFireSound;
   wetFireSound = PlasmaFireWetSound;

   hasLight    = true;
   lightRadius = 3.0;
   lightColor  = "1 0.75 0.25";
};

datablock ItemData(FireGrenadeThrown)
{
   shapeFile = "grenade.dts";
   mass = 0.7;
   elasticity = 0.2;
   friction = 1;
   pickupRadius = 2;
   maxDamage = 0.4;
   explosion = FlametongueExplosion;
   indirectDamage      = 0.5;
   damageRadius        = 12;
   radiusDamageType    = $DamageType::Burn;
   kickBackStrength    = 100;
   
   flags               = $Projectile::PlaysHitSound;
   ticking             = false;
   headshotMultiplier  = 1.0;
   
   radialStatusEffect  = "BurnEffect";
   statusEffectChance  = 1.0;
   statusEffectMask    = $TypeMasks::PlayerObjectType;
};

datablock ItemData(FireGrenade)
{
   className = HandInventory;
   catagory = "Handheld";
   shapeFile = "grenade.dts";
   mass = 0.7;
   elasticity = 0.2;
   friction = 1;
   pickupRadius = 2;
   thrownItem = FireGrenadeThrown;
	pickUpName = "some incendiary grenades";
	isGrenade = true;
    customProjectile = true;
   projectile = "FlametongueBolt";
   projectileType = "LinearFlareProjectile";
   customSound = "FlametongueFireSound";
   reloadSound = "FlametongueReloadSound";
   reloadTime = 2000;
};

function FireGrenadeThrown::onThrow(%this, %gren)
{
   AIGrenadeThrown(%gren);
   %gren.detThread = schedule(1500, %gren, "detonateGrenade", %gren);
}
