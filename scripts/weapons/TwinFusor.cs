//--------------------------------------------------------------------------
// Twinfusor
//--------------------------------------------------------------------------
datablock AudioProfile(TwinfusorFireSound)
{
   filename    = "fx/weapons/TR2spinfusor_fire.wav";
   description = AudioDefault3d;
   preload = true;
};

datablock LinearProjectileData(TwinDiscProjectile)
{
   projectileShapeName = "disc.dts";
   emitterDelay        = -1;
   directDamage        = 0.0;
   hasDamageRadius     = true;
   indirectDamage      = 0.5;
   damageRadius        = 5.0;
   radiusDamageType    = $DamageType::Disc;
   kickBackStrength    = 1000;  // z0dd - ZOD, 4/24/02. Was 1750

   sound 				= discProjectileSound;
   explosion           = "DiscExplosion";
   underwaterExplosion = "UnderwaterDiscExplosion";
   splash              = DiscSplash;

   flags               = $Projectile::PlayerFragment | $Projectile::PlaysHitSound;
   ticking             = false;
   headshotMultiplier  = 1.0;

   dryVelocity       = 200; // z0dd - ZOD, 3/30/02. Slight disc speed up. was 90
   wetVelocity       = 100; // z0dd - ZOD, 3/30/02. Slight disc speed up. was 50
   velInheritFactor  = 0.75; // z0dd - ZOD, 3/30/02. was 0.5
   fizzleTimeMS      = 2000;
   lifetimeMS        = 2000;
   explodeOnDeath    = true;
   reflectOnWaterImpactAngle = 15.0;
   explodeOnWaterImpact      = true;
   deflectionOnWaterImpact   = 20.0; // z0dd - ZOD, 4/24/02. Was 0.0. 20 degrees skips off water
   fizzleUnderwaterMS        = 2000;

   activateDelayMS = -1;

   hasLight    = true;
   lightRadius = 6.0;
   lightColor  = "0.175 0.175 0.5";
};

//--------------------------------------
// Rifle and item...
//--------------------------------------

datablock ItemData(TwinfusorAmmo)
{
   className = Ammo;
   catagory = "Ammo";
   shapeFile = "ammo_disc.dts";
   mass = 1;
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 2;
	pickUpName = "some twinfusor discs";
   computeCRC = true; // z0dd - ZOD, 5/19/03. Was missing this param
};

datablock ItemData(Twinfusor)
{
   className    = Weapon;
   catagory     = "Spawn Items";
   shapeFile    = "weapon_disc.dts";
   image        = TwinfusorImage;
   mass         = 1;
   elasticity   = 0.2;
   friction     = 0.6;
   pickupRadius = 2;
	pickUpName = "a twinfusor";

   computeCRC = true;
};

datablock ShapeBaseImageData(TwinfusorImage)
{
   className = WeaponImage;

   shapeFile = "weapon_targeting.dts";
   item = Twinfusor;
   offset = "0 0 -0.15";
   fireTimeout = 750;
   projectile = BasicTargeter;
   projectileType = TargetProjectile;

   usesEnergy = true;
   minEnergy = -1;
   fireEnergy = -1;
   ammo = TwinfusorAmmo;
   
   subImage1 = "TwinfusorAImage";
   subImage2 = "TwinfusorBImage";

   stateName[0]                     = "Activate";
   stateSequence[0]                 = "Activate";
	stateSound[0]                    = TargetingLaserSwitchSound;
   stateTimeoutValue[0]             = 0.5;
   stateTransitionOnTimeout[0]      = "ActivateReady";

   stateName[1]                     = "ActivateReady";
   stateTransitionOnAmmo[1]         = "Ready";
   stateTransitionOnNoAmmo[1]       = "NoAmmo";

   stateName[2]                     = "Ready";
   stateTransitionOnNoAmmo[2]       = "NoAmmo";
   stateTransitionOnTriggerDown[2]  = "Fire";

   stateName[3]                     = "Fire";
//	stateEnergyDrain[3]              = 3;
   stateFire[3]                     = true;
   stateAllowImageChange[3]         = false;
   stateScript[3]                   = "onFire";
   stateTransitionOnTriggerUp[3]    = "Deconstruction";
   stateTransitionOnNoAmmo[3]       = "Deconstruction";
//   stateSound[3]                    = TargetingLaserPaintSound;

   stateName[4]                     = "NoAmmo";
   stateTransitionOnAmmo[4]         = "Ready";

   stateName[5]                     = "Deconstruction";
   stateScript[5]                   = "deconstruct";
   stateTransitionOnTimeout[5]      = "Ready";
};

datablock ShapeBaseImageData(TwinfusorAImage)
{
   className = WeaponImage;
   shapeFile = "weapon_disc.dts";
   ammo = TwinfusorAmmo;
   rotation = "0 1 0 90";
   emap = true;

   subImage = true;

   projectile = DiscProjectile;
   projectileType = LinearProjectile;

   // State Data
   stateName[0]                     = "Preactivate";
   stateTransitionOnLoaded[0]       = "Activate";
   stateTransitionOnNoAmmo[0]       = "NoAmmo";

   stateName[1]                     = "Activate";
   stateTransitionOnTimeout[1]      = "Ready";
   stateTimeoutValue[1]             = 0.5;
   stateSequence[1]                 = "Activated";
   stateSound[1]                    = DiscSwitchSound;

   stateName[2]                     = "Ready";
   stateTransitionOnNoAmmo[2]       = "NoAmmo";
   stateTransitionOnTriggerDown[2]  = "Fire";
   stateSequence[2]                 = "DiscSpin";
   stateSound[2]                    = DiscLoopSound;

   stateName[3]                     = "Fire";
   stateTransitionOnTimeout[3]      = "Reload";
   stateTimeoutValue[3]             = 1.0;
   stateFire[3]                     = true;
   stateRecoil[3]                   = LightRecoil;
   stateAllowImageChange[3]         = false;
   stateSequence[3]                 = "Fire";
   stateScript[3]                   = "onFire";
   stateSound[3]                    = TwinfusorFireSound;

   stateName[4]                     = "Reload";
   stateTransitionOnNoAmmo[4]       = "NoAmmo";
   stateTransitionOnTimeout[4]      = "Ready";
   stateTimeoutValue[4]             = 0.5; // 0.25 load, 0.25 spinup
   stateAllowImageChange[4]         = false;
   stateSequence[4]                 = "Reload";
//   stateSound[4]                    = DiscReloadSound;

   stateName[5]                     = "NoAmmo";
   stateTransitionOnAmmo[5]         = "Reload";
   stateSequence[5]                 = "NoAmmo";
   stateTransitionOnTriggerDown[5]  = "DryFire";

   stateName[6]                     = "DryFire";
   stateSound[6]                    = DiscDryFireSound;
   stateTimeoutValue[6]             = 1.0;
   stateTransitionOnTimeout[6]      = "NoAmmo";
};

datablock ShapeBaseImageData(TwinfusorBImage) : TwinfusorAImage
{
   rotation = "0 1 0 -90";
   subImage = true;
};

function TwinfusorImage::onFire(%data,%obj,%slot)
{
    if(%obj.twinFusorActive == true)
        return;
        
    if(getSimTime() < %obj.fireTimeout[%data.item])
        return;

    %obj.twinFusorActive = true;
    %data.shootNextFusor(%obj);
}

function TwinfusorImage::deconstruct(%data,%obj,%slot)
{
     %obj.twinFusorActive = false;
     %obj.setImageTrigger($WeaponSubImage1, false);
     %obj.setImageTrigger($WeaponSubImage2, false);
}

function TwinfusorImage::shootNextFusor(%data,%obj)
{
    if(!%obj.twinFusorActive)
        return;
        
    %obj.nextFusor = %obj.nextFusor == $WeaponSubImage1 ? $WeaponSubImage2 : $WeaponSubImage1;
    
    %obj.setImageTrigger(%obj.nextFusor, true);
//    %obj.schedule(32, "setImageTrigger", %obj.nextFusor, false);
    %obj.fireTimeout[%data.item] = getSimTime() + %data.fireTimeout;
    
    %data.schedule(%data.fireTimeout, "shootNextFusor", %obj);
}
