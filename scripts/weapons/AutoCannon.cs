//--------------------------------------
// AutoCannon
//--------------------------------------
datablock AudioProfile(AutoCannonFire)
{
   filename    = "fx/vehicles/tank_chaingun.wav";
   description = AudioDefault3d;
   preload = true;
};

datablock AudioProfile(AutoCannonImpact)
{
   filename    = "fx/explosions/deployables_explosion.wav";
//   filename    = "fx/weapons/plasma_rifle_projectile_die.wav";
   description = AudioDefault3d;
   preload = true;
};

datablock ShockwaveData(AutoCannonImpactShockwave)
{
   width = 0.75;
   numSegments = 24;
   numVertSegments = 24;
   velocity = 6;
   acceleration = 7;
   lifetimeMS = 500;
   height = 0.2;
   verticalCurve = 0.5;
   is2D = false;

   texture[0] = "special/shockwave4";
   texture[1] = "special/gradient";
   texWrap = 6.0;

   times[0] = 0.0;
   times[1] = 0.5;
   times[2] = 1.0;

   colors[0] = 211.0/255.0 @ " " @ 215.0/255.0 @ " " @ 120.0/255.0 @ " 1.0";
   colors[1] = 211.0/255.0 @ " " @ 215.0/255.0 @ " " @ 120.0/255.0 @ " 0.5";
   colors[2] = 211.0/255.0 @ " " @ 215.0/255.0 @ " " @ 120.0/255.0 @ " 0.0";

   mapToTerrain = false;
   orientToNormal = true;
   renderBottom = true;
};

datablock ParticleData(AutoCannonDebrisParticle)
{
   dragCoeffiecient     = 0.4;
   gravityCoefficient   = -0.01;   // rises slowly
   inheritedVelFactor   = 0.125;

   lifetimeMS           =  800;
   lifetimeVarianceMS   =  100;
   useInvAlpha          =  true;
   spinRandomMin        = -100.0;
   spinRandomMax        =  100.0;

   animateTexture = false;

   textureName = "special/Smoke/bigSmoke";

   colors[0]     = 211.0/255.0 @ " " @ 215.0/255.0 @ " " @ 120.0/255.0 @ " 1.0";
   colors[1]     = "0.8 0.8 0.8 0.8";
   colors[2]     = "0.0 0.0 0.0 0.0";
   sizes[0]      = 0.225;
   sizes[1]      = 0.3;
   sizes[2]      = 0.325;
   times[0]      = 0.0;
   times[1]      = 0.25;
   times[2]      = 1.0;
};

datablock ParticleEmitterData(AutoCannonDebrisEmitter) : DefaultEmitter
{
   ejectionPeriodMS = 7;
   periodVarianceMS = 1;
   ejectionVelocity = 1.0;  // A little oomph at the back end
   velocityVariance = 0.2;
   thetaMax         = 40.0;
   particles = "AutoCannonDebrisParticle";
};

datablock DebrisData(AutoCannonDebris)
{
   emitters[0] = AutoCannonDebrisEmitter;

   explodeOnMaxBounce = true;

   elasticity = 0.4;
   friction = 0.2;

   lifetime = 0.75;
   lifetimeVariance = 0.25;

   numBounces = 1;
};

datablock ParticleData(AutoCannonSparks)
{
   dragCoefficient      = 1;
   gravityCoefficient   = 0.0;
   inheritedVelFactor   = 0.2;
   constantAcceleration = 0.0;
   lifetimeMS           = 300;
   lifetimeVarianceMS   = 0;
   textureName          = "special/spark00";
   colors[0]     = "0.56 0.36 0.26 1.0";
   colors[1]     = "0.56 0.36 0.26 1.0";
   colors[2]     = "1.0 0.36 0.26 0.0";
   sizes[0]      = 0.8;
   sizes[1]      = 0.4;
   sizes[2]      = 0.1;
   times[0]      = 0.0;
   times[1]      = 0.2;
   times[2]      = 1.0;
};

datablock ParticleEmitterData(AutoCannonSparkEmitter) : DefaultEmitter
{
   ejectionPeriodMS = 4;
   ejectionVelocity = 8;
   velocityVariance = 4.0;
   thetaMax         = 80;
   orientParticles  = true;
   lifetimeMS       = 100;
   particles = "AutoCannonSparks";
};

datablock ExplosionData(AutoCannonExplosion)
{
   explosionShape = "effect_plasma_explosion.dts";
   playSpeed = 2.0;
   soundProfile = AutoCannonImpact;
   faceViewer = true;

   shockwave = AutoCannonImpactShockwave;
   emitter[0] = AutoCannonSparkEmitter;

   debris = AutoCannonDebris;
   debrisThetaMin = 30;
   debrisThetaMax = 85;
   debrisNum = 3;
   debrisVelocity = 8.0;
   debrisVelocityVariance = 4.0;

   sizes[0] = "0.5 0.5 0.5";
   sizes[1] = "0.5 0.5 0.5";
   times[0] = 0.0;
   times[1] = 1.0;
};

datablock LinearProjectileData(AutoCannonRocket)
{
   scale = "1.5 1.5 1.5";
   doDynamicClientHits = true;
   projectileShapeName = "weapon_missile_projectile.dts";
   emitterDelay        = 250;
   directDamage        = 0.0;
   hasDamageRadius     = true;
   indirectDamage      = 0.4;
   damageRadius        = 6.0;
   radiusDamageType    = $DamageType::AutoCannon;
   kickBackStrength    = 100;
   bubbleEmitTime      = 1.0;

   explosion           = "AutoCannonExplosion";
   underwaterExplosion = "UnderwaterReaverExplosion";
   splash              = MissileSplash;
   velInheritFactor    = 1.0;

   flags               = $Projectile::PlayerFragment | $Projectile::PlaysHitSound;
   ticking             = false;
   headshotMultiplier  = 1.0;

   baseEmitter         = MissilePuffEmitter;
//   baseEmitter         = DiscMistEmitter; // used on plasma cannon
//   delayEmitter        = MissileFireEmitter;
//   puffEmitter         = MissilePuffEmitter;
   bubbleEmitter       = GrenadeBubbleEmitter;
   bubbleEmitTime      = 1.0;

   dryVelocity       = 250;
   wetVelocity       = 125;
   velInheritFactor  = 1.0;
   fizzleTimeMS      = 1500; //1000
   lifetimeMS        = 1500; //1000
   explodeOnDeath    = true;
   reflectOnWaterImpactAngle = 0.0;
   explodeOnWaterImpact      = false;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 1000;

   activateDelayMS = -1;

   hasLight    = true;
   lightRadius = 6.0;
   lightColor  = "0.5 0.5 0.5";
};

datablock DebrisData(AutoShellDebris)
{
   shapeName = "weapon_missile_casement.dts";

   lifetime = 5.0;

   minSpinSpeed = 300.0;
   maxSpinSpeed = 400.0;

   elasticity = 0.5;
   friction = 0.2;

   numBounces = 3;

   fade = true;
   staticOnMaxBounce = true;
   snapOnMaxBounce = true;
};

//--------------------------------------------------------------------------
// Ammo
//--------------------------------------

datablock ItemData(AutoCannonAmmo)
{
   className = Ammo;
   catagory = "Ammo";
   shapeFile = "ammo_mortar.dts";
   mass = 1;
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 2;
	pickUpName = "some auto cannon ammo";
};

//--------------------------------------------------------------------------
// Weapon
//--------------------------------------
datablock ShapeBaseImageData(AutoCannonImage)
{
   className = WeaponImage;
   shapeFile = "weapon_chaingun.dts";
   item      = AutoCannon;
   ammo 	 = AutoCannonAmmo;
   projectile = AutoCannonRocket;
   projectileType = LinearProjectile;
   emap = true;
   offset = "0 0.35 0.05";
   
   casing              = AutoShellDebris;
   shellExitDir        = "1.0 0.3 1.0";
   shellExitOffset     = "0.15 -0.56 -0.1";
   shellExitVariance   = 15.0;
   shellVelocity       = 3.0;

   projectileSpread = 4.0;
   
   subImage1 = "AutoCannonDecalAImage";
   subImage2 = "AutoCannonDecalBImage";
   subImage3 = "AutoCannonDecalCImage";
   
   //--------------------------------------
   stateName[0]             = "Activate";
   stateSequence[0]         = "Activate";
   stateSound[0]            = ChaingunSwitchSound;
   stateAllowImageChange[0] = false;
   //
   stateTimeoutValue[0]        = 0.5;
   stateTransitionOnTimeout[0] = "Ready";
   stateTransitionOnNoAmmo[0]  = "NoAmmo";

   //--------------------------------------
   stateName[1]       = "Ready";
   stateSpinThread[1] = Stop;
   //
   stateTransitionOnTriggerDown[1] = "Spinup";
   stateTransitionOnNoAmmo[1]      = "NoAmmo";

   //--------------------------------------
   stateName[2]               = "NoAmmo";
   stateTransitionOnAmmo[2]   = "Ready";
   stateSpinThread[2]         = Stop;
   stateTransitionOnTriggerDown[2] = "DryFire";

   //--------------------------------------
   stateName[3]         = "Spinup";
   stateSpinThread[3]   = SpinUp;
   stateSound[3]        = ChaingunSpinupSound;
   //
   stateTimeoutValue[3]          = 0.5;
   stateWaitForTimeout[3]        = false;
   stateTransitionOnTimeout[3]   = "Fire";
   stateTransitionOnTriggerUp[3] = "Spindown";

   //--------------------------------------
   stateName[4]             = "Fire";
   stateSequence[4]            = "Fire";
   stateSequenceRandomFlash[4] = true;
   stateSpinThread[4]       = FullSpeed;
   stateSound[4]            = AutoCannonFire;
   //stateRecoil[4]           = LightRecoil;
   stateAllowImageChange[4] = false;
   stateScript[4]           = "onFire";
   stateFire[4]             = true;
   stateEjectShell[4]       = true;
   //
   stateTimeoutValue[4]          = 0.25;
   stateTransitionOnTimeout[4]   = "Reload";
   stateTransitionOnTriggerUp[4] = "Spindown";
   stateTransitionOnNoAmmo[4]    = "EmptySpindown";

   //--------------------------------------
   stateName[5]       = "Spindown";
   stateSound[5]      = ChaingunSpinDownSound;
   stateSpinThread[5] = SpinDown;
   //
   stateTimeoutValue[5]            = 1.0;
   stateWaitForTimeout[5]          = true;
   stateTransitionOnTimeout[5]     = "Ready";
   stateTransitionOnTriggerDown[5] = "Spinup";

   //--------------------------------------
   stateName[6]       = "EmptySpindown";
   stateSound[6]      = ChaingunSpinDownSound;
   stateSpinThread[6] = SpinDown;
   //
   stateTimeoutValue[6]        = 0.5;
   stateTransitionOnTimeout[6] = "NoAmmo";
   
   //--------------------------------------
   stateName[7]       = "DryFire";
   stateSound[7]      = ChaingunDryFireSound;
   stateTimeoutValue[7]        = 0.5;
   stateTransitionOnTimeout[7] = "NoAmmo";
   
   stateName[8]       = "Reload";
   stateTransitionOnTriggerDown[8] = "Fire";
   stateSpinThread[8]       = FullSpeed;
//   stateTimeoutValue[8]          = 0.5;
//   stateTransitionOnTimeout[8]   = "Fire";
   stateTransitionOnTriggerUp[8] = "Spindown";
   stateTransitionOnNoAmmo[8]    = "EmptySpindown";
};

datablock ShapeBaseImageData(AutoCannonDecalAImage)
{
   shapeFile = "weapon_mortar.dts";
   offset = "0 -0.2 0.05";
   rotation = "0 0 1 0";
   
   subImage = true;
};

datablock ShapeBaseImageData(AutoCannonDecalBImage)
{
   shapeFile = "ammo_disc.dts";
   offset = "0.1 0.525 0.1";
   rotation = "1 0 0 90";
   
   subImage = true;
};

datablock ShapeBaseImageData(AutoCannonDecalCImage)
{
   shapeFile = "ammo_disc.dts";
   offset = "0.1 0.3 0.1";
   rotation = "1 0 0 90";
   
   subImage = true;
};

datablock ItemData(AutoCannon)
{
   className    = Weapon;
   catagory     = "Spawn Items";
   shapeFile    = "TR2weapon_chaingun.dts";
   image        = AutoCannonImage;
   mass         = 1;
   elasticity   = 0.2;
   friction     = 0.6;
   pickupRadius = 2;
   pickUpName   = "an autocannon";
   computeCRC = true;
   emap = true;
};
