//--------------------------------------
// Sentry Rifle
//--------------------------------------
datablock AudioProfile(SentryRifleFire)
{
   filename    = "fx/vehicles/shrike_blaster_projectile.wav";
   description = AudioDefault3d;
   preload = true;
};

datablock AudioProfile(SentryTurretExpSound)
{
   filename    = "fx/powered/turret_sentry_impact.WAV";
   description = AudioClosest3d;
   preload = true;
};

datablock AudioProfile(SentryTurretProjectileSound)
{
   filename    = "fx/weapons/blaster_projectile.WAV";
   description = AudioExplosion3d;
   preload = true;
};

// Explosion

datablock ParticleData(SentryTurretExplosionParticle1)
{
   dragCoefficient      = 0.65;
   gravityCoefficient   = 0.3;
   inheritedVelFactor   = 0.0;
   constantAcceleration = 0.0;
   lifetimeMS           = 500;
   lifetimeVarianceMS   = 150;
   textureName          = "particleTest";
   colors[0]     = "0.26 0.36 0.56 1.0";
   colors[1]     = "0.26 0.36 0.56 0.0";
   sizes[0]      = 0.0425;
   sizes[1]      = 0.15;
};

datablock ParticleEmitterData(SentryTurretExplosionEmitter)
{
   ejectionPeriodMS = 10;
   periodVarianceMS = 0;
   ejectionVelocity = 0.75;
   velocityVariance = 0.25;
   ejectionOffset   = 0.0;
   thetaMin         = 0;
   thetaMax         = 60;
   phiReferenceVel  = 0;
   phiVariance      = 360;
   overrideAdvances = false;
   particles = "SentryTurretExplosionParticle1";
};

datablock ExplosionData(SentryTurretExplosion)
{
   explosionShape = "energy_explosion.dts";
   soundProfile   = SentryTurretExpSound;

   particleEmitter = SentryTurretExplosionEmitter;
   particleDensity = 120;
   particleRadius = 0.15;

   faceViewer           = false;
};

// Projectile

datablock LinearFlareProjectileData(SentryTurretEnergyBolt)
{
   directDamage        = 0.6;
   directDamageType    = $DamageType::SentryTurret;

   explosion           = "SentryTurretExplosion";
   kickBackStrength  = 0.0;

   dryVelocity       = 250.0;
   wetVelocity       = 150.0;
   velInheritFactor  = 1.0;
   fizzleTimeMS      = 1500;
   lifetimeMS        = 2000;
   explodeOnDeath    = false;
   reflectOnWaterImpactAngle = 0.0;
   explodeOnWaterImpact      = false;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 2000;

   numFlares         = 15;
   size              = 0.7;
   flareColor        = "0.35 0.35 1";
   flareModTexture   = "flaremod";
   flareBaseTexture  = "flarebase";

   sound = SentryTurretProjectileSound;

   hasLight    = true;
   lightRadius = 1.5;
   lightColor  = "0.35 0.35 1";
};

datablock LinearFlareProjectileData(SentryRifleShot)
{
   directDamage        = 0.5;
   directDamageType    = $DamageType::SentryRifle;

   explosion           = "SentryTurretExplosion";
   kickBackStrength  = 0.0;

   dryVelocity       = 250.0;
   wetVelocity       = 150.0;
   velInheritFactor  = 1.0;
   fizzleTimeMS      = 1500;
   lifetimeMS        = 2000;
   explodeOnDeath    = false;
   reflectOnWaterImpactAngle = 0.0;
   explodeOnWaterImpact      = false;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 2000;

   numFlares         = 15;
   size              = 0.7;
   flareColor        = "0.35 0.35 1";
   flareModTexture   = "flaremod";
   flareBaseTexture  = "flarebase";

   sound = SentryTurretProjectileSound;

   hasLight    = true;
   lightRadius = 1.5;
   lightColor  = "0.35 0.35 1";
};

//--------------------------------------------------------------------------
// Weapon
//--------------------------------------
datablock ItemData(SentryRifle)
{
   className = Weapon;
   catagory = "Spawn Items";
   shapeFile = "weapon_missile.dts";
   image = SentryRifleImage;
   mass = 1;
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 2;
	pickUpName = "a sentry rifle";
};

datablock ShapeBaseImageData(SentryRifleImage)
{
   className = WeaponImage;
   shapeFile = "weapon_energy.dts";
   offset = "0 0.75 0.325";
   rotation = "0 1 0 180";

   item = SentryRifle;
   subImage1 = "SentryRifleDecalA";
   subImage2 = "SentryRifleDecalB";

   usesEnergy = true;
   fireEnergy = 10;
   minEnergy = 10;
   emap = true;

   projectile = SentryRifleShot;
   projectileType = LinearFlareProjectile;

   stateName[0] = "Activate";
   stateTransitionOnTimeout[0] = "ActivateReady";
   stateTimeoutValue[0] = 0.25;
   stateSequence[0] = "Activate";
   stateSound[0] = GrenadeReloadSound;

   stateName[1] = "ActivateReady";
   stateTransitionOnLoaded[1] = "Ready";
   stateTransitionOnNoAmmo[1] = "NoAmmo";

   stateName[2] = "Ready";
   stateTransitionOnNoAmmo[2] = "NoAmmo";
   stateTransitionOnTriggerDown[2] = "Fire";

   stateName[3] = "Fire";
   stateTransitionOnTimeout[3] = "Reload";
   stateTimeoutValue[3] = 0.25;
   stateFire[3] = true;
   stateRecoil[3] = LightRecoil;
   stateAllowImageChange[3] = false;
   stateSequence[3] = "fire";
   stateScript[3] = "onFire";
   stateSound[3] = SentryRifleFire;

   stateName[4] = "Reload";
   stateTransitionOnNoAmmo[4] = "NoAmmo";
   stateTransitionOnTimeout[4] = "Ready";
   stateTimeoutValue[4] = 0.25;
   stateAllowImageChange[4] = false;
//   stateSequence[4] = "Reload";

   stateName[5] = "NoAmmo";
   stateTransitionOnAmmo[5] = "Reload";
   stateSequence[5] = "NoAmmo";
   stateTransitionOnTriggerDown[5] = "DryFire";

   stateName[6]       = "DryFire";
//   stateSound[6]      = SolPisDryFireSound;
   stateTimeoutValue[6]        = 1.5;
   stateTransitionOnTimeout[6] = "NoAmmo";
};

datablock ShapeBaseImageData(SentryRifleDecalA)
{
   shapeFile = "weapon_grenade_launcher.dts";
   offset = "0.0 0.0 0.0";
   rotation = "1 0 0 0";

   subImage = true;
};

datablock ShapeBaseImageData(SentryRifleDecalB)
{
   shapeFile = "weapon_sniper.dts";
   offset = "0 0.55 0.075";
   rotation = "0 1 0 0";
   emap = true;

   subImage = true;
};
