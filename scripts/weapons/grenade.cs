// ------------------------------------------------------------------------
// grenade (thrown by hand) script
// ------------------------------------------------------------------------

datablock ItemData(GrenadeThrown)
{
	className = Weapon;
	shapeFile = "grenade.dts";
	mass = 0.7;
	elasticity = 0.2;
   friction = 1;
   pickupRadius = 2;
   maxDamage = 0.5;
	explosion = HandGrenadeExplosion;
	underwaterExplosion = UnderwaterHandGrenadeExplosion;
   indirectDamage      = 0.5;
   damageRadius        = 10.0;
   radiusDamageType    = $DamageType::Grenade;
   kickBackStrength    = 2000;

   flags               = $Projectile::PlayerFragment | $Projectile::PlaysHitSound;
   ticking             = false;
   headshotMultiplier  = 1.0;
   
   computeCRC = true;
};

datablock ItemData(Grenade)
{
	className = HandInventory;
	catagory = "Handheld";
	shapeFile = "grenade.dts";
	mass = 0.7;
	elasticity = 0.2;
   friction = 1;
   pickupRadius = 2;
   thrownItem = GrenadeThrown;
	pickUpName = "some grenades";
	isGrenade = true;

   computeCRC = true;
};

datablock LinearProjectileData(StandardRPG)
{
   scale = "1.5 3.0 1.5";
   projectileShapeName = "mortar_projectile.dts";
//   emitterDelay        = 250;
   directDamage        = 0;
   hasDamageRadius     = true;
   indirectDamage      = 0.625;
   damageRadius        = 10.0;
   radiusDamageType    = $DamageType::Grenade;
   kickBackStrength    = 2000;
   bubbleEmitTime      = 1.0;

   flags               = $Projectile::PlayerFragment | $Projectile::PlaysHitSound;
   ticking             = false;
   headshotMultiplier  = 1.0;

   explosion           = "HandGrenadeExplosion";
   underwaterExplosion = "UnderwaterHandGrenadeExplosion";
   splash              = MissileSplash;
   velInheritFactor    = 1.0;

//   baseEmitter         = MissileSmokeEmitter;
//   delayEmitter        = MissileFireEmitter;
//   puffEmitter         = MissilePuffEmitter;
   bubbleEmitter       = GrenadeBubbleEmitter;

   dryVelocity       = 200;
   wetVelocity       = 100;
   velInheritFactor  = 1.0;
   fizzleTimeMS      = 2500;
   lifetimeMS        = 3000;
   explodeOnDeath    = true;
   reflectOnWaterImpactAngle = 0.0;
   explodeOnWaterImpact      = true;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 2500;

   activateDelayMS = -1;

   hasLight    = true;
   lightRadius = 6.0;
   lightColor  = "0.5 0.5 0.5";
};
