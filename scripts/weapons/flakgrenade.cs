// ------------------------------------------------------------------------
// flak grenade
// ------------------------------------------------------------------------

datablock AudioProfile(FlakCannonFireSound)
{
   filename    = "fx/weapons/grenadelauncher_fire.wav";
   description = AudioDefault3d;
   preload = true;
};

datablock AudioProfile(FlakExplosionSound)
{
   filename    = "fx/weapons/grenade_explode_UW.wav";
   description = AudioBIGExplosion3d;
   preload = true;
};

datablock ShockwaveData(LFlakShockwave)
{
   width = 5.0;
   numSegments = 32;
   numVertSegments = 6;
   velocity = 15;
   acceleration = -30;
   lifetimeMS = 500;
   height = 0.75;
   verticalCurve = 0.4;
   is2D = true;

   texture[0] = "special/shockwave4";
   texture[1] = "special/gradient";
   texWrap = 6.0;

   times[0] = 0.0;
   times[1] = 0.5;
   times[2] = 1.0;

   colors[0] = "1.0 0.773 0.493 0.75";
   colors[1] = "1.0 0.773 0.493 0.5";
   colors[2] = "0.5 0.36 0.247 0.0";

   mapToTerrain = false;
   orientToNormal = false;
   renderBottom = true;
};

datablock ExplosionData(FlakShellExplosion) : HandGrenadeExplosion
{
   soundProfile   = FlakExplosionSound;
   shockwave = LFlakShockwave;

   debris = GrenadeDebris;
   debrisThetaMin = 1;
   debrisThetaMax = 180;
   debrisNum = 10;
   debrisVelocity = 26.0;
   debrisVelocityVariance = 7.0;
};

datablock LinearFlareProjectileData(FlakShellBurst)
{
   projectileShapeName = "turret_muzzlePoint.dts";
   faceViewer          = false;
   directDamage        = 0.0;
   hasDamageRadius     = true;
   indirectDamage      = 0.5;
   damageRadius        = 20.0;
   radiusDamageType    = $DamageType::Grenade;
   kickBackStrength    = 500;

   flags               = $Projectile::PlayerFragment | $Projectile::PlaysHitSound;
   ticking             = false;
   headshotMultiplier  = 1.25;

   sound               = GrenadeProjectileSound;
   explosion           = "FlakShellExplosion";
   underwaterExplosion = "UnderwaterGrenadeExplosion";

   splash              = MissileSplash;
   velInheritFactor    = 0;

   dryVelocity       = 0.1;
   wetVelocity       = 0.1;
   fizzleTimeMS      = 32;
   lifetimeMS        = 35;
   explodeOnDeath    = true;
   reflectOnWaterImpactAngle = 90;
   explodeOnWaterImpact      = false;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 35;

   activateDelayMS = -1;

   numFlares         = 0;
   flareColor        = "0 0 0";
   flareModTexture   = "flaremod";
   flareBaseTexture  = "flarebase";
};

datablock LinearProjectileData(FlakRPG) : StandardRPG
{
   scale = "2.0 2.0 2.0";
   projectileShapeName = "grenade_projectile.dts";
//   emitterDelay        = 250;
   directDamage        = 0;
   hasDamageRadius     = true;
   indirectDamage      = 0.625;
   damageRadius        = 10.0;
   radiusDamageType    = $DamageType::Grenade;
   kickBackStrength    = 2000;
   bubbleEmitTime      = 1.0;

   flags               = $Projectile::PlayerFragment | $Projectile::PlaysHitSound;
   ticking             = true;
   headshotMultiplier  = 1.0;

   explosion           = "FlakShellExplosion";
   underwaterExplosion = "UnderwaterHandGrenadeExplosion";
   splash              = MissileSplash;
   velInheritFactor    = 1.0;

//   baseEmitter         = MissileSmokeEmitter;
//   delayEmitter        = MissileFireEmitter;
//   puffEmitter         = MissilePuffEmitter;
   bubbleEmitter       = GrenadeBubbleEmitter;

   dryVelocity       = 200;
   wetVelocity       = 100;
   velInheritFactor  = 1.0;
   fizzleTimeMS      = 2500;
   lifetimeMS        = 3000;
   explodeOnDeath    = true;
   reflectOnWaterImpactAngle = 0.0;
   explodeOnWaterImpact      = true;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 2500;

   activateDelayMS = -1;

   hasLight    = true;
   lightRadius = 6.0;
   lightColor  = "0.5 0.5 0.5";
};

function FlakRPG::onTick(%this, %proj)
{
    Parent::onTick(%this, %proj);

    InitContainerRadiusSearch(%proj.position, %this.damageRadius * 0.675, $TypeMasks::VehicleObjectType | $TypeMasks::PlayerObjectType | $TypeMasks::ProjectileObjectType);

    while((%int = ContainerSearchNext()) != 0)
    {
        if(%int.client.team == %proj.instigator.team || %int.team == %proj.instigator.team)
            continue;

        if(%int.getType() & $TypeMasks::ProjectileObjectType)
        {
            if(%int.getDatablock().getName() $= "FlareGrenadeProj")
                return %this.detonate(%proj);
        }
        else
        {
            if(%int.getHeat() > 0.7)
                return %this.detonate(%proj);
        }
    }
}

function FlakRPG::detonate(%this, %proj)
{
    transformProjectile(%proj, "LinearFlareProjectile", "FlakShellBurst", %proj.position, %proj.initialDirection);
}

datablock ItemData(FlakGrenadeThrown)
{
	className = Weapon;
	shapeFile = "grenade_projectile.dts";
	mass = 0.7;
	elasticity = 0.2;
   friction = 1;
   pickupRadius = 2;
   maxDamage = 0.5;
	explosion = FlakShellExplosion;
   underwaterExplosion = "UnderwaterHandGrenadeExplosion";
   indirectDamage      = 0.5;
   damageRadius        = 20.0;
   radiusDamageType    = $DamageType::Grenade;
   kickBackStrength    = 2000;

   flags               = $Projectile::PlayerFragment | $Projectile::PlaysHitSound;
   ticking             = false;
   headshotMultiplier  = 1.0;
   
   computeCRC = true;
};

datablock ItemData(FlakGrenade)
{
	className = HandInventory;
	catagory = "Handheld";
	shapeFile = "grenade_projectile.dts";
	mass = 0.7;
	elasticity = 0.2;
   friction = 1;
   pickupRadius = 2;
   thrownItem = FlakGrenadeThrown;
	pickUpName = "some flak grenades";
	isGrenade = true;
   customProjectile = true;
   projectile = "FlakRPG";
   projectileType = "LinearProjectile";
   customSound = "FlakCannonFireSound";
   reloadSound = "";
   reloadTime = 750;
   
   computeCRC = true;
};

function FlakGrenadeThrown::onThrow(%this, %gren)
{
   AIGrenadeThrown(%gren);
   %gren.detThread = schedule(4000, %gren, "detonateGrenade", %gren);
   
   flakTickLoop(%gren);
}

function flakTickLoop(%proj)
{
    InitContainerRadiusSearch(%proj.position, %this.damageRadius * 0.675, $TypeMasks::VehicleObjectType | $TypeMasks::PlayerObjectType | $TypeMasks::ProjectileObjectType);

    while((%int = ContainerSearchNext()) != 0)
    {
        if(%int.client.team == %proj.instigator.team || %int.team == %proj.instigator.team)
            continue;

        if(%int.getType() & $TypeMasks::ProjectileObjectType)
        {
            if(%int.getDatablock().getName() $= "FlareGrenadeProj")
                return flakDetonateProj(%proj);
        }
        else
        {
            if(%int.getHeat() > 0.7)
                return flakDetonateProj(%proj);
        }
    }
}

function flakDetonateProj(%proj)
{
    transformProjectile(%proj, "LinearFlareProjectile", "FlakShellBurst", %proj.position, "0 0 0");
}
