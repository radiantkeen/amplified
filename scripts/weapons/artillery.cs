//--------------------------------------
// Artillery Cannon
//--------------------------------------
datablock AudioProfile(ArtilleryCannonSwitchSound)
{
   filename    = "fx/vehicles/bomber_turret_activate.wav";
   description = AudioDefault3d;
   preload = true;
};

datablock AudioProfile(ArtilleryCannonFireSound)
{
   filename    = "fx/powered/turret_mortar_fire.wav";
   description = AudioDefault3d;
   preload = true;
};

datablock AudioProfile(ArtilleryCannonReloadSound)
{
   filename    = "fx/powered/turret_heavy_reload.wav";
   description = AudioDefault3d;
   preload = true;
};

datablock AudioProfile(ArtilleryCannonIdleSound)
{
   filename = "fx/powered/turret_light_idle.wav";
   description = AudioDefaultLooping3d;
   preload = true;
};

//--------------------------------------------------------------------------
// Projectile
//--------------------------------------
datablock GrenadeProjectileData(ArtilleryShot)
{
   projectileShapeName = "mortar_projectile.dts";
   emitterDelay        = -1;
   directDamage        = 0.0;
   hasDamageRadius     = true;
   indirectDamage      = 1.5;
   damageRadius        = 15.0; // z0dd - ZOD, 8/13/02. Was 20.0
   radiusDamageType    = $DamageType::ArtilleryMortar;
   kickBackStrength    = 2000;

   flags               = $Projectile::PlayerFragment | $Projectile::PlaysHitSound;
   ticking             = false;
   headshotMultiplier  = 1.0;
   
   explosion           = "ImpactMortarExplosion";
   underwaterExplosion = "UnderwaterMortarExplosion";
   velInheritFactor    = 1.0;
   splash              = MortarSplash;
   depthTolerance      = 10.0; // depth at which it uses underwater explosion
   
   baseEmitter         = ImpactMortarSmokeEmitter;
   bubbleEmitter       = MortarBubbleEmitter;
   
   grenadeElasticity = 0.15;
   grenadeFriction   = 0.4;
   armingDelayMS     = -1; // 250

   gravityMod        = 1.0;  // z0dd - ZOD, 5/18/02. Make mortar projectile heavier, less floaty
   muzzleVelocity    = 150; // z0dd - ZOD, 8/13/02. More velocity to compensate for higher gravity. Was 63.7
   drag              = 0.1;
   sound	     = MortarProjectileSound;

   hasLight    = true;
   lightRadius = 4;
   lightColor  = "0.2 0.05 0.05";

   hasLightUnderwaterColor = true;
   underWaterLightColor = "0.05 0.075 0.2";

   // z0dd - ZOD, 5/22/03. Limit the mortars effective range to around 450 meters.
   // Addresses long range mortar spam exploit.
   explodeOnDeath = false;
   fizzleTimeMS = -1;
   lifetimeMS = -1;
   fizzleUnderwaterMS = -1;
   reflectOnWaterImpactAngle = 0.0;
   explodeOnWaterImpact = false;
   deflectionOnWaterImpact = 0.0;
};

//--------------------------------------------------------------------------
// Ammo
//--------------------------------------

datablock ItemData(ArtilleryCannonAmmo)
{
   className = Ammo;
   catagory = "Ammo";
   shapeFile = "ammo_mortar.dts";
   mass = 1;
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 2;
   pickUpName = "some artillery cannon ammo";
   computeCRC = true;
};

//--------------------------------------------------------------------------
// Weapon
//--------------------------------------
datablock ItemData(ArtilleryCannon)
{
   className = Weapon;
   catagory = "Spawn Items";
   shapeFile = "turret_mortar_large.dts";
   image = ArtilleryCannonImage;
   mass = 1;
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 2;
   pickUpName = "an artillery cannon";
   computeCRC = true;
   emap = true;
};

datablock ShapeBaseImageData(ArtilleryCannonImage)
{
   className = WeaponImage;
   shapeFile = "turret_mortar_large.dts";
   item = ArtilleryCannon;
   ammo = ArtilleryCannonAmmo;
   offset = "0 0 0";
   emap = true;

   projectile = ArtilleryShot;
   projectileType = GrenadeProjectile;

   stateName[0] = "Activate";
   stateTransitionOnTimeout[0] = "ActivateReady";
   stateTimeoutValue[0] = 0.5;
   stateSequence[0] = "Deploy";
   stateSound[0] = ArtilleryCannonSwitchSound;

   stateName[1] = "ActivateReady";
   stateTransitionOnLoaded[1] = "Ready";
   stateTransitionOnNoAmmo[1] = "NoAmmo";

   stateName[2] = "Ready";
   stateTransitionOnNoAmmo[2] = "NoAmmo";
   stateTransitionOnTriggerDown[2] = "Fire";
   stateSound[2] = ArtilleryCannonIdleSound;

   stateName[3] = "Fire";
   stateSequence[3] = "Fire";
   stateTransitionOnTimeout[3] = "Reload";
   stateTimeoutValue[3] = 1.4;
   stateFire[3] = true;
   stateRecoil[3] = LightRecoil;
   stateAllowImageChange[3] = false;
   stateScript[3] = "onFire";
   stateSound[3] = ArtilleryCannonFireSound;

   stateName[4] = "Reload";
   stateTransitionOnNoAmmo[4] = "NoAmmo";
   stateTransitionOnTimeout[4] = "Ready";
   stateTimeoutValue[4] = 1.4;
   stateAllowImageChange[4] = false;
   stateSequence[4] = "Deploy";
   stateSound[4] = ArtilleryCannonReloadSound;

   stateName[5] = "NoAmmo";
   stateTransitionOnAmmo[5] = "Reload";
   stateSequence[5] = "NoAmmo";
   stateTransitionOnTriggerDown[5] = "DryFire";

   stateName[6]       = "DryFire";
   stateSound[6]      = MortarDryFireSound;
   stateTimeoutValue[6]        = 1.5;
   stateTransitionOnTimeout[6] = "NoAmmo";
};
