// ------------------------------------------------------------------------
// HEAT Grenade
// ------------------------------------------------------------------------
datablock AudioProfile(HEATFireSound)
{
   filename = "fx/vehicles/tank_mortar_fire.wav";
   description = AudioDefault3d;
   preload = true;
};

datablock AudioProfile(HEATReloadSound)
{
   filename = "fx/vehicles/bomber_turret_reload.wav";
   description = AudioDefault3d;
   preload = true;
};

datablock AudioProfile(HEATExplosionSound)
{
   filename = "fx/explosions/explosion.xpl03.wav";
   description = AudioBIGExplosion3d;
   preload = true;
};

datablock AudioProfile(UnderwaterHEATExplosionSound)
{
   filename = "fx/weapons/grenade_explode_UW.wav";
   description = AudioBIGExplosion3d;
   preload = true;
};

datablock ParticleData(HowitzerSmokeParticle)
{
   dragCoeffiecient     = 0.0;
   gravityCoefficient   = -0.02;
   inheritedVelFactor   = 0.1;

   lifetimeMS           = 1200;
   lifetimeVarianceMS   = 100;

   textureName          = "special/flare";

   useInvAlpha = false;
   spinRandomMin = 0.0;
   spinRandomMax = 0.0;

   colors[0]     = "0.0 0.75 1.0 0.0";
   colors[1]     = "0.2 0.5 0.5 1.0";
   colors[2]     = "0.3 0.3 0.3 0.0";
   sizes[0]      = 1.3;
   sizes[1]      = 2.6;
   sizes[2]      = 3.5;
   times[0]      = 0.0;
   times[1]      = 0.1;
   times[2]      = 1.0;
};

datablock ParticleEmitterData(HowitzerSmokeEmitter)
{
   ejectionPeriodMS = 10;
   periodVarianceMS = 0;

   ejectionVelocity = 1.5;
   velocityVariance = 0.0;

   thetaMin         = 0.0;
   thetaMax         = 0.0;

   particles = "HowitzerSmokeParticle";
};

datablock ExplosionData(HEATExplosion) : MineExplosion
{
   explosionShape = "disc_explosion.dts";
   playSpeed      = 1.0;
   sizes[0] = "0.5 0.5 0.5";
   sizes[1] = "0.5 0.5 0.5";
   soundProfile   = HEATExplosionSound;
   faceViewer     = true;

   emitter[0] = MineExplosionSmokeEmitter;
   emitter[1] = MineCrescentEmitter;
};

datablock ExplosionData(UnderwaterHEATExplosion) : UnderwaterMineExplosion
{
   explosionShape = "disc_explosion.dts";
   playSpeed      = 1.0;
   sizes[0] = "0.4 0.4 0.4";
   sizes[1] = "0.4 0.4 0.4";
   soundProfile   = UnderwaterHEATExplosionSound;
   faceViewer     = true;

   emitter[0] = UnderwaterMineExplosionSmokeEmitter;
   emitter[1] = UnderwaterMineCrescentEmitter;
   emitter[2] = MineExplosionBubbleEmitter;
};

datablock LinearProjectileData(HEATRPG)
{
   scale = "2.0 2.0 2.0";
   projectileShapeName = "ammo_missile.dts";
//   emitterDelay        = 250;
   directDamage        = 0;
   hasDamageRadius     = true;
   indirectDamage      = 3.5;
   damageRadius        = 1.0;
   radiusDamageType    = $DamageType::HEAT;
   kickBackStrength    = 5000;
   bubbleEmitTime      = 1.0;

   flags               = $Projectile::PlayerFragment | $Projectile::PlaysHitSound;
   ticking             = false;
   headshotMultiplier  = 1.0;

   explosion           = "HEATExplosion";
   underwaterExplosion = "UnderwaterHEATExplosion";
   splash              = MissileSplash;
   velInheritFactor    = 1.0;

   baseEmitter         = HowitzerSmokeEmitter;
//   delayEmitter        = MissileFireEmitter;
//   puffEmitter         = MissilePuffEmitter;
   bubbleEmitter       = GrenadeBubbleEmitter;

   dryVelocity       = 400;
   wetVelocity       = 200;
   velInheritFactor  = 0.5;
   fizzleTimeMS      = 1500;
   lifetimeMS        = 2000;
   explodeOnDeath    = true;
   reflectOnWaterImpactAngle = 0.0;
   explodeOnWaterImpact      = true;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 1500;

   activateDelayMS = -1;

   hasLight    = true;
   lightRadius = 6.0;
   lightColor  = "0.5 0.5 0.5";
};

datablock ItemData(HEATGrenadeThrown)
{
	className = Weapon;
	shapeFile = "grenade.dts";
	mass = 0.7;
	elasticity = 0.2;
   friction = 1;
   pickupRadius = 2;
   maxDamage = 0.5;
	explosion = HandGrenadeExplosion;
	underwaterExplosion = UnderwaterHandGrenadeExplosion;
   indirectDamage      = 0.5;
   damageRadius        = 10.0;
   radiusDamageType    = $DamageType::Grenade;
   kickBackStrength    = 2000;

   flags               = $Projectile::PlayerFragment | $Projectile::PlaysHitSound;
   ticking             = false;
   headshotMultiplier  = 1.0;

   computeCRC = true;
};

datablock ItemData(HEATGrenade)
{
	className = HandInventory;
	catagory = "Handheld";
	shapeFile = "ammo_missile.dts";
	mass = 0.7;
	elasticity = 0.2;
   friction = 1;
   pickupRadius = 2;
   thrownItem = HEATGrenadeThrown;
	pickUpName = "some HEAT shells";
	isGrenade = true;
   customProjectile = true;
   projectile = "HEATRPG";
   projectileType = "LinearProjectile";
   customSound = "HEATFireSound";
   reloadSound = "HEATReloadSound";
   reloadTime = 4000;
   computeCRC = true;
};
