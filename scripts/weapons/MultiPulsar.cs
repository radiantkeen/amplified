//--------------------------------------
// MultiPulsar
//--------------------------------------
//--------------------------------------------------------------------------
// Sounds
//--------------------------------------

datablock AudioProfile(MultiPulsarFireSound)
{
   filename    = "fx/vehicles/bomber_turret_fire.wav";
   description = AudioDefault3d;
   preload = true;
};

datablock AudioProfile(MultiPulsarExpSound)
{
   filename    = "fx/armor/light_LF_metal.wav";
   description = AudioDefault3d;
   preload = true;
};

datablock AudioProfile(PulseProjectileSound)
{
   filename    = "fx/weapons/chaingun_projectile.WAV";
   description = ProjectileLooping3d;
   preload = true;
};

//--------------------------------------------------------------------------
// Explosion
//--------------------------------------

datablock ShockwaveData(PulseShockwave)
{
   width = 1;
   numSegments = 32;
   numVertSegments = 7;
   velocity = 8;
   acceleration = -32;
   lifetimeMS = 500;
   height = 0.2;
   verticalCurve = 0.375;

   mapToTerrain = false;
   renderBottom = true;
   orientToNormal = true;


//   texture[0] = "special/crescent4";
   texture[0] = "special/shockwave4";
   texture[1] = "special/gradient";
   texWrap = 3.0;

   times[0] = 0.0;
   times[1] = 0.5;
   times[2] = 1;

   colors[0] = "0.29 0.075 0.247 1.0";
   colors[1] = "0.29 0.075 0.247 1.0";
   colors[2] = "0.29 0.075 0.247 0.0";
};

datablock ExplosionData(MultiPulsarExplosion)
{
   soundProfile   = MultiPulsarExpSound;
   shockwave      = PulseShockwave;
   faceViewer     = true;

   sizes[0] = "0.1 0.1 0.1";
   sizes[1] = "0.1 0.1 0.1";
   times[0]      = 0.0;
   times[1]      = 1.0;
};

datablock ParticleData(LinkTrail)
{
   dragCoeffiecient     = 2.75;
   gravityCoefficient   = 0.1;
   inheritedVelFactor   = 0.2;

   lifetimeMS           = 1000;
   lifetimeVarianceMS   = 100;

   textureName          = "flarebase";

   useInvAlpha =  false;
   spinRandomMin = -100.0;
   spinRandomMax =  100.0;

//   textureName = "special/Smoke/bigSmoke";

   colors[0]     = "0.29 0.075 0.247 1.0";
   colors[1]     = "0.29 0.075 0.247 1.0";
   colors[2]     = "0.29 0.075 0.247 0.5";
   colors[3]     = "0.29 0.075 0.247 0.0";
   sizes[0]      = 1.0;
   sizes[1]      = 0.5;
   sizes[2]      = 0.3;
   sizes[3]      = 0.1;
   times[0]      = 0.0;
   times[1]      = 0.333;
   times[2]      = 0.666;
   times[3]      = 1.0;
};

datablock ParticleEmitterData(LinkTrailEmitter)
{
   ejectionPeriodMS = 10;
   periodVarianceMS = 0;

   ejectionOffset = 0;


   ejectionVelocity = 10;
   velocityVariance = 1.2;

   thetaMin         = 0.0;
   thetaMax         = 40;
   overrideAdvance  = true;

//   lifetimeMS       = 500;
//   particleRadius = 10.25;
particles = "LinkTrail";
};

datablock LinearFlareProjectileData(MultiPulsarBolt)
{
   //projectileShapeName = "energy_bolt.dts";
   scale               = "1.0 1.0 1.0";
   faceViewer          = true;
   hasDamageRadius     = true;
   damageRadius        = 2.0;
   directDamage        = 0.0;
   indirectDamage      = 0.3;
   radiusDamageType    = $DamageType::MultiPulsar;
   kickBackStrength    = 0.0;

   flags               = $Projectile::PlaysHitSound;
   ticking             = false;
   headshotMultiplier  = 1.0;

   baseEmitter[0]      = LinkTrailEmitter;
   explosion           = "MultiPulsarExplosion";
   splash              = discSplash;

   dryVelocity       = 300;
   wetVelocity       = 300;
   velInheritFactor  = 1.0;
   fizzleTimeMS      = 800;
   lifetimeMS        = 1000;
   explodeOnDeath    = false;
   reflectOnWaterImpactAngle = 90;
   explodeOnWaterImpact      = true;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 1500;

   activateDelayMS = -1;

   numFlares         = 10;
   flareColor        = "0.29 0.075 0.247 1";
   flareModTexture   = "flaremod";
   flareBaseTexture  = "flarebase";

   sound        = PulseProjectileSound;

   hasLight    = false;
   lightRadius = 4.0;
   lightColor  = "0 0.6 0";
};

//--------------------------------------------------------------------------
// Weapon
//--------------------------------------
datablock ShapeBaseImageData(MultiPulsarImage)
{
   className = WeaponImage;
   shapeFile = "turret_aa_large.dts";
   item = MultiPulsar;
   projectile = MultiPulsarBolt;
   projectileType = LinearFlareProjectile;

   usesEnergy = true;
   fireEnergy = 5;
   minEnergy = 4;
   
   projectileSpread = 5.0;
   
   subImage1 = "MultiPulsarDecalAImage";
   subImage2 = "MultiPulsarDecalBImage";
   subImage3 = "MultiPulsarDecalCImage";

   stateName[0] = "Activate";
   stateTransitionOnTimeout[0] = "ActivateReady";
   stateTimeoutValue[0] = 0.5;
   stateSequence[0] = "Activate";
   stateSound[0] = BlasterSwitchSound;

   stateName[1] = "ActivateReady";
   stateTransitionOnLoaded[1] = "Ready";
   stateTransitionOnNoAmmo[1] = "NoAmmo";

   stateName[2] = "Ready";
   stateTransitionOnNoAmmo[2] = "NoAmmo";
   stateTransitionOnTriggerDown[2] = "Fire";

   stateName[3] = "Fire";
   stateTransitionOnTimeout[3] = "Reload";
   stateTimeoutValue[3] = 0.2;
   stateFire[3] = true;
   stateRecoil[3] = NoRecoil;
   stateAllowImageChange[3] = false;
   stateSequence[3] = "Fire";
   stateSound[3] = MultiPulsarFireSound;
   stateScript[3] = "onFire";

   stateName[4] = "Reload";
   stateTransitionOnNoAmmo[4] = "NoAmmo";
   stateTransitionOnTimeout[4] = "Ready";
   stateAllowImageChange[4] = false;
   stateSequence[4] = "Reload";

   stateName[5] = "NoAmmo";
   stateTransitionOnAmmo[5] = "Reload";
   stateSequence[5] = "NoAmmo";
   stateTransitionOnTriggerDown[5] = "DryFire";
   
   stateName[6] = "DryFire";
   stateTimeoutValue[6] = 0.3;
   stateSound[6] = BlasterDryFireSound;
   stateTransitionOnTimeout[6] = "Ready";
};

datablock ItemData(MultiPulsar)
{
   className = Weapon;
   catagory = "Spawn Items";
   shapeFile = "turret_aa_large.dts";
   image = MultiPulsarImage;
   mass = 1;
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 2;
	pickUpName = "a multipulsar";
};

datablock ShapeBaseImageData(MultiPulsarDecalAImage)
{
   shapeFile = "TR2weapon_shocklance.dts";
   offset = "0.15 0.25 0";
   rotation = "0 1 0 90";
   emap = true;
   
   subImage = true;
};

datablock ShapeBaseImageData(MultiPulsarDecalBImage)
{
   shapeFile = "weapon_elf.dts";
   offset = "-0.05 0.25 0";
   rotation = "0 1 0 0";
   emap = true;

   subImage = true;
};

datablock ShapeBaseImageData(MultiPulsarDecalCImage)
{
   shapeFile = "weapon_elf.dts";
   rotation = "0 1 0 180";
   offset = "0 0.25 0";
   emap = true;

   subImage = true;
};
