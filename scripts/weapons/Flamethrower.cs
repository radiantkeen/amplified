//--------------------------------------
// Flamethrower
//--------------------------------------

//--------------------------------------------------------------------------
// Sounds
//--------------------------------------

datablock AudioProfile(FlamethrowerFireSound)
{
   filename    = "fx/vehicles/shrike_boost.wav";
   description = AudioDefaultLooping3d;
   preload = true;
};

datablock ParticleData(SmallFlameParticle)
{
    dragCoefficient = 0.0487805;
    gravityCoefficient = -0.25;
    windCoefficient = 0;
    inheritedVelFactor = 0.362903;
    constantAcceleration = 0;
    lifetimeMS = 1654;
    lifetimeVarianceMS = 443;
    useInvAlpha = 0;
    spinRandomMin = -145.161;
    spinRandomMax = 133.065;
    textureName = "special/tracer00.PNG";
    times[0] = 0;
    times[1] = 0.5;
    times[2] = 1;
    colors[0] = "0.892000 0.304000 0.340000 1.000000";
    colors[1] = "0.850394 0.304000 0.192000 0.850394";
    colors[2] = "0.000000 0.000000 0.000000 0.709677";
    sizes[0] = 1.97581;
    sizes[1] = 2.15163;
    sizes[1] = 2.35484;
};

datablock ParticleEmitterData(SmallFlameEmitter)
{
    ejectionPeriodMS = 18;
    periodVarianceMS = 0;
    ejectionVelocity = 5.79032;
    velocityVariance = 2.30645;
    ejectionOffset =   0;
    thetaMin = 0;
    thetaMax = 14.5161;
    phiReferenceVel = 0;
    phiVariance = 360;
    overrideAdvances = 0;
    orientParticles = 0;
    orientOnVelocity = 1;
    particles = "SmallFlameParticle";
};

//--------------------------------------------------------------------------
// Projectile
//--------------------------------------
datablock LinearProjectileData(Flameblast)
{
   projectileShapeName = "plasmabolt.dts";
   emitterDelay        = -1;
   directDamage        = 0.0;
   hasDamageRadius     = true;
   indirectDamage      = 0.1;
   damageRadius        = 6;
   radiusDamageType    = $DamageType::Flamethrower;
   kickBackStrength    = 1;
   baseEmitter         = SmallFlameEmitter;
   bubbleEmitter       = GrenadeBubbleEmitter;

   radialStatusEffect  = "BurnEffect";
   statusEffectChance  = 0.1;
   statusEffectMask    = $TypeMasks::PlayerObjectType;
   
   sound 				= GrenadeProjectileSound;
   explosion           = "PlasmaBoltExplosion";
   splash              = PlayerSplash;

   dryVelocity       = 80;
   wetVelocity       = -1;
   velInheritFactor  = 0.5;
   fizzleTimeMS      = 520;
   lifetimeMS        = 520;
   explodeOnDeath    = false;
   reflectOnWaterImpactAngle = 0;
   explodeOnWaterImpact      = true;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 32;

    activateDelayMS = -1;

   hasLight    = true;
   lightRadius = 6;
   lightColor  = "1 0.8 0.01";
};

//--------------------------------------------------------------------------
// Ammo
//--------------------------------------

datablock ItemData(FlamethrowerAmmo)
{
   className = Ammo;
   catagory = "Ammo";
   shapeFile = "ammo_plasma.dts";
   mass = 1;
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 2;
	pickUpName = "some flamethrower ammo";
};

//--------------------------------------------------------------------------
// Weapon
//--------------------------------------
datablock ShapeBaseImageData(FlamethrowerImage)
{
   className = WeaponImage;
   shapeFile = "turret_tank_barrelchain.dts";
   item = Flamethrower;
   usesEnergy = true;
   minEnergy = 2;
   fireEnergy = 1;
   ammo = FlamethrowerAmmo;

   projectile = Flameblast;
   projectileType = LinearProjectile;

   subImage1 = FlamethrowerDecalAImage;
   subImage2 = FlamethrowerDecalBImage;
   subImage3 = FlamethrowerDecalCImage;

   stateName[0] = "Activate";
   stateTransitionOnTimeout[0] = "ActivateReady";
   stateTimeoutValue[0] = 0.5;
   stateSequence[0] = "Activation";
   stateSound[0] = PlasmaSwitchSound;

   stateName[1] = "ActivateReady";
   stateTransitionOnLoaded[1] = "Ready";
   stateTransitionOnNoAmmo[1] = "NoAmmo";

   stateName[2] = "Ready"; 
   stateTransitionOnNoAmmo[2] = "NoAmmo";
   stateTransitionOnTriggerDown[2] = "Fire";

   stateName[3] = "Fire";
   stateTransitionOnTimeout[3] = "Fire";
   stateTransitionOnNoAmmo[3] = "NoAmmo";
   stateTransitionOnTriggerUp[3] = "Reload";
   stateTimeoutValue[3] = 0.05;
   stateFire[3] = true;
//   stateRecoil[3] = LightRecoil;
   stateAllowImageChange[3] = false;
   stateScript[3] = "onFire";
   stateEmitter[3]       = "SmallFlameEmitter";
   stateEmitterTime[3]       = 0.2;
   stateEmitterNode[3]       = 0;
   stateSequence[3] = "Fire_Vis";
   stateSound[3] = FlamethrowerFireSound;

   stateName[4] = "Reload";
   stateTransitionOnNoAmmo[4] = "NoAmmo";
   stateTransitionOnTimeout[4] = "Ready";
   stateTimeoutValue[4] = 0.5;
   stateAllowImageChange[4] = false;
   stateSequence[4] = "Reload";
   stateSound[4] = PlasmaReloadSound;

   stateName[5] = "NoAmmo";
   stateTransitionOnAmmo[5] = "Reload";
   stateSequence[5] = "NoAmmo";
   stateTransitionOnTriggerDown[5] = "DryFire";

   stateName[6]       = "DryFire";
   stateSound[6]      = PlasmaDryFireSound;
   stateTimeoutValue[6]        = 1.5;
   stateTransitionOnTimeout[6] = "NoAmmo";

   stateName[7]       = "WetFire";
   stateSound[7]      = PlasmaFireWetSound;
   stateTimeoutValue[7]        = 1.5;
   stateTransitionOnTimeout[7] = "Ready";

   stateName[8]               = "CheckWet";
   stateTransitionOnWet[8]    = "WetFire";
   stateTransitionOnNotWet[8] = "Fire";
};

datablock ItemData(Flamethrower)
{
   className = Weapon;
   catagory = "Spawn Items";
   shapeFile = "weapon_disc.dts";
   image = FlamethrowerImage;
   mass = 1;
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 2;
	pickUpName = "a Flamethrower";
};

datablock ShapeBaseImageData(FlamethrowerDecalAImage)
{
   shapeFile = "ammo_disc.dts";
   offset = "0 0.9 0";
   rotation = "1 0 0 90";
   
   subImage = true;
};

datablock ShapeBaseImageData(FlamethrowerDecalBImage)
{
   shapeFile = "ammo_disc.dts";
   offset = "0 0.35 0";
   rotation = "1 0 0 90";
   
   subImage = true;
};

datablock ShapeBaseImageData(FlamethrowerDecalCImage)
{
   shapeFile = "repair_patch.dts";
   offset = "0 0.6 0";
   rotation = "1 0 0 90";
   
   subImage = true;
};
