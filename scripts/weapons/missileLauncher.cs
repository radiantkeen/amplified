//--------------------------------------
// Missile launcher
//--------------------------------------

//--------------------------------------------------------------------------
// Debris
//--------------------------------------
datablock DebrisData( FlechetteDebris )
{
   shapeName = "weapon_missile_fleschette.dts";

   lifetime = 5.0;

   minSpinSpeed = -320.0;
   maxSpinSpeed = 320.0;

   elasticity = 0.2;
   friction = 0.3;

   numBounces = 3;

   gravModifier = 0.40;

   staticOnMaxBounce = true;
};             

//--------------------------------------------------------------------------
// Projectile
//--------------------------------------
datablock SeekerProjectileData(ShoulderMissile)
{
   casingShapeName     = "weapon_missile_casement.dts";
   projectileShapeName = "weapon_missile_projectile.dts";
   hasDamageRadius     = true;
   indirectDamage      = 2.0;
   damageRadius        = 8.0;
   radiusDamageType    = $DamageType::Missile;
   kickBackStrength    = 2000;

   explosion           = "MissileExplosion";
   splash              = MissileSplash;
   velInheritFactor    = 1.0;    // to compensate for slow starting velocity, this value
                                 // is cranked up to full so the missile doesn't start
                                 // out behind the player when the player is moving
                                 // very quickly - bramage

   flags               = $Projectile::PlayerFragment | $Projectile::PlaysHitSound;
   ticking             = false;
   headshotMultiplier  = 1.0;
   
   baseEmitter         = MissileSmokeEmitter;
   delayEmitter        = MissileFireEmitter;
   puffEmitter         = MissilePuffEmitter;
   bubbleEmitter       = GrenadeBubbleEmitter;
   bubbleEmitTime      = 1.0;

   exhaustEmitter      = MissileLauncherExhaustEmitter;
   exhaustTimeMs       = 500;
   exhaustNodeName     = "muzzlePoint1";

   lifetimeMS          = 7000; // z0dd - ZOD, 4/14/02. Was 6000
   muzzleVelocity      = 50.0;
   maxVelocity         = 112.0; // z0dd - ZOD, 4/14/02. Was 80.0
   turningSpeed        = 90.0;
   acceleration        = 200.0;

   proximityRadius     = 1.5;

   terrainAvoidanceSpeed         = 1;
   terrainScanAhead              = 1;
   terrainHeightFail             = 0.5;
   terrainAvoidanceRadius        = 1;
   
   flareDistance = 10;
   flareAngle    = 10;

   sound = MissileProjectileSound;

   hasLight    = true;
   lightRadius = 5.0;
   lightColor  = "0.2 0.05 0";

   useFlechette = true;
   flechetteDelayMs = 32;
   casingDeb = FlechetteDebris;

   explodeOnWaterImpact = false;
};

datablock SeekerProjectileData(LaserShoulderMissile) : ShoulderMissile
{
   muzzleVelocity      = 50.0;
   maxVelocity         = 100.0;
   acceleration        = 20.0;
   
   lifetimeMS          = 10000;
   turningSpeed        = 180.0;
};

datablock LinearProjectileData(Rocket)
{
   projectileShapeName = "weapon_missile_projectile.dts";
   emitterDelay = -1;
   baseEmitter = MissileSmokeEmitter;
   delayEmitter = MissileFireEmitter;
   bubbleEmitter = GrenadeBubbleEmitter;
   bubbleEmitTime = 1.0;
   directDamage = 0.0;
   hasDamageRadius = true;
   indirectDamage = 1.5;
   damageRadius = 6.0;

   flags               = $Projectile::PlayerFragment | $Projectile::PlaysHitSound;
   ticking             = false;
   headshotMultiplier  = 1.0;
   
   radiusDamageType = $DamageType::Missile;
   kickBackStrength = 2000;
   sound = MissileProjectileSound;
   explosion = "MissileExplosion";
   underwaterExplosion = "UnderwaterDiscExplosion";
   splash = MissileSplash;
   dryVelocity = 115;
   wetVelocity = 70;
   velInheritFactor = 0.75;
   fizzleTimeMS = 4000;
   lifetimeMS = 4000;
   explodeOnDeath = true;
   reflectOnWaterImpactAngle = 15.0;
   explodeOnWaterImpact = false;
   deflectionOnWaterImpact = 20.0;
   fizzleUnderwaterMS = 4000;
//   activateDelayMS = 200;
   hasLight = true;
   lightRadius = 6.0;
   lightColor = "0.175 0.175 0.5";
};

//--------------------------------------------------------------------------
// Ammo
//--------------------------------------

datablock ItemData(MissileLauncherAmmo)
{
   className = Ammo;
   catagory = "Ammo";
   shapeFile = "ammo_missile.dts";
   mass = 1;
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 2;
	pickUpName = "some missiles";

   computeCRC = true;

};

//--------------------------------------------------------------------------
// Weapon
//--------------------------------------
datablock ItemData(MissileLauncher)
{
   className = Weapon;
   catagory = "Spawn Items";
   shapeFile = "weapon_missile.dts";
   image = MissileLauncherImage;
   mass = 1;
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 2;
	pickUpName = "a missile launcher";

   computeCRC = true;
   emap = true;
};

datablock ShapeBaseImageData(MissileLauncherImage)
{
   className = WeaponImage;
   shapeFile = "weapon_missile.dts";
   item = MissileLauncher;
   ammo = MissileLauncherAmmo;
   offset = "0 0 0";
   armThread = lookms;
   emap = true;

   projectile = ShoulderMissile;
   projectileType = SeekerProjectile;

   isSeeker     = true;
   seekRadius   = 450;
   maxSeekAngle = 10;
   seekTime     = 0.8;
   minSeekHeat  = 0.6;  // the heat that must be present on a target to lock it.

   // only target objects outside this range
   minTargetingDistance             = 40;
   
   stateName[0]                     = "Activate";
   stateTransitionOnTimeout[0]      = "ActivateReady";
   stateTimeoutValue[0]             = 0.5;
   stateSequence[0]                 = "Activate";
   stateSound[0]                    = MissileSwitchSound;

   stateName[1]                     = "ActivateReady";
   stateTransitionOnLoaded[1]       = "Ready";
   stateTransitionOnNoAmmo[1]       = "NoAmmo";

   stateName[2]                     = "Ready";
   stateTransitionOnNoAmmo[2]       = "NoAmmo";
   stateTransitionOnTriggerDown[2]  = "Fire";

   stateName[3]                     = "Fire";
   stateTransitionOnTimeout[3]      = "Reload";
   stateTimeoutValue[3]             = 0.4;
   stateFire[3]                     = true;
   stateRecoil[3]                   = LightRecoil;
   stateAllowImageChange[3]         = false;
   stateSequence[3]                 = "Fire";
   stateScript[3]                   = "onFire";
   stateSound[3]                    = MissileFireSound;

   stateName[4]                     = "Reload";
   stateTransitionOnNoAmmo[4]       = "NoAmmo";
   stateTransitionOnTimeout[4]      = "Ready";
   stateTimeoutValue[4]             = 2.5;
   stateAllowImageChange[4]         = false;
   stateSequence[4]                 = "Reload";
   stateSound[4]                    = MissileReloadSound;

   stateName[5]                     = "NoAmmo";
   stateTransitionOnAmmo[5]         = "Reload";
   stateSequence[5]                 = "NoAmmo";
   stateTransitionOnTriggerDown[5]  = "DryFire";

   stateName[6]                     = "DryFire";
   stateSound[6]                    = MissileDryFireSound;
   stateTimeoutValue[6]             = 1.0;
   stateTransitionOnTimeout[6]      = "ActivateReady";
};

function MissileLauncherImage::spawnProjectile(%data, %obj, %slot)
{
    %vector = %obj.getMuzzleVector(%slot);
    %point = %obj.getMuzzlePoint(%slot);
    %target = %obj.getLockedTarget();

    if(%target || %obj.isLocked())
        %proj = createProjectile(%data.projectileType, %data.projectile, %vector, %point, %obj, %slot, %obj);
    else
        %proj = createProjectile("LinearProjectile", "Rocket", %vector, %point, %obj, %slot, %obj);
        
    %proj.damageBuffFactor = %obj.damageBuffFactor;
    MissileSet.add(%proj);
    
    if(%data.deleteLastProjectile)
    {
        if(isObject(%obj.lastProjectile))
            %obj.lastProjectile.delete();

        %obj.deleteLastProjectile = %data.deleteLastProjectile;
    }

    %obj.lastProjectile = %proj;

    // AI hook
    if(%obj.client)
        %obj.client.projectile = %proj;

    if(%target)
         %proj.setObjectTarget(%target);
    else if(%obj.isLocked())
        %proj.setPositionTarget(%obj.getLockedPosition());

    return %proj;
}

//--------------------------------------------------------------------------
// Weapon
//--------------------------------------
datablock TargetProjectileData(RLLaserPointer)
{
   directDamage        	= 0.0;
   hasDamageRadius     	= false;
   indirectDamage      	= 0.0;
   damageRadius        	= 0.0;
   velInheritFactor    	= 1.0;

   maxRifleRange       	= 300;
   beamColor           	= "0.25 1.0 0.1";

   startBeamWidth			= 0.20;
   pulseBeamWidth 	   = 0.15;
   beamFlareAngle 	   = 3.0;
   minFlareSize        	= 0.0;
   maxFlareSize        	= 400.0;
   pulseSpeed          	= 6.0;
   pulseLength         	= 0.150;

   textureName[0]      	= "special/nonlingradient";
   textureName[1]      	= "special/flare";
   textureName[2]      	= "special/pulse";
   textureName[3]      	= "special/expFlare";
   beacon               = true;
};

datablock ShapeBaseImageData(RLLaserEmitter)
{
   className = WeaponImage;
   shapeFile = "weapon_targeting.dts";
   emap = true;
   offset = "0.015 -0.5 0.15";
   rotation = "0 1 0 -10";
   armThread = lookms;

   subImage = true;

   stateName[0]                     = "Activate";
   stateSequence[0]                 = "Activate";
};

datablock ItemData(LaserMissileLauncher)
{
   className = Weapon;
   catagory = "Spawn Items";
   shapeFile = "weapon_missile.dts";
   image = LaserMissileLauncherImage;
   mass = 1;
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 2;
	pickUpName = "a laser missile launcher";

   computeCRC = true;
   emap = true;
};

datablock ShapeBaseImageData(LaserMissileLauncherImage)
{
   className = WeaponImage;
   shapeFile = "weapon_missile.dts";
   item = LaserMissileLauncher;
   ammo = MissileLauncherAmmo;
   offset = "0 0 0";
   armThread = lookms;
   emap = true;

   projectile = LaserShoulderMissile;
   projectileType = SeekerProjectile;

   subImage1 = RLLaserEmitter;

   stateName[0]                     = "Activate";
   stateTransitionOnTimeout[0]      = "ActivateReady";
   stateTimeoutValue[0]             = 0.5;
   stateSequence[0]                 = "Activate";
   stateSound[0]                    = MissileSwitchSound;

   stateName[1]                     = "ActivateReady";
   stateTransitionOnLoaded[1]       = "Ready";
   stateTransitionOnNoAmmo[1]       = "NoAmmo";

   stateName[2]                     = "Ready";
   stateTransitionOnNoAmmo[2]       = "NoAmmo";
   stateTransitionOnTriggerDown[2]  = "Fire";

   stateName[3]                     = "Fire";
   stateTransitionOnTimeout[3]      = "Reload";
   stateTimeoutValue[3]             = 0.4;
   stateFire[3]                     = true;
   stateRecoil[3]                   = LightRecoil;
   stateAllowImageChange[3]         = false;
   stateSequence[3]                 = "Fire";
   stateScript[3]                   = "onFire";
   stateSound[3]                    = MissileFireSound;

   stateName[4]                     = "Reload";
   stateTransitionOnNoAmmo[4]       = "NoAmmo";
   stateTransitionOnTimeout[4]      = "Ready";
   stateTimeoutValue[4]             = 2.5;
   stateAllowImageChange[4]         = false;
   stateSequence[4]                 = "Reload";
   stateSound[4]                    = MissileReloadSound;

   stateName[5]                     = "NoAmmo";
   stateTransitionOnAmmo[5]         = "Reload";
   stateSequence[5]                 = "NoAmmo";
   stateTransitionOnTriggerDown[5]  = "DryFire";

   stateName[6]                     = "DryFire";
   stateSound[6]                    = MissileDryFireSound;
   stateTimeoutValue[6]             = 1.0;
   stateTransitionOnTimeout[6]      = "ActivateReady";
};

function LaserMissileLauncherImage::onFire(%data, %obj, %slot)
{
    if(%obj.rlLaserMissile > 0)
        return;

    %p = Parent::onFire(%data, %obj, %slot);

    if(!%p)
        return;

    %obj.rlLaserMissile = %p;
    %obj.rlLaserProj = createProjectile("TargetProjectile", "RLLaserPointer", %obj.getMuzzleVector($WeaponSubImage), %obj.getMuzzlePoint($WeaponSubImage), %obj, $WeaponSubImage, %obj);
    
    %obj.rlBeacon = new WayPoint()
    {
        position = "0 0 10000";
        rotation = "1 0 0 0";
        scale = "1 1 1";
        name = "";
        dataBlock = "WayPointMarker";
        lockCount = "0";
        homingCount = "0";
        team = %obj.client.team;
    };

    MissionCleanup.add(%obj.rlBeacon);
    %p.setObjectTarget(%obj.rlBeacon);
    %data.missileTrackLaser(%obj, %p);
}

function LaserMissileLauncherImage::MissileTrackLaser(%data, %obj, %p)
{
    if(!%obj.isDead && isObject(%p) && !%p.hitSomething && %obj.rlLaserProj > 0)
    {
        %dist = RLLaserPointer.maxRifleRange;
        %muzzlePoint = %obj.getMuzzlePoint(0);
        %muzzleVector = %obj.getMuzzleVector(0);
        %point = castRay(%muzzlePoint, %muzzleVector, %dist, $TypeMasks::TerrainObjectType | $TypeMasks::InteriorObjectType | $TypeMasks::PlayerObjectType | $TypeMasks::VehicleObjectType | $TypeMasks::StaticShapeObjectType | $TypeMasks::TurretObjectType | $TypeMasks::ItemObjectType, %obj);

        if(%point)
            %obj.rlBeacon.setPosition(%point.hitPos);
        else
            %obj.rlbeacon.setPosition(vectorProject(%muzzlePoint, %muzzleVector, %dist));

        %data.schedule(32, "MissileTrackLaser", %obj, %p);
    }
    else
    {
        %obj.rlLaserMissile = 0;
        %obj.rlBeacon.delete();
        %obj.rlLaserProj.delete();
        %obj.rlLaserProj = 0;
    }
}
