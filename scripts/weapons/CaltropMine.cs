// ----------------------------------------------
// Caltrop Mine Script
// ----------------------------------------------
// ----------------------------------------------
// Item datablocks
// ----------------------------------------------

datablock ItemData(CaltropMineDeployed)
{
   className = Weapon;
   shapeFile = "mine.dts";
   mass = 0.75;
   elasticity = 0.2;
   friction = 0.8; // z0dd - ZOD, 9/27/03. was 0.6
   pickupRadius = 3;
   maxDamage = 0.01; // z0dd - ZOD, 9/27/03. was 0.2
   explosion = PoisonExplosion;
   underwaterExplosion = PoisonExplosion;
   indirectDamage = 0.5; // z0dd - ZOD, 7/14/03. Slight increase to dmg. Was 0.55
   damageRadius = 4; // z0dd - ZOD, 7/14/03. Slight increase to det range. Was 6.0
   radiusDamageType = $DamageType::Poison;
   kickBackStrength = 1500;
   aiAvoidThis = true;
   dynamicType = $TypeMasks::DamagableItemObjectType;
   spacing = 15.0; // how close together mines can be
   proximity = 0.5; // how close causes a detonation (by player/vehicle)
   armTime = 1500; // 2.2 seconds to arm a mine after it comes to rest
   maxDepCount = 5; // try to deploy this many times before detonating. // z0dd - ZOD, 9/27/02. Was 9
   isMine = true;
   
   radialStatusEffect  = "PoisonEffect";
   statusEffectChance  = 1.0;
   statusEffectMask    = $TypeMasks::PlayerObjectType;
   
   computeCRC = true;
};

datablock ItemData(CaltropMine)
{     
   className = HandInventory;
   catagory = "Handheld";
   shapeFile = "mine.dts";
   mass = 1;
   elasticity = 0.2;
   friction = 0.7;
   pickupRadius = 2;

   thrownItem = CaltropMineDeployed;
   pickUpName = "some caltrop mines";
   isMine = true; // z0dd - ZOD, 5/19/03. New param.
   computeCRC = true;
};

function CaltropMineDeployed::onThrow(%this, %mine, %thrower)
{
   %mine.armed = false;
   %mine.damaged = 0;
   %mine.detonated = false;
   %mine.depCount = 0;
   %mine.theClient = %thrower.client;
   $TeamDeployedCount[%mine.sourceObject.team, MineDeployed]++; // z0dd - ZOD, 8/13/02, Moved this from deployMineCheck to here. Fixes mine count bug

   schedule(1500, %mine, "deployCaltropCheck", %mine, %thrower);
}

function deployCaltropCheck(%mineObj, %player)
{
   if(%mineObj.depCount > %mineObj.getDatablock().maxDepCount)
      explodeMine(%mineObj, true);

   // wait until the mine comes to rest
   if(VectorLen(%mineObj.getVelocity()) < 1)
   {
      // 2-second delay before mine is armed -- let deploy thread play out etc.
      schedule(%mineObj.getDatablock().armTime, %mineObj, "armDeployedMine", %mineObj);

      // check for other deployed mines in the vicinity
      InitContainerRadiusSearch(%mineObj.getWorldBoxCenter(), %mineObj.getDatablock().spacing, $TypeMasks::ItemObjectType);
      while((%itemObj = containerSearchNext()) != 0)
      {
         if(%itemObj == %mineObj)
            continue;

         %isMine = (%itemObj.getDatablock().isMine == true);

         if(%isMine)
            schedule(100, %mineObj, "explodeMine", %mineObj, true);
         else
            continue;
      }
      // play "deploy" thread
      %mineObj.playThread(0, "deploy");
      serverPlay3D(MineDeploySound, %mineObj.getTransform());
      %mineTeam = %mineObj.sourceObject.team;
      //$TeamDeployedCount[%mineTeam, MineDeployed]++; // z0dd - ZOD, 8/13/02, Moved the increment to MineDeployed::onThrow. Fixes mine count bug
      if($TeamDeployedCount[%mineTeam, MineDeployed] > $TeamDeployableMax[MineDeployed])
      {
         messageClient( %player.client, '', 'Maximum allowable mines deployed.' );
         schedule(100, %mineObj, "explodeMine", %mineObj, true);
      }
      else
      {
         //start the thread that keeps checking for objects near the mine...
         mineCheckVicinity(%mineObj);

         //let the AI know *after* it's come to rest...
         AIDeployMine(%mineObj);

         //let the game know there's a deployed mine
         Game.notifyMineDeployed(%mineObj);
         %mineObj.setCloaked(true);
      }
   }
   else
   {
      //schedule this deploy check again a little later
      %mineObj.depCount++;
      schedule(500, %mineObj, "deployCaltropCheck", %mineObj, %player);
   }
}

function CaltropMineDeployed::onCollision(%data, %obj, %col)
{
   // don't detonate if mine isn't armed yet
   if(!%obj.armed)
      return;

   // don't detonate if mine is already detonating
   if(%obj.boom)
      return;

   %noExplode = 0;
   //check to see what it is that collided with the mine
   if(%col.isPlayer())
   {
      if(!$teamDamage)
      {
         if(%obj.team == %col.getOwnerClient().team)
            %noExplode = 1;
      }
      if(%noExplode == 0)
      {
         //error("Mine detonated due to collision with #"@%col@" ("@%struck@"); armed = "@%obj.armed);
         explodeMine(%obj, false);
      }
   }
}

function CaltropMineDeployed::damageObject(%data, %targetObject, %sourceObject, %position, %amount, %damageType)
{
   // -----------------------------
   // z0dd - ZOD, 5/09/04. If gameplay changes in affect, no mine disc
   // Caltrop mines need to be armed first
//   if($Host::ClassicLoadMineChanges)
//   {
      if(!%targetObject.armed)
         return;
//   }
   // -----------------------------

   if(%targetObject.boom)
      return;

   %targetObject.damaged += %amount;

   if(%targetObject.damaged >= %data.maxDamage)
   {
      %targetObject.setDamageState(Destroyed);
   }
}

function CaltropMineDeployed::onDestroyed(%data, %obj, %lastState)
{
   %obj.boom = true;
   %mineTeam = %obj.team;
   $TeamDeployedCount[%mineTeam, MineDeployed]--;

       RadiusExplosion(%obj,
                      %obj.getPosition(),
                      %data.damageRadius,
                      %data.indirectDamage,
                      %data.kickBackStrength,
                      %obj.sourceObject,
                      %data.radiusDamageType);

   %obj.schedule(600, "delete");
}
