datablock AudioProfile(ThrustUseSound)
{
   filename = "fx/weapons/grenade_flash_explode.wav";
   description = AudioDefault3d;
   preload = true;
};

datablock ItemData(ThrusterGrenade)
{
   className = HandInventory;
   catagory = "Handheld";
   shapeFile = "grenade.dts";
   mass = 0.7;
   elasticity = 0.2;
   friction = 1;
   pickupRadius = 2;
   thrownItem = FlareGrenadeThrown;
	pickUpName = "some Thrusters";
	isGrenade = true;

   computeCRC = true;
};

function ThrusterGrenade::onUse(%this, %obj)
{
    if(Game.handInvOnUse(%this, %obj))
    {
        %time = getSimTime();

        if(%time > %obj.nextUseTime[%this] && %obj.getEnergyLevel() > 25)
        {
            %obj.useEnergy(25);
            %obj.decInventory(%this, 1);
            %obj.applyKick(6000); // %obj.getDatablock().mass * 5 * 16
            %obj.play3D(ThrustUseSound);
            createRemoteProjectile("LinearFlareProjectile", "PowerDisplayCharge", %obj.getMuzzleVector(0), %obj.position, 0, %obj);

            // miscellaneous grenade-throwing cleanup stuff
            %obj.lastThrowTime[%this] = %time;
            %obj.nextUseTime[%this] = %time + 6000;
            %obj.throwStrength = 0;
        }
        else if(%obj.getEnergyLevel() < 25)
            messageClient(%obj.client, 'MsgThrusterLowPower', "\c3Not enough energy to to ignite thruster.");
        else
            messageClient(%obj.client, 'MsgThrusterRecharge', "\c3Thrusters recharging...");
    }
}
