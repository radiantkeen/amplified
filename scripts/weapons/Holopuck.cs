// ----------------------------------------------
// Holopuck Mine Script
// ----------------------------------------------
// ----------------------------------------------
// Item datablocks
// ----------------------------------------------

datablock ItemData(HolopuckMineDeployed)
{
   className = Weapon;
   shapeFile = "ammo_disc.dts";
   mass = 1.5;
   elasticity = 0.2;
   friction = 0.8; // z0dd - ZOD, 9/27/03. was 0.6
   pickupRadius = 3;
   maxDamage = 0.01; // z0dd - ZOD, 9/27/03. was 0.2
   explosion = MineExplosion;
   underwaterExplosion = UnderwaterMineExplosion;
   indirectDamage = 1.5; // z0dd - ZOD, 7/14/03. Slight increase to dmg. Was 0.55
   damageRadius = 16; // z0dd - ZOD, 7/14/03. Slight increase to det range. Was 6.0
   radiusDamageType = $DamageType::Mine;
   kickBackStrength = 1500;
   aiAvoidThis = true;
   dynamicType = $TypeMasks::DamagableItemObjectType;
   spacing = 50.0; // how close together mines can be
   proximity = 8; // how close causes a detonation (by player/vehicle)
   armTime = 1500; // 2.2 seconds to arm a mine after it comes to rest
   maxDepCount = 5; // try to deploy this many times before detonating. // z0dd - ZOD, 9/27/02. Was 9
   isMine = true;
   
   computeCRC = true;
};

datablock ItemData(HolopuckMine)
{     
   className = HandInventory;
   catagory = "Handheld";
   shapeFile = "ammo_disc.dts";
   mass = 1;
   elasticity = 0.2;
   friction = 0.7;
   pickupRadius = 2;

   thrownItem = HolopuckMineDeployed;
   pickUpName = "some holopuck mines";
   isMine = true; // z0dd - ZOD, 5/19/03. New param.
   computeCRC = true;
};

datablock AudioProfile(HoloPuckActivate)
{
   filename    = "fx/powered/vehicle_screen_off.wav";
   description = AudioDefault3d;
   preload = true;
};

datablock StaticShapeData(LightMaleHumanHolopuck) : StaticShapeDamageProfile
{
   shapeFile = "light_male.dts";
   maxDamage = 0.1;
   destroyedLevel = 0.1;
   disabledLevel = 0.1;
   explosion      = MineExplosion;
   explosion      = UnderwaterMineExplosion;
   expDmgRadius = 2.0;
   expDamage = 0.5;
   expImpulse = 0.0;

   isShielded = false;
   targetNameTag = 'Player';
   targetTypeTag = 'Test';

   debrisShapeName = "debris_player.dts";
   debris = playerDebris;
};

function LightMaleHumanHolopuck::onAdd(%data, %obj)
{
    %data.setupHolopuck(%obj);
}

datablock StaticShapeData(LightFemaleHumanHolopuck) : LightMaleHumanHolopuck
{
   shapeFile = "light_female.dts";
};

function LightFemaleHumanHolopuck::onAdd(%data, %obj)
{
    %data.setupHolopuck(%obj);
}

datablock StaticShapeData(LightMaleBiodermHolopuck) : LightMaleHumanHolopuck
{
   shapeFile = "bioderm_light.dts";
   
   debrisShapeName = "bio_player_debris.dts";
   debris = BiodermDebris;
};

function LightMaleBiodermHolopuck::onAdd(%data, %obj)
{
    %data.setupHolopuck(%obj);
}

datablock StaticShapeData(MediumMaleHumanHolopuck) : LightMaleHumanHolopuck
{
   shapeFile = "medium_male.dts";
};

function MediumMaleHumanHolopuck::onAdd(%data, %obj)
{
    %data.setupHolopuck(%obj);
}

datablock StaticShapeData(MediumFemaleHumanHolopuck) : LightMaleHumanHolopuck
{
   shapeFile = "medium_female.dts";
};

function MediumFemaleHumanHolopuck::onAdd(%data, %obj)
{
    %data.setupHolopuck(%obj);
}

datablock StaticShapeData(MediumMaleBiodermHolopuck) : LightMaleBiodermHolopuck
{
   shapeFile = "bioderm_medium.dts";
};

function MediumMaleBiodermHolopuck::onAdd(%data, %obj)
{
    %data.setupHolopuck(%obj);
}

datablock StaticShapeData(HeavyMaleHumanHolopuck) : LightMaleHumanHolopuck
{
   shapeFile = "heavy_male.dts";
};

function HeavyMaleHumanHolopuck::onAdd(%data, %obj)
{
    %data.setupHolopuck(%obj);
}

datablock StaticShapeData(HeavyMaleBiodermHolopuck) : LightMaleBiodermHolopuck
{
   shapeFile = "bioderm_heavy.dts";
};

function HeavyMaleBiodermHolopuck::onAdd(%data, %obj)
{
    %data.setupHolopuck(%obj);
}

//function LightMaleHumanHolopuck::damageObject(%data, %targetObject, %position, %sourceObject, %amount, %damageType)
//{
//    %targetObject.parent.getDatablock().damageObject(%targetObject, %position, %sourceObject, %amount, %damageType);
//}

//function LightMaleHumanHolopuck::onDestroyed(%data, %obj, %prevState)
//{
//    %obj.schedule(250, "delete");
//}

function ShapeBaseData::setupHolopuck(%data, %obj)
{
    %obj.target = createTarget(%obj, %obj.name, %obj.skin, %obj.voice, "", %obj.team, 0); // obj, name, skin, voice, type, sensorgroup, pitch
}

function HolopuckMineDeployed::onThrow(%this, %mine, %thrower)
{
   // Holopuck clones owner's visuals
   %client = %thrower.client;
   %mine.armorSize = %thrower.getDatablock().flags;
   %mine.armor = %client.armor;
   %mine.sex = %client.sex;
   %mine.race = %client.race;
   %mine.holoname = %client.name;
   %mine.holoskin = %client.skin;
   %mine.holovoice = %client.voice;
   
   for(%i = 0; %i < 8; %i++)
      %mine.images[%i] = %thrower.getMountedImage(%i);
   
   %mine.armed = false;
   %mine.damaged = 0;
   %mine.detonated = false;
   %mine.depCount = 0;
   %mine.theClient = %thrower.client;
   $TeamDeployedCount[%mine.sourceObject.team, MineDeployed]++; // z0dd - ZOD, 8/13/02, Moved this from deployMineCheck to here. Fixes mine count bug

   schedule(1500, %mine, "deployHolopuckCheck", %mine, %thrower);
}

function deployHolopuckCheck(%mineObj, %player)
{
   if(%mineObj.depCount > %mineObj.getDatablock().maxDepCount)
      explodeMine(%mineObj, true);

   // wait until the mine comes to rest
   if(VectorLen(%mineObj.getVelocity()) < 1)
   {
      // 2-second delay before mine is armed -- let deploy thread play out etc.
      schedule(%mineObj.getDatablock().armTime, %mineObj, "armDeployedMine", %mineObj);

      // check for other deployed mines in the vicinity
      InitContainerRadiusSearch(%mineObj.getWorldBoxCenter(), %mineObj.getDatablock().spacing, $TypeMasks::ItemObjectType);
      while((%itemObj = containerSearchNext()) != 0)
      {
         if(%itemObj == %mineObj)
            continue;

         %isMine = (%itemObj.getDatablock().isMine == true);

         if(%isMine)
            schedule(100, %mineObj, "explodeMine", %mineObj, true);
         else
            continue;
      }
      // play "deploy" thread
//      %mineObj.playThread(0, "deploy");
      %mineTeam = %mineObj.sourceObject.team;
      //$TeamDeployedCount[%mineTeam, MineDeployed]++; // z0dd - ZOD, 8/13/02, Moved the increment to MineDeployed::onThrow. Fixes mine count bug
      if($TeamDeployedCount[%mineTeam, MineDeployed] > $TeamDeployableMax[MineDeployed])
      {
         messageClient( %player.client, '', 'Maximum allowable mines deployed.' );
         schedule(100, %mineObj, "explodeMine", %mineObj, true);
      }
      else
      {
         //start the thread that keeps checking for objects near the mine...
         mineCheckVicinity(%mineObj);

         //let the AI know *after* it's come to rest...
         AIDeployMine(%mineObj);

         //let the game know there's a deployed mine
         Game.notifyMineDeployed(%mineObj);
         %mineObj.play3D("HoloPuckActivate");
         
         %size = "Light";
         %gender = %mineObj.sex;
         %race = %mineObj.race;
         
         if(%mineObj.armorSize & $ArmorMask::Heavy)
         {
            %gender = "Male";
            %size = "Heavy";
         }
         else if(%mineObj.armorSize & $ArmorMask::Medium)
            %size = "Medium";
         else if(%mineObj.armorSize & $ArmorMask::Light)
            %size = "Light";

         %block = %size@%gender@%race@"Holopuck";
         error(%block);
//         %block = "LightMaleHumanHolopuck";
         
         %clone = new StaticShape()
         {
            dataBlock = %block;
            armor = %mineObj.armor;
            armorSize = %mineObj.armorSize;
            sex = %mineObj.sex;
            race = %mineObj.race;
            name = %mineObj.holoname;
            voice = %mineObj.holovoice; // potential expansion
            skin = %mineObj.holoskin;
            parent = %mineObj;
            team = %mineTeam;
         };
         
         for(%i = 0; %i < 8; %i++)
            %clone.mountImage(%mineObj.images[%i], %i);
         
         %mineObj.setCloaked(true);
         %clone.setCloaked(true);
         %clone.schedule(1000, "setCloaked", false);
         %mineObj.holo = %clone;
         %clone.setPosition(%mineObj.position);
      }
   }
   else
   {
      //schedule this deploy check again a little later
      %mineObj.depCount++;
      schedule(500, %mineObj, "deployHolopuckCheck", %mineObj, %player);
   }
}

function HolopuckMineDeployed::onCollision(%data, %obj, %col)
{
   // don't detonate if mine isn't armed yet
   if(!%obj.armed)
      return;

   // don't detonate if mine is already detonating
   if(%obj.boom)
      return;

   %noExplode = 0;
   //check to see what it is that collided with the mine
   if(%col.isPlayer() || %col.isVehicle())
   {
      if(!$teamDamage)
      {
         if(%obj.team == %col.getOwnerClient().team)
            %noExplode = 1;
      }
      if(%noExplode == 0)
      {
         //error("Mine detonated due to collision with #"@%col@" ("@%struck@"); armed = "@%obj.armed);
         explodeMine(%obj, false);
      }
   }
}

function HolopuckMineDeployed::damageObject(%data, %targetObject, %sourceObject, %position, %amount, %damageType)
{
   // -----------------------------
   // z0dd - ZOD, 5/09/04. If gameplay changes in affect, no mine disc
   // Holopuck mines need to be armed first
//   if($Host::ClassicLoadMineChanges)
//   {
      if(!%targetObject.armed)
         return;
//   }
   // -----------------------------

   if(%targetObject.boom)
      return;

   %targetObject.damaged += %amount;

   if(%targetObject.damaged >= %data.maxDamage)
   {
      %targetObject.setDamageState(Destroyed);
   }
}

function HolopuckMineDeployed::onDestroyed(%data, %obj, %lastState)
{
   %obj.boom = true;
   %mineTeam = %obj.team;
   $TeamDeployedCount[%mineTeam, MineDeployed]--;

       RadiusExplosion(%obj,
                      %obj.getPosition(),
                      %data.damageRadius,
                      %data.indirectDamage,
                      %data.kickBackStrength,
                      %obj.sourceObject,
                      %data.radiusDamageType);

   %obj.schedule(600, "delete");
   
   if(isObject(%obj.holo))
      %obj.holo.schedule(256, "delete");
}
