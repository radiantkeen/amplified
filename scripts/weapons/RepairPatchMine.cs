// ----------------------------------------------
// Repair Patch (Mine)
// ----------------------------------------------

$TeamDeployableMax[RepairPatchMineDeployed]		= 10;

datablock ItemData(RepairPatchMineDeployed) : MineDeployed
{
   className = Weapon;
   shapeFile = "repair_kit.dts";
   mass = 0.75;
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 3;
   maxDamage = 0.2;
   explosion = ConcussionGrenadeExplosion;
//   underwaterExplosion = UnderwaterMineExplosion;
   indirectDamage = 0.001;
   damageRadius = 0.0;
   radiusDamageType = $DamageType::Mine;
   kickBackStrength = 0;
	aiAvoidThis = false;
   dynamicType = $TypeMasks::DamagableItemObjectType;
	spacing = 6.0; // how close together mines can be
	proximity = 1.0; // how close causes a detonation (by player/vehicle)
	armTime = 2000; // 2.2 seconds to arm a mine after it comes to rest
	maxDepCount = 10; // try to deploy this many times before detonating
};

datablock ItemData(RepairPatchMine)
{
   className = HandInventory;
   catagory = "Handheld";
   shapeFile = "repair_patch.dts";
   mass = 1;
   elasticity = 0.2;
   friction = 0.7;
   pickupRadius = 2;

   thrownItem = RepairPatchMineDeployed; // RepairPatch
	pickUpName = "some deployable repair patches";
};

function RepairPatchMineDeployed::onThrow(%this, %mine, %thrower)
{
   %mine.armed = false;
   %mine.damaged = 0;
   %mine.detonated = false;
   %mine.depCount = 0;
   %mine.theClient = %thrower.client;

   schedule(1500, %mine, "deployRPMineCheck", %mine, %thrower);
}

function deployRPMineCheck(%mineObj, %player)
{
   if(vectorLen(%mineObj.getVelocity()) < 1)
   {
      %thrownItem = new Item()
      {
         dataBlock = RepairPatch;
         sourceObject = %obj;
         isTemporary = true;
         rotate = false;
      };
      
      MissionCleanup.add(%thrownItem);
      %thrownItem.setPosition(%mineObj.getPosition());
      %mineObj.schedule(500, "delete");
   }
   else
   {
      //schedule this deploy check again a little later
      %mineObj.depCount++;

      if(%mineObj.depCount > 30)
        %mineObj.delete();
        
      schedule(500, %mineObj, "deployRPMineCheck", %mineObj, %player);
   }
}

function RepairPatchMineDeployed::onCollision(%data, %obj, %col)
{
    return;
}
