// ------------------------------------------------------------------------
// EMP grenade (thrown by hand) script
// ------------------------------------------------------------------------
datablock AudioProfile(EMPExpSound)
{
   filename    = "fx/weapons/shocklance_fire.wav";
   description = AudioExplosion3d;
};

datablock AudioProfile(EMPRPGFireSound)
{
   filename    = "fx/powered/turret_missile_fire.wav";
   description = AudioDefault3d;
};

datablock AudioProfile(EMPRPGReloadSound)
{
   filename    = "fx/powered/turret_heavy_reload.wav";
   description = AudioDefault3d;
};

datablock ShockwaveData(EMPShockwave)
{
   width = 6.0;
   numSegments = 32;
   numVertSegments = 6;
   velocity = 45;
   acceleration = 100.0;
   lifetimeMS = 500;
   height = 1.0;
   verticalCurve = 0.5;
   is2D = false;

   texture[0] = "special/shockwave4";
   texture[1] = "special/gradient";
   texWrap = 6.0;

   times[0] = 0.0;
   times[1] = 0.5;
   times[2] = 1.0;

   colors[0] = "0.4 1.0 0.4 0.50";
   colors[1] = "0.4 1.0 0.4 0.25";
   colors[2] = "0.4 1.0 0.4 0.0";

   mapToTerrain = false;
   orientToNormal = false;
   renderBottom = true;
};

datablock ShockwaveData(EMPShockwave2D)
{
   width = 6.0;
   numSegments = 32;
   numVertSegments = 6;
   velocity = 45;
   acceleration = 100.0;
   lifetimeMS = 500;
   height = 1.0;
   verticalCurve = 0.5;
   is2D = true;

   texture[0] = "special/shockwave4";
   texture[1] = "special/gradient";
   texWrap = 6.0;

   times[0] = 0.0;
   times[1] = 0.5;
   times[2] = 1.0;

   colors[0] = "0.4 1.0 0.4 0.50";
   colors[1] = "0.4 1.0 0.4 0.25";
   colors[2] = "0.4 1.0 0.4 0.0";

   mapToTerrain = false;
   orientToNormal = false;
   renderBottom = true;
};

datablock ExplosionData(EMPSubExplosion)
{
   explosionShape = "mortar_explosion.dts";
   faceViewer           = true;
   playSpeed = 1.5;

   sizes[0] = "2.5 2.5 2.5";
   sizes[1] = "2.5 2.5 2.5";
   times[0] = 0.0;
   times[1] = 1.0;

   shockwave = EMPShockwave2D;
};

datablock ExplosionData(EMPExplosion)
{
   soundProfile   = EMPExpSound;

   shockwave = EMPShockwave;

   emitter[0] = GrenadeSparksEmitter;
   subExplosion[0] = EMPSubExplosion;

   shakeCamera = true;
   camShakeFreq = "10.0 6.0 9.0";
   camShakeAmp = "5.0 5.0 5.0";
   camShakeDuration = 0.25;
   camShakeRadius = 10.0;
};

datablock LinearProjectileData(EMPRPG) : StandardRPG
{
   scale = "1 1 1";
   projectileShapeName = "grenade_flash.dts";

   flags               = $Projectile::PlaysHitSound;
   ticking             = false;
   headshotMultiplier  = 1.0;
   
   indirectDamage      = 0.25;
   damageRadius        = 15.0;

   explosion           = "EMPExplosion";
   underwaterExplosion = "EMPExplosion";
   
   radialStatusEffect  = "EMPulseEffect";
   statusEffectChance  = 1.0;
   statusEffectMask    = $TypeMasks::PlayerObjectType | $TypeMasks::VehicleObjectType | $TypeMasks::StationObjectType | $TypeMasks::GeneratorObjectType | $TypeMasks::SensorObjectType | $TypeMasks::TurretObjectType;
};

datablock ItemData(EMPGrenadeThrown)
{
	className = Weapon;
	shapeFile = "grenade.dts";
	mass = 0.7;
	elasticity = 0.2;
   friction = 1;
   pickupRadius = 2;
   maxDamage = 0.5;
	explosion = EMPExplosion;
   indirectDamage      = 0.25;
   damageRadius        = 15.0;
   radiusDamageType    = $DamageType::EMP;
   kickBackStrength    = 100;
   
   flags               = $Projectile::PlaysHitSound;
   ticking             = false;
   headshotMultiplier  = 1.0;
   
   radialStatusEffect  = "EMPulseEffect";
   statusEffectChance  = 1.0;
   statusEffectMask    = $TypeMasks::PlayerObjectType | $TypeMasks::VehicleObjectType | $TypeMasks::StationObjectType | $TypeMasks::GeneratorObjectType | $TypeMasks::SensorObjectType | $TypeMasks::TurretObjectType;
};

datablock ItemData(EMPGrenade)
{
	className = HandInventory;
	catagory = "Handheld";
	shapeFile = "grenade.dts";
	mass = 0.7;
	elasticity = 0.2;
   friction = 1;
   pickupRadius = 2;
   thrownItem = EMPGrenadeThrown;
	pickUpName = "some emp grenades";
	isGrenade = true;
   customProjectile = true;
   projectile = "EMPRPG";
   projectileType = "LinearProjectile";
   customSound = "EMPRPGFireSound";
   reloadSound = "EMPRPGReloadSound";
   reloadTime = 4000;
};

function EMPGrenadeThrown::onThrow(%this, %gren)
{
   AIGrenadeThrown(%gren);
   %gren.detThread = schedule(1500, %gren, "detonateGrenade", %gren);
}
