// grenade (thrown by hand) script
// ------------------------------------------------------------------------
datablock EffectProfile(FlashGrenadeExplosionEffect)
{
   effectname = "explosions/grenade_flash_explode";
   minDistance = 10;
   maxDistance = 30;
};

datablock AudioProfile(FlashGrenadeExplosionSound)
{
   filename = "fx/explosions/grenade_flash_explode.wav";
   description = AudioExplosion3d;
   preload = true;
   effect = FlashGrenadeExplosionEffect;
};

datablock ExplosionData(FlashGrenadeExplosion)
{
   explosionShape = "disc_explosion.dts";
   soundProfile   = FlashGrenadeExplosionSound;

   faceViewer     = true;
};

datablock LinearProjectileData(FlashRPG) : StandardRPG
{
   scale = "1 1 1";
   projectileShapeName = "grenade_flash.dts";

   flags               = 0;
   ticking             = false;
   headshotMultiplier  = 1.0;

   explosion           = "FlashGrenadeExplosion";
   underwaterExplosion = "FlashGrenadeExplosion";
};

function FlashRPG::onExplode(%data, %proj, %pos, %mod)
{
    %proj.hitSomething = true;

   %maxWhiteout = FlashGrenadeThrown.maxWhiteout;
   %thrower = %proj.instigator.client;
   %hgt = %pos;
   %plX = firstword(%hgt);
   %plY = getWord(%hgt, 1);
   %plZ = getWord(%hgt, 2);
   %pos = %plX @ " " @ %plY @ " " @ %plZ;
   //all this stuff below ripped from projectiles.cs

   InitContainerRadiusSearch(%pos, 100.0, $TypeMasks::PlayerObjectType |
                                          $TypeMasks::TurretObjectType);

   while ((%damage = containerSearchNext()) != 0)
   {
      %dist = containerSearchCurrDist();

      %eyeXF = %damage.getEyeTransform();
      %epX   = firstword(%eyeXF);
      %epY   = getWord(%eyeXF, 1);
      %epZ   = getWord(%eyeXF, 2);
      %eyePos = %epX @ " " @ %epY @ " " @ %epZ;
      %eyeVec = %damage.getEyeVector();

      // Make sure we can see the thing...
      if (ContainerRayCast(%eyePos, %pos, $TypeMasks::TerrainObjectType |
                                          $TypeMasks::InteriorObjectType |
                                          $TypeMasks::StaticObjectType, %damage) !$= "0")
      {
         continue;
      }

      %distFactor = 1.0;
      if (%dist >= 100)
         %distFactor = 0.0;
      else if (%dist >= 20) {
         %distFactor = 1.0 - ((%dist - 20.0) / 80.0);
      }

      %dif = VectorNormalize(VectorSub(%pos, %eyePos));
      %dot = VectorDot(%eyeVec, %dif);

      %difAcos = mRadToDeg(mAcos(%dot));
      %dotFactor = 1.0;
      if (%difAcos > 60)
         %dotFactor = ((1.0 - ((%difAcos - 60.0) / 120.0)) * 0.2) + 0.3;
      else if (%difAcos > 45)
         %dotFactor = ((1.0 - ((%difAcos - 45.0) / 15.0)) * 0.5) + 0.5;

      %totalFactor = %dotFactor * %distFactor;

	  %prevWhiteOut = %damage.getWhiteOut();

		if(!%prevWhiteOut)
			if(!$teamDamage)
			{
				if(%damage.client != %thrower && %damage.client.team == %thrower.team)
					messageClient(%damage.client, 'teamWhiteOut', '\c1You were hit by %1\'s whiteout grenade.', getTaggedString(%thrower.name));
			}

      %whiteoutVal = %prevWhiteOut + %totalFactor;
      if(%whiteoutVal > %maxWhiteout)
      {
        //error("whitout at max");
        %whiteoutVal = %maxWhiteout;
      }
      //bot cheat! don't blind the thrower - Lagg... 1-8-2004
      if (%damage.client == %thrower && %thrower.isAIControlled())
         continue;

      %damage.setWhiteOut( %whiteoutVal );
   }
}

datablock ItemData(FlashGrenadeThrown)
{
   shapeFile = "grenade_flash.dts"; // z0dd - ZOD, 5/19/03. Was grenade.dts
   mass = 0.7;
   elasticity = 0.2;
   friction = 1;
   pickupRadius = 2;
   maxDamage = 0.4;
   explosion = FlashGrenadeExplosion;
   indirectDamage = 0.5;
   damageRadius = 10.0;
   radiusDamageType = $DamageType::Grenade;
   kickBackStrength = 1000;
   computeCRC = true;
   maxWhiteout = 0.9; // z0dd - ZOD, 9/8/02. Was 1.2
   
   flags               = $Projectile::PlaysHitSound;
   ticking             = false;
   headshotMultiplier  = 1.0;
};

datablock ItemData(FlashGrenade)
{
   className = HandInventory;
   catagory = "Handheld";
   shapeFile = "grenade_flash.dts"; // z0dd - ZOD, 5/19/03. Was grenade.dts
   mass = 0.7;
   elasticity = 0.2;
   friction = 1;
   pickupRadius = 2;
   thrownItem = FlashGrenadeThrown;
   pickUpName = "some flash grenades";
   isGrenade = true;
   customProjectile = true;
   projectile = "FlashRPG";
   projectileType = "LinearProjectile";
   //computeCRC = true; // z0dd - ZOD, 5/19/03. Only need to check this model once.
};

//--------------------------------------------------------------------------
// Functions:
//--------------------------------------------------------------------------
function FlashGrenadeThrown::onCollision( %data, %obj, %col )
{
   // Do nothing...
}

