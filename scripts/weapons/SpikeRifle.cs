//--------------------------------------
// Spike Rifle
//--------------------------------------
datablock AudioProfile(DUPHeadCollision)
{
   filename    = "fx/vehicles/crash_ground_vehicle.wav";
   description = AudioExplosion3d;
   preload = true;
};

datablock AudioProfile(SpikeFireSound)
{
   filename    = "fx/powered/turret_outdoor_fire.wav";
   description = AudioDefault3d;
   preload = true;
};

datablock AudioProfile(SpikeReloadSound)
{
   filename    = "fx/weapons/grenadelauncher_dryfire.wav";
   description = AudioDefault3d;
   preload = true;
};

datablock ParticleData(SpikeSparks)
{
   dragCoefficient      = 1;
   gravityCoefficient   = 0.0;
   inheritedVelFactor   = 0.2;
   constantAcceleration = 0.3;
   lifetimeMS           = 600;
   lifetimeVarianceMS   = 150;
   textureName          = "special/bigspark";
   colors[0]     = 211.0/255.0 @ " " @ 215.0/255.0 @ " " @ 120.0/255.0 @ " 1.0";
   colors[1]     = 211.0/255.0 @ " " @ 215.0/255.0 @ " " @ 120.0/255.0 @ " 0.5";
   colors[2]     = 211.0/255.0 @ " " @ 215.0/255.0 @ " " @ 120.0/255.0 @ " 0.0";
   sizes[0]      = 0.5;
   sizes[1]      = 0.75;
   sizes[2]      = 1.0;
   times[0]      = 0.25;
   times[1]      = 0.5;
   times[2]      = 1.0;

};

datablock ParticleEmitterData(SpikeSparksEmitter)
{
   ejectionPeriodMS = 2;
   periodVarianceMS = 1;
   ejectionVelocity = 10;
   velocityVariance = 4.375;
   ejectionOffset   = 0.0;
   thetaMin         = 0;
   thetaMax         = 180;
   phiReferenceVel  = 0;
   phiVariance      = 360;
   overrideAdvances = false;
   orientParticles  = true;
   lifetimeMS       = 200;
   particles = "SpikeSparks";
};

datablock ExplosionData(SpikeCoreExplosion)
{
   explosionShape = "effect_plasma_explosion.dts";
   faceViewer           = true;

   delayMS = 150;

   offset = 0;

   playSpeed = 1.5;

   sizes[0] = "0.5 0.5 0.5";
   sizes[1] = "0.5 0.5 0.5";
   times[0] = 0.0;
   times[1] = 1.0;
};

datablock ExplosionData(SpikeExplosion)
{
   explosionShape = "effect_plasma_explosion.dts";
   soundProfile   = DUPHeadCollision;

   emitter[0] = SpikeSparksEmitter;
   subExplosion[0] = SpikeCoreExplosion;

   shakeCamera = true;
   camShakeFreq = "5.0 5.0 5.0";
   camShakeAmp = "5.0 5.0 5.0";
   camShakeDuration = 0.35;
   camShakeRadius = 4.0;

   faceViewer           = true;
};

datablock DecalData(SpikeHitDecal)
{
   sizeX       = 0.5;
   sizeY       = 0.5;
   textureName = "special/bullethole1";
};

datablock TracerProjectileData(SpikeShot)
{
   doDynamicClientHits = true;

   projectileShapeName = "";
   directDamage        = 0.0;
   directDamageType    = $DamageType::SpikeRifle;
   hasDamageRadius     = true;
   indirectDamage      = 0.75;
   damageRadius        = 1.0;
   kickBackStrength    = 1000.0;
   radiusDamageType    = $DamageType::SpikeRifle;
   sound          	   = ShrikeBlasterProjectileSound;
   explosion           = SpikeExplosion;

   flags               = $Projectile::CanHeadshot | $Projectile::CountMAs | $Projectile::PlaysHitSound;
   ticking             = false;
   headshotMultiplier  = 1.5;
   
   dryVelocity       = 425.0;
   wetVelocity       = 225.0;
   velInheritFactor  = 1.0;
   fizzleTimeMS      = 800;
   lifetimeMS        = 1000;
   explodeOnDeath    = false;
   reflectOnWaterImpactAngle = 0.0;
   explodeOnWaterImpact      = true;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = -1;

   activateDelayMS = -1;

   tracerLength    = 5;
   tracerAlpha     = false;
   tracerMinPixels = 3;
   tracerColor     = "1 0 0 1";
	tracerTex[0]  	 = "special/landSpikeBolt";
	tracerTex[1]  	 = "special/landSpikeBoltCross";
	tracerWidth     = 0.35;
   crossSize       = 0.79;
   crossViewAng    = 0.990;
   renderCross     = true;
   emap = true;
};

datablock DebrisData(SpikeRifleShells)
{
   shapeName = "grenade_projectile.dts";

   lifetime = 7.0;

   minSpinSpeed = 300.0;
   maxSpinSpeed = 400.0;

   elasticity = 0.75;
   friction = 0.1;

   numBounces = 3;

   fade = true;
   staticOnMaxBounce = true;
   snapOnMaxBounce = true;
};

datablock ShockwaveData(SpikeMuzzleFlash)
{
   width = 0.45;
   numSegments = 13;
   numVertSegments = 3;
   velocity = 3.0;
   acceleration = -1.0;
   lifetimeMS = 300;
   height = 0.1;
   verticalCurve = 0.5;

   mapToTerrain = false;
   renderBottom = false;
   orientToNormal = true;
   renderSquare = true;

   texture[0] = "special/blasterHit";
   texture[1] = "special/gradient";
   texWrap = 3.0;

   times[0] = 0.0;
   times[1] = 0.5;
   times[2] = 1.0;

   colors[0] = "1.0 0.8 0.5 1.0";
   colors[1] = "1.0 0.8 0.5 1.0";
   colors[2] = "1.0 0.8 0.5 0.0";
};

//--------------------------------------------------------------------------
// Weapon
//--------------------------------------
datablock ItemData(SpikeAmmo)
{
   className = Ammo;
   catagory = "Ammo";
   shapeFile = "ammo_grenade.dts";
   mass = 1;
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 2;
	pickUpName = "a spike clip";
   emap = true;
};

datablock ShapeBaseImageData(SpikeRifleImage)
{
   className = WeaponImage;
   shapeFile = "weapon_mortar.dts";
   offset = "0.2 0.7 0.125";
   rotation = "1 0 0 180";

   item = SpikeRifle;
   ammo = SpikeAmmo;
   subImage1 = "SpikeDecalAImage";
   subImage2 = "SpikeDecalBImage";
   
   emap = true;

   projectile = SpikeShot;
   projectileType = TracerProjectile;

   casing              = SpikeRifleShells;
   shellExitDir        = "1.0 0.3 1.0";
   shellExitOffset     = "0.15 -0.56 -0.1";
   shellExitVariance   = 12.0;
   shellVelocity       = 10.0;

   stateName[0] = "Activate";
   stateTransitionOnTimeout[0] = "ActivateReady";
   stateTimeoutValue[0] = 0.25;
   stateSequence[0] = "Activate";
   stateSound[0] = GrenadeReloadSound;

   stateName[1] = "ActivateReady";
   stateTransitionOnLoaded[1] = "Ready";
   stateTransitionOnNoAmmo[1] = "NoAmmo";

   stateName[2] = "Ready";
   stateTransitionOnNoAmmo[2] = "NoAmmo";
   stateTransitionOnTriggerDown[2] = "Fire";

   stateName[3] = "Fire";
   stateTransitionOnTimeout[3] = "Reload";
   stateTimeoutValue[3] = 0.75;
   stateFire[3] = true;
   stateRecoil[3] = LightRecoil;
   stateAllowImageChange[3] = false;
   stateSequence[3] = "Recoil";
   stateScript[3] = "onFire";
   stateSound[3] = SpikeFireSound;

   stateName[4] = "Reload";
   stateTransitionOnNoAmmo[4] = "NoAmmo";
   stateTransitionOnTimeout[4] = "Ready";
   stateTimeoutValue[4] = 0.25;
   stateAllowImageChange[4] = false;
   stateSequence[4] = "Reload";
   stateSound[4] = SpikeReloadSound;
   stateEjectShell[4]       = true;

   stateName[5] = "NoAmmo";
   stateTransitionOnAmmo[5] = "Reload";
   stateSequence[5] = "NoAmmo";
   stateTransitionOnTriggerDown[5] = "DryFire";

   stateName[6]       = "DryFire";
//   stateSound[6]      = SolPisDryFireSound;
   stateTimeoutValue[6]        = 1.5;
   stateTransitionOnTimeout[6] = "NoAmmo";
};

datablock ItemData(SpikeRifle)
{
   className = Weapon;
   catagory = "Spawn Items";
   shapeFile = "weapon_mortar.dts";
   image = SpikeRifleImage;
   mass = 1;
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 2;
	pickUpName = "a spike rifle";
};

datablock ShapeBaseImageData(SpikeDecalAImage)
{
   shapeFile = "weapon_plasma.dts";
   offset = "0.3 0.4 0.025";
   rotation = "1 0 0 0";
   emap = true;
   subImage = true;
   usesEnergy = true;
   minEnergy = -1;

//   muzzleFlash = OutdoorTurretMuzzleFlash;

   stateName[0] = "Activate";
   stateTransitionOnTimeout[0] = "ActivateReady";
   stateTimeoutValue[0] = 0.01;
   stateSequence[0] = "Activate";

   stateName[1] = "ActivateReady";
   stateTransitionOnLoaded[1] = "Ready";
   stateTransitionOnNoAmmo[1] = "NoAmmo";

   stateName[2] = "Ready";
   stateTransitionOnNoAmmo[2] = "NoAmmo";
   stateTransitionOnTriggerDown[2] = "Fire";

   stateName[3] = "Fire";
   stateTransitionOnTimeout[3] = "Reload";
   stateTimeoutValue[3] = 0.75;
   stateFire[3] = true;
   stateSequence[3] = "Fire";
//   stateShockwave[3] = true;

   stateName[4] = "Reload";
   stateTransitionOnNoAmmo[4] = "NoAmmo";
   stateTransitionOnTimeout[4] = "Ready";
   stateTimeoutValue[4] = 0.2;
   stateAllowImageChange[4] = false;
   stateSequence[4] = "Reload";

   stateName[5] = "NoAmmo";
   stateTransitionOnAmmo[5] = "Reload";
   stateSequence[5] = "NoAmmo";
   stateTransitionOnTriggerDown[5] = "DryFire";

   stateName[6]       = "DryFire";
   stateTimeoutValue[6]        = 1.5;
   stateTransitionOnTimeout[6] = "NoAmmo";
};

datablock ShapeBaseImageData(SpikeDecalBImage)
{
   shapeFile = "weapon_sniper.dts";
   offset = "0.3 0.7 0.1";
   rotation = "0 1 0 180";
   emap = true;
   subImage = true;
};
