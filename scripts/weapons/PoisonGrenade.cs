// Extended Grenades
// ------------------------------------------------------------------------
datablock AudioProfile(PoisonExpSound)
{
   filename    = "fx/weapons/plasma_rifle_projectile_die.wav";
   description = AudioExplosion3d;
};

datablock AudioProfile(PoisonRPGFireSound)
{
   filename    = "fx/powered/turret_missile_fire.wav";
   description = AudioDefault3d;
};

datablock AudioProfile(PoisonRPGReloadSound)
{
   filename    = "fx/powered/turret_heavy_reload.wav";
   description = AudioDefault3d;
};

datablock ParticleData(PoisonExplosionSmoke)
{
   dragCoeffiecient     = 0.4;
   gravityCoefficient   = 0.0;   // rises slowly
   inheritedVelFactor   = 0.0;

   lifetimeMS           = 2000;
   lifetimeVarianceMS   = 500;

   textureName          = "particleTest";

   useInvAlpha =  true;
   spinRandomMin = -100.0;
   spinRandomMax =  100.0;

   textureName = "special/Smoke/bigSmoke";

   colors[0]     = "0.3 0.7 0.7 1.0";
   colors[1]     = "0.2 0.8 0.8 0.75";
   colors[2]     = "0.1 0.3 0.3 0.5";
   colors[3]     = "0.9 0.1 0.1 0.0";
   sizes[0]      = 5.0;
   sizes[1]      = 6.0;
   sizes[2]      = 10.0;
   sizes[3]      = 12.0;
   times[0]      = 0.0;
   times[1]      = 0.333;
   times[2]      = 0.666;
   times[3]      = 1.0;
};

datablock ParticleEmitterData(PoisonExplosionSmokeEmitter)
{
   ejectionPeriodMS = 10;
   periodVarianceMS = 0;

   ejectionOffset = 6.0;
   
   ejectionVelocity = 1.25;
   velocityVariance = 1.2;

   thetaMin         = 0.0;
   thetaMax         = 180.0;

   lifetimeMS       = 1000;

   particles = "PoisonExplosionSmoke";
};

datablock ExplosionData(PoisonExplosion)
{
   soundProfile   = PoisonExpSound;

   emitter[0] = PoisonExplosionSmokeEmitter;
   emitter[1] = MBExplosionEmitter;
};

datablock LinearProjectileData(PoisonRPG) : StandardRPG
{
   scale = "1 1 1";
   projectileShapeName = "grenade_flash.dts";

   flags               = $Projectile::PlaysHitSound;
   ticking             = false;
   headshotMultiplier  = 1.0;

   indirectDamage      = 0.45;
   damageRadius        = 10.0;

   explosion           = "PoisonExplosion";
   underwaterExplosion = "PoisonExplosion";
   
   radialStatusEffect  = "PoisonEffect";
   statusEffectChance  = 1.0;
   statusEffectMask    = $TypeMasks::PlayerObjectType;
};

datablock ItemData(PoisonGrenadeThrown)
{
   shapeFile = "grenade.dts";
   mass = 0.7;
   elasticity = 0.2;
   friction = 1;
   pickupRadius = 2;
   maxDamage = 0.4;
   explosion = PoisonExplosion;
   underwaterExplosion = "PoisonExplosion";
   indirectDamage      = 0.45;
   damageRadius        = 10;
   radiusDamageType    = $DamageType::Poison;
   kickBackStrength    = 100;
   
   flags               = $Projectile::PlaysHitSound;
   ticking             = false;
   headshotMultiplier  = 1.0;
   
   radialStatusEffect  = "PoisonEffect";
   statusEffectChance  = 1.0;
   statusEffectMask    = $TypeMasks::PlayerObjectType;
};

datablock ItemData(PoisonGrenade)
{
   className = HandInventory;
   catagory = "Handheld";
   shapeFile = "grenade.dts";
   mass = 0.7;
   elasticity = 0.2;
   friction = 1;
   pickupRadius = 2;
   thrownItem = PoisonGrenadeThrown;
	pickUpName = "some poison gas grenades";
	isGrenade = true;
    customProjectile = true;
   projectile = "PoisonRPG";
   projectileType = "LinearProjectile";
   customSound = "PoisonRPGFireSound";
   reloadSound = "PoisonRPGReloadSound";
   reloadTime = 3000;
};

function PoisonGrenadeThrown::onThrow(%this, %gren)
{
   AIGrenadeThrown(%gren);
   %gren.detThread = schedule(1500, %gren, "detonateGrenade", %gren);
}
