//--------------------------------------
// Scattergun
//--------------------------------------

datablock AudioProfile(ScattergunFireSound)
{
   filename    = "fx/weapons/mortar_fire.wav";
   description = AudioDefault3d;
   preload = true;
};

datablock AudioProfile(ScattergunReloadSound)
{
   filename    = "fx/weapons/mortar_reload.wav";
   description = AudioDefault3d;
   preload = true;
};

//--------------------------------------------------------------------------
// Projectile
//--------------------------------------

datablock TracerProjectileData(ScattergunShell)
{
   doDynamicClientHits = true;

   directDamage        = 0.2; // z0dd - ZOD, 9-27-02. Was 0.0825
   directDamageType    = $DamageType::Scattergun;
   explosion           = "ChaingunExplosion";
   splash              = ChaingunSplash;

   kickBackStrength  = 0.0;
   sound 				= ChaingunProjectile;

   flags               = $Projectile::PlayerFragment;
   ticking             = false;
   headshotMultiplier  = 1.0;

   //dryVelocity       = 425.0;
   dryVelocity       = 400.0; // z0dd - ZOD, 8-12-02. Was 425.0
   wetVelocity       = 100.0;
   velInheritFactor  = 1.0;
   fizzleTimeMS      = 1000;
   lifetimeMS        = 1000;
   explodeOnDeath    = false;
   reflectOnWaterImpactAngle = 0.0;
   explodeOnWaterImpact      = false;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 1000;

   tracerLength    = 15.0;
   tracerAlpha     = false;
   tracerMinPixels = 6;
   tracerColor     = 211.0/255.0 @ " " @ 215.0/255.0 @ " " @ 120.0/255.0 @ " 0.75";
	tracerTex[0]  	 = "special/tracer00";
	tracerTex[1]  	 = "special/tracercross";
	tracerWidth     = 0.10;
   crossSize       = 0.20;
   crossViewAng    = 0.990;
   renderCross     = true;

   decalData[0] = ChaingunDecal1;
   decalData[1] = ChaingunDecal2;
   decalData[2] = ChaingunDecal3;
   decalData[3] = ChaingunDecal4;
   decalData[4] = ChaingunDecal5;
   decalData[5] = ChaingunDecal6;
};

//--------------------------------------------------------------------------
// Ammo
//--------------------------------------

datablock ItemData(ScattergunAmmo)
{
   className = Ammo;
   catagory = "Ammo";
   shapeFile = "ammo_chaingun.dts";
   mass = 1;
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 2;
	pickUpName = "some scattergun ammo";
};

//--------------------------------------------------------------------------
// Weapon
//--------------------------------------
datablock ItemData(Scattergun)
{
   className = Weapon;
   catagory = "Spawn Items";
   shapeFile = "weapon_plasma.dts";
   image = ScattergunImage;
   mass = 1;
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 2;
	pickUpName = "a scattergun";
};

datablock DebrisData(ScattergunDebris)
{
   shapeName = "weapon_missile_casement.dts";

   lifetime = 3.0;

   minSpinSpeed = 300.0;
   maxSpinSpeed = 400.0;

   elasticity = 0.5;
   friction = 0.2;

   numBounces = 3;

   fade = true;
   staticOnMaxBounce = true;
   snapOnMaxBounce = true;
};

datablock ShapeBaseImageData(ScattergunImage)
{
   className = WeaponImage;
   shapeFile = "weapon_plasma.dts";
   item = Scattergun;
   ammo = ScattergunAmmo;
   offset = "0 0 0";
//   armThread = looksn;

   usesEnergy = false;
   minEnergy = 0;
   fireEnergy = 0;

   casing              = ScattergunDebris;
   shellExitDir        = "1.0 0.3 1.0";
   shellExitOffset     = "0.15 -0.56 -0.1";
   shellExitVariance   = 15.0;
   shellVelocity       = 6.0;

   projectileSpread = 18.0;
   staggerCount = 11;
   staggerDelay = 0;
   
   projectile = ScattergunShell;
   projectileType = TracerProjectile;

   subImage1 = "ScattergunDecalA";
   subImage2 = "ScattergunDecalB";
   subImage3 = "ScattergunDecalC";
   
   stateName[0] = "Activate";
   stateTransitionOnTimeout[0] = "ActivateReady";
   stateTimeoutValue[0] = 0.5;
   stateSequence[0] = "Activate";
   stateSound[0] = PlasmaSwitchSound;

   stateName[1] = "ActivateReady";
   stateTransitionOnLoaded[1] = "Ready";
   stateTransitionOnNoAmmo[1] = "NoAmmo";

   stateName[2] = "Ready";
   stateTransitionOnNoAmmo[2] = "NoAmmo";
   stateTransitionOnTriggerDown[2] = "Fire";

   stateName[3] = "Fire";
   stateTransitionOnTimeout[3] = "Reload";
   stateTimeoutValue[3] = 1.0;
   stateFire[3] = true;
   stateRecoil[3] = LightRecoil;
   stateAllowImageChange[3] = false;
   stateScript[3] = "onFire";
   stateSound[3] = ScattergunFireSound;
   stateSequence[3] = "Fire";
   
   stateName[4] = "Reload";
   stateTransitionOnNoAmmo[4] = "NoAmmo";
   stateTransitionOnTimeout[4] = "Ready";
   stateTimeoutValue[4] = 0.75;
   stateAllowImageChange[4] = false;
   stateSound[4] = ScattergunReloadSound;
   stateEjectShell[4]       = true;
   
   stateName[5] = "NoAmmo";
   stateTransitionOnAmmo[5] = "Reload";
   stateSequence[5] = "NoAmmo";
   stateTransitionOnTriggerDown[5] = "DryFire";

   stateName[6]       = "DryFire";
   stateSound[6]      = MortarDryFireSound;
   stateTimeoutValue[6]        = 1.5;
   stateTransitionOnTimeout[6] = "NoAmmo";
   
   stateName[7]       = "WetFire";
   stateSound[7]      = PlasmaFireWetSound;
   stateTimeoutValue[7]        = 1.5;
   stateTransitionOnTimeout[7] = "Ready";
   
   stateName[8]               = "CheckWet";
   stateTransitionOnWet[8]    = "WetFire";
   stateTransitionOnNotWet[8] = "Fire"; 
};

datablock ShapeBaseImageData(ScattergunDecalA)
{
   shapeFile = "weapon_elf.dts";
   offset = "0.04 0.0 0.205";
   rotation = "0 1 0 90";

   subImage = true;
};

datablock ShapeBaseImageData(ScattergunDecalB)
{
   shapeFile = "weapon_elf.dts";
   offset = "-0.05 0 0.25";
   rotation = "0 1 0 -90";
   emap = true;

   subImage = true;
};

datablock ShapeBaseImageData(ScattergunDecalC)
{
   shapeFile = "ammo_disc.dts";
   offset = "0 0.3 0.15";
   rotation = "1 0 0 0";
   emap = true;

   subImage = true;
};
