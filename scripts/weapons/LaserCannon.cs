//--------------------------------------------------------------------------
// Laser Cannon
//--------------------------------------------------------------------------

datablock AudioProfile(LaserSwitchSound)
{
   filename    = "fx/misc/diagnostic_on.wav";
   description = AudioDefault3d;
   preload = true;
};

//--------------------------------------------------------------------------
// Explosion
//--------------------------------------
datablock AudioProfile(LaserHitSound)
{
   filename    = "fx/weapons/blaster_impact.wav";
   description = AudioClosest3d;
   preload = true;
};

datablock AudioProfile(LaserFireSound)
{
   filename    = "fx/vehicles/outrider_skid.wav";
   description = AudioDefaultLooping3d;
   preload = true;
};

datablock AudioProfile(LaserSpinUpSound)
{
   filename    = "fx/weapons/shocklance_activate.wav";
   description = AudioDefault3d;
   preload = true;
};

datablock AudioProfile(LaserSpinDownSound)
{
   filename    = "fx/weapons/chaingun_off.wav";
   description = AudioDefault3d;
   preload = true;
};

datablock ExplosionData(LaserExplosion)
{
   explosionShape = "energy_explosion.dts";
   soundProfile   = LaserHitSound;

   particleEmitter = SniperExplosionEmitter;
   particleDensity = 150;
   particleRadius = 0.25;

   faceViewer           = false;
};

//--------------------------------------
// Projectile
//--------------------------------------
datablock SniperProjectileData(LaserShot)
{
   directDamage        = 0.4;
   hasDamageRadius     = false;
   indirectDamage      = 0.0;
   damageRadius        = 0.0;
   velInheritFactor    = 1.0;
   sound 				  = SniperRifleProjectileSound;
   explosion           = "LaserExplosion";
   splash              = SniperSplash;
   directDamageType    = $DamageType::Laser;

   maxRifleRange       = 500;
   rifleHeadMultiplier = 1.0;
   beamColor           = "1 0.1 0.1";
   fadeTime            = 1.0;

   flags               = $Projectile::PlaysHitSound | $Projectile::Energy;
   ticking             = false;
   headshotMultiplier  = 1.0;
   
   hasFalloff = true;
   optimalRange = 100;
   falloffRange = 500;
   falloffDamagePct = 0.1;
   
   startBeamWidth		  = 0.5;
   endBeamWidth 	     = 0.1;
   pulseBeamWidth 	  = 1.0;
   beamFlareAngle 	  = 3.0;
   minFlareSize        = 0.0;
   maxFlareSize        = 400.0;
   pulseSpeed          = 18.0;
   pulseLength         = 0.2;

   lightRadius         = 1.0;
   lightColor          = "0.3 0.0 0.0";

   textureName[0]      = "special/flare";
   textureName[1]      = "special/nonlingradient";
   textureName[2]      = "special/laserrip01";
   textureName[3]      = "special/laserrip02";
   textureName[4]      = "special/laserrip03";
   textureName[5]      = "special/laserrip04";
   textureName[6]      = "special/laserrip05";
   textureName[7]      = "special/laserrip06";
   textureName[8]      = "special/laserrip07";
   textureName[9]      = "special/laserrip08";
   textureName[10]     = "special/laserrip09";
   textureName[11]     = "special/sniper00";
};

//--------------------------------------
// Rifle and item...
//--------------------------------------
datablock ItemData(LaserCannon)
{
   className    = Weapon;
   catagory     = "Spawn Items";
   shapeFile    = "turret_elf_large.dts";
   image        = LaserCannonImage;
   mass         = 1;
   elasticity   = 0.2;
   friction     = 0.6;
   pickupRadius = 2;
	pickUpName = "a laser cannon";

   computeCRC = true;

};

datablock ShapeBaseImageData(LaserCannonImage)
{
	className = WeaponImage;
   shapeFile = "turret_elf_large.dts";
   item = LaserCannon;
   projectile = LaserShot;
   projectileType = SniperProjectile;
//	armThread = looksn;

    usesEnergy = true;
	minEnergy = 3;
    fireEnergy = 4; // 5 * 10 = 50/sec
//    defaultModeFireSound = "LaserFireSound";
    
   //--------------------------------------
   stateName[0]             = "Activate";
   stateSound[0]                    = LaserSwitchSound;
   stateSequence[0]                 = "deploy";
   stateAllowImageChange[0] = false;
   //
   stateTimeoutValue[0]        = 0.5;
   stateTransitionOnTimeout[0] = "Ready";
   stateTransitionOnNoAmmo[0]  = "NoAmmo";

   //--------------------------------------
   stateName[1]       = "Ready";
   stateSpinThread[1] = Stop;
   //
   stateTransitionOnTriggerDown[1] = "Spinup";
   stateTransitionOnNoAmmo[1]      = "NoAmmo";

   //--------------------------------------
   stateName[2]               = "NoAmmo";
   stateTransitionOnAmmo[2]   = "Ready";
   stateSpinThread[2]         = Stop;
   stateTransitionOnTriggerDown[2] = "DryFire";

   //--------------------------------------
   stateName[3]         = "Spinup";
   stateSpinThread[3]   = SpinUp;
   stateSound[3]        = LaserSpinupSound;
   //
   stateTimeoutValue[3]          = 1.5;
   stateWaitForTimeout[3]        = false;
   stateTransitionOnTimeout[3]   = "Fire";
   stateTransitionOnTriggerUp[3] = "Spindown";

   //--------------------------------------
   stateName[4]             = "Fire";
//   stateSequence[4]            = "Fire";
   stateSequenceRandomFlash[4] = true;
   stateSpinThread[4]       = FullSpeed;
   stateSound[4]            = LaserFireSound;
   //stateRecoil[4]           = LightRecoil;
   stateAllowImageChange[4] = false;
   stateScript[4]           = "onFire";
   stateFire[4]             = true;
//   stateEjectShell[4]       = true;
   //
   stateTimeoutValue[4]          = 0.1;
   stateTransitionOnTimeout[4]   = "Fire";
   stateTransitionOnTriggerUp[4] = "Spindown";
   stateTransitionOnNoAmmo[4]    = "EmptySpindown";

   //--------------------------------------
   stateName[5]       = "Spindown";
   stateSound[5]      = LaserSpinDownSound;
   stateSpinThread[5] = SpinDown;
   //
   stateTimeoutValue[5]            = 2.0;
   stateWaitForTimeout[5]          = true;
   stateTransitionOnTimeout[5]     = "Ready";
   stateTransitionOnTriggerDown[5] = "Spinup";

   //--------------------------------------
   stateName[6]       = "EmptySpindown";
   stateSound[6]      = LaserSpinDownSound;
   stateSpinThread[6] = SpinDown;
   //
   stateTimeoutValue[6]        = 0.5;
   stateTransitionOnTimeout[6] = "NoAmmo";

   //--------------------------------------
   stateName[7]       = "DryFire";
   stateSound[7]      = SniperRifleDryFireSound;
   stateTimeoutValue[7]        = 0.5;
   stateTransitionOnTimeout[7] = "NoAmmo";
};

