// ------------------------------------------------------------------------
// mortar grenade script
// ------------------------------------------------------------------------

datablock GrenadeProjectileData(ShoulderMortarShot) : MortarShot
{
   velInheritFactor    = 1.0;
   armingDelayMS     = 1000; // z0dd - ZOD, 4/14/02. Was 2000

   gravityMod        = 1.25;  // z0dd - ZOD, 5/18/02. Make mortar projectile heavier, less floaty
   muzzleVelocity    = 100; // z0dd - ZOD, 8/13/02. More velocity to compensate for higher gravity. Was 63.7
   
   flags               = $Projectile::PlayerFragment | $Projectile::PlaysHitSound;
   ticking             = false;
   headshotMultiplier  = 1.0;
};

datablock ItemData(MortarGrenadeThrown)
{
	className = Weapon;
	shapeFile = "mortar_projectile.dts";
	mass = 1.4;
	elasticity = 0.2;
   friction = 1;
   pickupRadius = 2;
   maxDamage = 0.5;
   explosion           = "MortarExplosion";
   underwaterExplosion = "UnderwaterMortarExplosion";
   indirectDamage      = 2.0;
   damageRadius        = 20.0;
   radiusDamageType    = $DamageType::Mortar;
   kickBackStrength    = 3500;

   flags               = $Projectile::PlayerFragment | $Projectile::PlaysHitSound;
   ticking             = false;
   headshotMultiplier  = 1.0;
   
   computeCRC = true;
};

datablock ItemData(MortarGrenade)
{
	className = HandInventory;
	catagory = "Handheld";
	shapeFile = "mortar_projectile.dts";
	mass = 2.1;
	elasticity = 0.2;
   friction = 1;
   pickupRadius = 2;
   thrownItem = MortarGrenadeThrown;
	pickUpName = "a mortar shell";
	isGrenade = true;
   customProjectile = true;
   projectile = "ShoulderMortarShot";
   projectileType = "GrenadeProjectile";
   customSound = "MortarFireSound";
   reloadSound = "MortarReloadSound";
   reloadTime = 3000;

   computeCRC = true;
};

function MortarGrenadeThrown::onThrow(%this, %gren)
{
   AIGrenadeThrown(%gren);
   %gren.detThread = schedule(3000, %gren, "detonateGrenade", %gren);
}
