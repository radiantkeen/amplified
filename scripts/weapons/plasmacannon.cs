//--------------------------------------
// Plasma Cannon
//--------------------------------------

//--------------------------------------------------------------------------
// Sounds
//--------------------------------------
datablock AudioProfile(PlasmaCannonSwitchSound)
{
   filename    = "fx/powered/sensor_activate.wav";
   description = AudioDefault3d;
   preload = true;
};

datablock AudioProfile(PlasmaCannonFireSound)
{
   filename    = "fx/powered/turret_plasma_fire.wav";
   description = AudioDefault3d;
   preload = true;
};

datablock AudioProfile(PlasmaCannonShockwaveSound)
{
   filename    = "fx/misc/launcher.wav";
   description = AudioDefault3d;
   preload = true;
};

//--------------------------------------------------------------------------
// Projectile
//--------------------------------------
datablock LinearFlareProjectileData(PlasmaCannonBolt)
{
   doDynamicClientHits = true;

   directDamage        = 0;
   directDamageType    = $DamageType::PlasmaCannon;
   hasDamageRadius     = true;
   indirectDamage      = 1.0;
   damageRadius        = 10.0;
   kickBackStrength    = 500;
   radiusDamageType    = $DamageType::PlasmaCannon;
   explosion           = PlasmaBarrelBoltExplosion;
   splash              = PlasmaSplash;

   flags               = $Projectile::CanHeadshot | $Projectile::CountMAs | $Projectile::PlaysHitSound;
   ticking             = false;
   headshotMultiplier  = 1.5;

   radialStatusEffect  = "BurnEffect";
   statusEffectChance  = 0.4;
   statusEffectMask    = $TypeMasks::PlayerObjectType;
   
   baseEmitter         = DiscMistEmitter;
   
   dryVelocity       = 250;
   wetVelocity       = -1;
   velInheritFactor  = 1.0;
   fizzleTimeMS      = 1000;
   lifetimeMS        = 1500; // z0dd - ZOD, 4/25/02. Was 6000
   explodeOnDeath    = false;
   reflectOnWaterImpactAngle = 0.0;
   explodeOnWaterImpact      = true;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = -1;

   activateDelayMS = -1;

   scale             = "1.5 1.5 1.5";
   numFlares         = 30;
   flareColor        = "0.1 0.3 1.0";
   flareModTexture   = "flaremod";
   flareBaseTexture  = "flarebase";
};

function xPlasmaCannonBolt::onCollision(%data, %projectile, %targetObject, %modifier, %position, %normal)
{
    Parent::onCollision(%data, %projectile, %targetObject, %modifier, %position, %normal);

    if(getRandom() > 0.25)
        StatusEffect.applyEffect("BurnEffect", %targetObject, %projectile.instigator);
}

datablock ShockwaveData(PCFireShockwave)
{
   width = 5;
   numSegments = 24;
   numVertSegments = 7;
   velocity = 1;
   acceleration = -90.0;
   lifetimeMS = 250;
   height = 0.5;
   verticalCurve = 0.375;

   mapToTerrain = false;
   renderBottom = true;
   orientToNormal = true;

   texture[0] = "special/shockwave5";
   texture[1] = "special/gradient";
   texWrap = 1.0;

   times[0] = 0.0;
   times[1] = 0.5;
   times[2] = 1.0;

   colors[0] = "0.3 0.3 1.0 1.0";
   colors[1] = "0.6 0.6 1.0 0.5";
   colors[2] = "1.0 1.0 1.0 0.0";
};

datablock ExplosionData(PCDisplayExplosion)
{
//   soundProfile   = PBCShockwaveSound;
   shockwave = PCFireShockwave;

   faceViewer = true;
};

datablock LinearFlareProjectileData(PCDisplayCharge)
{
   projectileShapeName = "turret_muzzlePoint.dts";
   faceViewer          = false;
   directDamage        = 0;
   hasDamageRadius     = true;
   indirectDamage      = 0.0001;
   damageRadius        = 0;
   radiusDamageType    = $DamageType::Default;
   kickBackStrength    = 0;

   explosion           = "PCDisplayExplosion";

   splash              = MissileSplash;
   velInheritFactor    = 0;

   dryVelocity       = 0.1;
   wetVelocity       = 0.1;
   fizzleTimeMS      = 35;
   lifetimeMS        = 35;
   explodeOnDeath    = true;
   reflectOnWaterImpactAngle = 90;
   explodeOnWaterImpact      = false;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 35;

   activateDelayMS = -1;

   numFlares         = 0;
   flareColor        = "0 0 0";
   flareModTexture   = "flaremod";
   flareBaseTexture  = "flarebase";
};

//--------------------------------------------------------------------------
// Ammo
//--------------------------------------

datablock ItemData(PlasmaCannonAmmo)
{
   className = Ammo;
   catagory = "Ammo";
   shapeFile = "ammo_plasma.dts";
   mass = 1;
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 2;
	pickUpName = "some plasma cannon ammo";
};

//--------------------------------------------------------------------------
// Weapon
//--------------------------------------
datablock ItemData(PlasmaCannon)
{
   className = Weapon;
   catagory = "Spawn Items";
   shapeFile = "turret_fusion_large.dts";
   image = PlasmaCannonImage;
   mass = 1;
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 2;
	pickUpName = "a plasma cannon";
};

datablock ShapeBaseImageData(PlasmaCannonTrigger) : TriggerProxyImage
{
   shapeFile = "weapon_mortar.dts";
   rotation = "0 0 1 180";
   offset = "0.09 1.4 -0.025";
   
   subImage = true;
};

datablock ShapeBaseImageData(PlasmaCannonImage)
{
   className = WeaponImage;
   shapeFile = "turret_fusion_large.dts";
   item = PlasmaCannon;
   ammo = PlasmaCannonAmmo;
   offset = "0 0 0";

   subImage1 = "PlasmaCannonTrigger";

   projectile = PlasmaCannonBolt;
   projectileType = LinearFlareProjectile;

   stateName[0] = "Activate";
   stateTransitionOnTimeout[0] = "ActivateReady";
   stateTimeoutValue[0] = 0.5;
   stateSequence[0] = "Deploy";
   stateSound[0] = PlasmaCannonSwitchSound;

   stateName[1] = "ActivateReady";
   stateTransitionOnLoaded[1] = "Ready";
   stateTransitionOnNoAmmo[1] = "NoAmmo";

   stateName[2] = "Ready";
   stateTransitionOnNoAmmo[2] = "NoAmmo";
   stateTransitionOnTriggerDown[2] = "CheckWet";

   stateName[3] = "Fire";
   stateTransitionOnTimeout[3] = "Reload";
   stateTimeoutValue[3] = 0.1;
   stateFire[3] = true;
   stateRecoil[3] = LightRecoil;
   stateAllowImageChange[3] = false;
   stateScript[3] = "onFire";
   stateEmitterTime[3] = 0.2;
   stateSound[3] = PlasmaCannonFireSound;

   stateName[4] = "Reload";
   stateTransitionOnNoAmmo[4] = "NoAmmo";
   stateTransitionOnTimeout[4] = "Ready";
   stateTimeoutValue[4] = 1.15;
   stateAllowImageChange[4] = false;
//   stateSequence[4] = "Reload";
   stateSound[4] = PlasmaReloadSound;

   stateName[5] = "NoAmmo";
   stateTransitionOnAmmo[5] = "Reload";
   stateSequence[5] = "NoAmmo";
   stateTransitionOnTriggerDown[5] = "DryFire";

   stateName[6]       = "DryFire";
   stateSound[6]      = PlasmaDryFireSound;
   stateTimeoutValue[6]        = 1.5;
   stateTransitionOnTimeout[6] = "NoAmmo";

   stateName[7]       = "WetFire";
   stateSound[7]      = PlasmaFireWetSound;
   stateTimeoutValue[7]        = 1.5;
   stateTransitionOnTimeout[7] = "Ready";

   stateName[8]               = "CheckWet";
   stateTransitionOnWet[8]    = "WetFire";
   stateTransitionOnNotWet[8] = "Fire";
};

function PlasmaCannonImage::spawnProjectile(%data, %obj, %slot, %mode)
{
    %proj = Parent::spawnProjectile(%data, %obj, %slot, %mode);
    %sw = createProjectile("LinearFlareProjectile", "PCDisplayCharge", %obj.getMuzzleVector(%slot), %obj.getMuzzlePoint(%slot), %obj, %slot, %obj);
    %obj.playAudio(0, "PlasmaCannonShockwaveSound");
    
    return %proj;
}
