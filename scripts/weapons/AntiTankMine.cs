// ----------------------------------------------
// Anti-Tank Mine Script
// ----------------------------------------------
// ----------------------------------------------
// Item datablocks
// ----------------------------------------------

datablock ItemData(AntiTankMineDeployed)
{
   className = Weapon;
   shapeFile = "ammo_chaingun.dts";
   mass = 1.5;
   elasticity = 0.2;
   friction = 0.8; // z0dd - ZOD, 9/27/03. was 0.6
   pickupRadius = 3;
   maxDamage = 0.01; // z0dd - ZOD, 9/27/03. was 0.2
   explosion           = "MortarExplosion";
   underwaterExplosion = "UnderwaterMortarExplosion";
   indirectDamage = 12.0; // z0dd - ZOD, 7/14/03. Slight increase to dmg. Was 0.55
   damageRadius = 18.0; // z0dd - ZOD, 7/14/03. Slight increase to det range. Was 6.0
   radiusDamageType = $DamageType::Mine;
   kickBackStrength = 1500;
   aiAvoidThis = true;
   dynamicType = $TypeMasks::DamagableItemObjectType;
   spacing = 40.0; // how close together mines can be
   proximity = 2.5; // how close causes a detonation (by player/vehicle)
   armTime = 2200; // 2.2 seconds to arm a mine after it comes to rest
   maxDepCount = 5; // try to deploy this many times before detonating. // z0dd - ZOD, 9/27/02. Was 9
   isMine = true;
   
   computeCRC = true;
};

datablock ItemData(AntiTankMine)
{     
   className = HandInventory;
   catagory = "Handheld";
   shapeFile = "ammo_chaingun.dts";
   mass = 1;
   elasticity = 0.2;
   friction = 0.7;
   pickupRadius = 2;

   thrownItem = AntiTankMineDeployed;
   pickUpName = "some anti-tank mines";
   isMine = true; // z0dd - ZOD, 5/19/03. New param.
   computeCRC = true;
};

function AntiTankMineDeployed::onThrow(%this, %mine, %thrower)
{
   %mine.armed = false;
   %mine.damaged = 0;
   %mine.detonated = false;
   %mine.depCount = 0;
   %mine.theClient = %thrower.client;
   $TeamDeployedCount[%mine.sourceObject.team, MineDeployed]++; // z0dd - ZOD, 8/13/02, Moved this from deployMineCheck to here. Fixes mine count bug

   schedule(1500, %mine, "deployMineCheck", %mine, %thrower);
}

// Here too
function deployMineCheck(%mineObj, %player)
{
   if(%mineObj.depCount > %mineObj.getDatablock().maxDepCount)
      explodeMine(%mineObj, true);

   // wait until the mine comes to rest
   if(VectorLen(%mineObj.getVelocity()) < 1)
   {
      // 2-second delay before mine is armed -- let deploy thread play out etc.
      schedule(%mineObj.getDatablock().armTime, %mineObj, "armDeployedMine", %mineObj);

      // check for other deployed mines in the vicinity
      InitContainerRadiusSearch(%mineObj.getWorldBoxCenter(), %mineObj.getDatablock().spacing, $TypeMasks::ItemObjectType);
      while((%itemObj = containerSearchNext()) != 0)
      {
         if(%itemObj == %mineObj)
            continue;
            
         %isMine = (%itemObj.getDatablock().isMine == true);
         
         if(%isMine)
            schedule(100, %mineObj, "explodeMine", %mineObj, true);
         else
            continue;
      }
      // play "deploy" thread
      %mineObj.playThread(0, "deploy");
      serverPlay3D(MineDeploySound, %mineObj.getTransform());
      %mineTeam = %mineObj.sourceObject.team;
      //$TeamDeployedCount[%mineTeam, MineDeployed]++; // z0dd - ZOD, 8/13/02, Moved the increment to MineDeployed::onThrow. Fixes mine count bug
      if($TeamDeployedCount[%mineTeam, MineDeployed] > $TeamDeployableMax[MineDeployed])
      {
         messageClient( %player.client, '', 'Maximum allowable mines deployed.' );
         schedule(100, %mineObj, "explodeMine", %mineObj, true);
      }
      else
      {
         //start the thread that keeps checking for objects near the mine...
         mineCheckVicinity(%mineObj);

         //let the AI know *after* it's come to rest...
         AIDeployMine(%mineObj);

         //let the game know there's a deployed mine
         Game.notifyMineDeployed(%mineObj);
      }
   }
   else
   {
      //schedule this deploy check again a little later
      %mineObj.depCount++;
      schedule(500, %mineObj, "deployMineCheck", %mineObj, %player);
   }
}

// Here too
function MineDeployed::onCollision(%data, %obj, %col)
{
   // don't detonate if mine isn't armed yet
   if(!%obj.armed)
      return;

   // don't detonate if mine is already detonating
   if(%obj.boom)
      return;

   %noExplode = 0;
   //check to see what it is that collided with the mine
   if(%col.isVehicle())
   {
      if(!$teamDamage)
      {
         if(%obj.team == %col.getOwnerClient().team)
            %noExplode = 1;
      }
      if(%noExplode == 0)
      {
         //error("Mine detonated due to collision with #"@%col@" ("@%struck@"); armed = "@%obj.armed);
         explodeMine(%obj, false);
      }
   }
}

function AntiTankMineDeployed::onCollision(%data, %obj, %col)
{
   // don't detonate if mine isn't armed yet
   if(!%obj.armed)
      return;

   // don't detonate if mine is already detonating
   if(%obj.boom)
      return;

   %noExplode = 0;
   //check to see what it is that collided with the mine
   if(%col.isPlayer() || %col.isVehicle())
   {
      if(!$teamDamage)
      {
         if(%obj.team == %col.getOwnerClient().team)
            %noExplode = 1;
      }
      if(%noExplode == 0)
      {
         //error("Mine detonated due to collision with #"@%col@" ("@%struck@"); armed = "@%obj.armed);
         explodeMine(%obj, false);
      }
   }
}

function AntiTankMineDeployed::damageObject(%data, %targetObject, %sourceObject, %position, %amount, %damageType)
{
   // -----------------------------
   // z0dd - ZOD, 5/09/04. If gameplay changes in affect, no mine disc
   // Anti-Tank mines need to be armed first
//   if($Host::ClassicLoadMineChanges)
//   {
      if(!%targetObject.armed)
         return;
//   }
   // -----------------------------

   if(%targetObject.boom)
      return;

   %targetObject.damaged += %amount;

   if(%targetObject.damaged >= %data.maxDamage)
   {
      %targetObject.setDamageState(Destroyed);
   }
}

function AntiTankMineDeployed::onDestroyed(%data, %obj, %lastState)
{
   %obj.boom = true;
   %mineTeam = %obj.team;
   $TeamDeployedCount[%mineTeam, MineDeployed]--;

       RadiusExplosion(%obj,
                      %obj.getPosition(),
                      %data.damageRadius,
                      %data.indirectDamage,
                      %data.kickBackStrength,
                      %obj.sourceObject,
                      %data.radiusDamageType);

   %obj.schedule(600, "delete");
}
