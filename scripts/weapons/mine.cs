// ----------------------------------------------
// mine script
// ----------------------------------------------

$TeamDeployableMax[MineDeployed]		= 25; // z0dd - ZOD, 6/10/02. Was 20.

// ----------------------------------------------
// Item datablocks
// ----------------------------------------------

datablock ItemData(MineDeployed)
{
   className = Weapon;
   shapeFile = "mine.dts";
   mass = 0.75;
   elasticity = 0.2;
   friction = 0.8; // z0dd - ZOD, 9/27/03. was 0.6
   pickupRadius = 3;
   maxDamage = 0.01; // z0dd - ZOD, 9/27/03. was 0.2
   explosion = MineExplosion;
   underwaterExplosion = UnderwaterMineExplosion;
   indirectDamage = 1.5; // z0dd - ZOD, 7/14/03. Slight increase to dmg. Was 0.55
   damageRadius = 6.0; // z0dd - ZOD, 7/14/03. Slight increase to det range. Was 6.0
   radiusDamageType = $DamageType::Mine;
   kickBackStrength = 1500;
   aiAvoidThis = true;
   dynamicType = $TypeMasks::DamagableItemObjectType;
   spacing = 6.0; // how close together mines can be
   proximity = 2.5; // how close causes a detonation (by player/vehicle)
   armTime = 2200; // 2.2 seconds to arm a mine after it comes to rest
   maxDepCount = 5; // try to deploy this many times before detonating. // z0dd - ZOD, 9/27/02. Was 9
   isMine = true;
   
   computeCRC = true;
};

datablock ItemData(Mine)
{     
   className = HandInventory;
   catagory = "Handheld";
   shapeFile = "ammo_mine.dts";
   mass = 1;
   elasticity = 0.2;
   friction = 0.7;
   pickupRadius = 2;

   thrownItem = MineDeployed;
   pickUpName = "some mines";
   isMine = true; // z0dd - ZOD, 5/19/03. New param.
   computeCRC = true;
};
