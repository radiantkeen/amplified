datablock AudioProfile(BatteryUseSound)
{
   filename = "fx/Bonuses/upward_straipass2_elevator.wav";
   description = AudioDefault3d;
   preload = true;
};

datablock ItemData(BatteryGrenade)
{
   className = HandInventory;
   catagory = "Handheld";
   shapeFile = "grenade.dts";
   mass = 0.7;
   elasticity = 0.2;
   friction = 1;
   pickupRadius = 2;
   thrownItem = FlareGrenadeThrown;
	pickUpName = "a power injector";
	isGrenade = true;

   computeCRC = true;
};

function BatteryGrenade::onUse(%this, %obj)
{
    if(Game.handInvOnUse(%this, %obj))
    {
        %time = getSimTime();

        if(%time > %obj.nextUseTime[%this] && %obj.getEnergyPct() < 1)
        {
            %obj.setEnergyLevel(%obj.getEnergyLevel() + 100);
            messageClient(%obj.client, 'MsgBatteryUse', "\c3Boosted energy level.");
            %obj.decInventory(%this, 1);
            %obj.play3D(BatteryUseSound);

            // miscellaneous grenade-throwing cleanup stuff
            %obj.lastThrowTime[%this] = %time;
            %obj.nextUseTime[%this] = %time + 5000;
            %obj.throwStrength = 0;
        }
        else 
            messageClient(%obj.client, 'MsgBatteryWait', "\c3Power injector cooling down.");
    }
}
