// Extended Grenades
// ------------------------------------------------------------------------
datablock AudioProfile(SmokeExpSound)
{
   filename    = "fx/environment/sandstorm2.wav";
   description = AudioExplosion3d;
};

datablock AudioProfile(SmokeRPGFireSound)
{
   filename    = "fx/powered/turret_missile_fire.wav";
   description = AudioDefault3d;
};

datablock AudioProfile(SmokeRPGReloadSound)
{
   filename    = "fx/powered/turret_heavy_reload.wav";
   description = AudioDefault3d;
};

datablock ParticleData(SmokeGrenParticle)
{
   dragCoeffiecient     = 0.9;
   gravityCoefficient   = -0.05;   // rises slowly
   inheritedVelFactor   = 0.0;

   lifetimeMS           = 8000;
   lifetimeVarianceMS   = 1500;

   textureName          = "particleTest";

   useInvAlpha =  true;
   spinRandomMin = -200.0;
   spinRandomMax =  200.0;

   textureName = "special/Smoke/smoke_001";

   colors[0]     = "0.1 0.1 0.1 1.0";
   colors[1]     = "0.2 0.2 0.2 1.0";
   colors[2]     = "0.7 0.7 0.7 0.0";
   sizes[0]      = 8.0;
   sizes[1]      = 10.0;
   sizes[2]      = 12.0;
   times[0]      = 0.0;
   times[1]      = 0.6;
   times[2]      = 1.0;

};

datablock ParticleEmitterData(SmokeGrenEmitter)
{
   ejectionPeriodMS = 4;
   periodVarianceMS = 1;

   ejectionVelocity = 0.1;
   velocityVariance = 0.0;
   ejectionOffset = 10.0;
   
   thetaMin         = 0.0;
   thetaMax         = 180.0;

   lifetimeMS       = 1500;

   particles = "SmokeGrenParticle";
};

datablock ExplosionData(SmokeGrenExplosion)
{
   soundProfile   = SmokeExpSound;

   faceViewer           = true;
   explosionScale = "0.8 0.8 0.8";

   emitter[0] = SmokeGrenEmitter;

   shakeCamera = true;
   camShakeFreq = "10.0 6.0 9.0";
   camShakeAmp = "20.0 20.0 20.0";
   camShakeDuration = 0.2;
   camShakeRadius = 5.0;
};

datablock LinearProjectileData(SmokeRPG) : StandardRPG
{
   scale = "1 1 1";
   projectileShapeName = "ammo_mortar.dts";

   flags               = $Projectile::PlaysHitSound;
   ticking             = false;
   headshotMultiplier  = 1.0;

   indirectDamage      = 0.45;
   damageRadius        = 10.0;

   explosion           = "SmokeGrenExplosion";
   underwaterExplosion = "SmokeGrenExplosion";
};

datablock ItemData(SmokeGrenadeThrown)
{
   shapeFile = "ammo_mortar.dts";
   mass = 0.7;
   elasticity = 0.2;
   friction = 1;
   pickupRadius = 2;
   maxDamage = 0.4;
   explosion = SmokeGrenExplosion;
   underwaterExplosion = "SmokeGrenExplosion";
   indirectDamage      = 0.45;
   damageRadius        = 10;
   radiusDamageType    = $DamageType::Grenade;
   kickBackStrength    = 100;
   
   flags               = $Projectile::PlayerFragment | $Projectile::PlaysHitSound;
   ticking             = false;
   headshotMultiplier  = 1.0;
};

datablock ItemData(SmokeGrenade)
{
   className = HandInventory;
   catagory = "Handheld";
   shapeFile = "ammo_mortar.dts";
   mass = 0.7;
   elasticity = 0.2;
   friction = 1;
   pickupRadius = 2;
   thrownItem = SmokeGrenadeThrown;
	pickUpName = "some smoke grenades";
	isGrenade = true;
    customProjectile = true;
   projectile = "SmokeRPG";
   projectileType = "LinearProjectile";
   customSound = "SmokeRPGFireSound";
   reloadSound = "SmokeRPGReloadSound";
   reloadTime = 2000;
};

function SmokeGrenadeThrown::onThrow(%this, %gren)
{
   AIGrenadeThrown(%gren);
   %gren.detThread = schedule(1500, %gren, "detonateGrenade", %gren);
}
