//--------------------------------------------------------------------------
// Plasma Turret
// 
// 
//--------------------------------------------------------------------------


//--------------------------------------------------------------------------
// Projectile
//--------------------------------------

datablock LinearFlareProjectileData(PlasmaBarrelBolt)
{
   doDynamicClientHits = true;

   directDamage        = 0;
   directDamageType    = $DamageType::PlasmaTurret;
   hasDamageRadius     = true;
   // z0dd - ZOD, 5/07/04. Lower damage if gameplay changes in affect
   indirectDamage      = 1.25;
   damageRadius        = 10.0;
   kickBackStrength    = 500;
   radiusDamageType    = $DamageType::PlasmaTurret;
   explosion           = PlasmaBarrelBoltExplosion;
   splash              = PlasmaSplash;

   radialStatusEffect  = "BurnEffect";
   statusEffectChance  = 0.4;
   statusEffectMask    = $TypeMasks::PlayerObjectType;
   
   baseEmitter         = DiscMistEmitter;
   
   // z0dd - ZOD, 5/07/04. Lower velocity if gameplay changes in affect
   dryVelocity       = $Host::ClassicLoadPlasmaTurretChanges ? 65 : 75;
   wetVelocity       = -1;
   velInheritFactor  = 1.0;
   fizzleTimeMS      = 4000;
   lifetimeMS        = 4000; // z0dd - ZOD, 4/25/02. Was 6000
   explodeOnDeath    = false;
   reflectOnWaterImpactAngle = 0.0;
   explodeOnWaterImpact      = true;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = -1;

   activateDelayMS = 100;

   scale             = "1.5 1.5 1.5";
   numFlares         = 30;
   flareColor        = "0.1 0.3 1.0";
   flareModTexture   = "flaremod";
   flareBaseTexture  = "flarebase";
};

//--------------------------------------------------------------------------
// Plasma Turret Image
//--------------------------------------------------------------------------

datablock TurretImageData(PlasmaBarrelLarge)
{
   shapeFile = "turret_fusion_large.dts";
   item      = PlasmaBarrelPack; // z0dd - ZOD, 4/25/02. Was wrong: PlasmaBarrelLargePack

   projectile = PlasmaBarrelBolt;
   projectileType = LinearFlareProjectile;
   usesEnergy = true;
   fireEnergy = 10;
   minEnergy = 10;
   emap = true;

   // Turret parameters
   activationMS      = 700; // z0dd - ZOD, 3/27/02. Was 1000. Amount of time it takes turret to unfold
   deactivateDelayMS = 1500;
   thinkTimeMS       = 140; // z0dd - ZOD, 3/27/02. Was 200. Amount of time before turret starts to unfold (activate)
   degPerSecTheta    = 300;
   degPerSecPhi      = 500;
   attackRadius      = 150; // z0dd - ZOD, 3/27/02. Was 120

   // State transitions
   stateName[0]                  = "Activate";
   stateTransitionOnNotLoaded[0] = "Dead";
   stateTransitionOnLoaded[0]    = "ActivateReady";

   stateName[1]                  = "ActivateReady";
   stateSequence[1]              = "Activate";
   stateSound[1]                 = PBLSwitchSound;
   stateTimeoutValue[1]          = 1;
   stateTransitionOnTimeout[1]   = "Ready";
   stateTransitionOnNotLoaded[1] = "Deactivate";
   stateTransitionOnNoAmmo[1]    = "NoAmmo";

   stateName[2]                    = "Ready";
   stateTransitionOnNotLoaded[2]   = "Deactivate";
   stateTransitionOnTriggerDown[2] = "Fire";
   stateTransitionOnNoAmmo[2]      = "NoAmmo";

   stateName[3]                = "Fire";
   stateTransitionOnTimeout[3] = "Reload";
   stateTimeoutValue[3]        = 0.3;
   stateFire[3]                = true;
   stateRecoil[3]              = LightRecoil;
   stateAllowImageChange[3]    = false;
   stateSequence[3]            = "Fire";
   stateSound[3]               = PBLFireSound;
   stateScript[3]              = "onFire";

   stateName[4]                  = "Reload";
   stateTimeoutValue[4]          = 0.80;
   stateAllowImageChange[4]      = false;
   stateSequence[4]              = "Reload";
   stateTransitionOnTimeout[4]   = "Ready";
   stateTransitionOnNotLoaded[4] = "Deactivate";
   stateTransitionOnNoAmmo[4]    = "NoAmmo";

   stateName[5]                = "Deactivate";
   stateSequence[5]            = "Activate";
   stateDirection[5]           = false;
   stateTimeoutValue[5]        = 1;
   stateTransitionOnLoaded[5]  = "ActivateReady";
   stateTransitionOnTimeout[5] = "Dead";

   stateName[6]               = "Dead";
   stateTransitionOnLoaded[6] = "ActivateReady";

   stateName[7]             = "NoAmmo";
   stateTransitionOnAmmo[7] = "Reload";
   stateSequence[7]         = "NoAmmo";
};
