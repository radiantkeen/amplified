// ------------------------------------------------------------------
// SENSOR JAMMER PACK
//
// When activated, the sensor jammer pack emits a sensor-jamming field of
// 20m radius. Any players within this field are completely invisible to
// all sensors, turrets and cameras.
//
// When not activated, the pack has no effect.
//

datablock AudioProfile(SensorJammerActivateSound)
{
   filename = "fx/packs/sensorjammerpack_on.wav";
   description = ClosestLooping3d;
   preload = true;
};

datablock ShapeBaseImageData(SensorJammerPackImage)
{
   shapeFile = "pack_upgrade_sensorjammer.dts";
   item = SensorJammerPack;
   mountPoint = 1;
   offset = "0 0 0";

   stateName[0] = "Idle";
   stateSequence[0] = "fire";
   stateSound[0] = SensorJammerActivateSound;
};

datablock ItemData(SensorJammerPack)
{
   className = Pack;
   catagory = "Packs";
   shapeFile = "pack_upgrade_sensorjammer.dts";
   mass = 1;
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 2;
   rotate = true;
   image = "SensorJammerPackImage";
   pickUpName = "a sensor jammer pack";

   computeCRC = true;
};

// datablock SensorData(JammerSensorObjectPassive)
// {
//    // same detection info as 'PlayerObject' sensorData
//    detects = true;
//    detectsUsingLOS = true;
//    detectsPassiveJammed = true;
//    detectRadius = 2000;
//    detectionPings = false;
//    detectsFOVOnly = true;
//    detectFOVPercent = 1.3;
//    useObjectFOV = true;
//
//    jams = true;
//    jamsOnlyGroup = true;
//    jamsUsingLOS = true;
//    jamRadius = 0;
// };

datablock SensorData(JammerSensorObjectActive)
{
   // same detection info as 'PlayerObject' sensorData
   detects = true;
   detectsUsingLOS = true;
   detectsPassiveJammed = true;
   detectRadius = 1000;
   detectionPings = false;
   detectsFOVOnly = true;
   detectFOVPercent = 1.3;
   useObjectFOV = true;
   detectsCloaked = true;
   
   jams = true;
   jamsOnlyGroup = true;
   jamsUsingLOS = true;
   jamRadius = 30;
};

datablock SensorData(JammerSensorObjectPassive)
{
   // same detection info as 'PlayerObject' sensorData
   detects = true;
   detectsUsingLOS = true;
   detectsPassiveJammed = true;
   detectRadius = 2000;
   detectionPings = false;
   detectsFOVOnly = true;
   detectFOVPercent = 1.3;
   useObjectFOV = true;

   jams = true;
   jamsOnlyGroup = true;
   jamsUsingLOS = true;
   jamRadius = 2;
};

function SensorJammerPackImage::onUnmount(%data, %obj, %slot)
{
   messageClient(%obj.client, 'MsgSensorJammerPackOff', '\c2Sensor jammer pack deactivated.');
   commandToClient( %obj.client, 'setSenJamIconOff' );
   setTargetSensorData(%obj.client.target, PlayerSensor);
   %obj.setJammerFX(false);
   %obj.isJammed = false;
}

function SensorJammerPackImage::onMount(%data, %obj, %slot)
{
   messageClient(%obj.client, 'MsgSensorJammerPackOn', '\c2Sensor jammer pack active.');
   commandToClient( %obj.client, 'setSenJamIconOn' );
   setTargetSensorData(%obj.client.target, JammerSensorObjectActive);
   %obj.setJammerFX(true);
   %obj.isJammed = true;
}

function SensorJammerPack::onPickup(%this, %obj, %shape, %amount)
{
   // created to prevent console errors
}
