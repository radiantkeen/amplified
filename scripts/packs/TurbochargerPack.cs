// ------------------------------------------------------------------
// Turbocharger PACK
// can be used by any armor type

datablock ShapeBaseImageData(HSDecalImage)
{
   mountPoint = 1;
   shapeFile = "ammo_plasma.dts";
   offset = "0.2 -0.2 -0.6";
//   rotation = "0 1 0 90";
   emap = 1;
};

datablock ShapeBaseImageData(TurbochargerPackImage)
{
   shapeFile = "ammo_plasma.dts";
   item = TurbochargerPack;
   mountPoint = 1;
   offset = "-0.2 -0.2 -0.6";
   emap = 1;

   stateName[0] = "default";
   stateSequence[0] = "ambient";
};

datablock ItemData(TurbochargerPack)
{
   className = Pack;
   catagory = "Packs";
   shapeFile = "ammo_plasma.dts";
   mass = 1;
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 2;
   rotate = true;
   image = "TurbochargerPackImage";
	pickUpName = "a turbocharger pack";
};

function TurbochargerPackImage::onMount(%data, %obj, %node)
{
     %obj.mountImage(HSDecalImage, $BackpackSlot2);
     %obj.damageBuffFactor += 0.5;
     %obj.turbocharger = true;
}

function TurbochargerPackImage::onUnmount(%data, %obj, %node)
{
     %obj.damageBuffFactor -= 0.5;
     %obj.turbocharger = false;
     %obj.unmountImage($BackpackSlot2);
}

function TurbochargerPack::onPickup(%this, %obj, %shape, %amount)
{
	// created to prevent console errors
}
