// -----------------------------------------------------------------
// Vampire Pack

datablock ShapeBaseImageData(VampirePackImage)
{
   shapeFile = "pack_upgrade_ammo.dts";
   item = VampirePack;
   mountPoint = 1;
   offset = "0 0 0";
};

datablock ShapeBaseImageData(VampireDecalImage)
{
   mountPoint = 1;
   shapeFile = "pack_upgrade_satchel.dts";
   offset = "0 -0.15 0.025";
};

datablock ItemData(VampirePack)
{
   className = Pack;
   catagory = "Packs";
   shapeFile = "pack_upgrade_ammo.dts";
   mass = 1;
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 2;
   rotate = true;
   image = "VampirePackImage";
	pickUpName = "a vampire pack";

   computeCRC = true;

};

function VampirePackImage::onMount(%data, %obj, %node)
{
   %obj.mountImage(VampireDecalImage, $BackpackSlot2);
   %obj.vampire = true;
}

function VampirePackImage::onUnmount(%data, %obj, %node)
{
   %obj.vampire = false;
   %obj.unmountImage($BackpackSlot2);
}

function VampirePack::onPickup(%this, %obj, %shape, %amount)
{
	// created to prevent console errors
}
