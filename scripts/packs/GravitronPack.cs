// ------------------------------------------------------------------
// Gravitron Pack

datablock ShapeBaseImageData(GravitronPackImage)
{
   shapeFile = "stackable1s.dts";
   item = GravitronPack;
   mountPoint = 1;
   mass = -20;
   offset = "0.3 -0.23 0";
   rotation = "0 1 0 90";
   
	stateName[0] = "default";
	stateSequence[0] = "activate";
};

datablock ItemData(GravitronPack)
{
   className = Pack;
   catagory = "Packs";
   shapeFile = "stackable1s.dts";
   elasticity = 0.2;
   friction = 0.6;
   pickupRadius = 2;

   image = "GravitronPackImage";
	pickUpName = "a gravitron pack";
};

function GravitronPack::onPickup(%this, %obj, %shape, %amount)
{
	// created to prevent console errors
}
