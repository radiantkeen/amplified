//------------------------------------------------------------------------------
// All string table definitions that need to be loaded before everything else

$BackpackSlot2 = 1;
$ArmorWeightSlot = 3; // Flag slot too
$WeaponSubImage1 = 4;
$WeaponSubImage2 = 5;
$WeaponSubImage3 = 6;
$WeightImageSlot = 7;
$ShoulderSlot2 = 7;

// Projectile flags
$Projectile::One                        = 1 << 0;
$Projectile::IgnoreShields              = 1 << 1;   // ex: blaster, ignores shield and damages armor
$Projectile::IgnoreReflections          = 1 << 2;   // not affected by reflectors (ex. forcefields)
$Projectile::PlayerFragment             = 1 << 3;   // will gib the player if killed by this projectile
$Projectile::CanHeadshot                = 1 << 4;   // if hit in the head, will use headshotMultipler to increase damage
$Projectile::CountMAs                   = 1 << 5;   // Run MA damage detection if flagged
$Projectile::PlaysHitSound              = 1 << 6;   // Play hit sound if flagged
$Projectile::ShieldsOnly                = 1 << 7;   // Only do damage to shields, return 0 to armor
$Projectile::ArmorOnly                  = 1 << 8;   // Only do damage to armor, 0 damage on shields
$Projectile::RepairProjectile           = 1 << 9;   // Special flag to take damage dealt with this projectile as healing instead

// Armor Sizes
$ArmorMask::None            = 1 << 0;
$ArmorMask::Scout           = 1 << 1;
$ArmorMask::Ranger          = 1 << 2;
$ArmorMask::Assault         = 1 << 3;
$ArmorMask::Engineer        = 1 << 4;
$ArmorMask::Juggernaut      = 1 << 5;
$ArmorMask::Dreadnought     = 1 << 6;
$ArmorMask::Walker          = 1 << 7;

$ArmorMask::Light = $ArmorMask::Scout | $ArmorMask::Ranger;
$ArmorMask::Medium = $ArmorMask::Assault | $ArmorMask::Engineer;
$ArmorMask::Heavy = $ArmorMask::Juggernaut | $ArmorMask::Dreadnought;
$ArmorMask::Pilot = $ArmorMask::Scout | $ArmorMask::Engineer;
$ArmorMask::NoFlagCapture = $ArmorMask::Ranger | $ArmorMask::Dreadnought | $ArmorMask::Walker;
