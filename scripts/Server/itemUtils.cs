// Item Utils: Registration factory
// Consolidate changes here

// Found and overriden in item.cs - load after that for permanent change
$WeaponSlot = 0;
$ShoulderSlot = 1;
$BackpackSlot = 2;
$FlagSlot = 3;

//------------------------------------------------------------------------------
// Armors
$ArmorCount = 0;

$InvArmor[$ArmorCount] = "Scout";
$NameToInv["Scout"] = "Light";
$ArmorCount++;

$InvArmor[$ArmorCount] = "Ranger";
$NameToInv["Ranger"] = "Ranger";
$ArmorCount++;

$InvArmor[$ArmorCount] = "Assault";
$NameToInv["Assault"] = "Medium";
$ArmorCount++;

$InvArmor[$ArmorCount] = "Engineer";
$NameToInv["Engineer"] = "Engineer";
$ArmorCount++;

$InvArmor[$ArmorCount] = "Juggernaut";
$NameToInv["Juggernaut"]  = "Heavy";
$ArmorCount++;

$InvArmor[$ArmorCount] = "Dreadnought";
$NameToInv["Dreadnought"]  = "Dreadnought";
$ArmorCount++;

//------------------------------------------------------------------------------
// Weapons
$WeaponCount = 0;

$InvWeapon[$WeaponCount] = "Blaster";
$NameToInv["Blaster"] = "Blaster";
$WeaponsHudData[$WeaponCount, bitmapName] = "gui/hud_blaster";
$WeaponsHudData[$WeaponCount, itemDataName] = "Blaster";
$WeaponsHudData[$WeaponCount, ammoDataName] = "";
$WeaponsHudData[$WeaponCount, reticle] = "gui/ret_blaster";
$WeaponsHudData[$WeaponCount, visible] = "true";
$WeaponsListID["Blaster"] = $WeaponCount;
$WeaponsListData[$WeaponCount, "name"] = $InvWeapon[$WeaponCount];
$WeaponsListData[$WeaponCount, "desc"] = "Directed energy weapon";
$WeaponsListData[$WeaponCount, "stats"] = "Damage: 11 | Speed: 300 | Energy: 5z";
$WeaponCount++;

$InvWeapon[$WeaponCount] = "Plasma Rifle";
$NameToInv["Plasma Rifle"] = "Plasma";
$WeaponsHudData[$WeaponCount, bitmapName] = "gui/hud_plasma";
$WeaponsHudData[$WeaponCount, itemDataName] = "Plasma";
$WeaponsHudData[$WeaponCount, ammoDataName] = "PlasmaAmmo";
$WeaponsHudData[$WeaponCount, reticle] = "gui/ret_plasma";
$WeaponsHudData[$WeaponCount, visible] = "true";
$AmmoIncrement[PlasmaAmmo] = 10;
$WeaponsListID["Plasma"] = $WeaponCount;
$WeaponsListData[$WeaponCount, "name"] = $InvWeapon[$WeaponCount];
$WeaponsListData[$WeaponCount, "desc"] = "Good close range weapon, fizzles out quickly";
$WeaponsListData[$WeaponCount, "stats"] = "Damage: 90 | Speed: 100 | Force: 500";
$WeaponCount++;

$InvWeapon[$WeaponCount] = "Chaingun";
$NameToInv["Chaingun"] = "Chaingun";
$WeaponsHudData[$WeaponCount, bitmapName] = "gui/hud_chaingun";
$WeaponsHudData[$WeaponCount, itemDataName] = "Chaingun";
$WeaponsHudData[$WeaponCount, ammoDataName] = "ChaingunAmmo";
$WeaponsHudData[$WeaponCount, reticle] = "gui/ret_chaingun";
$WeaponsHudData[$WeaponCount, visible] = "true";
$AmmoIncrement[ChaingunAmmo] = 25;
$WeaponsListID["Chaingun"] = $WeaponCount;
$WeaponsListData[$WeaponCount, "name"] = $InvWeapon[$WeaponCount];
$WeaponsListData[$WeaponCount, "desc"] = "Motor powered gattling weapon - pretty accurate";
$WeaponsListData[$WeaponCount, "stats"] = "Damage: 25 | Speed: 600 | Force: 0 | Spread: 3";
$WeaponCount++;

$InvWeapon[$WeaponCount] = "Spinfusor";
$NameToInv["Spinfusor"] = "Disc";
$WeaponsHudData[$WeaponCount, bitmapName] = "gui/hud_disc";
$WeaponsHudData[$WeaponCount, itemDataName] = "Disc";
$WeaponsHudData[$WeaponCount, ammoDataName] = "DiscAmmo";
$WeaponsHudData[$WeaponCount, reticle] = "gui/ret_disc";
$WeaponsHudData[$WeaponCount, visible] = "true";
$AmmoIncrement[DiscAmmo] = 5;
$WeaponsListID["Disc"] = $WeaponCount;
$WeaponsListData[$WeaponCount, "name"] = $InvWeapon[$WeaponCount];
$WeaponsListData[$WeaponCount, "desc"] = "Maglev-powered high explosive skeet launcher";
$WeaponsListData[$WeaponCount, "stats"] = "Damage: 100 | Speed: 250 | Force: 2500 | Style Points: +1";
$WeaponCount++;

$InvWeapon[$WeaponCount] = "Grenade Launcher";
$NameToInv["Grenade Launcher"] = "GrenadeLauncher";
$WeaponsHudData[$WeaponCount, bitmapName] = "gui/hud_grenlaunch";
$WeaponsHudData[$WeaponCount, itemDataName] = "GrenadeLauncher";
$WeaponsHudData[$WeaponCount, ammoDataName] = "GrenadeLauncherAmmo";
$WeaponsHudData[$WeaponCount, reticle] = "gui/ret_grenade";
$WeaponsHudData[$WeaponCount, visible] = "true";
$AmmoIncrement[GrenadeLauncherAmmo] = 5;
$WeaponsListID["GrenadeLauncher"] = $WeaponCount;
$WeaponsListData[$WeaponCount, "name"] = $InvWeapon[$WeaponCount];
$WeaponsListData[$WeaponCount, "desc"] = "Like little eggs of death!";
$WeaponsListData[$WeaponCount, "stats"] = "Damage: variable | Speed: 200 | Force: variable";
$WeaponCount++;

$InvWeapon[$WeaponCount] = "Laser Rifle";
$NameToInv["Laser Rifle"] = "SniperRifle";
$WeaponsHudData[$WeaponCount, bitmapName] = "gui/hud_sniper";
$WeaponsHudData[$WeaponCount, itemDataName] = "SniperRifle";
$WeaponsHudData[$WeaponCount, ammoDataName] = "";
$WeaponsHudData[$WeaponCount, reticle] = "gui/hud_ret_sniper";
$WeaponsHudData[$WeaponCount, visible] = "false";
$WeaponsListID["SniperRifle"] = $WeaponCount;
$WeaponsListData[$WeaponCount, "name"] = $InvWeapon[$WeaponCount];
$WeaponsListData[$WeaponCount, "desc"] = "Completely drains capacitor for full damage output - optimal range: 400m";
$WeaponsListData[$WeaponCount, "stats"] = "Damage: 150 | Optimal Range: 75 | Maximum Range: 150 | Energy: 75";
$WeaponCount++;

$InvWeapon[$WeaponCount] = "ELF Projector";
$NameToInv["ELF Projector"] = "ELFGun";
$WeaponsHudData[$WeaponCount, bitmapName] = "gui/hud_elfgun";
$WeaponsHudData[$WeaponCount, itemDataName] = "ELFGun";
$WeaponsHudData[$WeaponCount, ammoDataName] = "";
$WeaponsHudData[$WeaponCount, reticle] = "gui/ret_elf";
$WeaponsHudData[$WeaponCount, visible] = "true";
$WeaponsListID["ELFGun"] = $WeaponCount;
$WeaponsListData[$WeaponCount, "name"] = $InvWeapon[$WeaponCount];
$WeaponsListData[$WeaponCount, "desc"] = "Danger danger! High voltage! When we touch...";
$WeaponsListData[$WeaponCount, "stats"] = "Damage: 64/sec | Maximum Range: 25 | Energy: 32/sec";
$WeaponCount++;

$InvWeapon[$WeaponCount] = "Mortar";
$NameToInv["Mortar"] = "Mortar";
$WeaponsHudData[$WeaponCount, bitmapName] = "gui/hud_mortor";
$WeaponsHudData[$WeaponCount, itemDataName] = "Mortar";
$WeaponsHudData[$WeaponCount, ammoDataName] = "MortarAmmo";
$WeaponsHudData[$WeaponCount, reticle] = "gui/ret_mortor";
$WeaponsHudData[$WeaponCount, visible] = "true";
$AmmoIncrement[MortarAmmo] = 5;
$WeaponsListID["Mortar"] = $WeaponCount;
$WeaponsListData[$WeaponCount, "name"] = $InvWeapon[$WeaponCount];
$WeaponsListData[$WeaponCount, "desc"] = "Long fuse, big boom";
$WeaponsListData[$WeaponCount, "stats"] = "Damage: 300 | Speed: 125 | Force: 5000";
$WeaponCount++;

$InvWeapon[$WeaponCount] = "Missile Launcher";
$NameToInv["Missile Launcher"] = "MissileLauncher";
$WeaponsHudData[$WeaponCount, bitmapName] = "gui/hud_missiles";
$WeaponsHudData[$WeaponCount, itemDataName] = "MissileLauncher";
$WeaponsHudData[$WeaponCount, ammoDataName] = "MissileLauncherAmmo";
$WeaponsHudData[$WeaponCount, reticle] = "gui/ret_missile";
$WeaponsHudData[$WeaponCount, visible] = "true";
$AmmoIncrement[MissileLauncherAmmo] = 2;
$WeaponsListID["MissileLauncher"] = $WeaponCount;
$WeaponsListData[$WeaponCount, "name"] = $InvWeapon[$WeaponCount];
$WeaponsListData[$WeaponCount, "desc"] = "Launches rockets if no heat lock is established";
$WeaponsListData[$WeaponCount, "stats"] = "Damage: 250 | Speed: 250 | Force: 3000 | Lifetime: 10";
$WeaponCount++;

$InvWeapon[$WeaponCount] = "Shocklance";
$NameToInv["Shocklance"] = "ShockLance";
$WeaponsHudData[$WeaponCount, bitmapName]   = "gui/hud_shocklance";
$WeaponsHudData[$WeaponCount, itemDataName] = "ShockLance";
$WeaponsHudData[$WeaponCount, ammoDataName] = "";
$WeaponsHudData[$WeaponCount, reticle] = "gui/hud_ret_shocklance";
$WeaponsHudData[$WeaponCount, visible] = "false";
$WeaponsListID["ShockLance"] = $WeaponCount;
$WeaponsListData[$WeaponCount, "name"] = $InvWeapon[$WeaponCount];
$WeaponsListData[$WeaponCount, "desc"] = "For some reason, this label reads \"For Rectal use only\"";
$WeaponsListData[$WeaponCount, "stats"] = "Damage: 150 (x2 from behind) | Maximum Range: 20 | Force: 4000 | Energy: 40";
$WeaponCount++;

$InvWeapon[$WeaponCount] = "Targeting Laser";
//$NameToInv["Targeting Laser"] = "TargetingLaser";
// WARNING!!! If you change the weapon index of the targeting laser,
// you must change the HudWeaponInvBase::addWeapon function to test
// for the new value!
// 9
$WeaponsHudData[$WeaponCount, bitmapName]   = "gui/hud_targetlaser";
$WeaponsHudData[$WeaponCount, itemDataName] = "TargetingLaser";
$WeaponsHudData[$WeaponCount, ammoDataName] = "";
$WeaponsHudData[$WeaponCount, reticle] = "gui/hud_ret_targlaser";
$WeaponsHudData[$WeaponCount, visible] = "false";
$WeaponsListID["TargetingLaser"] = $WeaponCount;
$WeaponsListData[$WeaponCount, "name"] = $InvWeapon[$WeaponCount];
$WeaponsListData[$WeaponCount, "desc"] = "Extra handy for teammates with mortars and missile launchers";
$WeaponsListData[$WeaponCount, "stats"] = "";
$WeaponCount++;

$InvWeapon[$WeaponCount] = "Laser Missile Launcher";
$NameToInv["Laser Missile Launcher"] = "LaserMissileLauncher";
$WeaponsHudData[$WeaponCount, bitmapName] = "gui/hud_missiles";
$WeaponsHudData[$WeaponCount, itemDataName] = "LaserMissileLauncher";
$WeaponsHudData[$WeaponCount, ammoDataName] = "MissileLauncherAmmo";
$WeaponsHudData[$WeaponCount, reticle] = "gui/ret_missile";
$WeaponsHudData[$WeaponCount, visible] = "true";
$AmmoIncrement[MissileLauncherAmmo] = 2;
$WeaponsListID["LaserMissileLauncher"] = $WeaponCount;
$WeaponsListData[$WeaponCount, "name"] = $InvWeapon[$WeaponCount];
$WeaponsListData[$WeaponCount, "desc"] = "Similar model to the Missile Launcher, except these are laser guided missiles";
$WeaponsListData[$WeaponCount, "stats"] = "Damage: 250 | Speed: 250 | Force: 3000 | Lifetime: 10";
$WeaponCount++;

$InvWeapon[$WeaponCount] = "Spike Rifle";
$NameToInv["Spike Rifle"] = "SpikeRifle";
$WeaponsHudData[$WeaponCount, bitmapName] = "gui/hud_mortor";
$WeaponsHudData[$WeaponCount, itemDataName] = "SpikeRifle";
$WeaponsHudData[$WeaponCount, ammoDataName] = "SpikeAmmo";
$WeaponsHudData[$WeaponCount, reticle] = "gui/hud_ret_sniper";
$WeaponsHudData[$WeaponCount, visible] = "false";
$AmmoIncrement[SpikeAmmo] = 10;
$WeaponsListID["SpikeRifle"] = $WeaponCount;
$WeaponsListData[$WeaponCount, "name"] = $InvWeapon[$WeaponCount];
$WeaponsListData[$WeaponCount, "desc"] = "The illegitmate lovechild of a marksman rifle and a landspike turret";
$WeaponsListData[$WeaponCount, "stats"] = "Damage: 75 | Speed: 500 | Force: 750";
$WeaponCount++;

$InvWeapon[$WeaponCount] = "Minigun";
$NameToInv["Minigun"] = "Minigun";
$WeaponsHudData[$WeaponCount, bitmapName] = "gui/hud_chaingun";
$WeaponsHudData[$WeaponCount, itemDataName] = "Minigun";
$WeaponsHudData[$WeaponCount, ammoDataName] = "MinigunAmmo";
$WeaponsHudData[$WeaponCount, reticle] = "gui/ret_chaingun";
$WeaponsHudData[$WeaponCount, visible] = "true";
$AmmoIncrement[MinigunAmmo] = 25;
$WeaponsListID["Minigun"] = $WeaponCount;
$WeaponsListData[$WeaponCount, "name"] = $InvWeapon[$WeaponCount];
$WeaponsListData[$WeaponCount, "desc"] = "Shoots a hailstorm of bullets, less accurate than the Chaingun";
$WeaponsListData[$WeaponCount, "stats"] = "Damage: 25 | Speed: 600 | Force: 0 | Spread: 3";
$WeaponCount++;

$InvWeapon[$WeaponCount] = "Gauss Rifle";
$NameToInv["Gauss Rifle"] = "GaussRifle";
$WeaponsHudData[$WeaponCount, bitmapName] = "gui/hud_shocklance";
$WeaponsHudData[$WeaponCount, itemDataName] = "GaussRifle";
$WeaponsHudData[$WeaponCount, ammoDataName] = "GaussAmmo";
$WeaponsHudData[$WeaponCount, reticle] = "gui/hud_ret_sniper";
$WeaponsHudData[$WeaponCount, visible] = "false";
$AmmoIncrement[GaussAmmo] = 10;
$WeaponsListID["GaussRifle"] = $WeaponCount;
$WeaponsListData[$WeaponCount, "name"] = $InvWeapon[$WeaponCount];
$WeaponsListData[$WeaponCount, "desc"] = "Like a baby railgun, uses energy and ammo and doesn't skimp on damage";
$WeaponsListData[$WeaponCount, "stats"] = "Damage: 75 | Speed: 500 | Force: 750";
$WeaponCount++;

$InvWeapon[$WeaponCount] = "Scattergun";
$NameToInv["Scattergun"] = "Scattergun";
$WeaponsHudData[$WeaponCount, bitmapName] = "gui/hud_plasma";
$WeaponsHudData[$WeaponCount, itemDataName] = "Scattergun";
$WeaponsHudData[$WeaponCount, ammoDataName] = "ScattergunAmmo";
$WeaponsHudData[$WeaponCount, reticle] = "gui/ret_plasma";
$WeaponsHudData[$WeaponCount, visible] = "true";
$AmmoIncrement[ScattergunAmmo] = 10;
$WeaponsListID["Scattergun"] = $WeaponCount;
$WeaponsListData[$WeaponCount, "name"] = $InvWeapon[$WeaponCount];
$WeaponsListData[$WeaponCount, "desc"] = "12-bullet wide spread of pain";
$WeaponsListData[$WeaponCount, "stats"] = "Damage: 360 (30x12) | Speed: 425 | Force: 0 | Spread: 21";
$WeaponCount++;

$InvWeapon[$WeaponCount] = "Flamethrower";
$NameToInv["Flamethrower"] = "Flamethrower";
$WeaponsHudData[$WeaponCount, bitmapName] = "gui/hud_plasma";
$WeaponsHudData[$WeaponCount, itemDataName] = "Flamethrower";
$WeaponsHudData[$WeaponCount, ammoDataName] = "FlamethrowerAmmo";
$WeaponsHudData[$WeaponCount, reticle] = "gui/ret_plasma";
$WeaponsHudData[$WeaponCount, visible] = "true";
$AmmoIncrement[FlamethrowerAmmo] = 100;
$WeaponsListID["Flamethrower"] = $WeaponCount;
$WeaponsListData[$WeaponCount, "name"] = $InvWeapon[$WeaponCount];
$WeaponsListData[$WeaponCount, "desc"] = "Toasty!";
$WeaponsListData[$WeaponCount, "stats"] = "Damage: 240 | Speed: 250 | Force: 1500";
$WeaponCount++;

$InvWeapon[$WeaponCount] = "Sentry Rifle";
$NameToInv["Sentry Rifle"] = "SentryRifle";
$WeaponsHudData[$WeaponCount, bitmapName] = "gui/hud_blaster";
$WeaponsHudData[$WeaponCount, itemDataName] = "SentryRifle";
$WeaponsHudData[$WeaponCount, ammoDataName] = "";
$WeaponsHudData[$WeaponCount, reticle] = "gui/ret_blaster";
$WeaponsHudData[$WeaponCount, visible] = "true";
$WeaponsListID["SentryRifle"] = $WeaponCount;
$WeaponsListData[$WeaponCount, "name"] = $InvWeapon[$WeaponCount];
$WeaponsListData[$WeaponCount, "desc"] = "The power of a Sentry Turret in the palm of your hands";
$WeaponsListData[$WeaponCount, "stats"] = "Damage: 75 | Speed: 500 | Force: 750";
$WeaponCount++;

$InvWeapon[$WeaponCount] = "Plasma Cannon";
$NameToInv["Plasma Cannon"] = "PlasmaCannon";
$WeaponsHudData[$WeaponCount, bitmapName] = "gui/hud_plasma";
$WeaponsHudData[$WeaponCount, itemDataName] = "PlasmaCannon";
$WeaponsHudData[$WeaponCount, ammoDataName] = "PlasmaCannonAmmo";
$WeaponsHudData[$WeaponCount, reticle] = "gui/ret_plasma";
$WeaponsHudData[$WeaponCount, visible] = "true";
$AmmoIncrement[PlasmaCannonAmmo] = 10;
$WeaponsListID["PlasmaCannon"] = $WeaponCount;
$WeaponsListData[$WeaponCount, "name"] = $InvWeapon[$WeaponCount];
$WeaponsListData[$WeaponCount, "desc"] = "Man portable plasma turret, great balls of fire!";
$WeaponsListData[$WeaponCount, "stats"] = "Damage: 240 | Speed: 250 | Force: 1500";
$WeaponCount++;

$InvWeapon[$WeaponCount] = "Artillery Cannon";
$NameToInv["Artillery Cannon"] = "ArtilleryCannon";
$WeaponsHudData[$WeaponCount, bitmapName] = "gui/hud_mortor";
$WeaponsHudData[$WeaponCount, itemDataName] = "ArtilleryCannon";
$WeaponsHudData[$WeaponCount, ammoDataName] = "ArtilleryCannonAmmo";
$WeaponsHudData[$WeaponCount, reticle] = "gui/ret_mortor";
$WeaponsHudData[$WeaponCount, visible] = "true";
$AmmoIncrement[ArtilleryCannonAmmo] = 5;
$WeaponsListID["ArtilleryCannon"] = $WeaponCount;
$WeaponsListData[$WeaponCount, "name"] = $InvWeapon[$WeaponCount];
$WeaponsListData[$WeaponCount, "desc"] = "Trades damage for speed and impact-explosive capability";
$WeaponsListData[$WeaponCount, "stats"] = "Damage: 450 | Speed: 400 | Force: 6000";
$WeaponCount++;

$InvWeapon[$WeaponCount] = "AutoCannon";
$NameToInv["AutoCannon"] = "AutoCannon";
$WeaponsHudData[$WeaponCount, bitmapName] = "gui/hud_missiles";
$WeaponsHudData[$WeaponCount, itemDataName] = "AutoCannon";
$WeaponsHudData[$WeaponCount, ammoDataName] = "AutoCannonAmmo";
$WeaponsHudData[$WeaponCount, reticle] = "gui/ret_chaingun";
$WeaponsHudData[$WeaponCount, visible] = "true";
$AmmoIncrement[AutoCannonAmmo] = 25;
$WeaponsListID["AutoCannon"] = $WeaponCount;
$WeaponsListData[$WeaponCount, "name"] = $InvWeapon[$WeaponCount];
$WeaponsListData[$WeaponCount, "desc"] = "Rotary mini-rocket launcher";
$WeaponsListData[$WeaponCount, "stats"] = "Damage: 25 | Speed: 600 | Force: 0 | Spread: 3";
$WeaponCount++;

$InvWeapon[$WeaponCount] = "Twinfusor";
$NameToInv["Twinfusor"] = "Twinfusor";
$WeaponsHudData[$WeaponCount, bitmapName] = "gui/hud_disc";
$WeaponsHudData[$WeaponCount, itemDataName] = "Twinfusor";
$WeaponsHudData[$WeaponCount, ammoDataName] = "TwinfusorAmmo";
$WeaponsHudData[$WeaponCount, reticle] = "gui/ret_disc";
$WeaponsHudData[$WeaponCount, visible] = "true";
$AmmoIncrement[TwinfusorAmmo] = 25;
$WeaponsListID["Twinfusor"] = $WeaponCount;
$WeaponsListData[$WeaponCount, "name"] = $InvWeapon[$WeaponCount];
$WeaponsListData[$WeaponCount, "desc"] = "These discs may be smaller, but you'll be sending more of them down range";
$WeaponsListData[$WeaponCount, "stats"] = "Damage: 25 | Speed: 600 | Force: 0 | Spread: 3";
$WeaponCount++;

$InvWeapon[$WeaponCount] = "Multipulsar";
$NameToInv["Multipulsar"] = "Multipulsar";
$WeaponsHudData[$WeaponCount, bitmapName] = "gui/hud_elfgun";
$WeaponsHudData[$WeaponCount, itemDataName] = "Multipulsar";
$WeaponsHudData[$WeaponCount, ammoDataName] = "";
$WeaponsHudData[$WeaponCount, reticle] = "gui/ret_missile";
$WeaponsHudData[$WeaponCount, visible] = "true";
$WeaponsListID["Multipulsar"] = $WeaponCount;
$WeaponsListData[$WeaponCount, "name"] = $InvWeapon[$WeaponCount];
$WeaponsListData[$WeaponCount, "desc"] = "A disruptor pulse cannon that means business!";
$WeaponsListData[$WeaponCount, "stats"] = "Damage: 75 | Speed: 500 | Force: 750";
$WeaponCount++;

$InvWeapon[$WeaponCount] = "Laser Cannon";
$NameToInv["Laser Cannon"] = "LaserCannon";
$WeaponsHudData[$WeaponCount, bitmapName] = "gui/hud_sniper";
$WeaponsHudData[$WeaponCount, itemDataName] = "LaserCannon";
$WeaponsHudData[$WeaponCount, ammoDataName] = "";
$WeaponsHudData[$WeaponCount, reticle] = "gui/hud_ret_sniper";
$WeaponsHudData[$WeaponCount, visible] = "false";
$WeaponsListID["LaserCannon"] = $WeaponCount;
$WeaponsListData[$WeaponCount, "name"] = $InvWeapon[$WeaponCount];
$WeaponsListData[$WeaponCount, "desc"] = "Constant stream of laser energy - optimal range: 100m";
$WeaponsListData[$WeaponCount, "stats"] = "Damage: 40/sec | Optimal Range: 100m | Max Range: 200m | Energy: 5/sec";
$WeaponCount++;

$InvWeapon[$WeaponCount] = "Deconstructor";
//$NameToInv["Deconstructor"] = "Deconstructor";
$WeaponsHudData[$WeaponCount, bitmapName]   = "gui/hud_shocklance";
$WeaponsHudData[$WeaponCount, itemDataName] = "Deconstructor";
$WeaponsHudData[$WeaponCount, ammoDataName] = "";
$WeaponsHudData[$WeaponCount, reticle] = "gui/hud_ret_shocklance";
$WeaponsHudData[$WeaponCount, visible] = "false";
$WeaponsListID["Deconstructor"] = $WeaponCount;
$WeaponsListData[$WeaponCount, "name"] = $InvWeapon[$WeaponCount];
$WeaponsListData[$WeaponCount, "desc"] = "With it's simple point and click interface, you can convert assets into packs!";
$WeaponsListData[$WeaponCount, "stats"] = "";
$WeaponCount++;

$WeaponsHudCount = $WeaponCount;

//------------------------------------------------------------------------------
// Inventory Defines
$InventoryHudCount = 0;

//------------------------------------------------------------------------------
// Grenades
$GrenadeCount = 0;

$InvGrenade[$GrenadeCount] = "Grenade";
$NameToInv["Grenade"] = "Grenade";
$AmmoIncrement[Grenade]             = 5;
$InventoryHudData[$InventoryHudCount, bitmapName]   = "gui/hud_handgren";
$InventoryHudData[$InventoryHudCount, itemDataName] = Grenade;
$InventoryHudData[$InventoryHudCount, ammoDataName] = Grenade;
$InventoryHudData[$InventoryHudCount, slot]         = 0;
$InventoryHudCount++;
$GrenadeCount++;

$InvGrenade[$GrenadeCount] = "Smoke Grenade";
$NameToInv["Smoke Grenade"] = "SmokeGrenade";
$AmmoIncrement[SmokeGrenade]       = 2;
$InventoryHudData[$InventoryHudCount, bitmapName]   = "gui/hud_new_packsensjam";
$InventoryHudData[$InventoryHudCount, itemDataName] = SmokeGrenade;
$InventoryHudData[$InventoryHudCount, ammoDataName] = SmokeGrenade;
$InventoryHudData[$InventoryHudCount, slot]         = 0;
$InventoryHudCount++;
$GrenadeCount++;

$InvGrenade[$GrenadeCount] = "Whiteout Grenade";
$NameToInv["Whiteout Grenade"] = "FlashGrenade";
$AmmoIncrement[FlashGrenade]        = 5;
$InventoryHudData[$InventoryHudCount, bitmapName]   = "gui/hud_whiteout_gren";
$InventoryHudData[$InventoryHudCount, itemDataName] = FlashGrenade;
$InventoryHudData[$InventoryHudCount, ammoDataName] = FlashGrenade;
$InventoryHudData[$InventoryHudCount, slot]         = 0;
$InventoryHudCount++;
$GrenadeCount++;

$InvGrenade[$GrenadeCount] = "Concussion Grenade";
$NameToInv["Concussion Grenade"] = "ConcussionGrenade";
$AmmoIncrement[ConcussionGrenade]   = 5;
$InventoryHudData[$InventoryHudCount, bitmapName]   = "gui/hud_concuss_gren";
$InventoryHudData[$InventoryHudCount, itemDataName] = ConcussionGrenade;
$InventoryHudData[$InventoryHudCount, ammoDataName] = ConcussionGrenade;
$InventoryHudData[$InventoryHudCount, slot]         = 0;
$InventoryHudCount++;
$GrenadeCount++;

$InvGrenade[$GrenadeCount] = "EM Pulse Grenade";
$NameToInv["EM Pulse Grenade"] = "EMPGrenade";
$AmmoIncrement[EMPGrenade]       = 2;
$InventoryHudData[$InventoryHudCount, bitmapName]   = "gui/hud_ergbaricon";
$InventoryHudData[$InventoryHudCount, itemDataName] = EMPGrenade;
$InventoryHudData[$InventoryHudCount, ammoDataName] = EMPGrenade;
$InventoryHudData[$InventoryHudCount, slot]         = 0;
$InventoryHudCount++;
$GrenadeCount++;

$InvGrenade[$GrenadeCount] = "Incendiary Grenade";
$NameToInv["Incendiary Grenade"] = "FireGrenade";
$AmmoIncrement[FireGrenade]       = 2;
$InventoryHudData[$InventoryHudCount, bitmapName]   = "gui/hud_plasma";
$InventoryHudData[$InventoryHudCount, itemDataName] = FireGrenade;
$InventoryHudData[$InventoryHudCount, ammoDataName] = FireGrenade;
$InventoryHudData[$InventoryHudCount, slot]         = 0;
$InventoryHudCount++;
$GrenadeCount++;

$InvGrenade[$GrenadeCount] = "Poison Gas Grenade";
$NameToInv["Poison Gas Grenade"] = "PoisonGrenade";
$AmmoIncrement[PoisonGrenade]       = 2;
$InventoryHudData[$InventoryHudCount, bitmapName]   = "gui/hud_shocklance";
$InventoryHudData[$InventoryHudCount, itemDataName] = PoisonGrenade;
$InventoryHudData[$InventoryHudCount, ammoDataName] = PoisonGrenade;
$InventoryHudData[$InventoryHudCount, slot]         = 0;
$InventoryHudCount++;
$GrenadeCount++;

$InvGrenade[$GrenadeCount] = "Mortar Shell";
$NameToInv["Mortar Shell"] = "MortarGrenade";
$AmmoIncrement[MortarGrenade]       = 2;
$InventoryHudData[$InventoryHudCount, bitmapName]   = "gui/hud_veh_bomb";
$InventoryHudData[$InventoryHudCount, itemDataName] = MortarGrenade;
$InventoryHudData[$InventoryHudCount, ammoDataName] = MortarGrenade;
$InventoryHudData[$InventoryHudCount, slot]         = 0;
$InventoryHudCount++;
$GrenadeCount++;

$InvGrenade[$GrenadeCount] = "HEAT Shell";
$NameToInv["HEAT Shell"] = "HEATGrenade";
$AmmoIncrement[HEATGrenade]       = 2;
$InventoryHudData[$InventoryHudCount, bitmapName]   = "gui/hud_mortor";
$InventoryHudData[$InventoryHudCount, itemDataName] = HEATGrenade;
$InventoryHudData[$InventoryHudCount, ammoDataName] = HEATGrenade;
$InventoryHudData[$InventoryHudCount, slot]         = 0;
$InventoryHudCount++;
$GrenadeCount++;

$InvGrenade[$GrenadeCount] = "Proximity Flak Grenade";
$NameToInv["Proximity Flak Grenade"] = "FlakGrenade";
$AmmoIncrement[FlakGrenade]       = 2;
$InventoryHudData[$InventoryHudCount, bitmapName]   = "gui/hud_grenlaunch";
$InventoryHudData[$InventoryHudCount, itemDataName] = FlakGrenade;
$InventoryHudData[$InventoryHudCount, ammoDataName] = FlakGrenade;
$InventoryHudData[$InventoryHudCount, slot]         = 0;
$InventoryHudCount++;
$GrenadeCount++;

$InvGrenade[$GrenadeCount] = "Power Injector";
$NameToInv["Power Injector"] = "BatteryGrenade";
$AmmoIncrement[BatteryGrenade]       = 1;
$InventoryHudData[$InventoryHudCount, bitmapName]   = "gui/hud_elfgun";
$InventoryHudData[$InventoryHudCount, itemDataName] = BatteryGrenade;
$InventoryHudData[$InventoryHudCount, ammoDataName] = BatteryGrenade;
$InventoryHudData[$InventoryHudCount, slot]         = 0;
$InventoryHudCount++;
$GrenadeCount++;

$InvGrenade[$GrenadeCount] = "Jet Thruster";
$NameToInv["Jet Thruster"] = "ThrusterGrenade";
$AmmoIncrement[ThrusterGrenade]       = 2;
$InventoryHudData[$InventoryHudCount, bitmapName]   = "gui/hud_armbaricon";
$InventoryHudData[$InventoryHudCount, itemDataName] = ThrusterGrenade;
$InventoryHudData[$InventoryHudCount, ammoDataName] = ThrusterGrenade;
$InventoryHudData[$InventoryHudCount, slot]         = 0;
$InventoryHudCount++;
$GrenadeCount++;

$InvGrenade[$GrenadeCount] = "Flare Grenade";
$NameToInv["Flare Grenade"] = "FlareGrenade";
$AmmoIncrement[FlareGrenade]        = 1;
$InventoryHudData[$InventoryHudCount, bitmapName]   = "gui/hud_handgren";
$InventoryHudData[$InventoryHudCount, itemDataName] = FlareGrenade;
$InventoryHudData[$InventoryHudCount, ammoDataName] = FlareGrenade;
$InventoryHudData[$InventoryHudCount, slot]         = 0;
$InventoryHudCount++;
$GrenadeCount++;

$InvGrenade[$GrenadeCount] = "Deployable Camera";
$NameToInv["Deployable Camera"] = "CameraGrenade";
$AmmoIncrement[CameraGrenade]       = 2; // z0dd - ZOD, 4/17/02. Camera ammo pickup fix.
$InventoryHudData[$InventoryHudCount, bitmapName]   = "gui/hud_handgren";
$InventoryHudData[$InventoryHudCount, itemDataName] = CameraGrenade;
$InventoryHudData[$InventoryHudCount, ammoDataName] = CameraGrenade;
$InventoryHudData[$InventoryHudCount, slot]         = 0;
$InventoryHudCount++;
$GrenadeCount++;

//------------------------------------------------------------------------------
// Mines
$MineCount = 0;

$InvMine[$MineCount] = "Mine";
$NameToInv["Mine"] = "Mine";
$AmmoIncrement[Mine] = 3;
$InventoryHudData[$InventoryHudCount, bitmapName]   = "gui/hud_mine";
$InventoryHudData[$InventoryHudCount, itemDataName] = Mine;
$InventoryHudData[$InventoryHudCount, ammoDataName] = Mine;
$InventoryHudData[$InventoryHudCount, slot]         = 1;
$InventoryHudCount++;
$MineCount++;

$InvMine[$MineCount] = "Anti-Tank Mine";
$NameToInv["Anti-Tank Mine"] = "AntiTankMine";
$AmmoIncrement[AntiTankMine] = 3;
$InventoryHudData[$InventoryHudCount, bitmapName]   = "gui/hud_mine";
$InventoryHudData[$InventoryHudCount, itemDataName] = AntiTankMine;
$InventoryHudData[$InventoryHudCount, ammoDataName] = AntiTankMine;
$InventoryHudData[$InventoryHudCount, slot]         = 1;
$InventoryHudCount++;
$MineCount++;

$InvMine[$MineCount] = "Caltrop Mine";
$NameToInv["Caltrop Mine"] = "CaltropMine";
$AmmoIncrement[CaltropMine] = 3;
$InventoryHudData[$InventoryHudCount, bitmapName]   = "gui/hud_mine";
$InventoryHudData[$InventoryHudCount, itemDataName] = CaltropMine;
$InventoryHudData[$InventoryHudCount, ammoDataName] = CaltropMine;
$InventoryHudData[$InventoryHudCount, slot]         = 1;
$InventoryHudCount++;
$MineCount++;

$InvMine[$MineCount] = "Holopuck Mine";
$NameToInv["Holopuck Mine"] = "HolopuckMine";
$AmmoIncrement[HolopuckMine] = 3;
$InventoryHudData[$InventoryHudCount, bitmapName]   = "gui/hud_mine";
$InventoryHudData[$InventoryHudCount, itemDataName] = HolopuckMine;
$InventoryHudData[$InventoryHudCount, ammoDataName] = HolopuckMine;
$InventoryHudData[$InventoryHudCount, slot]         = 1;
$InventoryHudCount++;
$MineCount++;

$InvMine[$MineCount] = "Repair Patch Mine";
$NameToInv["Repair Patch Mine"] = "RepairPatchMine";
$AmmoIncrement[RepairPatchMine] = 3;
$InventoryHudData[$InventoryHudCount, bitmapName]   = "gui/hud_new_packrepair";
$InventoryHudData[$InventoryHudCount, itemDataName] = RepairPatchMine;
$InventoryHudData[$InventoryHudCount, ammoDataName] = RepairPatchMine;
$InventoryHudData[$InventoryHudCount, slot]         = 1;
$InventoryHudCount++;
$MineCount++;

//-----------------------------------------------------------------------------
// Misc
$AmmoIncrement[RepairKit]           = 1;
$InventoryHudData[$InventoryHudCount, bitmapName]   = "gui/hud_medpack";
$InventoryHudData[$InventoryHudCount, itemDataName] = RepairKit;
$InventoryHudData[$InventoryHudCount, ammoDataName] = RepairKit;
$InventoryHudData[$InventoryHudCount, slot]         = 3;
$InventoryHudCount++;

$AmmoIncrement[Beacon]              = 1; // z0dd - ZOD, 4/17/02. Beacon ammo pickup fix.
$InventoryHudData[$InventoryHudCount, bitmapName]   = "gui/hud_beacon";
$InventoryHudData[$InventoryHudCount, itemDataName] = Beacon;
$InventoryHudData[$InventoryHudCount, ammoDataName] = Beacon;
$InventoryHudData[$InventoryHudCount, slot]         = 2;
$InventoryHudCount++;

//-----------------------------------------------------------------------------
// Packs
$PackCount = 0;

$InvPack[$PackCount] = "Energy Pack";
$NameToInv["Energy Pack"] = "EnergyPack";
$BackpackListID[$NameToInv[$InvPack[$PackCount]]] = $PackCount;
$BackpackListData[$PackCount, "name"] = $InvPack[$PackCount];
$BackpackListData[$PackCount, "desc"] = "Doubles energy recharge rate and protects from EMP effects";
$PackCount++;

$InvPack[$PackCount] = "Ammunition Pack";
$NameToInv["Ammunition Pack"] = "AmmoPack";
$BackpackListID[$NameToInv[$InvPack[$PackCount]]] = $PackCount;
$BackpackListData[$PackCount, "name"] = $InvPack[$PackCount];
$BackpackListData[$PackCount, "desc"] = "Increases ammo storage";
$PackCount++;

$InvPack[$PackCount] = "Shield Pack";
$NameToInv["Shield Pack"] = "ShieldPack";
$BackpackListID[$NameToInv[$InvPack[$PackCount]]] = $PackCount;
$BackpackListData[$PackCount, "name"] = $InvPack[$PackCount];
$BackpackListData[$PackCount, "desc"] = "When activated, creates a shield matrix that draws from your capacitor";
$PackCount++;

$InvPack[$PackCount] = "Cloak Pack";
$NameToInv["Cloak Pack"] = "CloakingPack";
$BackpackListID[$NameToInv[$InvPack[$PackCount]]] = $PackCount;
$BackpackListData[$PackCount, "name"] = $InvPack[$PackCount];
$BackpackListData[$PackCount, "desc"] = "When activated, turns you invisible";
$PackCount++;

$InvPack[$PackCount] = "Gravitron Pack";
$NameToInv["Gravitron Pack"] = "GravitronPack";
$BackpackListID[$NameToInv[$InvPack[$PackCount]]] = $PackCount;
$BackpackListData[$PackCount, "name"] = $InvPack[$PackCount];
$BackpackListData[$PackCount, "desc"] = "Reduces your local gravity - forces like jets and explosions are more effective";
$PackCount++;

$InvPack[$PackCount] = "Turbocharger";
$NameToInv["Turbocharger"] = "TurbochargerPack";
$BackpackListID[$NameToInv[$InvPack[$PackCount]]] = $PackCount;
$BackpackListData[$PackCount, "name"] = $InvPack[$PackCount];
$BackpackListData[$PackCount, "desc"] = "Increases damage output by 50%, and a chance to spontaneously explode on damage";
$PackCount++;

$InvPack[$PackCount] = "Vampire Pack";
$NameToInv["Vampire Pack"] = "VampirePack";
$BackpackListID[$NameToInv[$InvPack[$PackCount]]] = $PackCount;
$BackpackListData[$PackCount, "name"] = $InvPack[$PackCount];
$BackpackListData[$PackCount, "desc"] = "Upon killing an enemy player, receive 20% of their maximum health back";
$PackCount++;

$InvPack[$PackCount] = "Sensor Jammer Pack";
$NameToInv["Sensor Jammer Pack"] = "SensorJammerPack";
$BackpackListID[$NameToInv[$InvPack[$PackCount]]] = $PackCount;
$BackpackListData[$PackCount, "name"] = $InvPack[$PackCount];
$BackpackListData[$PackCount, "desc"] = "Prevents pulse sensors from detecting you";
$PackCount++;

$InvPack[$PackCount] = "Repair Pack";
$NameToInv["Repair Pack"] = "RepairPack";
$BackpackListID[$NameToInv[$InvPack[$PackCount]]] = $PackCount;
$BackpackListData[$PackCount, "name"] = $InvPack[$PackCount];
$BackpackListData[$PackCount, "desc"] = "Repairs objects, reduces damage by 25%, and Engineers get 2x repair speed";
$PackCount++;

$InvPack[$PackCount] = "Motion Sensor Pack";
$NameToInv["Motion Sensor Pack"] = "MotionSensorDeployable";
$BackpackListID[$NameToInv[$InvPack[$PackCount]]] = $PackCount;
$BackpackListData[$PackCount, "name"] = $InvPack[$PackCount];
$BackpackListData[$PackCount, "desc"] = "Places a deployable Motion Sensor";
$PackCount++;

$InvPack[$PackCount] = "Pulse Sensor Pack";
$NameToInv["Pulse Sensor Pack"] = "PulseSensorDeployable";
$BackpackListID[$NameToInv[$InvPack[$PackCount]]] = $PackCount;
$BackpackListData[$PackCount, "name"] = $InvPack[$PackCount];
$BackpackListData[$PackCount, "desc"] = "Places a deployable Pulse Sensor";
$PackCount++;

$InvPack[$PackCount] = "Jammer Beacon Pack";
$NameToInv["Jammer Beacon Pack"] = "JammerBeaconPack";
$BackpackListID[$NameToInv[$InvPack[$PackCount]]] = $PackCount;
$BackpackListData[$PackCount, "name"] = $InvPack[$PackCount];
$BackpackListData[$PackCount, "desc"] = "Places a deployable Sensor Jammer Beacon";
$PackCount++;

$InvPack[$PackCount] = "Inventory Station";
$NameToInv["Inventory Station"] = "InventoryDeployable";
$BackpackListID[$NameToInv[$InvPack[$PackCount]]] = $PackCount;
$BackpackListData[$PackCount, "name"] = $InvPack[$PackCount];
$BackpackListData[$PackCount, "desc"] = "Places a deployable inventory station";
$PackCount++;

$InvPack[$PackCount] = "Landspike Turret";
$NameToInv["Landspike Turret"] = "TurretOutdoorDeployable";
$BackpackListID[$NameToInv[$InvPack[$PackCount]]] = $PackCount;
$BackpackListData[$PackCount, "name"] = $InvPack[$PackCount];
$BackpackListData[$PackCount, "desc"] = "Places a deployable landspike turret";
$PackCount++;

$InvPack[$PackCount] = "Spider Clamp Turret";
$NameToInv["Spider Clamp Turret"] = "TurretIndoorDeployable";
$BackpackListID[$NameToInv[$InvPack[$PackCount]]] = $PackCount;
$BackpackListData[$PackCount, "name"] = $InvPack[$PackCount];
$BackpackListData[$PackCount, "desc"] = "Places a deployable spider clamp turret";
$PackCount++;

$InvPack[$PackCount] = "Blast Wall";
$NameToInv["Blast Wall"] = "BlastWallPack";
$BackpackListID[$NameToInv[$InvPack[$PackCount]]] = $PackCount;
$BackpackListData[$PackCount, "name"] = $InvPack[$PackCount];
$BackpackListData[$PackCount, "desc"] = "Places down a deployable wall which completely blocks a passageway";
$PackCount++;

$InvPack[$PackCount] = "Core Node";
$NameToInv["Core Node"] = "CoreNodePack";
$BackpackListID[$NameToInv[$InvPack[$PackCount]]] = $PackCount;
$BackpackListData[$PackCount, "name"] = $InvPack[$PackCount];
$BackpackListData[$PackCount, "desc"] = "Places down a core node, reducing damage on all other nodes by 50%";
$PackCount++;

$InvPack[$PackCount] = "Spawn Favs Node";
$NameToInv["Spawn Favs Node"] = "NodeSpawnFavsPack";
$BackpackListID[$NameToInv[$InvPack[$PackCount]]] = $PackCount;
$BackpackListData[$PackCount, "name"] = $InvPack[$PackCount];
$BackpackListData[$PackCount, "desc"] = "Places down a node, allowing any players within 200m to spawn in loadout";
$PackCount++;

$InvPack[$PackCount] = "Regen Node";
$NameToInv["Regen Node"] = "NodeRegeneratorPack";
$BackpackListID[$NameToInv[$InvPack[$PackCount]]] = $PackCount;
$BackpackListData[$PackCount, "name"] = $InvPack[$PackCount];
$BackpackListData[$PackCount, "desc"] = "Places down a node, slowly repairing all objects within 200m if powered";
$PackCount++;

//$InvPack[$PackCount] = "Cloak Field Node";
//$NameToInv["Cloak Field Node"] = "NodeCloakPack";
//$BackpackListID[$NameToInv[$InvPack[$PackCount]]] = $PackCount;
//$BackpackListData[$PackCount, "name"] = $InvPack[$PackCount];
//$BackpackListData[$PackCount, "desc"] = "Places down a node, cloaking anything within 30m and disables weaponry";
//$PackCount++;

$InvPack[$PackCount] = "Portal Node";
$NameToInv["Portal Node"] = "NodePortalPack";
$BackpackListID[$NameToInv[$InvPack[$PackCount]]] = $PackCount;
$BackpackListData[$PackCount, "name"] = $InvPack[$PackCount];
$BackpackListData[$PackCount, "desc"] = "Places down a node that creates a linked portal between two nodes";
$PackCount++;

$InvPack[$PackCount] = "ELF Turret Barrel";
$NameToInv["ELF Turret Barrel"] = "ELFBarrelPack";
$BackpackListID[$NameToInv[$InvPack[$PackCount]]] = $PackCount;
$BackpackListData[$PackCount, "name"] = $InvPack[$PackCount];
$BackpackListData[$PackCount, "desc"] = "Use this to change the weapon on a Gunship, MPB, or Base Turret";
$PackCount++;

$InvPack[$PackCount] = "Mortar Turret Barrel";
$NameToInv["Mortar Turret Barrel"] = "MortarBarrelPack";
$BackpackListID[$NameToInv[$InvPack[$PackCount]]] = $PackCount;
$BackpackListData[$PackCount, "name"] = $InvPack[$PackCount];
$BackpackListData[$PackCount, "desc"] = "Use this to change the weapon on a Gunship, MPB, or Base Turret";
$PackCount++;

$InvPack[$PackCount] = "Plasma Turret Barrel";
$NameToInv["Plasma Turret Barrel"] = "PlasmaBarrelPack";
$BackpackListID[$NameToInv[$InvPack[$PackCount]]] = $PackCount;
$BackpackListData[$PackCount, "name"] = $InvPack[$PackCount];
$BackpackListData[$PackCount, "desc"] = "Use this to change the weapon on a Gunship, MPB, or Base Turret";
$PackCount++;

$InvPack[$PackCount] = "AA Turret Barrel";
$NameToInv["AA Turret Barrel"] = "AABarrelPack";
$BackpackListID[$NameToInv[$InvPack[$PackCount]]] = $PackCount;
$BackpackListData[$PackCount, "name"] = $InvPack[$PackCount];
$BackpackListData[$PackCount, "desc"] = "Use this to change the weapon on a Gunship, MPB, or Base Turret";
$PackCount++;

$InvPack[$PackCount] = "Missile Turret Barrel";
$NameToInv["Missile Turret Barrel"] = "MissileBarrelPack";
$BackpackListID[$NameToInv[$InvPack[$PackCount]]] = $PackCount;
$BackpackListData[$PackCount, "name"] = $InvPack[$PackCount];
$BackpackListData[$PackCount, "desc"] = "Use this to change the weapon on a Gunship, MPB, or Base Turret";
$PackCount++;

$InvPack[$PackCount] = "Satchel Charge";
$NameToInv["Satchel Charge"] = "SatchelCharge";
$BackpackListID[$NameToInv[$InvPack[$PackCount]]] = $PackCount;
$BackpackListData[$PackCount, "name"] = $InvPack[$PackCount];
$BackpackListData[$PackCount, "desc"] = "Throws a highly explosive breaching charge, remotely detonated";
$PackCount++;

// Original pack array setup - can't override for some reason

//----------------------------------------------------------------------------
//   Backpack Hud
//----------------------------------------------------------------------------

$BackpackHudData[0, itemDataName] = "AmmoPack";
$BackpackHudData[0, bitmapName] = "gui/hud_new_packammo";
$BackpackHudData[1, itemDataName] = "CloakingPack";
$BackpackHudData[1, bitmapName] = "gui/hud_new_packcloak";
$BackpackHudData[2, itemDataName] = "EnergyPack";
$BackpackHudData[2, bitmapName] = "gui/hud_new_packenergy";
$BackpackHudData[3, itemDataName] = "RepairPack";
$BackpackHudData[3, bitmapName] = "gui/hud_new_packrepair";
$BackpackHudData[4, itemDataName] = "SatchelCharge";
$BackpackHudData[4, bitmapName] = "gui/hud_new_packsatchel";
$BackpackHudData[5, itemDataName] = "ShieldPack";
$BackpackHudData[5, bitmapName] = "gui/hud_new_packshield";
$BackpackHudData[6, itemDataName] = "InventoryDeployable";
$BackpackHudData[6, bitmapName] = "gui/hud_new_packinventory";
$BackpackHudData[7, itemDataName] = "MotionSensorDeployable";
$BackpackHudData[7, bitmapName] = "gui/hud_new_packmotionsens";
$BackpackHudData[8, itemDataName] = "PulseSensorDeployable";
$BackpackHudData[8, bitmapName] = "gui/hud_new_packradar";
$BackpackHudData[9, itemDataName] = "TurretOutdoorDeployable";
$BackpackHudData[9, bitmapName] = "gui/hud_new_packturretout";
$BackpackHudData[10, itemDataName] = "TurretIndoorDeployable";
$BackpackHudData[10, bitmapName] = "gui/hud_new_packturretin";
$BackpackHudData[11, itemDataName] = "SensorJammerPack";
$BackpackHudData[11, bitmapName] = "gui/hud_new_packsensjam";
$BackpackHudData[12, itemDataName] = "AABarrelPack";
$BackpackHudData[12, bitmapName] = "gui/hud_new_packturret";
$BackpackHudData[13, itemDataName] = "FusionBarrelPack";
$BackpackHudData[13, bitmapName] = "gui/hud_new_packturret";
$BackpackHudData[14, itemDataName] = "MissileBarrelPack";
$BackpackHudData[14, bitmapName] = "gui/hud_new_packturret";
$BackpackHudData[15, itemDataName] = "PlasmaBarrelPack";
$BackpackHudData[15, bitmapName] = "gui/hud_new_packturret";
$BackpackHudData[16, itemDataName] = "ELFBarrelPack";
$BackpackHudData[16, bitmapName] = "gui/hud_new_packturret";
$BackpackHudData[17, itemDataName] = "MortarBarrelPack";
$BackpackHudData[17, bitmapName] = "gui/hud_new_packturret";
$BackpackHudData[18, itemDataName] = "SatchelUnarmed";
$BackpackHudData[18, bitmapName] = "gui/hud_satchel_unarmed";

// TR2
$BackpackHudData[19, itemDataName] = "TR2EnergyPack";
$BackpackHudData[19, bitmapName] = "gui/hud_new_packenergy";

// Amplified
$BackpackHudData[20, itemDataName] = "GravitronPack";
$BackpackHudData[20, bitmapName] = "gui/hud_new_packsatchel";
$BackpackHudData[21, itemDataName] = "TurbochargerPack";
$BackpackHudData[21, bitmapName] = "gui/hud_elfgun";
$BackpackHudData[22, itemDataName] = "VampirePack";
$BackpackHudData[22, bitmapName] = "gui/hud_armbaricon";
$BackpackHudData[23, itemDataName] = "JammerBeaconPack";
$BackpackHudData[23, bitmapName] = "gui/hud_new_packradar";
$BackpackHudData[24, itemDataName] = "BlastWallPack";
$BackpackHudData[24, bitmapName] = "gui/hud_medpack";
$BackpackHudData[25, itemDataName] = "CoreNodePack";
$BackpackHudData[25, bitmapName] = "gui/hud_grenlaunch";
$BackpackHudData[26, itemDataName] = "NodeSpawnFavsPack";
$BackpackHudData[26, bitmapName] = "gui/hud_new_packinventory";
$BackpackHudData[27, itemDataName] = "NodeRegeneratorPack";
$BackpackHudData[27, bitmapName] = "gui/hud_new_packinventory";
$BackpackHudData[28, itemDataName] = "NodeCloakPack";
$BackpackHudData[28, bitmapName] = "gui/hud_new_packcloak";
$BackpackHudData[29, itemDataName] = "NodePortalPack";
$BackpackHudData[29, bitmapName] = "gui/hud_ergbaricon";

$BackpackHudCount = 30;

//-----------------------------------------------------------------------------
// ShapeBase - Inventory Management

function ShapeBase::clearInventory(%this)
{
    for(%i = 0; %i < $WeaponCount; %i++)
    {
        %this.setInventory($WeaponsHudData[%i, itemDataName], 0);
        %this.setInventory($WeaponsHudData[%i, ammoDataName], 0);
    }
    
    for(%j = 0; %j < $InventoryHudCount; %j++)
        %this.setInventory($InventoryHudData[%j, itemDataName], 0);

    // take away any pack the player has
    %curPack = %this.getMountedImage($BackpackSlot);

    if(%curPack > 0)
        %this.setInventory(%curPack.item, 0);
}

// Weapons system transplant - must be loaded before any image
// Trigger Proxy Image
datablock ShapeBaseImageData(TriggerProxyImage)
{
   className = WeaponImage;

   shapeFile = "turret_muzzlepoint.dts";
   item = TargetingLaser;
   offset = "0 0 0";
   triggerProxySlot = $WeaponSubImage1;

   usesEnergy = true;
   minEnergy = 0;

   stateName[0]                     = "Activate";
   stateTimeoutValue[0]             = 0.5;
   stateTransitionOnTimeout[0]      = "ActivateReady";

   stateName[1]                     = "ActivateReady";
   stateTransitionOnAmmo[1]         = "Ready";
   stateTransitionOnNoAmmo[1]       = "NoAmmo";

   stateName[2]                     = "Ready";
   stateTransitionOnNoAmmo[2]       = "NoAmmo";
   stateTransitionOnTriggerDown[2]  = "Trigger";

   stateName[3]                     = "Trigger";
   stateAllowImageChange[3]         = false;
   stateScript[3]                   = "onTriggerDown";
   stateTransitionOnTriggerUp[3]    = "Release";
   stateTransitionOnNoAmmo[3]       = "Release";

   stateName[4]                     = "NoAmmo";
   stateTransitionOnAmmo[4]         = "Ready";

   stateName[5]                     = "Release";
   stateScript[5]                   = "onTriggerUp";
   stateTransitionOnTimeout[5]      = "Ready";
};

function ShapeBaseImageData::onTriggerDown(%data, %obj, %slot)
{
    %obj.setImageTrigger(%data.triggerProxySlot, true);
}

function ShapeBaseImageData::onTriggerUp(%data, %obj, %slot)
{
    %obj.setImageTrigger(%data.triggerProxySlot, false);
}

//-----------------------------------------------------------------------------
// Misc Inventory Overrides

function ShapeBase::maxInventory(%this, %data)
{
   return %this.getDatablock().max[%data.getName()];
}

function ShapeBase::maxBaseInventory(%this, %data)
{
   return %this.getDatablock().max[%data.getName()];
}
