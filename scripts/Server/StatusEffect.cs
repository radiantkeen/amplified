// Status effects
function StatusEffect::__construct(%this)
{
    StatusEffect.Version = 1.0;
    StatusEffect.effectCount = 0;
}

function StatusEffect::__destruct(%this)
{
   // Thou shalt not spam
}

if(!isObject(StatusEffect))
   System.addClass(StatusEffect);

// create a list so we can go through all effects if need be
function StatusEffect::registerEffect(%this, %effect)
{
    if(StatusEffect.registeredEffect[%effect] == true)
        return;
        
    %class = new ScriptObject(%effect)
    {
        class = %effect;
        superClass = StatusEffect;
    };
    
    %class.setup();

    StatusEffect.effect[StatusEffect.effectCount] = %class;
    StatusEffect.registeredEffect[%effect] = true;
    StatusEffect.effectCount++;
}

function StatusEffect::setup(%this)
{
    %this.duration = 10000;
    %this.stackable = false;
    %this.stackableDuration = 3000;
    %this.ticking = false;
    %this.tickTimeMultiplier = 1.0;
    %this.effectInvulnPeriod = 5000;
}

function StatusEffect::applyEffect(%this, %effect, %obj, %instigator)
{
    %time = getSimTime();
    
    if((%obj.getType() & ($TypeMasks::PlayerObjectType | $TypeMasks::VehicleObjectType | $TypeMasks::StaticShapeObjectType)) == 0)
        return;
    
    // T2 likes turning objects into their IDs occasionally, let's put a stop to that
    %effect = %effect.getName();
    
    if(%obj.effectBlockTime[%effect] > %time)
        return;
    else if(%obj.effectDurationEnd[%effect] < %time)
    {
        if(!%effect.validateEffect(%obj))
            return;
            
        %obj.forceEndedEffect[%effect] = false;
        
        if(%effect.stackable && %obj.effectStacks[%this] > 0)
        {
            %obj.effectDurationEnd[%effect] += %effect.stackableDuration;
            %effect.onStackEffect(%obj, %instigator);
        }
        else
        {
            %obj.effectDurationEnd[%effect] = %time + %effect.duration;
            %effect.startEffect(%obj, %instigator);
        }
    }
}

// statically defined test function
function doStatusEffect(%cl, %effect)
{
    // T2 likes turning objects into their IDs occasionally, let's put a stop to that
    %effect = %effect.getName();
    
    StatusEffect.applyEffect(%effect, %cl.player, %cl.player);
}

// default: return true, allow validation to process blocking vs vehicles, etc
function StatusEffect::validateEffect(%this, %obj, %instigator)
{
    return true;
}

function StatusEffect::startEffect(%this, %obj, %instigator)
{
    %self = %this.getName();
    
    if(%this.stackable)
        %obj.effectStacks[%self] = 1;

    // Reset tick count, used for timing effects and such
    %obj.tickCount[%self] = 0;

    if(%this.ticking)
        %this.schedule($g_tickTime * %this.tickTimeMultiplier, "tickEffect", %obj, %instigator);
    else
        %this.schedule(%this.duration, "endEffect", %obj);
}

function StatusEffect::tickEffect(%this, %obj, %instigator)
{
    if(!isObject(%obj))
        return false;

    if(%obj.isFragmented)
    {
        %this.forceEndEffect(%obj);
        return false;
    }

    // use Self name object because it resolves to ID and won't work in an array
    %self = %this.getName();
    %time = getSimTime();

    if(%obj.effectDurationEnd[%self] > %time)
        %this.schedule($g_tickTime * %this.tickTimeMultiplier, "tickEffect", %obj, %instigator);
    else
        %this.endEffect(%obj);

    %obj.tickCount[%self]++;
    
    return true;
}

function StatusEffect::onStackEffect(%this, %obj, %instigator)
{
    %obj.effectStacks[%this.getName()]++;
}

function StatusEffect::endEffect(%this, %obj)
{
    if(!isObject(%obj))
        return false;
        
    %time = getSimTime();
    %self = %this.getName();
    
    // Added stacking time to effect, reschedule
    if(%obj.effectDurationEnd[%self] > %time && !%obj.forceEndedEffect[%self])
    {
        %this.schedule(%obj.effectDurationEnd[%self] - %time, "endEffect", %obj);
        return false;
    }
    
    if(%this.stackable)
        %obj.effectStacks[%self] = 0;

    if(%this.effectInvulnPeriod)
        %this.setEffectImmune(%obj, %this.effectInvulnPeriod);
        
    return true;
}

function StatusEffect::forceEndEffect(%this, %obj)
{
    %time = getSimTime();
    %effect = %this.getName();

    if(%obj.effectDurationEnd[%effect] > %time)
    {
        %obj.effectDurationEnd[%effect] = %time;
        %obj.forceEndedEffect[%effect] = true;
    }
}

function StatusEffect::stopEffect(%this, %effect, %obj)
{
    %time = getSimTime();
    %effect = %effect.getName();
    
    if(%obj.effectDurationEnd[%effect] > %time)
    {
        %obj.effectDurationEnd[%effect] = %time;
        %obj.forceEndedEffect[%effect] = true;
    }
}

function StatusEffect::stopAllEffects(%this, %obj)
{
    for(%i = 0; %i < StatusEffect.effectCount; %i++)
        StatusEffect.effect[%i].forceEndEffect(%obj);
}

function StatusEffect::setEffectImmune(%this, %effect, %obj, %time)
{
    %effect = %effect.getName();
    
    %obj.effectBlockTime[%effect] = getSimTime() + %time;
}

function StatusEffect::setAllEffectsImmune(%this, %obj, %time)
{
    for(%i = 0; %i < StatusEffect.effectCount; %i++)
        %obj.effectBlockTime[StatusEffect.effect[%i].getName()] = getSimTime() + %time;
}

function RadiusEffect(%pos, %radius, %instigator, %effect, %chance, %hitMask, %ignoreMask)
{
   %effect = %effect.getName();
    
   InitContainerRadiusSearch(%pos, %radius, %hitMask);
   %numTargets = 0;

   while ((%targetObject = containerSearchNext()) != 0)
   {
      %dist = containerSearchCurrRadDamageDist();

      if (%dist > %radius)
         continue;

      if(%targetObject.isMounted())
      {
         %mount = %targetObject.getObjectMount();
         %found = -1;
         for (%i = 0; %i < %mount.getDataBlock().numMountPoints; %i++)
         {
            if (%mount.getMountNodeObject(%i) == %targetObject)
            {
               %found = %i;
               break;
            }
         }

         if (%found != -1)
         {
            if (%mount.getDataBlock().isProtectedMountPoint[%found])
            {
               continue;
            }
         }
      }

      %targets[%numTargets]     = %targetObject;
      %targetDists[%numTargets] = %dist;
      %numTargets++;
   }

   for (%i = 0; %i < %numTargets; %i++)
   {
      %targetObject = %targets[%i];
      %dist = %targetDists[%i];

      %coverage = calcExplosionCoverage(%pos, %targetObject, %ignoreMask);
      
      if(%coverage == 0)
         continue;
         
      if(%chance >= getRandom())
          StatusEffect.applyEffect(%effect, %targetObject, %instigator);
   }
}

execDir("StatusEffects");
