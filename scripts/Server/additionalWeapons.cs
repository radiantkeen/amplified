// Execute additional weapons added here

// Weapons
exec("scripts/weapons/SpikeRifle.cs");
exec("scripts/weapons/Minigun.cs");
exec("scripts/weapons/GaussRifle.cs");
exec("scripts/weapons/Scattergun.cs");
exec("scripts/weapons/SentryRifle.cs");
exec("scripts/weapons/Flamethrower.cs");
exec("scripts/weapons/Twinfusor.cs");
exec("scripts/weapons/AutoCannon.cs");
exec("scripts/weapons/plasmacannon.cs");
exec("scripts/weapons/artillery.cs");
exec("scripts/weapons/MultiPulsar.cs");
exec("scripts/weapons/LaserCannon.cs");

// Grenades
exec("scripts/weapons/ThrustGrenade.cs");
exec("scripts/weapons/BatteryGrenade.cs");
exec("scripts/weapons/flakgrenade.cs");
exec("scripts/weapons/mortargrenade.cs");
exec("scripts/weapons/EMPGrenade.cs");
exec("scripts/weapons/FireGrenade.cs");
exec("scripts/weapons/PoisonGrenade.cs");
exec("scripts/weapons/SmokeGrenade.cs");
exec("scripts/weapons/HEATGrenade.cs");

// Mines
exec("scripts/weapons/RepairPatchMine.cs");
exec("scripts/weapons/AntiTankMine.cs");
exec("scripts/weapons/Holopuck.cs");
exec("scripts/weapons/CaltropMine.cs");


// Packs
exec("scripts/packs/GravitronPack.cs");
exec("scripts/packs/TurbochargerPack.cs");
exec("scripts/packs/VampirePack.cs");
