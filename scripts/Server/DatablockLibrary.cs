// Global Visual Effects library
//------------------------------------------------------------------------------
// Code

$g_FXOrigin = "0 0 -1000 1 0 0 0";

datablock StaticShapeData(FXPoint)
{
   shapeFile = "turret_muzzlepoint.dts";
};

function getFXPoint()
{
    if(isObject(System.g_FXPoint))
        return System.g_FXPoint;

    %fxPoint = new StaticShape()
    {
        dataBlock        = FXPoint;
    };

    MissionCleanup.add(%fxPoint);
    System.g_FXPoint = %fxPoint;

    %fxPoint.startFade(1, 0, true);
    %fxPoint.setTransform($g_FXOrigin);

    return System.g_FXPoint;
}

function getFXPhantom()
{
    if(isObject(System.g_FXPhantom))
        return System.g_FXPhantom;

    %fxPhantom = new Player()
    {
        dataBlock = "LightMaleHumanArmor";
    };

    MissionCleanup.add(%fxPhantom);

    %fxPhantom.setInvincible(true);
    %fxPhantom.startFade(1, 0, true);
    %fxPhantom.setTransform($g_FXOrigin);
    %fxPhantom.isZapper = true;
    System.g_FXPhantom = %fxPhantom;

    return System.g_FXPhantom;
}

datablock ShockLanceProjectileData(FXZap)
{
   directDamage        = 0;
   radiusDamageType    = $DamageType::Default;
   kickBackStrength    = 0;
   velInheritFactor    = 0;
   sound               = "";

   zapDuration = 1.0;
   impulse = 0;
   boltLength = 14.0;
   extension = 14.0;            // script variable indicating distance you can shock people from
   lightningFreq = 50.0;
   lightningDensity = 18.0;
   lightningAmp = 0.25;
   lightningWidth = 0.05;


   boltSpeed[0] = 2.0;
   boltSpeed[1] = -0.5;

   texWrap[0] = 1.5;
   texWrap[1] = 1.5;

   startWidth[0] = 0.3;
   endWidth[0] = 0.6;
   startWidth[1] = 0.3;
   endWidth[1] = 0.6;

   texture[0] = "special/shockLightning01";
   texture[1] = "special/shockLightning02";
   texture[2] = "special/shockLightning03";
   texture[3] = "special/ELFBeam";
};

datablock ShockLanceProjectileData(FXPulse)
{
   directDamage        = 0;
   radiusDamageType    = $DamageType::Default;
   kickBackStrength    = 0;
   velInheritFactor    = 0;
   sound               = "";

   zapDuration = 1.0;
   impulse = 0;
   boltLength = 14.0;
   extension = 14.0;            // script variable indicating distance you can shock people from
   lightningFreq = 50.0;
   lightningDensity = 18.0;
   lightningAmp = 0.25;
   lightningWidth = 0.05;


   boltSpeed[0] = 2.0;
   boltSpeed[1] = -0.5;

   texWrap[0] = 1.5;
   texWrap[1] = 1.5;

   startWidth[0] = 0.3;
   endWidth[0] = 0.6;
   startWidth[1] = 0.3;
   endWidth[1] = 0.6;

   texture[0] = "special/nonlingradient";
   texture[1] = "special/nonlingradient";
   texture[2] = "special/nonlingradient";
   texture[3] = "special/nonlingradient";
};

datablock ShockLanceProjectileData(FXHealGreen)
{
   directDamage        = 0;
   radiusDamageType    = $DamageType::Default;
   kickBackStrength    = 0;
   velInheritFactor    = 0;
   sound               = "";

   zapDuration = 1.5;
   impulse = 0;
   boltLength = 14.0;
   extension = 14.0;            // script variable indicating distance you can shock people from
   lightningFreq = 50.0;
   lightningDensity = 18.0;
   lightningAmp = 0.25;
   lightningWidth = 0.05;

   boltSpeed[0] = 6.0;
   boltSpeed[1] = -3.0;

   texWrap[0] = 1.5;
   texWrap[1] = 1.5;

   startWidth[0] = 0.3;
   endWidth[0] = 0.6;
   startWidth[1] = 0.3;
   endWidth[1] = 0.6;

   texture[0] = "liquidTiles/LushWater01_Algae";
   texture[1] = "liquidTiles/LushWater01";
   texture[2] = "liquidTiles/LushWater01_Algae";
   texture[3] = "liquidTiles/LushWater01";
};

datablock ShockLanceProjectileData(FXBlueShift)
{
   directDamage        = 0;
   radiusDamageType    = $DamageType::Default;
   kickBackStrength    = 0;
   velInheritFactor    = 0;
   sound               = "";

   zapDuration = 0.6;
   impulse = 0;
   boltLength = 14.0;
   extension = 14.0;            // script variable indicating distance you can shock people from
   lightningFreq = 50.0;
   lightningDensity = 18.0;
   lightningAmp = 0.25;
   lightningWidth = 0.05;


   boltSpeed[0] = 2.0;
   boltSpeed[1] = -0.5;

   texWrap[0] = 1.5;
   texWrap[1] = 1.5;

   startWidth[0] = 0.3;
   endWidth[0] = 0.6;
   startWidth[1] = 0.3;
   endWidth[1] = 0.6;

   texture[0] = "special/shieldenvmap";
   texture[1] = "special/shieldmap";
   texture[2] = "special/shieldenvmap";
   texture[3] = "special/shieldmap";
};

datablock ShockLanceProjectileData(FXRedShift)
{
   directDamage        = 0;
   radiusDamageType    = $DamageType::Default;
   kickBackStrength    = 0;
   velInheritFactor    = 0;
   sound               = "";

   zapDuration = 1.0;
   impulse = 0;
   boltLength = 14.0;
   extension = 14.0;            // script variable indicating distance you can shock people from
   lightningFreq = 50.0;
   lightningDensity = 18.0;
   lightningAmp = 0.25;
   lightningWidth = 0.05;


   boltSpeed[0] = 2.0;
   boltSpeed[1] = -0.5;

   texWrap[0] = 1.5;
   texWrap[1] = 1.5;

   startWidth[0] = 0.3;
   endWidth[0] = 0.6;
   startWidth[1] = 0.3;
   endWidth[1] = 0.6;

   texture[0] = "special/redbump2";
   texture[1] = "special/redbump2";
   texture[2] = "special/redbump2";
   texture[3] = "special/redbump2";
};

datablock ShockLanceProjectileData(FXWhiteNoise)
{
   directDamage        = 0;
   radiusDamageType    = $DamageType::Default;
   kickBackStrength    = 0;
   velInheritFactor    = 0;
   sound               = "";

   zapDuration = 1.0;
   impulse = 0;
   boltLength = 14.0;
   extension = 14.0;            // script variable indicating distance you can shock people from
   lightningFreq = 50.0;
   lightningDensity = 18.0;
   lightningAmp = 0.25;
   lightningWidth = 0.05;


   boltSpeed[0] = 2.0;
   boltSpeed[1] = -0.5;

   texWrap[0] = 1.5;
   texWrap[1] = 1.5;

   startWidth[0] = 0.3;
   endWidth[0] = 0.6;
   startWidth[1] = 0.3;
   endWidth[1] = 0.6;

   texture[0] = "special/cloaktexture";
   texture[1] = "special/cloaktexture";
   texture[2] = "special/cloaktexture";
   texture[3] = "special/cloaktexture";
};

function zapVehicle(%obj, %lanceData)
{
    %pilot = %obj.getMountNodeObject(0);
    %zapper = !isObject(%pilot) ? getFXPhantom() : %pilot;
    %pos = %pilot ? %pilot.getPosition() : %obj.getPosition();

    if(!%pilot)
    {
        %zapper.setPosition(vectorAdd(%pos, "0 0 -1"));
        %zapper.setVelocity("0 0 0");
    }

    %zap = new ShockLanceProjectile()
    {
        dataBlock        = %lanceData;
        initialDirection = VectorRand();
        initialPosition  = vectorAdd(%pos, "0 0 0.6");
        sourceObject     = %zapper;
        sourceSlot       = 0;
        targetId         = %obj;
    };

    MissionCleanup.add(%zap);

    if(!%pilot)
        %zapper.setTransform($g_FXOrigin);
}

// Uncomment this line if zapObject is being fiaky
function zapObject(%obj, %lanceData)
{
//    return zapEffect(%obj, %lanceData);

    %zap = new ShockLanceProjectile()
    {
        dataBlock        = %lanceData;
        initialDirection = "0 0 1";
        initialPosition  = %obj.position;
        sourceObject     = %obj;
        sourceSlot       = 1;
        targetId         = %obj;
    };

    MissionCleanup.add(%zap);
}

function zapEffect(%obj, %lanceData)
{
    if(%obj.isVehicle())
        return zapVehicle(%obj, %lanceData);

    %pos = vectorAdd(%obj.position, "0 0 -0.25");

    %point = getFXPoint();
    %point.setPosition(%pos);

    %zap = new ShockLanceProjectile()
    {
        dataBlock        = %lanceData;
        initialDirection = "0 0 1"; // VectorRand()
        initialPosition  = %pos;
        sourceObject     = %point;
        sourceSlot       = 0;
        targetId         = %obj;
    };

    MissionCleanup.add(%zap);

    %point.setTransform($g_FXOrigin);
}

datablock ShapeBaseImageData(MainAPEImage)
{
   className = WeaponImage;
   shapeFile = "turret_muzzlepoint.dts";
   offset = "0 0 0";
   rotation = "0 0 1 0";
   usesEnergy = true;
   minEnergy = -1; // fool the energy system into firing constant.

   stateName[0] = "Activate";
   stateTransitionOnTimeout[0] = "Idle";
   stateTimeoutValue[0] = 0.01;

   stateName[2] = "Idle";
   stateTransitionOnTriggerDown[2] = "Emit";

   stateName[3] = "Emit";
   stateTransitionOnTimeout[3] = "Idle";
   stateTimeoutValue[3] = 0.032; // min 32ms recharge time
   stateFire[3] = true;

   stateEmitter[3]       = ""; // emitter - ChaingunFireEmitter
   stateEmitterTime[3]       = 0.1; //100ms
   stateEmitterNode[3]       = 0; // model node (most models dont come with any nodes)
//   stateScript[3]           = "onFire";
};

datablock ShapeBaseImageData(EngineAPEImage)
{
   className = WeaponImage;
   shapeFile = "turret_muzzlepoint.dts";
   offset = "0 0 0";
   rotation = "0 0 1 0";
   usesEnergy = true;
   minEnergy = -1; // fool the energy system into firing constant.

   stateName[0] = "Activate";
   stateTransitionOnTimeout[0] = "Idle";
   stateTimeoutValue[0] = 0.01;

   stateName[1] = "Idle";
   stateTransitionOnTriggerDown[1] = "JetOn";

   stateName[2] = "JetOn";
   stateTransitionOnTriggerDown[2] = "JetMaintain";
   stateSequence[2] = "ActivateBack";
   stateDirection[2] = true;
   stateTimeoutValue[2] = 0.5;

   stateName[3] = "JetMaintain";
   stateTransitionOnTriggerUp[3] = "JetOff";
   stateTransitionOnTimeout[3]   = "JetMaintain";
   stateTimeoutValue[3] = 0.01;
   stateSequence[3] = "MaintainBack";
//   stateFire[3] = true;

   stateName[4] = "JetOff";
   stateTransitionOnTriggerDown[4] = "JetOn";
   stateSequence[4] = "ActivateBack";
   stateDirection[4] = false;
   stateTimeoutValue[4] = 0.5;
   stateWaitForTimeout[4]          = false;
   stateTransitionOnTimeout[4]     = "Idle";
};

datablock ShapeBaseImageData(TriggerAPEImage)
{
   className = WeaponImage;
   shapeFile = "turret_muzzlepoint.dts";
   offset = "0 0 0";
   rotation = "0 0 1 0";
//   item = Plasma; // hehe
   usesEnergy = true;
   minEnergy = -1; // fool the energy system into firing constant.

   stateName[0] = "Activate";
   stateTransitionOnTimeout[0] = "Idle";
   stateTimeoutValue[0] = 0.01;

   stateName[2] = "Idle";
   stateTransitionOnTriggerDown[2] = "Emit";

   stateName[3] = "Emit";
   stateTransitionOnTriggerUp[3] = "Idle";
   stateTransitionOnTimeout[3]   = "Emit";
   stateTimeoutValue[3] = 0.032; // min 32ms recharge time
   stateFire[3] = true;

//   stateEmitter[3]       = "ChaingunFireSmoke"; // emitter
//   stateEmitterTime[3]       = 0.1; //100ms
//   stateEmitterNode[3]       = 0; // model node (most models dont come with any nodes)
//   stateScript[3]           = "onFire";
//   stateEjectShell[3]       = true;
};

datablock ParticleData(TeleporterParticle)
{
   dragCoeffiecient     = 0.0;
   gravityCoefficient   = -0.5;
   inheritedVelFactor   = 0.0;

   lifetimeMS           = 100;
   lifetimeVarianceMS   = 50;

   textureName          = "particleTest";

   useInvAlpha = false;
   spinRandomMin = -160.0;
   spinRandomMax = 160.0;

   animateTexture = true;
   framesPerSec = 15;


   animTexName[0]       = "special/Explosion/exp_0016";
   animTexName[1]       = "special/Explosion/exp_0018";
   animTexName[2]       = "special/Explosion/exp_0020";
   animTexName[3]       = "special/Explosion/exp_0022";
   animTexName[4]       = "special/Explosion/exp_0024";
   animTexName[5]       = "special/Explosion/exp_0026";
   animTexName[6]       = "special/Explosion/exp_0028";
   animTexName[7]       = "special/Explosion/exp_0030";
   animTexName[8]       = "special/Explosion/exp_0032";

   colors[0]     = "1 1 1 1";
   colors[1]     = "0.75  0.75 1.0";
   colors[2]     = "0.5 0.5 0.5 0.0";
   sizes[0]      = 0.5;
   sizes[1]      = 1;
   sizes[2]      = 2.5;
   times[0]      = 0.0;
   times[1]      = 0.7;
   times[2]      = 1.0;
};

datablock ParticleEmitterData(TeleporterEmitter)
{
   ejectionPeriodMS = 3;
   periodVarianceMS = 0;
   ejectionVelocity = 6;
   velocityVariance = 2.9;
   ejectionOffset   = 0.0;
   thetaMin         = 0;
   thetaMax         = 5;
   phiReferenceVel  = 0;
   phiVariance      = 360;
   overrideAdvances = false;
   particles = "TeleporterParticle";
};

function createEmitter(%pos, %emitter, %rot)
{
    %dummy = new ParticleEmissionDummy()
    {
          position = %pos;
          rotation = %rot;
          scale = "1 1 1";
          dataBlock = defaultEmissionDummy;
          emitter = %emitter;
          velocity = "1";
    };
    MissionCleanup.add(%dummy);

    if(isObject(%dummy))
       return %dummy;
}

function createLifeEmitter(%pos, %emitter, %lifeMS, %rot)
{
    %dummy = createEmitter(%pos, %emitter, %rot);
    %dummy.schedule(%lifeMS, delete);
    return %dummy;
}

function projectileTrail(%obj, %time, %projType, %proj, %bSkipFirst, %offset)
{
   if(isObject(%obj) && isObject(%obj.sourceObject))
   {
      if(!%obj.lastBlinkPos)
         %obj.lastBlinkPos = "65536 65536 65536";

      if(%bSkipFirst)
      {
         schedule(%time, 0, "projectileTrail", %obj, %time, %projType, %proj, false, %offset);
         return;
      }

      if(%offset)
         %pos = vectorAdd(%obj.position, vectorScale(%obj.initialDirection, %offset));
      else
         %pos = %obj.position;

      if( %obj.exploded )	// +soph
         return;		// +soph

      if(vectorCompare(%pos, %obj.lastBlinkPos)) //same position...BUG!
         return;

      %p = new (%projType)()
      {
         dataBlock        = %proj;
         initialDirection = %obj.initialDirection;
         initialPosition  = %pos;
         sourceObject     = %obj.sourceObject;
         sourceSlot       = 0;
      };
      MissionCleanup.add(%p);
      %p.exploded = true;	// +soph

      schedule(%time, 0, projectileTrail, %obj, %time, %projType, %proj, false, %offset);
      %obj.lastBlinkPos = %pos;
   }
}

function spawnShockwave(%data, %pos, %vec) // originally found by Lt. Earthworm
{
   %wave = new Shockwave()
   {
      dataBlock = %data;
      pos = %pos;
      normal = %vec;
   };
   MissionCleanup.add(%wave);

   return %wave;
}

function spawnShockwaveTrail(%obj, %time, %data)
{
   if(isObject(%obj))
   {
      %pos = %obj.position;

      if(vectorCompare(%pos, %obj.lastShockBlinkPos)) //same position...BUG!
         return;

      %wave = spawnShockwave(%data, %pos, %obj.initialDirection);

      schedule(%time, 0, spawnShockwaveTrail, %obj, %time, %data);
      %obj.lastShockBlinkPos = %pos;
   }
}

function createFlash(%pos, %radius, %intensity, %disableFlashOverlap)
{
   InitContainerRadiusSearch(%pos, %radius, $TypeMasks::PlayerObjectType | $TypeMasks::TurretObjectType);

   while((%flashMe = containerSearchNext()) != 0)
   {
      %eyePointVec = getVectorFromPoints(%flashMe.getEyePoint(), %pos);
      %eyeVec = %flashMe.getEyeVector();
      %eyeDot = (%d = VectorDot(%eyeVec, %eyePointVec)) <= 0 ? 0 : %d;
      %eyeDistPct = getDistance3D(%flashMe.getEyePoint(), %pos) / %radius;
      %eyeFlashPct = (%intensity * %eyeDistPct) / 2 + (%intensity * %eyeDot) / 2;
      %whiteoutVal = %disableFlashOverlap == true ? %eyeFlashPct : %flashMe.getWhiteOut() + %eyeFlashPct;
      %flashMe.setWhiteOut(%whiteoutVal);
   }
}

function createRealisticFlash(%pos, %radius, %intensity, %disableFlashOverlap)
{
   InitContainerRadiusSearch(%pos, %radius, $TypeMasks::PlayerObjectType | $TypeMasks::TurretObjectType);

   while((%flashMe = containerSearchNext()) != 0)
   {
      %eyePointVec = getVectorFromPoints(%flashMe.getEyePoint(), %pos);
      %vec = VectorScale(%eyePointVec, %radius);
      %end = VectorAdd(%pos, %vec);
      %canFlash = containerRayCast(%pos, %end, $TypeMasks::NonTransparent);

      if(%canFlash)
      {
         %eyeVec = %flashMe.getEyeVector();
         %eyeDistPct = getDistance3D(%flashMe.getEyePoint(), %pos) / %radius;
         %eyeDot = VectorDot(%eyeVec, %eyePointVec);
         %eyeDot = %eyeDot > 0 ? %eyeDot : 0;
         %eyeFlashPct = %intensity * %eyeDot * %eyeDistPct;

         if($FuncDebugMode) // example of FuncDebugMode in action
         {
            echo("Flash Obj  :" SPC %flashMe);
            echo("Intensity  :" SPC %intensity);
            echo("Max Radius :" SPC %radius);
            echo("DistPct    :" SPC %eyeDistPct);
            echo("Dot        :" SPC %eyeDot);
            echo("FlashPct   :" SPC %eyeFlashPct);
         }

         %whiteoutVal = %disableFlashOverlap == true ? %eyeFlashPct : %flashMe.getWhiteOut() + %eyeFlashPct;
         %flashMe.setWhiteOut(%whiteoutVal);
      }
   }
}

// keen: WARNING! Lightning objects are not cleaned up properly (or at all)
// on the server side, creating too many lightning objects can run a server
// out of memory... though much more relevant when servers had less than
// 512mb of ram...
function createLightning(%pos, %rad, %strikesPerMin, %hitPercentage, %scale1000)
{
   %obj = new Lightning() // new Lightning(Lightning)? necessary?
   {
      position = %pos;
     	rotation = "1 0 0 0";
     	scale = %scale1000 ? %rad SPC %rad SPC (%rad+1000) : %rad SPC %rad SPC (%rad + 1000);
    	dataBlock = "DefaultStorm";
    	lockCount = "0";
    	homingCount = "0";
    	strikesPerMinute = %strikesPerMin;
    	strikeWidth = "2.5";
    	chanceToHitTarget = %hitPercentage;
    	strikeRadius = "20";
    	boltStartRadius = "20";
    	color = "1.000000 1.000000 1.000000 1.000000";
    	fadeColor = "0.100000 0.100000 1.000000 1.000000";
    	useFog = "0";
  	};

   return %obj;
}

function createMeteorShower(%pos, %rad, %dropsPerMin, %hitPercentage)
{
   %obj = new FireballAtmosphere()
   {
      position = %pos;
     	rotation = "1 0 0 0";
     	scale = "1 1 1";
     	dataBlock = "fireball";
     	lockCount = "0";
     	homingCount = "0";
     	dropRadius = %rad;
     	dropsPerMinute = %dropsPerMin;
     	minDropAngle = "0";
     	maxDropAngle = "30";
     	startVelocity = "300";
     	dropHeight = "1000";
     	dropDir = "0.212 0.212 -0.953998";
  	};

   return %obj;
}

$DFX_ID = 0;

function loopDamageFX2(%obj, %pos, %radius, %count, %rate, %blocktype, %block, %id) // enhanced; for displaying random explosions around a position
{
   if(!isObject(%obj))
      return;

   if(%blocktype $= "" || %block $= "")
      return;

   if(%rate > 31)
   {
      error("loopDamageFX::rate cannot exceed 31 iterations/second due to engine limitations");
      %rate = 31;
   }

   if(%rate < 1)
   {
      error("loopDamageFX::rate cannot be lower than 1 iteration/second");
      %rate = 1;
   }

   if(%id $= "")
   {
      $DFX_ID++;
      %id = $DFX_ID;
   }

   if(%obj.proj[%id, DFX_Count] <= %count && %obj)
   {
      %px = getWord(%pos, 0) + getRandomT(%radius);
      %py = getWord(%pos, 1) + getRandomT(%radius);
      %pz = getWord(%pos, 2) + getRandomT(%radius);
      %npos = %px SPC %py SPC %pz;

      %p = new (%blocktype)() {
         dataBlock        = %block;
         initialDirection = "0 0 0";
         initialPosition  = %npos;
         sourceObject     = %obj;
         sourceSlot       = 0;
      };
      MissionCleanup.add(%p);

      %obj.proj[%id, DFX_Count]++;
      schedule((1/%rate)*1000, 0, "loopDamageFX2", %obj, %pos, %radius, %count, %rate, %blocktype, %block, %id);
   }
   else
      %obj.DFX_Count = 0;
}

//------------------------------------------------------------------------------
// Various Effect Datablocks

datablock AudioProfile(DreadnoughtExplodeSound)
{
   filename    = "fx/misc/MA1.wav";
   description = AudioBIGExplosion3d;
   preload = true;
};

datablock AudioProfile(DreadnoughtWarningSound)
{
   filename    = "fx/misc/red_alert.wav";
   description = AudioBIGExplosion3d;
   preload = true;
};

datablock AudioProfile(TurbochargerExplodeSound)
{
   filename    = "fx/Bonuses/down_passback3_rocket.wav";
   description = AudioBIGExplosion3d;
   preload = true;
};

datablock AudioProfile(HeavyRocketFireSound)
{
   filename    = "fx/Bonuses/high-level4-blazing.wav";
   description = AudioDefault3d;
   preload = true;
};

datablock AudioProfile(PhaserExpSound)
{
   filename    = "fx/weapons/cg_water_4.wav";
   description = AudioDefault3d;
   preload = true;
};

datablock AudioProfile(PhaserCExpSound)
{
   filename    = "fx/weapons/grenade_explode_UW.wav";
   description = AudioDefault3d;
   preload = true;
};

datablock AudioProfile(PCRFireSound)
{
   filename    = "fx/weapons/plasma_rifle_projectile_hit.wav";
   description = AudioDefault3d;
   preload = true;
};

datablock ParticleData(OnFireParticle)
{
    dragCoefficient = 0;
    gravityCoefficient = 0.3;
    windCoefficient = 0;
    inheritedVelFactor = 0.862903;
    constantAcceleration = 0;
    lifetimeMS = 750;
    lifetimeVarianceMS = 749; //750; --- can't be > or = (ST)
    useInvAlpha = 0;
    spinRandomMin = -145.161;
    spinRandomMax = 133.065;
    textureName = "special/tracer00.PNG";
//    textureName = "flarebase.png";
    times[0] = 0;
    times[1] = 1;
    colors[0] = "0.850394 0.304000 0.192000 0.709677";
    colors[1] = "0.000000 0.000000 0.000000 0.451613";
    sizes[0] = 1.97581;
    sizes[1] = 1.35484;
};

datablock ParticleEmitterData(OnFireEmitter)
{
    ejectionPeriodMS = 7;
    periodVarianceMS = 0;
    ejectionVelocity = 5.79032;
    velocityVariance = 2.30645;
    ejectionOffset =   0;
    thetaMin = 0;
    thetaMax = 14.5161;
    phiReferenceVel = 0;
    phiVariance = 360;
    overrideAdvances = 0;
    orientParticles= 0;
    orientOnVelocity = 1;
    particles = "OnFireParticle";
};

datablock ParticleData(BlueJetParticle)
{
    dragCoefficient = 0;
    gravityCoefficient = -0.01;
    windCoefficient = 1;
    inheritedVelFactor = 0.624194;
    constantAcceleration = -1.6129;
    lifetimeMS = 325;
    lifetimeVarianceMS = 96;
    useInvAlpha = 0;
    spinRandomMin = -64.5161;
    spinRandomMax = 381.048;
    textureName = "particleTest.png";
    times[0] = 0;
    times[1] = 0.112903;
    times[2] = 1;
    colors[0] = "1.000000 1.000000 1.000000 0.209677";
    colors[1] = "0.000000 0.304000 1.000000 0.314516";
    colors[2] = "0.464567 0.608000 1.000000 0.000000";
    sizes[0] = 6;
    sizes[1] = 6;
    sizes[2] = 2;
};

datablock ParticleEmitterData(BlueJetEmitter)
{
    ejectionPeriodMS = 3;
    periodVarianceMS = 2;
    ejectionVelocity = 9.78226;
    velocityVariance = 3.17742;
    ejectionOffset =   1;
    thetaMin = 0;
    thetaMax = 0;
    phiReferenceVel = 0;
    phiVariance = 360;
    overrideAdvances = 0;
    orientParticles= 0;
    orientOnVelocity = 1;
   particles = "BlueJetParticle";
};

datablock ParticleData(RedJetParticle)
{
    dragCoefficient = 2;
    gravityCoefficient = 0.2;
    windCoefficient = 0;
    inheritedVelFactor = 0.9;
    constantAcceleration = -1.1129;
    lifetimeMS = 250;
    lifetimeVarianceMS = 125;
    useInvAlpha = 0;
    spinRandomMin = -64.5161;
    spinRandomMax = 381.048;
    textureName = "special/cloudflash2.png";
    times[0] = 0;
    times[1] = 1;
    colors[0] = "1.000000 0.104000 0.000000 0.758065";
    colors[1] = "1.000000 0.648000 0.384000 0.000000";
    sizes[0] = 6;
    sizes[1] = 2;
};

datablock ParticleEmitterData(RedJetEmitter)
{
    ejectionPeriodMS = 7;
    periodVarianceMS = 2;
    ejectionVelocity = 12;
    velocityVariance = 3.17742;
    ejectionOffset =   2;
    thetaMin = 0;
    thetaMax = 2.90323;
    phiReferenceVel = 0;
    phiVariance = 360;
    overrideAdvances = 0;
    orientParticles= 0;
    orientOnVelocity = 1;
   particles = "RedJetParticle";
};

datablock ParticleData(UnderwaterPhaserExplosionSmoke)
{
   dragCoeffiecient     = 80.0;
   gravityCoefficient   = -0.3;   // rises slowly
   inheritedVelFactor   = 0.025;

   constantAcceleration = -0.75;

   lifetimeMS           = 1250;
   lifetimeVarianceMS   = 0;

   textureName          = "particleTest";

   useInvAlpha =  false;
   spinRandomMin = -200.0;
   spinRandomMax =  200.0;

   textureName = "special/Smoke/smoke_001";

   colors[0]     = "0.2 0.9 0.1 1.0";
   colors[1]     = "0.2 1.0 0.1 0.5";
   colors[2]     = "0.0 0.0 0.0 0.0";
   sizes[0]      = 1.0;
   sizes[1]      = 3.0;
   sizes[2]      = 5.0;
   times[0]      = 0.0;
   times[1]      = 0.2;
   times[2]      = 1.0;
};

datablock ParticleData( PhaserDebrisSmokeParticle )
{
   dragCoeffiecient     = 0.4;
   gravityCoefficient   = -0.01;   // rises slowly
   inheritedVelFactor   = 0.125;

   lifetimeMS           =  800;
   lifetimeVarianceMS   =  400;
   useInvAlpha          =  false;
   spinRandomMin        = -100.0;
   spinRandomMax        =  100.0;

   animateTexture = false;

   textureName = "flarebase"; //"special/Smoke/bigSmoke";

   colors[0]     = "0.9 0.9 0.9 0.5";
   colors[1]     = "0.8 0.0 0.6 0.8";
   colors[2]     = "0.4 0.0 0.3 0.345";
   colors[3]     = "0.0 0.0 0.0 0.0";
   sizes[0]      = 0.25;
   sizes[1]      = 0.35;
   sizes[2]      = 0.425;
   sizes[3]      = 0.65;
   times[0]      = 0.0;
   times[1]      = 0.45;
   times[2]      = 0.9;
   times[3]      = 1.0;
};

datablock ParticleEmitterData( PhaserDebrisSmokeEmitter )
{
   ejectionPeriodMS = 7;
   periodVarianceMS = 1;

   ejectionVelocity = 1.0;  // A little oomph at the back end
   velocityVariance = 0.2;

   thetaMin         = 0.0;
   thetaMax         = 40.0;

   particles = "PhaserDebrisSmokeParticle";
};

datablock DebrisData(PhaserDebris)
{
   emitters[0] = PhaserDebrisSmokeEmitter;

   explodeOnMaxBounce = true;

   elasticity = 0.4;
   friction = 0.2;

   lifetime = 2.5;
   lifetimeVariance = 0.8;

   numBounces = 1;
};

datablock ParticleData(PhaserExplosionParticle)
{
    dragCoefficient = 0;
    gravityCoefficient = -0;
    windCoefficient = 1;
    inheritedVelFactor = 0.025;
    constantAcceleration = -0.8;
    lifetimeMS = 1500;
    lifetimeVarianceMS = 500;
    useInvAlpha = 0;
    spinRandomMin = -200;
    spinRandomMax = 200;
    textureName = "special/Smoke/smoke_001";
    times[0] = 0;
    times[1] = 0.5;
    times[2] = 1;
    colors[0] = "1.000000 1.000000 1.000000 1.000000";
    colors[1] = "0.8 0.0 0.6 1.000000";
    colors[2] = "0.000000 0.000000 0.000000 0.000000";
    sizes[0] = 2.29839;
    sizes[1] = 3.87903;
    sizes[2] = 7.5;
};

datablock ParticleEmitterData(PhaserExplosionEmitter)
{
    ejectionPeriodMS = 8;
    periodVarianceMS = 2;
    ejectionVelocity = 8.0;
    velocityVariance = 3.0;
    ejectionOffset =   0.25;
    thetaMin = 0;
    thetaMax = 180;
    phiReferenceVel = 0;
    phiVariance = 360;
    overrideAdvances = 0;
    orientParticles= 0;
    orientOnVelocity = 1;
    lifeTimeMs = 500;
    particles = "PhaserExplosionParticle";
};

datablock ParticleData(PhaserSparksParticle)
{
    dragCoefficient = 1;
    gravityCoefficient = 0;
    windCoefficient = 1;
    inheritedVelFactor = 0.2;
    constantAcceleration = 0;
    lifetimeMS = 750;
    lifetimeVarianceMS = 250;
    useInvAlpha = 0;
    spinRandomMin = 0;
    spinRandomMax = 0;
    textureName = "special/bigspark";
    times[0] = 0;
    times[1] = 0.540323;
    times[2] = 1;
    colors[0] = "0.8 0.0 0.6 1.000000";
    colors[1] = "0.8 0.0 0.6 0.524231";
    colors[2] = "0.8 0.0 0.6 0.000000";
    sizes[0] = 0.733871;
    sizes[1] = 0.733871;
    sizes[2] = 0.75;
};

datablock ParticleEmitterData(PhaserSparksEmitter)
{
    ejectionPeriodMS = 6;
    periodVarianceMS = 1;
    ejectionVelocity = 18;
    velocityVariance = 5;
    ejectionOffset =   0;
    thetaMin = 0;
    thetaMax = 180;
    phiReferenceVel = 0;
    phiVariance = 360;
    overrideAdvances = 0;
    orientParticles= 1;
    orientOnVelocity = 1;
    lifeTimeMs = 250;
    particles = "PhaserSparksParticle";
};

datablock ExplosionData(PhaserExplosion)
{
   soundProfile   = PhaserExpSound;

   emitter[0] = PhaserExplosionEmitter;
   emitter[1] = PhaserSparksEmitter;

   debris = PhaserDebris;
   debrisThetaMin = 25;
   debrisThetaMax = 80;
   debrisNum = 6;
   debrisVelocity = 14.0;
   debrisVelocityVariance = 3.0;

   shakeCamera = false;
   camShakeFreq = "12.0 13.0 11.0";
   camShakeAmp = "35.0 35.0 35.0";
   camShakeDuration = 0.5;
   camShakeRadius = 12.0;
};

datablock ParticleData(PCRSparks)
{
   dragCoefficient      = 1;
   gravityCoefficient   = 0.0;
   inheritedVelFactor   = 0.2;
   constantAcceleration = 0;
   lifetimeMS           = 1000;
   lifetimeVarianceMS   = 500;
   textureName          = "special/bigspark";
   colors[0]     = "0.0 0.4 0.4 1.0";
   colors[1]     = "0.1 0.3 0.5 0.5";
   colors[2]     = "0.05 0.2 0.15 0.0";
   sizes[0]      = 1;
   sizes[1]      = 2;
   times[0]      = 0.0;
   times[1]      = 1.0;
};

datablock ParticleEmitterData(PCRSparksEmitter)
{
   ejectionPeriodMS = 1;
   periodVarianceMS = 0;
   ejectionVelocity = 13;
   velocityVariance = 5;
   ejectionOffset   = 0.0;
   thetaMin         = 0;
   thetaMax         = 180;
   phiReferenceVel  = 0;
   phiVariance      = 360;
   overrideAdvances = false;
   orientParticles  = true;
   lifetimeMS       = 200;
   particles = "PCRSparks";
};

datablock ParticleData(UnderwaterPhaserCExplosionSmoke)
{
   dragCoeffiecient     = 80.0;
   gravityCoefficient   = -0.3;   // rises slowly
   inheritedVelFactor   = 0.025;

   constantAcceleration = -0.75;

   lifetimeMS           = 1250;
   lifetimeVarianceMS   = 0;

   textureName          = "particleTest";

   useInvAlpha =  false;
   spinRandomMin = -200.0;
   spinRandomMax =  200.0;

   textureName = "special/Smoke/smoke_001";

   colors[0]     = "0.2 0.9 1.0 1.0";
   colors[1]     = "0.4 1.0 0.9 0.5";
   colors[2]     = "0.0 0.0 0.0 0.0";
   sizes[0]      = 1.0;
   sizes[1]      = 3.0;
   sizes[2]      = 5.0;
   times[0]      = 0.0;
   times[1]      = 0.2;
   times[2]      = 1.0;
};

datablock ParticleEmitterData(UnderwaterPhaserCExplosionSmokeEmitter)
{
   ejectionPeriodMS = 10;
   periodVarianceMS = 0;

   ejectionVelocity = 5.25;
   velocityVariance = 0.25;

   thetaMin         = 0.0;
   thetaMax         = 180.0;

   lifetimeMS       = 250;

   particles = "UnderwaterPhaserCExplosionSmoke";
};

datablock ParticleData(UnderwaterPhaserCSparks)
{
   dragCoefficient      = 1;
   gravityCoefficient   = 0.0;
   inheritedVelFactor   = 0.2;
   constantAcceleration = 0.0;
   lifetimeMS           = 500;
   lifetimeVarianceMS   = 350;
   textureName          = "special/droplet";
   colors[0]     = "0.2 0.9 1.0 1.0";
   colors[1]     = "0.2 0.9 1.0 1.0";
   colors[2]     = "0.2 0.9 1.0 0.0";
   sizes[0]      = 0.5;
   sizes[1]      = 0.25;
   sizes[2]      = 0.25;
   times[0]      = 0.0;
   times[1]      = 0.5;
   times[2]      = 1.0;
};

datablock ParticleEmitterData(UnderwaterPhaserCSparkEmitter)
{
   ejectionPeriodMS = 3;
   periodVarianceMS = 0;
   ejectionVelocity = 20;
   velocityVariance = 10;
   ejectionOffset   = 0.0;
   thetaMin         = 0;
   thetaMax         = 180;
   phiReferenceVel  = 0;
   phiVariance      = 360;
   overrideAdvances = false;
   orientParticles  = true;
   lifetimeMS       = 100;
   particles = "UnderwaterPhaserCSparks";
};

datablock ExplosionData(UnderwaterPhaserCSubExplosion1)
{
   offset = 1.0;
   emitter[0] = UnderwaterPhaserCExplosionSmokeEmitter;
   emitter[1] = UnderwaterPhaserCSparkEmitter;
};

datablock ExplosionData(UnderwaterPhaserCSubExplosion2)
{
   offset = 1.0;
   emitter[0] = UnderwaterPhaserCExplosionSmokeEmitter;
   emitter[1] = UnderwaterPhaserCSparkEmitter;
};

datablock ExplosionData(UnderwaterPhaserCExplosion)
{
//   soundProfile   = GrenadeExplosionSound;

   soundProfile = PhaserCExpSound;
//   emitter[0] = PCRSparksEmitter;

   emitter[0] = UnderwaterPhaserCExplosionSmokeEmitter;
   emitter[1] = UnderwaterPhaserCSparkEmitter;

   subExplosion[0] = UnderwaterPhaserCSubExplosion1;
   subExplosion[1] = UnderwaterPhaserCSubExplosion2;

   shakeCamera = true;
   camShakeFreq = "12.0 13.0 11.0";
   camShakeAmp = "35.0 35.0 35.0";
   camShakeDuration = 0.4;
   camShakeRadius = 10.0;
};

datablock ParticleData( PhaserCDebrisSmokeParticle )
{
   dragCoeffiecient     = 0.4;
   gravityCoefficient   = -0.01;   // rises slowly
   inheritedVelFactor   = 0.125;

   lifetimeMS           =  800;
   lifetimeVarianceMS   =  400;
   useInvAlpha          =  false;
   spinRandomMin        = -100.0;
   spinRandomMax        =  100.0;

   animateTexture = false;

   textureName = "flarebase"; //"special/Smoke/bigSmoke";

   colors[0]     = "0.9 0.9 0.9 0.5";
   colors[1]     = "0.2 0.5 0.1 0.8";
   colors[2]     = "0.1 0.3 0.05 0.345";
   colors[3]     = "0.0 0.0 0.0 0.0";
   sizes[0]      = 0.25;
   sizes[1]      = 0.35;
   sizes[2]      = 0.425;
   sizes[3]      = 0.65;
   times[0]      = 0.0;
   times[1]      = 0.45;
   times[2]      = 0.9;
   times[3]      = 1.0;
};

datablock ParticleEmitterData( PhaserCDebrisSmokeEmitter )
{
   ejectionPeriodMS = 7;
   periodVarianceMS = 1;

   ejectionVelocity = 1.0;  // A little oomph at the back end
   velocityVariance = 0.2;

   thetaMin         = 0.0;
   thetaMax         = 40.0;

   particles = "PhaserCDebrisSmokeParticle";
};

datablock DebrisData(PhaserCDebris)
{
   emitters[0] = PhaserCDebrisSmokeEmitter;

   explodeOnMaxBounce = true;

   elasticity = 0.4;
   friction = 0.2;

   lifetime = 2.5;
   lifetimeVariance = 0.8;

   numBounces = 1;
};

datablock ParticleData(PhaserCExplosionParticle)
{
    dragCoefficient = 0;
    gravityCoefficient = -0;
    windCoefficient = 1;
    inheritedVelFactor = 0.025;
    constantAcceleration = -0.8;
    lifetimeMS = 1500;
    lifetimeVarianceMS = 500;
    useInvAlpha = 0;
    spinRandomMin = -200;
    spinRandomMax = 200;
    textureName = "special/Smoke/smoke_003";
    times[0] = 0;
    times[1] = 0.5;
    times[2] = 1;
    colors[0] = "1.000000 1.000000 1.000000 1.000000";
    colors[1] = "0.192000 0.700787 0.088000 1.000000";
    colors[2] = "0.000000 0.000000 0.000000 0.000000";
    sizes[0] = 2.29839;
    sizes[1] = 3.87903;
    sizes[2] = 7.5;
};

datablock ParticleEmitterData(PhaserCExplosionEmitter)
{
    ejectionPeriodMS = 8;
    periodVarianceMS = 2;
    ejectionVelocity = 10.0;
    velocityVariance = 3.0;
    ejectionOffset =   0.25;
    thetaMin = 0;
    thetaMax = 180;
    phiReferenceVel = 0;
    phiVariance = 360;
    overrideAdvances = 0;
    orientParticles= 0;
    orientOnVelocity = 1;
    lifeTimeMs = 500;
    particles = "PhaserCExplosionParticle";
};

datablock ParticleData(PhaserCSparksParticle)
{
    dragCoefficient = 1;
    gravityCoefficient = 0;
    windCoefficient = 1;
    inheritedVelFactor = 0.2;
    constantAcceleration = 0;
    lifetimeMS = 750;
    lifetimeVarianceMS = 250;
    useInvAlpha = 0;
    spinRandomMin = 0;
    spinRandomMax = 0;
    textureName = "special/bigspark";
    times[0] = 0;
    times[1] = 0.540323;
    times[2] = 1;
    colors[0] = "0.253749 1.000000  0.253534 1.000000";
    colors[1] = "0.253749 1.000000  0.253534 0.524231";
    colors[2] = "0.253749 1.000000  0.253534 0.000000";
    sizes[0] = 0.733871;
    sizes[1] = 0.733871;
    sizes[2] = 0.75;
};

datablock ParticleEmitterData(PhaserCSparksEmitter)
{
    ejectionPeriodMS = 6;
    periodVarianceMS = 1;
    ejectionVelocity = 18;
    velocityVariance = 5;
    ejectionOffset =   0;
    thetaMin = 0;
    thetaMax = 180;
    phiReferenceVel = 0;
    phiVariance = 360;
    overrideAdvances = 0;
    orientParticles= 1;
    orientOnVelocity = 1;
    lifeTimeMs = 250;
    particles = "PhaserCSparksParticle";
};

datablock ExplosionData(PhaserCExplosion)
{
   soundProfile   = PhaserCExpSound;

   emitter[0] = PhaserCExplosionEmitter;
   emitter[1] = PhaserCSparksEmitter;

   debris = PhaserCDebris;
   debrisThetaMin = 0;
   debrisThetaMax = 180;
   debrisNum = 6;
   debrisVelocity = 25.0;
   debrisVelocityVariance = 7.5;

   shakeCamera = true;
   camShakeFreq = "6.0 7.0 5.0";
   camShakeAmp = "35.0 35.0 35.0";
   camShakeDuration = 0.4;
   camShakeRadius = 10.0;
};

datablock ExplosionData(PCRExplosion)
{
   explosionShape = "mortar_explosion.dts";
   playSpeed = 1.5;
   soundProfile = PhaserExpSound;
   emitter[0] = PCRSparksEmitter;
   faceViewer = true;

   sizes[0]      = 0.25;
   sizes[1]      = 0.5;
   times[0]      = 0.0;
   times[1]      = 1.0;

   shakeCamera = true;
   camShakeFreq = "10.0 6.0 9.0";
   camShakeAmp = "10.0 10.0 10.0";
   camShakeDuration = 0.5;
   camShakeRadius = 10.0;
};

//------------------------------------------------------------------------------
// Chaingun sound library
$BulletImpactDrySoundCount = 0;
$BulletImpactWetSoundCount = 0;

// Wet
datablock AudioProfile(ChaingunWaterImpact)
{
   filename    = "fx/weapons/cg_water1.WAV";
   description = AudioDefault3d;
   preload = true;
};
$BulletImpactWetSound[$BulletImpactWetSoundCount] = "ChaingunWaterImpact";
$BulletImpactWetSoundCount++;

datablock AudioProfile(ChaingunWaterImpact2)
{
   filename    = "fx/weapons/cg_water2.WAV";
   description = AudioDefault3d;
   preload = true;
};
$BulletImpactWetSound[$BulletImpactWetSoundCount] = "ChaingunWaterImpact2";
$BulletImpactWetSoundCount++;

datablock AudioProfile(ChaingunWaterImpact3)
{
   filename    = "fx/weapons/cg_water3.WAV";
   description = AudioDefault3d;
   preload = true;
};
$BulletImpactWetSound[$BulletImpactWetSoundCount] = "ChaingunWaterImpact3";
$BulletImpactWetSoundCount++;

// Dry
datablock AudioProfile(ChaingunImpact)
{
   filename    = "fx/weapons/chaingun_impact.WAV";
   description = AudioDefault3d;
   preload = true;
};
$BulletImpactDrySound[$BulletImpactDrySoundCount] = "ChaingunImpact";
$BulletImpactDrySoundCount++;

datablock AudioProfile(ChaingunImpact1)
{
   filename    = "fx/weapons/cg_hard1.WAV";
   description = AudioDefault3d;
   preload = true;
};
$BulletImpactDrySound[$BulletImpactDrySoundCount] = "ChaingunImpact1";
$BulletImpactDrySoundCount++;

datablock AudioProfile(ChaingunImpact2)
{
   filename    = "fx/weapons/cg_hard2.WAV";
   description = AudioDefault3d;
   preload = true;
};
$BulletImpactDrySound[$BulletImpactDrySoundCount] = "ChaingunImpact2";
$BulletImpactDrySoundCount++;

datablock AudioProfile(ChaingunImpact3)
{
   filename    = "fx/weapons/cg_metal1.WAV";
   description = AudioDefault3d;
   preload = true;
};
$BulletImpactDrySound[$BulletImpactDrySoundCount] = "ChaingunImpact3";
$BulletImpactDrySoundCount++;

datablock AudioProfile(ChaingunImpact4)
{
   filename    = "fx/weapons/cg_metal2.WAV";
   description = AudioDefault3d;
   preload = true;
};
$BulletImpactDrySound[$BulletImpactDrySoundCount] = "ChaingunImpact4";
$BulletImpactDrySoundCount++;

datablock AudioProfile(ChaingunImpact5)
{
   filename    = "fx/weapons/cg_metal3.WAV";
   description = AudioDefault3d;
   preload = true;
};
$BulletImpactDrySound[$BulletImpactDrySoundCount] = "ChaingunImpact5";
$BulletImpactDrySoundCount++;

datablock AudioProfile(ChaingunImpact6)
{
   filename    = "fx/weapons/cg_metal4.WAV";
   description = AudioDefault3d;
   preload = true;
};
$BulletImpactDrySound[$BulletImpactDrySoundCount] = "ChaingunImpact6";
$BulletImpactDrySoundCount++;

datablock AudioProfile(ChaingunImpact7)
{
   filename    = "fx/misc/Ricoche1.WAV";
   description = AudioDefault3d;
   preload = true;
};
$BulletImpactDrySound[$BulletImpactDrySoundCount] = "ChaingunImpact7";
$BulletImpactDrySoundCount++;

datablock AudioProfile(ChaingunImpact8)
{
   filename    = "fx/misc/Ricoche2.WAV";
   description = AudioDefault3d;
   preload = true;
};
$BulletImpactDrySound[$BulletImpactDrySoundCount] = "ChaingunImpact8";
$BulletImpactDrySoundCount++;

datablock AudioProfile(ChaingunImpact9)
{
   filename    = "fx/misc/Ricoche3.WAV";
   description = AudioDefault3d;
   preload = true;
};
$BulletImpactDrySound[$BulletImpactDrySoundCount] = "ChaingunImpact9";
$BulletImpactDrySoundCount++;

datablock AudioProfile(ChaingunImpact10)
{
   filename    = "fx/misc/bullet_ricochet1.WAV";
   description = AudioDefault3d;
   preload = true;
};
$BulletImpactDrySound[$BulletImpactDrySoundCount] = "ChaingunImpact10";
$BulletImpactDrySoundCount++;

datablock AudioProfile(ChaingunImpact11)
{
   filename    = "fx/misc/bullet_ricochet2.WAV";
   description = AudioDefault3d;
   preload = true;
};
$BulletImpactDrySound[$BulletImpactDrySoundCount] = "ChaingunImpact11";
$BulletImpactDrySoundCount++;

function playRandomChaingunSound(%pos)
{
    InitContainerRadiusSearch(%pos, 0.1, $TypeMasks::WaterObjectType);

    %snd = ContainerSearchNext() ? $BulletImpactWetSound[getRandom($BulletImpactWetSoundCount - 1)] : $BulletImpactDrySound[getRandom($BulletImpactDrySoundCount - 1)];

    serverPlay3D(%snd, %pos);
}

//------------------------------------------------------------------------------
// Injected datablocks from other parts of T2

// ----------------------------------------------
// audio datablocks
// ----------------------------------------------

datablock AudioProfile(MineDeploySound)
{
   filename = "fx/weapons/mine_deploy.wav";
   description = AudioClose3D;
   preload = true;
};

datablock AudioProfile(MineExplosionSound)
{
   filename = "fx/weapons/mine_detonate.wav";
   description = AudioBIGExplosion3d;
   preload = true;
};

datablock AudioProfile(UnderwaterMineExplosionSound)
{
   filename = "fx/weapons/mine_detonate_UW.wav";
   description = AudioBIGExplosion3d;
   preload = true;
};

//--------------------------------------------------------------------------
// Mine Particle effects
//--------------------------------------------------------------------------
datablock ParticleData(MineExplosionBubbleParticle)
{
   dragCoefficient      = 0.0;
   gravityCoefficient   = -0.25;
   inheritedVelFactor   = 0.0;
   constantAcceleration = 0.0;
   lifetimeMS           = 2000;
   lifetimeVarianceMS   = 750;
   useInvAlpha          = false;
   textureName          = "special/bubbles";

   spinRandomMin        = -100.0;
   spinRandomMax        =  100.0;

   colors[0]     = "0.7 0.8 1.0 0.0";
   colors[1]     = "0.7 0.8 1.0 0.4";
   colors[2]     = "0.7 0.8 1.0 0.0";
   sizes[0]      = 1.0;
   sizes[1]      = 1.0;
   sizes[2]      = 1.0;
   times[0]      = 0.0;
   times[1]      = 0.3;
   times[2]      = 1.0;
};
datablock ParticleEmitterData(MineExplosionBubbleEmitter)
{
   ejectionPeriodMS = 7;
   periodVarianceMS = 0;
   ejectionVelocity = 1.0;
   ejectionOffset   = 2.0;
   velocityVariance = 0.5;
   thetaMin         = 0;
   thetaMax         = 80;
   phiReferenceVel  = 0;
   phiVariance      = 360;
   overrideAdvances = false;
   particles = "MineExplosionBubbleParticle";
};
datablock ParticleData( UnderwaterMineCrescentParticle )
{
   dragCoefficient      = 2;
   gravityCoefficient   = 0.0;
   inheritedVelFactor   = 0.2;
   constantAcceleration = -0.0;
   lifetimeMS           = 600;
   lifetimeVarianceMS   = 000;
   textureName          = "special/crescent3";
   colors[0] = "0.5 0.5 1.0 1.0";
   colors[1] = "0.5 0.5 1.0 1.0";
   colors[2] = "0.5 0.5 1.0 0.0";
   sizes[0]      = 0.5;
   sizes[1]      = 1.0;
   sizes[2]      = 2.0;
   times[0]      = 0.0;
   times[1]      = 0.5;
   times[2]      = 1.0;
};

datablock ParticleEmitterData( UnderwaterMineCrescentEmitter )
{
   ejectionPeriodMS = 10;
   periodVarianceMS = 0;
   ejectionVelocity = 10;
   velocityVariance = 5.0;
   ejectionOffset   = 0.0;
   thetaMin         = 0;
   thetaMax         = 80;
   phiReferenceVel  = 0;
   phiVariance      = 360;
   overrideAdvances = false;
   orientParticles  = true;
   lifetimeMS       = 200;
   particles = "UnderwaterMineCrescentParticle";
};

datablock ParticleData(UnderwaterMineExplosionSmoke)
{
   dragCoeffiecient     = 105.0;
   gravityCoefficient   = -0.0;
   inheritedVelFactor   = 0.025;
   constantAcceleration = -1.0;

   lifetimeMS           = 1200;
   lifetimeVarianceMS   = 00;

   textureName          = "particleTest";

   useInvAlpha =  false;
   spinRandomMin = -200.0;
   spinRandomMax =  200.0;

   textureName = "special/Smoke/smoke_001";

   colors[0]     = "0.7 0.7 1.0 1.0";
   colors[1]     = "0.3 0.3 1.0 1.0";
   colors[2]     = "0.0 0.0 1.0 0.0";
   sizes[0]      = 1.0;
   sizes[1]      = 3.0;
   sizes[2]      = 1.0;
   times[0]      = 0.0;
   times[1]      = 0.5;
   times[2]      = 1.0;

};

datablock ParticleEmitterData(UnderwaterMineExplosionSmokeEmitter)
{
   ejectionPeriodMS = 8;
   periodVarianceMS = 0;

   ejectionVelocity = 4.25;
   velocityVariance = 1.25;

   thetaMin         = 0.0;
   thetaMax         = 80.0;

   lifetimeMS       = 250;

   particles = "UnderwaterMineExplosionSmoke";
};

//--------------------------------------------------------------------------
// Mine Particle effects
//--------------------------------------------------------------------------
datablock ParticleData( MineCrescentParticle )
{
   dragCoefficient      = 2;
   gravityCoefficient   = 0.0;
   inheritedVelFactor   = 0.2;
   constantAcceleration = -0.0;
   lifetimeMS           = 600;
   lifetimeVarianceMS   = 000;
   textureName          = "special/crescent3";
   colors[0] = "1.0 0.8 0.2 1.0";
   colors[1] = "1.0 0.4 0.2 1.0";
   colors[2] = "1.0 0.0 0.0 0.0";
   sizes[0]      = 0.5;
   sizes[1]      = 1.0;
   sizes[2]      = 2.0;
   times[0]      = 0.0;
   times[1]      = 0.5;
   times[2]      = 1.0;
};

datablock ParticleEmitterData( MineCrescentEmitter )
{
   ejectionPeriodMS = 10;
   periodVarianceMS = 0;
   ejectionVelocity = 10;
   velocityVariance = 5.0;
   ejectionOffset   = 0.0;
   thetaMin         = 0;
   thetaMax         = 80;
   phiReferenceVel  = 0;
   phiVariance      = 360;
   overrideAdvances = false;
   orientParticles  = true;
   lifetimeMS       = 200;
   particles = "MineCrescentParticle";
};

datablock ParticleData(MineExplosionSmoke)
{
   dragCoeffiecient     = 105.0;
   gravityCoefficient   = -0.0;
   inheritedVelFactor   = 0.025;

   lifetimeMS           = 1200;
   lifetimeVarianceMS   = 00;

   textureName          = "particleTest";

   useInvAlpha =  true;
   spinRandomMin = -200.0;
   spinRandomMax =  200.0;

   textureName = "special/Smoke/smoke_001";

   colors[0]     = "1.0 0.7 0.0 1.0";
   colors[1]     = "0.2 0.2 0.2 1.0";
   colors[2]     = "0.0 0.0 0.0 0.0";
   sizes[0]      = 1.0;
   sizes[1]      = 3.0;
   sizes[2]      = 1.0;
   times[0]      = 0.0;
   times[1]      = 0.5;
   times[2]      = 1.0;
};

datablock ParticleEmitterData(MineExplosionSmokeEmitter)
{
   ejectionPeriodMS = 8;
   periodVarianceMS = 0;

   ejectionVelocity = 4.25;
   velocityVariance = 1.25;

   thetaMin         = 0.0;
   thetaMax         = 80.0;

   lifetimeMS       = 250;

   particles = "MineExplosionSmoke";
};

datablock ExplosionData(MineExplosion)
{
   explosionShape = "effect_plasma_explosion.dts";
   playSpeed      = 1.0;
   sizes[0] = "0.5 0.5 0.5";
   sizes[1] = "0.5 0.5 0.5";
   soundProfile   = MineExplosionSound;
   faceViewer     = true;

   emitter[0] = MineExplosionSmokeEmitter;
   emitter[1] = MineCrescentEmitter;

   shakeCamera = true;
   camShakeFreq = "8.0 7.0 9.0";
   camShakeAmp = "50.0 50.0 50.0";
   camShakeDuration = 1.0;
   camShakeRadius = 10.0;
};

datablock ExplosionData(UnderwaterMineExplosion)
{
   explosionShape = "disc_explosion.dts";
   playSpeed      = 1.0;
   sizes[0] = "0.4 0.4 0.4";
   sizes[1] = "0.4 0.4 0.4";
   soundProfile   = UnderwaterMineExplosionSound;
   faceViewer     = true;

   emitter[0] = UnderwaterMineExplosionSmokeEmitter;
   emitter[1] = UnderwaterMineCrescentEmitter;
   emitter[2] = MineExplosionBubbleEmitter;

   shakeCamera = true;
   camShakeFreq = "8.0 7.0 9.0";
   camShakeAmp = "50.0 50.0 50.0";
   camShakeDuration = 1.0;
   camShakeRadius = 10.0;
};

//--------------------------------------------------------------------------
// Force-Feedback Effects
//--------------------------------------
datablock EffectProfile(MortarSwitchEffect)
{
   effectname = "weapons/mortar_activate";
   minDistance = 2.5;
   maxDistance = 2.5;
};

datablock EffectProfile(MortarFireEffect)
{
   effectname = "weapons/mortar_fire";
   minDistance = 2.5;
   maxDistance = 5.0;
};

datablock EffectProfile(MortarReloadEffect)
{
   effectname = "weapons/mortar_reload";
   minDistance = 2.5;
   maxDistance = 2.5;
};

datablock EffectProfile(MortarDryFireEffect)
{
   effectname = "weapons/mortar_dryfire";
   minDistance = 2.5;
   maxDistance = 2.5;
};

datablock EffectProfile(MortarExplosionEffect)
{
   effectname = "explosions/explosion.xpl03";
   minDistance = 30;
   maxDistance = 65;
};

//--------------------------------------------------------------------------
// Sounds
//--------------------------------------
datablock AudioProfile(MortarSwitchSound)
{
   filename    = "fx/weapons/mortar_activate.wav";
   description = AudioClosest3d;
   preload = true;
   effect = MortarSwitchEffect;
};

datablock AudioProfile(MortarReloadSound)
{
   filename    = "fx/weapons/mortar_reload.wav";
   description = AudioClosest3d;
   preload = true;
   effect = MortarReloadEffect;
};

datablock AudioProfile(MortarIdleSound)
{
   //filename    = "fx/weapons/weapon.mortarIdle.wav";
   filename = "fx/weapons/plasma_rifle_idle.wav";
   description = AudioDefaultLooping3d;
   preload = true;
};

datablock AudioProfile(MortarFireSound)
{
   filename    = "fx/weapons/mortar_fire.wav";
   description = AudioDefault3d;
   preload = true;
   effect = MortarFireEffect;
};

datablock AudioProfile(MortarProjectileSound)
{
   filename    = "fx/weapons/mortar_projectile.wav";
   description = ProjectileLooping3d;
   preload = true;
};

datablock AudioProfile(MortarExplosionSound)
{
   filename    = "fx/weapons/mortar_explode.wav";
   description = AudioBIGExplosion3d;
   preload = true;
};

datablock AudioProfile(ImpactMortarExplosionSound)
{
   filename    = "fx/powered/turret_mortar_explode.wav";
   description = AudioBIGExplosion3d;
   preload = true;
};

datablock AudioProfile(UnderwaterMortarExplosionSound)
{
   filename    = "fx/weapons/mortar_explode_UW.wav";
   description = AudioBIGExplosion3d;
   preload = true;
   effect = MortarExplosionEffect;
};

datablock AudioProfile(MortarDryFireSound)
{
   filename    = "fx/weapons/mortar_dryfire.wav";
   description = AudioClose3d;
   preload = true;
   effect = MortarDryFireEffect;
};

//----------------------------------------------------------------------------
// Bubbles
//----------------------------------------------------------------------------
datablock ParticleData(MortarBubbleParticle)
{
   dragCoefficient      = 0.0;
   gravityCoefficient   = -0.25;
   inheritedVelFactor   = 0.0;
   constantAcceleration = 0.0;
   lifetimeMS           = 1500;
   lifetimeVarianceMS   = 600;
   useInvAlpha          = false;
   textureName          = "special/bubbles";

   spinRandomMin        = -100.0;
   spinRandomMax        =  100.0;

   colors[0]     = "0.7 0.8 1.0 0.4";
   colors[1]     = "0.7 0.8 1.0 0.4";
   colors[2]     = "0.7 0.8 1.0 0.0";
   sizes[0]      = 0.8;
   sizes[1]      = 0.8;
   sizes[2]      = 0.8;
   times[0]      = 0.0;
   times[1]      = 0.5;
   times[2]      = 1.0;
};

datablock ParticleEmitterData(MortarBubbleEmitter)
{
   ejectionPeriodMS = 9;
   periodVarianceMS = 0;
   ejectionVelocity = 1.0;
   ejectionOffset   = 0.1;
   velocityVariance = 0.5;
   thetaMin         = 0;
   thetaMax         = 80;
   phiReferenceVel  = 0;
   phiVariance      = 360;
   overrideAdvances = false;
   particles = "MortarBubbleParticle";
};

//--------------------------------------------------------------------------
// Splash
//--------------------------------------------------------------------------
datablock ParticleData( MortarSplashParticle )
{
   dragCoefficient      = 1;
   gravityCoefficient   = 0.0;
   inheritedVelFactor   = 0.2;
   constantAcceleration = -1.4;
   lifetimeMS           = 300;
   lifetimeVarianceMS   = 0;
   textureName          = "special/droplet";
   colors[0]     = "0.7 0.8 1.0 1.0";
   colors[1]     = "0.7 0.8 1.0 0.5";
   colors[2]     = "0.7 0.8 1.0 0.0";
   sizes[0]      = 0.05;
   sizes[1]      = 0.2;
   sizes[2]      = 0.2;
   times[0]      = 0.0;
   times[1]      = 0.5;
   times[2]      = 1.0;
};

datablock ParticleEmitterData( MortarSplashEmitter )
{
   ejectionPeriodMS = 4;
   periodVarianceMS = 0;
   ejectionVelocity = 3;
   velocityVariance = 1.0;
   ejectionOffset   = 0.0;
   thetaMin         = 0;
   thetaMax         = 50;
   phiReferenceVel  = 0;
   phiVariance      = 360;
   overrideAdvances = false;
   orientParticles  = true;
   lifetimeMS       = 100;
   particles = "MortarSplashParticle";
};

datablock SplashData(MortarSplash)
{
   numSegments = 10;
   ejectionFreq = 10;
   ejectionAngle = 20;
   ringLifetime = 0.4;
   lifetimeMS = 400;
   velocity = 3.0;
   startRadius = 0.0;
   acceleration = -3.0;
   texWrap = 5.0;

   texture = "special/water2";

   emitter[0] = MortarSplashEmitter;

   colors[0] = "0.7 0.8 1.0 0.0";
   colors[1] = "0.7 0.8 1.0 1.0";
   colors[2] = "0.7 0.8 1.0 0.0";
   colors[3] = "0.7 0.8 1.0 0.0";
   times[0] = 0.0;
   times[1] = 0.4;
   times[2] = 0.8;
   times[3] = 1.0;
};

//---------------------------------------------------------------------------
// Mortar Shockwaves
//---------------------------------------------------------------------------
datablock ShockwaveData(UnderwaterMortarShockwave)
{
   width = 6.0;
   numSegments = 32;
   numVertSegments = 6;
   velocity = 10;
   acceleration = 20.0;
   lifetimeMS = 900;
   height = 1.0;
   verticalCurve = 0.5;
   is2D = false;

   texture[0] = "special/shockwave4";
   texture[1] = "special/gradient";
   texWrap = 6.0;

   times[0] = 0.0;
   times[1] = 0.5;
   times[2] = 1.0;

   colors[0] = "0.4 0.4 1.0 0.50";
   colors[1] = "0.4 0.4 1.0 0.25";
   colors[2] = "0.4 0.4 1.0 0.0";

   mapToTerrain = true;
   orientToNormal = false;
   renderBottom = false;
};

datablock ShockwaveData(MortarShockwave)
{
   width = 6.0;
   numSegments = 32;
   numVertSegments = 6;
   velocity = 15;
   acceleration = 20.0;
   lifetimeMS = 500;
   height = 1.0;
   verticalCurve = 0.5;
   is2D = false;

   texture[0] = "special/shockwave4";
   texture[1] = "special/gradient";
   texWrap = 6.0;

   times[0] = 0.0;
   times[1] = 0.5;
   times[2] = 1.0;

   colors[0] = "0.4 1.0 0.4 0.50";
   colors[1] = "0.4 1.0 0.4 0.25";
   colors[2] = "0.4 1.0 0.4 0.0";

   mapToTerrain = true;
   orientToNormal = false;
   renderBottom = false;
};

//--------------------------------------------------------------------------
// Mortar Explosion Particle effects
//--------------------------------------
datablock ParticleData( MortarCrescentParticle )
{
   dragCoefficient      = 2;
   gravityCoefficient   = 0.0;
   inheritedVelFactor   = 0.2;
   constantAcceleration = -0.0;
   lifetimeMS           = 600;
   lifetimeVarianceMS   = 000;
   textureName          = "special/crescent3";
   colors[0]     = "0.7 1.0 0.7 1.0";
   colors[1]     = "0.7 1.0 0.7 0.5";
   colors[2]     = "0.7 1.0 0.7 0.0";
   sizes[0]      = 4.0;
   sizes[1]      = 8.0;
   sizes[2]      = 9.0;
   times[0]      = 0.0;
   times[1]      = 0.5;
   times[2]      = 1.0;
};

datablock ParticleEmitterData( MortarCrescentEmitter )
{
   ejectionPeriodMS = 25;
   periodVarianceMS = 0;
   ejectionVelocity = 40;
   velocityVariance = 5.0;
   ejectionOffset   = 0.0;
   thetaMin         = 0;
   thetaMax         = 80;
   phiReferenceVel  = 0;
   phiVariance      = 360;
   overrideAdvances = false;
   orientParticles  = true;
   lifetimeMS       = 200;
   particles = "MortarCrescentParticle";
};

datablock ParticleData(MortarExplosionSmoke)
{
   dragCoeffiecient     = 0.4;
   gravityCoefficient   = -0.30;   // rises slowly
   inheritedVelFactor   = 0.025;

   lifetimeMS           = 1250;
   lifetimeVarianceMS   = 500;

   textureName          = "particleTest";

   useInvAlpha =  true;
   spinRandomMin = -100.0;
   spinRandomMax =  100.0;

   textureName = "special/Smoke/bigSmoke";

   colors[0]     = "0.7 0.7 0.7 0.0";
   colors[1]     = "0.4 0.4 0.4 0.5";
   colors[2]     = "0.4 0.4 0.4 0.5";
   colors[3]     = "0.4 0.4 0.4 0.0";
   sizes[0]      = 5.0;
   sizes[1]      = 6.0;
   sizes[2]      = 10.0;
   sizes[3]      = 12.0;
   times[0]      = 0.0;
   times[1]      = 0.333;
   times[2]      = 0.666;
   times[3]      = 1.0;
};

datablock ParticleEmitterData(MortarExplosionSmokeEmitter)
{
   ejectionPeriodMS = 10;
   periodVarianceMS = 0;

   ejectionOffset = 8.0;


   ejectionVelocity = 1.25;
   velocityVariance = 1.2;

   thetaMin         = 0.0;
   thetaMax         = 90.0;

   lifetimeMS       = 500;

   particles = "MortarExplosionSmoke";
};

//---------------------------------------------------------------------------
// Underwater Explosion
//---------------------------------------------------------------------------
datablock ParticleData(UnderwaterExplosionSparks)
{
   dragCoefficient      = 0;
   gravityCoefficient   = 0.0;
   inheritedVelFactor   = 0.2;
   constantAcceleration = 0.0;
   lifetimeMS           = 500;
   lifetimeVarianceMS   = 350;
   textureName          = "special/crescent3";
   colors[0]     = "0.4 0.4 1.0 1.0";
   colors[1]     = "0.4 0.4 1.0 1.0";
   colors[2]     = "0.4 0.4 1.0 0.0";
   sizes[0]      = 3.5;
   sizes[1]      = 3.5;
   sizes[2]      = 3.5;
   times[0]      = 0.0;
   times[1]      = 0.5;
   times[2]      = 1.0;

};

datablock ParticleEmitterData(UnderwaterExplosionSparksEmitter)
{
   ejectionPeriodMS = 2;
   periodVarianceMS = 0;
   ejectionVelocity = 17;
   velocityVariance = 4;
   ejectionOffset   = 0.0;
   thetaMin         = 0;
   thetaMax         = 60;
   phiReferenceVel  = 0;
   phiVariance      = 360;
   overrideAdvances = false;
   orientParticles  = true;
   lifetimeMS       = 100;
   particles = "UnderwaterExplosionSparks";
};

datablock ParticleData(MortarExplosionBubbleParticle)
{
   dragCoefficient      = 0.0;
   gravityCoefficient   = -0.25;
   inheritedVelFactor   = 0.0;
   constantAcceleration = 0.0;
   lifetimeMS           = 1500;
   lifetimeVarianceMS   = 600;
   useInvAlpha          = false;
   textureName          = "special/bubbles";

   spinRandomMin        = -100.0;
   spinRandomMax        =  100.0;

   colors[0]     = "0.7 0.8 1.0 0.0";
   colors[1]     = "0.7 0.8 1.0 0.4";
   colors[2]     = "0.7 0.8 1.0 0.0";
   sizes[0]      = 2.0;
   sizes[1]      = 2.0;
   sizes[2]      = 2.0;
   times[0]      = 0.0;
   times[1]      = 0.8;
   times[2]      = 1.0;
};

datablock ParticleEmitterData(MortarExplosionBubbleEmitter)
{
   ejectionPeriodMS = 5;
   periodVarianceMS = 0;
   ejectionVelocity = 1.0;
   ejectionOffset   = 7.0;
   velocityVariance = 0.5;
   thetaMin         = 0;
   thetaMax         = 80;
   phiReferenceVel  = 0;
   phiVariance      = 360;
   overrideAdvances = false;
   particles = "MortarExplosionBubbleParticle";
};

datablock DebrisData( UnderwaterMortarDebris )
{
   emitters[0] = MortarExplosionBubbleEmitter;

   explodeOnMaxBounce = true;

   elasticity = 0.4;
   friction = 0.2;

   lifetime = 1.5;
   lifetimeVariance = 0.2;

   numBounces = 1;
};

datablock ExplosionData(UnderwaterMortarSubExplosion1)
{
   explosionShape = "disc_explosion.dts";
   faceViewer           = true;
   delayMS = 100;
   offset = 3.0;
   playSpeed = 1.5;

   sizes[0] = "0.75 0.75 0.75";
   sizes[1] = "1.0  1.0  1.0";
   sizes[2] = "0.5 0.5 0.5";
   times[0] = 0.0;
   times[1] = 0.5;
   times[2] = 1.0;

};

datablock ExplosionData(UnderwaterMortarSubExplosion2)
{
   explosionShape = "disc_explosion.dts";
   faceViewer           = true;
   delayMS = 50;
   offset = 3.0;
   playSpeed = 0.75;

   sizes[0] = "1.5 1.5 1.5";
   sizes[1] = "1.5 1.5 1.5";
   sizes[2] = "1.0 1.0 1.0";
   times[0] = 0.0;
   times[1] = 0.5;
   times[2] = 1.0;
};

datablock ExplosionData(UnderwaterMortarSubExplosion3)
{
   explosionShape = "disc_explosion.dts";
   faceViewer           = true;
   delayMS = 0;
   offset = 0.0;
   playSpeed = 0.5;

   sizes[0] = "1.0 1.0 1.0";
   sizes[1] = "2.0 2.0 2.0";
   sizes[2] = "1.5 1.5 1.5";
   times[0] = 0.0;
   times[1] = 0.5;
   times[2] = 1.0;
};

datablock ExplosionData(UnderwaterMortarExplosion)
{
   soundProfile   = UnderwaterMortarExplosionSound;

   shockwave = UnderwaterMortarShockwave;
   shockwaveOnTerrain = true;

   subExplosion[0] = UnderwaterMortarSubExplosion1;
   subExplosion[1] = UnderwaterMortarSubExplosion2;
   subExplosion[2] = UnderwaterMortarSubExplosion3;

   emitter[0] = MortarExplosionBubbleEmitter;
   emitter[1] = UnderwaterExplosionSparksEmitter;

   shakeCamera = true;
   camShakeFreq = "8.0 9.0 7.0";
   camShakeAmp = "100.0 100.0 100.0";
   camShakeDuration = 1.3;
   camShakeRadius = 25.0;
};

//---------------------------------------------------------------------------
// Explosion
//---------------------------------------------------------------------------

datablock ExplosionData(MortarSubExplosion1)
{
   explosionShape = "mortar_explosion.dts";
   faceViewer           = true;

   delayMS = 100;

   offset = 5.0;

   playSpeed = 1.5;

   sizes[0] = "0.5 0.5 0.5";
   sizes[1] = "0.5 0.5 0.5";
   times[0] = 0.0;
   times[1] = 1.0;
};

datablock ExplosionData(MortarSubExplosion2)
{
   explosionShape = "mortar_explosion.dts";
   faceViewer           = true;

   delayMS = 50;

   offset = 5.0;

   playSpeed = 1.0;

   sizes[0] = "1.0 1.0 1.0";
   sizes[1] = "1.0 1.0 1.0";
   times[0] = 0.0;
   times[1] = 1.0;
};

datablock ExplosionData(MortarSubExplosion3)
{
   explosionShape = "mortar_explosion.dts";
   faceViewer           = true;
   delayMS = 0;
   offset = 0.0;
   playSpeed = 0.7;

   sizes[0] = "1.0 1.0 1.0";
   sizes[1] = "2.0 2.0 2.0";
   times[0] = 0.0;
   times[1] = 1.0;
};

datablock ExplosionData(MortarExplosion)
{
   soundProfile   = MortarExplosionSound;

   shockwave = MortarShockwave;
   shockwaveOnTerrain = true;

   subExplosion[0] = MortarSubExplosion1;
   subExplosion[1] = MortarSubExplosion2;
   subExplosion[2] = MortarSubExplosion3;

   emitter[0] = MortarExplosionSmokeEmitter;
   emitter[1] = MortarCrescentEmitter;

   shakeCamera = true;
   camShakeFreq = "8.0 9.0 7.0";
   camShakeAmp = "100.0 100.0 100.0";
   camShakeDuration = 1.3;
   camShakeRadius = 25.0;
};

//------------------------------------------------------------------------------
// Impact mortar FX - alternate

datablock ShockwaveData(ImpactMortarShockwave)
{
   width = 6.0;
   numSegments = 32;
   numVertSegments = 6;
   velocity = 15;
   acceleration = 20.0;
   lifetimeMS = 1000;
   height = 1.0;
   verticalCurve = 0.5;
   is2D = false;

   texture[0] = "special/shockwave4";
   texture[1] = "special/gradient";
   texWrap = 6.0;

   times[0] = 0.0;
   times[1] = 0.5;
   times[2] = 1.0;

   colors[0] = "0.85 0.8 0.0 0.5";
   colors[1] = "0.85 0.8 0.0 0.25";
   colors[2] = "0.85 0.8 0.0 0.0";

   mapToTerrain = true;
   orientToNormal = false;
   renderBottom = false;
};

datablock ParticleData( ImpactCrescentParticle )
{
   dragCoefficient      = 2;
   gravityCoefficient   = 0.0;
   inheritedVelFactor   = 0.2;
   constantAcceleration = -0.0;
   lifetimeMS           = 600;
   lifetimeVarianceMS   = 000;
   textureName          = "special/crescent3";
   colors[0]     = "0.8 0.8 0.1 1.0";
   colors[1]     = "0.8 0.8 0.1 0.5";
   colors[2]     = "0.8 0.8 0.1 0.0";
   sizes[0]      = 4.0;
   sizes[1]      = 8.0;
   sizes[2]      = 9.0;
   times[0]      = 0.0;
   times[1]      = 0.5;
   times[2]      = 1.0;
};

datablock ParticleEmitterData( ImpactCrescentEmitter )
{
   ejectionPeriodMS = 25;
   periodVarianceMS = 0;
   ejectionVelocity = 40;
   velocityVariance = 5.0;
   ejectionOffset   = 0.0;
   thetaMin         = 0;
   thetaMax         = 80;
   phiReferenceVel  = 0;
   phiVariance      = 360;
   overrideAdvances = false;
   orientParticles  = true;
   lifetimeMS       = 200;
   particles = "ImpactCrescentParticle";
};

datablock ExplosionData(ImpactSubExplosion1)
{
   explosionShape = "effect_plasma_explosion.dts";
   faceViewer           = true;

   delayMS = 100;

   offset = 5.0;

   playSpeed = 1.5;

   sizes[0] = "2.5 2.5 2.5";
   sizes[1] = "5.0 5.0 5.0";
   times[0] = 0.0;
   times[1] = 1.0;

};

datablock ExplosionData(ImpactSubExplosion2)
{
   explosionShape = "effect_plasma_explosion.dts";
   faceViewer           = true;

   delayMS = 50;

   offset = 5.0;

   playSpeed = 1.0;

   sizes[0] = "2.5 2.5 2.5";
   sizes[1] = "5.0 5.0 5.0";
   times[0] = 0.0;
   times[1] = 1.0;
};

datablock ExplosionData(ImpactSubExplosion3)
{
   explosionShape = "effect_plasma_explosion.dts";
   faceViewer           = true;
   delayMS = 0;
   offset = 0.0;
   playSpeed = 0.7;

   sizes[0] = "2.5 2.5 2.5";
   sizes[1] = "5.0 5.0 5.0";
   times[0] = 0.0;
   times[1] = 1.0;
};

datablock ExplosionData(ImpactMortarExplosion)
{
   soundProfile   = ImpactMortarExplosionSound;

   shockwave = ImpactMortarShockwave;
   shockwaveOnTerrain = true;

   subExplosion[0] = ImpactSubExplosion1;
   subExplosion[1] = ImpactSubExplosion2;
   subExplosion[2] = ImpactSubExplosion3;

   emitter[0] = MortarExplosionSmokeEmitter;
   emitter[1] = ImpactCrescentEmitter;

   debris = MortarDebris;
   debrisThetaMin = 10;
   debrisThetaMax = 60;
   debrisNum = 12;
   debrisVelocity = 20.0;
   debrisVelocityVariance = 10.0;

   shakeCamera = true;
   camShakeFreq = "8.0 9.0 7.0";
   camShakeAmp = "100.0 100.0 100.0";
   camShakeDuration = 1.3;
   camShakeRadius = 25.0;
};

datablock ParticleData(MBExplosionParticle)
{
   dragCoefficient      = 2;
   gravityCoefficient   = 0.0;
   inheritedVelFactor   = 0.2;
   constantAcceleration = 0.0;
   lifetimeMS           = 750;
   lifetimeVarianceMS   = 150;
   textureName          = "particleTest";
   colors[0]     = "1.0 1.0 1.0 1.0";
   colors[1]     = "1.0 1.0 0.1 0.0";
   sizes[0]      = 2;
   sizes[1]      = 5;
};

datablock ParticleEmitterData(MBExplosionEmitter)
{
   ejectionPeriodMS = 7;
   periodVarianceMS = 0;
   ejectionVelocity = 5;
   velocityVariance = 1.0;
   ejectionOffset   = 0.0;
   thetaMin         = 0;
   thetaMax         = 60;
   phiReferenceVel  = 0;
   phiVariance      = 360;
   overrideAdvances = false;
   particles = "MBExplosionParticle";
};

//--------------------------------------------------------------------------
// Force-Feedback Effects
//--------------------------------------
datablock EffectProfile(MissileSwitchEffect)
{
   effectname = "weapons/missile_launcher_activate";
   minDistance = 2.5;
   maxDistance = 2.5;
};

datablock EffectProfile(MissileFireEffect)
{
   effectname = "weapons/missile_fire";
   minDistance = 2.5;
   maxDistance = 5.0;
};

datablock EffectProfile(MissileDryFireEffect)
{
   effectname = "weapons/missile_launcher_dryfire";
   minDistance = 2.5;
   maxDistance = 2.5;
};

datablock EffectProfile(MissileExplosionEffect)
{
   effectname = "explosions/explosion.xpl23";
   minDistance = 10;
   maxDistance = 30;
};

//--------------------------------------------------------------------------
// Sounds
//--------------------------------------
datablock AudioProfile(MissileSwitchSound)
{
   filename    = "fx/weapons/missile_launcher_activate.wav";
   description = AudioClosest3d;
   preload = true;
   effect = MissileSwitchEffect;
};

datablock AudioProfile(MissileFireSound)
{
   filename    = "fx/weapons/missile_fire.WAV";
   description = AudioDefault3d;
   preload = true;
   effect = MissileFireEffect;
};

datablock AudioProfile(MissileProjectileSound)
{
   filename    = "fx/weapons/missile_projectile.wav";
   description = ProjectileLooping3d;
   preload = true;
};

datablock AudioProfile(MissileReloadSound)
{
   filename    = "fx/weapons/weapon.missilereload.wav";
   description = AudioClosest3d;
   preload = true;
};

datablock AudioProfile(MissileLockSound)
{
   filename    = "fx/weapons/missile_launcher_searching.WAV";
   description = AudioClosest3d;
   preload = true;
};

datablock AudioProfile(MissileExplosionSound)
{
   filename    = "fx/explosions/explosion.xpl23.wav";
   description = AudioBIGExplosion3d;
   preload = true;
   effect = MissileExplosionEffect;
};

datablock AudioProfile(MissileDryFireSound)
{
   filename    = "fx/weapons/missile_launcher_dryfire.wav";
   description = AudioClose3d;
   preload = true;
   effect = MissileDryFireEffect;
};


//----------------------------------------------------------------------------
// Splash Debris
//----------------------------------------------------------------------------
datablock ParticleData( MDebrisSmokeParticle )
{
   dragCoeffiecient     = 1.0;
   gravityCoefficient   = 0.10;
   inheritedVelFactor   = 0.1;

   lifetimeMS           = 1000;
   lifetimeVarianceMS   = 100;

   textureName          = "particleTest";

//   useInvAlpha =     true;

   spinRandomMin = -60.0;
   spinRandomMax = 60.0;

   colors[0]     = "0.7 0.8 1.0 1.0";
   colors[1]     = "0.7 0.8 1.0 0.5";
   colors[2]     = "0.7 0.8 1.0 0.0";
   sizes[0]      = 0.0;
   sizes[1]      = 0.8;
   sizes[2]      = 0.8;
   times[0]      = 0.0;
   times[1]      = 0.5;
   times[2]      = 1.0;
};

datablock ParticleEmitterData( MDebrisSmokeEmitter )
{
   ejectionPeriodMS = 10;
   periodVarianceMS = 1;

   ejectionVelocity = 1.0;  // A little oomph at the back end
   velocityVariance = 0.2;

   thetaMin         = 0.0;
   thetaMax         = 40.0;

   particles = "MDebrisSmokeParticle";
};


datablock DebrisData( MissileSplashDebris )
{
   emitters[0] = MDebrisSmokeEmitter;

   explodeOnMaxBounce = true;

   elasticity = 0.4;
   friction = 0.2;

   lifetime = 0.3;
   lifetimeVariance = 0.1;

   numBounces = 1;
};


//----------------------------------------------------------------------------
// Missile smoke spike (for debris)
//----------------------------------------------------------------------------
datablock ParticleData( MissileSmokeSpike )
{
   dragCoeffiecient     = 1.0;
   gravityCoefficient   = 0.0;
   inheritedVelFactor   = 0.2;

   lifetimeMS           = 1000;
   lifetimeVarianceMS   = 100;

   textureName          = "particleTest";

   useInvAlpha =     true;

   spinRandomMin = -60.0;
   spinRandomMax = 60.0;

   colors[0]     = "0.6 0.6 0.6 1.0";
   colors[1]     = "0.4 0.4 0.4 0.5";
   colors[2]     = "0.4 0.4 0.4 0.0";
   sizes[0]      = 0.0;
   sizes[1]      = 1.0;
   sizes[2]      = 0.5;
   times[0]      = 0.0;
   times[1]      = 0.5;
   times[2]      = 1.0;
};

datablock ParticleEmitterData( MissileSmokeSpikeEmitter )
{
   ejectionPeriodMS = 7;
   periodVarianceMS = 1;

   ejectionVelocity = 1.0;  // A little oomph at the back end
   velocityVariance = 0.2;

   thetaMin         = 0.0;
   thetaMax         = 40.0;

   particles = "MissileSmokeSpike";
};


//----------------------------------------------------------------------------
// Explosion smoke particles
//----------------------------------------------------------------------------

datablock ParticleData(MissileExplosionSmoke)
{
   dragCoeffiecient     = 0.3;
   gravityCoefficient   = -0.2;
   inheritedVelFactor   = 0.025;

   lifetimeMS           = 1250;
   lifetimeVarianceMS   = 0;

   textureName          = "particleTest";

   useInvAlpha =  true;
   spinRandomMin = -100.0;
   spinRandomMax =  100.0;

   textureName = "special/Smoke/bigSmoke";

   colors[0]     = "1.0 0.7 0.0 1.0";
   colors[1]     = "0.4 0.4 0.4 0.5";
   colors[2]     = "0.4 0.4 0.4 0.0";
   sizes[0]      = 1.0;
   sizes[1]      = 3.0;
   sizes[2]      = 1.0;
   times[0]      = 0.0;
   times[1]      = 0.5;
   times[2]      = 1.0;

};

datablock ParticleEmitterData(MissileExplosionSmokeEMitter)
{
   ejectionOffset = 0.0;
   ejectionPeriodMS = 5;
   periodVarianceMS = 0;

   ejectionVelocity = 3.25;
   velocityVariance = 0.25;

   thetaMin         = 0.0;
   thetaMax         = 180.0;

   lifetimeMS       = 250;

   particles = "MissileExplosionSmoke";
};



datablock DebrisData( MissileSpikeDebris )
{
   emitters[0] = MissileSmokeSpikeEmitter;
   explodeOnMaxBounce = true;
   elasticity = 0.4;
   friction = 0.2;
   lifetime = 0.3;
   lifetimeVariance = 0.02;
};

//--------------------------------------------------------------------------
// Sounds
//--------------------------------------------------------------------------
datablock EffectProfile(PBLSwitchEffect)
{
   effectname = "powered/turret_light_activate";
   minDistance = 2.5;
   maxDistance = 5.0;
};

datablock EffectProfile(PBLFireEffect)
{
   effectname = "powered/turret_plasma_fire";
   minDistance = 2.5;
   maxDistance = 5.0;
};

datablock AudioProfile(PBLSwitchSound)
{
   filename    = "fx/powered/turret_light_activate.wav";
   description = AudioClose3d;
   preload = true;
   effect = PBLSwitchEffect;
};

datablock AudioProfile(PBLFireSound)
{
   filename    = "fx/powered/turret_plasma_fire.wav";
   description = AudioDefault3d;
   preload = true;
   effect = PBLFireEffect;
};

//--------------------------------------------------------------------------
// Explosion
//--------------------------------------------------------------------------

datablock AudioProfile(PlasmaBarrelExpSound)
{
   filename    = "fx/powered/turret_plasma_explode.wav";
   description = "AudioExplosion3d";
   preload = true;
};


datablock ParticleData( PlasmaBarrelCrescentParticle )
{
   dragCoefficient      = 2;
   gravityCoefficient   = 0.0;
   inheritedVelFactor   = 0.2;
   constantAcceleration = -0.0;
   lifetimeMS           = 600;
   lifetimeVarianceMS   = 000;
   textureName          = "special/crescent3";

   colors[0]     = "0.3 0.4 1.0 1.0";
   colors[1]     = "0.3 0.4 1.0 0.5";
   colors[2]     = "0.3 0.4 1.0 0.0";
   sizes[0]      = 2.0;
   sizes[1]      = 4.0;
   sizes[2]      = 5.0;
   times[0]      = 0.0;
   times[1]      = 0.5;
   times[2]      = 1.0;
};

datablock ParticleEmitterData( PlasmaBarrelCrescentEmitter )
{
   ejectionPeriodMS = 25;
   periodVarianceMS = 0;
   ejectionVelocity = 20;
   velocityVariance = 5.0;
   ejectionOffset   = 0.0;
   thetaMin         = 0;
   thetaMax         = 80;
   phiReferenceVel  = 0;
   phiVariance      = 360;
   overrideAdvances = false;
   orientParticles  = true;
   lifetimeMS       = 200;
   particles = "PlasmaBarrelCrescentParticle";
};

datablock ParticleData(PlasmaBarrelExplosionParticle)
{
   dragCoefficient      = 2;
   gravityCoefficient   = 0.2;
   inheritedVelFactor   = 0.2;
   constantAcceleration = 0.0;
   lifetimeMS           = 750;
   lifetimeVarianceMS   = 150;
   textureName          = "particleTest";
   colors[0]     = "0.3 0.4 1.0 1.0";
   colors[1]     = "0.3 0.4 1.0 0.0";
   sizes[0]      = 1;
   sizes[1]      = 2;
};

datablock ParticleEmitterData(PlasmaBarrelExplosionEmitter)
{
   ejectionPeriodMS = 7;
   periodVarianceMS = 0;
   ejectionVelocity = 12;
   velocityVariance = 1.75;
   ejectionOffset   = 0.0;
   thetaMin         = 0;
   thetaMax         = 60;
   phiReferenceVel  = 0;
   phiVariance      = 360;
   overrideAdvances = false;
   particles = "PlasmaBarrelExplosionParticle";
};


datablock ExplosionData(PlasmaBarrelSubExplosion1)
{
   explosionShape = "disc_explosion.dts";
   faceViewer           = true;

   delayMS = 50;

   offset = 3.0;

   playSpeed = 1.5;

   sizes[0] = "0.25 0.25 0.25";
   sizes[1] = "0.25 0.25 0.25";
   times[0] = 0.0;
   times[1] = 1.0;

};

datablock ExplosionData(PlasmaBarrelSubExplosion2)
{
   explosionShape = "disc_explosion.dts";
   faceViewer           = true;

   delayMS = 100;

   offset = 3.5;

   playSpeed = 1.0;

   sizes[0] = "0.5 0.5 0.5";
   sizes[1] = "0.5 0.5 0.5";
   times[0] = 0.0;
   times[1] = 1.0;
};

datablock ExplosionData(PlasmaBarrelSubExplosion3)
{
   explosionShape = "disc_explosion.dts";
   faceViewer           = true;

   delayMS = 0;

   offset = 0.0;

   playSpeed = 0.7;


   sizes[0] = "0.5 0.5 0.5";
   sizes[1] = "1.0 1.0 1.0";
   times[0] = 0.0;
   times[1] = 1.0;

};

datablock ExplosionData(PlasmaBarrelBoltExplosion)
{
   soundProfile   = PlasmaBarrelExpSound;
   particleEmitter = PlasmaBarrelExplosionEmitter;
   particleDensity = 250;
   particleRadius = 1.25;
   faceViewer = true;

   emitter[0] = PlasmaBarrelCrescentEmitter;

   subExplosion[0] = PlasmaBarrelSubExplosion1;
   subExplosion[1] = PlasmaBarrelSubExplosion2;
   subExplosion[2] = PlasmaBarrelSubExplosion3;

   shakeCamera = true;
   camShakeFreq = "10.0 9.0 9.0";
   camShakeAmp = "10.0 10.0 10.0";
   camShakeDuration = 0.5;
   camShakeRadius = 15.0;
};

//---------------------------------------------------------------------------
// Explosions
//---------------------------------------------------------------------------
datablock ExplosionData(MissileExplosion)
{
   explosionShape = "effect_plasma_explosion.dts";
   playSpeed = 1.5;
   soundProfile   = MissileExplosionSound;
   faceViewer = true;

   sizes[0] = "0.5 0.5 0.5";
   sizes[1] = "0.5 0.5 0.5";
   sizes[2] = "0.5 0.5 0.5";

   emitter[0] = MissileExplosionSmokeEmitter;

   debris = MissileSpikeDebris;
   debrisThetaMin = 10;
   debrisThetaMax = 170;
   debrisNum = 8;
   debrisNumVariance = 6;
   debrisVelocity = 15.0;
   debrisVelocityVariance = 2.0;

   shakeCamera = true;
   camShakeFreq = "6.0 7.0 7.0";
   camShakeAmp = "70.0 70.0 70.0";
   camShakeDuration = 1.0;
   camShakeRadius = 7.0;
};

datablock ExplosionData(MissileSplashExplosion)
{
   explosionShape = "disc_explosion.dts";

   faceViewer           = true;
   explosionScale = "1.0 1.0 1.0";

   debris = MissileSplashDebris;
   debrisThetaMin = 10;
   debrisThetaMax = 80;
   debrisNum = 10;
   debrisVelocity = 10.0;
   debrisVelocityVariance = 4.0;

   sizes[0] = "0.35 0.35 0.35";
   sizes[1] = "0.15 0.15 0.15";
   sizes[2] = "0.15 0.15 0.15";
   sizes[3] = "0.15 0.15 0.15";

   times[0] = 0.0;
   times[1] = 0.333;
   times[2] = 0.666;
   times[3] = 1.0;

};

//--------------------------------------------------------------------------
// Splash
//--------------------------------------------------------------------------
datablock ParticleData(MissileMist)
{
   dragCoefficient      = 2.0;
   gravityCoefficient   = -0.05;
   inheritedVelFactor   = 0.0;
   constantAcceleration = 0.0;
   lifetimeMS           = 400;
   lifetimeVarianceMS   = 100;
   useInvAlpha          = false;
   spinRandomMin        = -90.0;
   spinRandomMax        = 500.0;
   textureName          = "particleTest";
   colors[0]     = "0.7 0.8 1.0 1.0";
   colors[1]     = "0.7 0.8 1.0 0.5";
   colors[2]     = "0.7 0.8 1.0 0.0";
   sizes[0]      = 0.5;
   sizes[1]      = 0.5;
   sizes[2]      = 0.8;
   times[0]      = 0.0;
   times[1]      = 0.5;
   times[2]      = 1.0;
};

datablock ParticleEmitterData(MissileMistEmitter)
{
   ejectionPeriodMS = 5;
   periodVarianceMS = 0;
   ejectionVelocity = 6.0;
   velocityVariance = 4.0;
   ejectionOffset   = 0.0;
   thetaMin         = 85;
   thetaMax         = 85;
   phiReferenceVel  = 0;
   phiVariance      = 360;
   overrideAdvances = false;
   lifetimeMS       = 250;
   particles = "MissileMist";
};

datablock ParticleData( MissileSplashParticle )
{
   dragCoefficient      = 1;
   gravityCoefficient   = 0.2;
   inheritedVelFactor   = 0.2;
   constantAcceleration = -0.0;
   lifetimeMS           = 600;
   lifetimeVarianceMS   = 0;
   textureName          = "special/droplet";
   colors[0]     = "0.7 0.8 1.0 1.0";
   colors[1]     = "0.7 0.8 1.0 0.5";
   colors[2]     = "0.7 0.8 1.0 0.0";
   sizes[0]      = 0.5;
   sizes[1]      = 0.5;
   sizes[2]      = 0.5;
   times[0]      = 0.0;
   times[1]      = 0.5;
   times[2]      = 1.0;
};

datablock ParticleEmitterData( MissileSplashEmitter )
{
   ejectionPeriodMS = 1;
   periodVarianceMS = 0;
   ejectionVelocity = 6;
   velocityVariance = 3.0;
   ejectionOffset   = 0.0;
   thetaMin         = 60;
   thetaMax         = 80;
   phiReferenceVel  = 0;
   phiVariance      = 360;
   overrideAdvances = false;
   orientParticles  = true;
   lifetimeMS       = 100;
   particles = "MissileSplashParticle";
};


datablock SplashData(MissileSplash)
{
   numSegments = 15;
   ejectionFreq = 0.0001;
   ejectionAngle = 45;
   ringLifetime = 0.5;
   lifetimeMS = 400;
   velocity = 5.0;
   startRadius = 0.0;
   acceleration = -3.0;
   texWrap = 5.0;

   explosion = MissileSplashExplosion;

   texture = "special/water2";

   emitter[0] = MissileSplashEmitter;
   emitter[1] = MissileMistEmitter;

   colors[0] = "0.7 0.8 1.0 0.0";
   colors[1] = "0.7 0.8 1.0 1.0";
   colors[2] = "0.7 0.8 1.0 0.0";
   colors[3] = "0.7 0.8 1.0 0.0";
   times[0] = 0.0;
   times[1] = 0.4;
   times[2] = 0.8;
   times[3] = 1.0;
};

//--------------------------------------------------------------------------
// Particle effects
//--------------------------------------
datablock ParticleData(MissileSmokeParticle)
{
   dragCoeffiecient     = 0.0;
   gravityCoefficient   = -0.02;
   inheritedVelFactor   = 0.1;

   lifetimeMS           = 1200;
   lifetimeVarianceMS   = 100;

   textureName          = "particleTest";

   useInvAlpha = true;
   spinRandomMin = -90.0;
   spinRandomMax = 90.0;

   colors[0]     = "1.0 0.75 0.0 0.0";
   colors[1]     = "0.5 0.5 0.5 1.0";
   colors[2]     = "0.3 0.3 0.3 0.0";
   sizes[0]      = 1;
   sizes[1]      = 2;
   sizes[2]      = 3;
   times[0]      = 0.0;
   times[1]      = 0.1;
   times[2]      = 1.0;

};

datablock ParticleEmitterData(MissileSmokeEmitter)
{
   ejectionPeriodMS = 10;
   periodVarianceMS = 0;

   ejectionVelocity = 1.5;
   velocityVariance = 0.3;

   thetaMin         = 0.0;
   thetaMax         = 50.0;

   particles = "MissileSmokeParticle";
};

datablock ParticleData(MissileFireParticle)
{
   dragCoeffiecient     = 0.0;
   gravityCoefficient   = 0.0;
   inheritedVelFactor   = 1.0;

   lifetimeMS           = 300;
   lifetimeVarianceMS   = 000;

   textureName          = "particleTest";

   spinRandomMin = -135;
   spinRandomMax =  135;

   colors[0]     = "1.0 0.75 0.2 1.0";
   colors[1]     = "1.0 0.5 0.0 1.0";
   colors[2]     = "1.0 0.40 0.0 0.0";
   sizes[0]      = 0;
   sizes[1]      = 1;
   sizes[2]      = 1.5;
   times[0]      = 0.0;
   times[1]      = 0.3;
   times[2]      = 1.0;
};

datablock ParticleEmitterData(MissileFireEmitter)
{
   ejectionPeriodMS = 15;
   periodVarianceMS = 0;

   ejectionVelocity = 15.0;
   velocityVariance = 0.0;

   thetaMin         = 0.0;
   thetaMax         = 0.0;

   particles = "MissileFireParticle";
};

datablock ParticleData(MissilePuffParticle)
{
   dragCoeffiecient     = 0.0;
   gravityCoefficient   = 0.0;
   inheritedVelFactor   = 0.0;

   lifetimeMS           = 500;
   lifetimeVarianceMS   = 300;

   textureName          = "particleTest";

   spinRandomMin = -135;
   spinRandomMax =  135;

   colors[0]     = "1.0 1.0 1.0 0.5";
   colors[1]     = "0.7 0.7 0.7 0.0";
   sizes[0]      = 0.25;
   sizes[1]      = 1.0;
   times[0]      = 0.0;
   times[1]      = 1.0;
};

datablock ParticleEmitterData(MissilePuffEmitter)
{
   ejectionPeriodMS = 50;
   periodVarianceMS = 3;

   ejectionVelocity = 0.5;
   velocityVariance = 0.0;

   thetaMin         = 0.0;
   thetaMax         = 90.0;

   particles = "MissilePuffParticle";
};

datablock ParticleData(MissileLauncherExhaustParticle)
{
   dragCoeffiecient     = 0.0;
   gravityCoefficient   = 0.01;
   inheritedVelFactor   = 1.0;

   lifetimeMS           = 500;
   lifetimeVarianceMS   = 300;

   textureName          = "particleTest";

   useInvAlpha = true;
   spinRandomMin = -135;
   spinRandomMax =  135;

   colors[0]     = "1.0 1.0 1.0 0.5";
   colors[1]     = "0.7 0.7 0.7 0.0";
   sizes[0]      = 0.25;
   sizes[1]      = 1.0;
   times[0]      = 0.0;
   times[1]      = 1.0;
};

datablock ParticleEmitterData(MissileLauncherExhaustEmitter)
{
   ejectionPeriodMS = 15;
   periodVarianceMS = 0;

   ejectionVelocity = 3.0;
   velocityVariance = 0.0;

   thetaMin         = 0.0;
   thetaMax         = 20.0;

   particles = "MissileLauncherExhaustParticle";
};

datablock AudioProfile(BreacherMExpSound)
{
   filename = "fx/Bonuses/down_passback3_rocket.wav";
   description = AudioBIGExplosion3d;
   preload = true;
};

datablock ShockwaveData(BMissileShockwave)
{
   width = 7.0;
   numSegments = 24;
   numVertSegments = 8;
   velocity = 36;
   acceleration = 36.0;
   lifetimeMS = 500;
   height = 1.3;
   verticalCurve = 0.5;

   mapToTerrain = false;
   renderBottom = true;

   texture[0] = "special/shockwave4";
   texture[1] = "special/gradient";
   texWrap = 6.0;

   times[0] = 0.0;
   times[1] = 0.5;
   times[2] = 1.0;

   colors[0] = "1.0 1.0 1.0 1.0";
   colors[1] = "0.8 0.5 0.2 0.75";
   colors[2] = "0.4 0.25 0.05 0.0";
};

datablock ParticleData( BMissileCrescentParticle )
{
   dragCoefficient      = 2;
   gravityCoefficient   = 0.357;
   inheritedVelFactor   = 0.2;
   constantAcceleration = -20.0;
   lifetimeMS           = 750;
   lifetimeVarianceMS   = 100;
   textureName          = "special/crescent3";
   colors[0]     = "0.75 0.7 0.2 1.0";
   colors[1]     = "0.75 0.7 0.2 0.5";
   colors[2]     = "0.75 0.7 0.2 0.0";
   sizes[0]      = 2.0;
   sizes[1]      = 4.0;
   sizes[2]      = 8.0;
   times[0]      = 0.25;
   times[1]      = 0.5;
   times[2]      = 1.0;
};

datablock ParticleEmitterData( BMissileCrescentEmitter )
{
   ejectionPeriodMS = 10;
   periodVarianceMS = 0;
   ejectionVelocity = 15;
   velocityVariance = 5.0;
   ejectionOffset   = 0.0;
   thetaMin         = 0;
   thetaMax         = 180;
   phiReferenceVel  = 0;
   phiVariance      = 360;
   overrideAdvances = false;
   orientParticles  = true;
   lifetimeMS       = 500;
   particles = "BMissileCrescentParticle";
};

datablock ParticleData(BMissileSparks)
{
   dragCoefficient      = 1;
   gravityCoefficient   = 0.0;
   inheritedVelFactor   = 0.0;
   constantAcceleration = 7.5;
   lifetimeMS           = 750;
   lifetimeVarianceMS   = 250;
   textureName          = "special/bigspark";
   colors[0]     = 211.0/255.0 @ " " @ 215.0/255.0 @ " " @ 120.0/255.0 @ " 1.0";
   colors[1]     = 211.0/255.0 @ " " @ 215.0/255.0 @ " " @ 120.0/255.0 @ " 0.5";
   colors[2]     = 211.0/255.0 @ " " @ 215.0/255.0 @ " " @ 120.0/255.0 @ " 0.0";
   sizes[0]      = 1;
   sizes[1]      = 2;
   sizes[2]      = 3;
   times[0]      = 0.0;
   times[1]      = 0.5;
   times[2]      = 1.0;
};

datablock ParticleEmitterData(MissileSparkEmitter)
{
   ejectionPeriodMS = 5;
   periodVarianceMS = 1;
   ejectionVelocity = 32;
   velocityVariance = 8;
   ejectionOffset   = 0.0;
   thetaMin         = 0;
   thetaMax         = 180;
   phiReferenceVel  = 0;
   phiVariance      = 360;
   overrideAdvances = false;
   orientParticles  = true;
   lifetimeMS       = 640;
   particles = "BMissileSparks";
};

datablock ParticleData(BreacherMExplosionSmoke)
{
   dragCoeffiecient     = 0.4;
   gravityCoefficient   = 0.0;   // rises slowly
   inheritedVelFactor   = 0.0;

   lifetimeMS           = 1250;
   lifetimeVarianceMS   = 500;

//   textureName          = "particleTest";

   useInvAlpha =  false;
   spinRandomMin = -100.0;
   spinRandomMax =  100.0;

   textureName = "special/Smoke/bigSmoke";

   colors[0]     = "1.0 1.0 1.0 1.0";
   colors[1]     = "0.75 0.75 0.75 0.75";
   colors[2]     = "0.5 0.5 0.5 0.5";
   colors[3]     = "0 0 0 0";
   sizes[0]      = 2.0;
   sizes[1]      = 3.0;
   sizes[2]      = 5.0;
   sizes[3]      = 6.0;
   times[0]      = 0.0;
   times[1]      = 0.333;
   times[2]      = 0.666;
   times[3]      = 1.0;
};

datablock ParticleEmitterData(BreacherMExplosionSmokeEmitter)
{
   ejectionPeriodMS = 10;
   periodVarianceMS = 0;

   ejectionOffset = 8.0;


   ejectionVelocity = 1.25;
   velocityVariance = 1.2;

   thetaMin         = 0.0;
   thetaMax         = 90.0;

   lifetimeMS       = 500;

   particles = "BreacherMExplosionSmoke";
};

datablock ExplosionData(BMissileSubExplosion)
{
   explosionShape = "effect_plasma_explosion.dts";
   faceViewer           = true;

   sizes[0] = "4 4 4";
   sizes[1] = "3 3 3";
   times[0] = 0.0;
   times[1] = 1.0;

   delayMS = 75;
   offset = 2.5;
   playSpeed = 0.75;
};

datablock DebrisData(BreacherMDebris)
{
   emitters[0] = MissileSmokeSpikeEmitter;
   explodeOnMaxBounce = true;
   elasticity = 0.4;
   friction = 0.2;
   lifetime = 1.3;
   lifetimeVariance = 0.5;
};

datablock ExplosionData(BreacherMExplosion)
{
   explosionShape = "effect_plasma_explosion.dts";
   playSpeed = 0.75;
   soundProfile = BreacherMExpSound;
   faceViewer = true;

   sizes[0] = "4 4 4";
   sizes[1] = "3 3 3";
   times[0] = 0.0;
   times[1] = 1.0;

   subExplosion[0] = BMissileSubExplosion;
   emitter[0] = BreacherMExplosionSmokeEmitter;
   emitter[1] = BMissileCrescentEmitter;
   emitter[2] = BMissileSparkEmitter;
   shockwave = BMissileShockwave;

   debris = BreacherMDebris;
   debrisThetaMin = 5;
   debrisThetaMax = 85;
   debrisNum = 8;
   debrisNumVariance = 2;
   debrisVelocity = 30.0;
   debrisVelocityVariance = 5.0;

   shakeCamera = true;
   camShakeFreq = "6.0 7.0 7.0";
   camShakeAmp = "70.0 70.0 70.0";
   camShakeDuration = 1.0;
   camShakeRadius = 7.0;
};

//--------------------------------------------------------------------------
// Force-Feedback Effects
//--------------------------------------
datablock EffectProfile(PlasmaSwitchEffect)
{
   effectname = "weapons/plasma_rifle_activate";
   minDistance = 2.5;
   maxDistance = 2.5;
};

datablock EffectProfile(PlasmaFireEffect)
{
   effectname = "weapons/plasma_fire";
   minDistance = 2.5;
   maxDistance = 2.5;
};

datablock EffectProfile(PlasmaDryFireEffect)
{
   effectname = "weapons/plasma_dryfire";
   minDistance = 2.5;
   maxDistance = 2.5;
};

datablock EffectProfile(PlasmaIdleEffect)
{
   effectname = "weapons/plasma_rifle_idle";
   minDistance = 2.5;
};

datablock EffectProfile(PlasmaReloadEffect)
{
   effectname = "weapons/plasma_rifle_reload";
   minDistance = 2.5;
   maxDistance = 2.5;
};

datablock EffectProfile(PlasmaExpEffect)
{
   effectname = "explosions/explosion.xpl10";
   minDistance = 10;
   maxDistance = 25;
};

//--------------------------------------------------------------------------
// Sounds
//--------------------------------------
datablock AudioProfile(PlasmaSwitchSound)
{
   filename    = "fx/weapons/plasma_rifle_activate.wav";
   description = AudioClosest3d;
   preload = true;
   effect = PlasmaSwitchEffect;
};

datablock AudioProfile(PlasmaFireSound)
{
   filename    = "fx/weapons/plasma_rifle_fire.wav";
   description = AudioDefault3d;
   preload = true;
   effect = PlasmaFireEffect;
};

datablock AudioProfile(PlasmaIdleSound)
{
   filename    = "fx/weapons/plasma_rifle_idle.wav";
   description = AudioDefaultLooping3d;
   preload = true;
   //effect = PlasmaIdleEffect;
};

datablock AudioProfile(PlasmaReloadSound)
{
   filename    = "fx/weapons/plasma_rifle_reload.wav";
   description = Audioclosest3d;
   preload = true;
   effect = PlasmaReloadEffect;
};

datablock AudioProfile(plasmaExpSound)
{
   filename    = "fx/explosions/explosion.xpl10.wav";
   description = AudioExplosion3d;
   effect = PlasmaExpEffect;
};

datablock AudioProfile(PlasmaProjectileSound)
{
   filename    = "fx/weapons/plasma_rifle_projectile.WAV";
   description = ProjectileLooping3d;
   preload = true;
};

datablock AudioProfile(PlasmaDryFireSound)
{
   filename    = "fx/weapons/plasma_dryfire.wav";
   description = AudioClose3d;
   preload = true;
   effect = PlasmaDryFireEffect;
};

datablock AudioProfile(PlasmaFireWetSound)
{
   filename    = "fx/weapons/plasma_fizzle.wav";
   description = AudioClose3d;
   preload = true;
};
//--------------------------------------------------------------------------
// Explosion
//--------------------------------------
datablock ParticleData(PlasmaExplosionParticle)
{
   dragCoefficient      = 2;
   gravityCoefficient   = 0.2;
   inheritedVelFactor   = 0.2;
   constantAcceleration = 0.0;
   lifetimeMS           = 750;
   lifetimeVarianceMS   = 150;
   textureName          = "particleTest";
   colors[0]     = "0.56 0.36 0.26 1.0";
   colors[1]     = "0.56 0.36 0.26 0.0";
   sizes[0]      = 0.5;
   sizes[1]      = 2;
};

datablock ParticleEmitterData(PlasmaExplosionEmitter)
{
   ejectionPeriodMS = 7;
   periodVarianceMS = 0;
   ejectionVelocity = 5;
   velocityVariance = 1.0;
   ejectionOffset   = 0.0;
   thetaMin         = 0;
   thetaMax         = 60;
   phiReferenceVel  = 0;
   phiVariance      = 360;
   overrideAdvances = false;
   particles = "PlasmaExplosionParticle";
};

datablock ExplosionData(PlasmaBoltExplosion)
{
   explosionShape = "effect_plasma_explosion.dts";
   soundProfile   = plasmaExpSound;
   particleEmitter = PlasmaExplosionEmitter;
   particleDensity = 150;
   particleRadius = 1.25;
   faceViewer = true;

   sizes[0] = "1.0 1.0 1.0";
   sizes[1] = "1.0 1.0 1.0";
   times[0] = 0.0;
   times[1] = 1.5;
};


//--------------------------------------------------------------------------
// Splash
//--------------------------------------------------------------------------
datablock ParticleData(PlasmaMist)
{
   dragCoefficient      = 2.0;
   gravityCoefficient   = -0.05;
   inheritedVelFactor   = 0.0;
   constantAcceleration = 0.0;
   lifetimeMS           = 400;
   lifetimeVarianceMS   = 100;
   useInvAlpha          = false;
   spinRandomMin        = -90.0;
   spinRandomMax        = 500.0;
   textureName          = "particleTest";
   colors[0]     = "0.7 0.8 1.0 1.0";
   colors[1]     = "0.7 0.8 1.0 0.5";
   colors[2]     = "0.7 0.8 1.0 0.0";
   sizes[0]      = 0.5;
   sizes[1]      = 0.5;
   sizes[2]      = 0.8;
   times[0]      = 0.0;
   times[1]      = 0.5;
   times[2]      = 1.0;
};

datablock ParticleEmitterData(PlasmaMistEmitter)
{
   ejectionPeriodMS = 5;
   periodVarianceMS = 0;
   ejectionVelocity = 3.0;
   velocityVariance = 2.0;
   ejectionOffset   = 0.0;
   thetaMin         = 85;
   thetaMax         = 85;
   phiReferenceVel  = 0;
   phiVariance      = 360;
   overrideAdvances = false;
   lifetimeMS       = 250;
   particles = "PlasmaMist";
};

datablock ParticleData( PlasmaSplashParticle2 )
{

   dragCoeffiecient     = 0.4;
   gravityCoefficient   = -0.03;   // rises slowly
   inheritedVelFactor   = 0.025;

   lifetimeMS           = 600;
   lifetimeVarianceMS   = 300;

   textureName          = "particleTest";

   useInvAlpha =  false;
   spinRandomMin = -200.0;
   spinRandomMax =  200.0;


   colors[0]     = "0.7 0.8 1.0 1.0";
   colors[1]     = "0.7 0.8 1.0 0.5";
   colors[2]     = "0.7 0.8 1.0 0.0";
   sizes[0]      = 0.5;
   sizes[1]      = 1.0;
   sizes[2]      = 2.0;
   times[0]      = 0.0;
   times[1]      = 0.5;
   times[2]      = 1.0;
};

datablock ParticleEmitterData( PlasmaSplashEmitter2 )
{
   ejectionPeriodMS = 25;
   ejectionOffset = 0.2;
   periodVarianceMS = 0;
   ejectionVelocity = 2.25;
   velocityVariance = 0.50;
   thetaMin         = 0.0;
   thetaMax         = 30.0;
   lifetimeMS       = 250;

   particles = "PlasmaSplashParticle2";
};


datablock ParticleData( PlasmaSplashParticle )
{
   dragCoefficient      = 1;
   gravityCoefficient   = 0.2;
   inheritedVelFactor   = 0.2;
   constantAcceleration = -0.0;
   lifetimeMS           = 600;
   lifetimeVarianceMS   = 0;
   textureName          = "special/droplet";
   colors[0]     = "0.7 0.8 1.0 1.0";
   colors[1]     = "0.7 0.8 1.0 0.5";
   colors[2]     = "0.7 0.8 1.0 0.0";
   sizes[0]      = 0.5;
   sizes[1]      = 0.5;
   sizes[2]      = 0.5;
   times[0]      = 0.0;
   times[1]      = 0.5;
   times[2]      = 1.0;
};

datablock ParticleEmitterData( PlasmaSplashEmitter )
{
   ejectionPeriodMS = 1;
   periodVarianceMS = 0;
   ejectionVelocity = 3;
   velocityVariance = 1.0;
   ejectionOffset   = 0.0;
   thetaMin         = 60;
   thetaMax         = 80;
   phiReferenceVel  = 0;
   phiVariance      = 360;
   overrideAdvances = false;
   orientParticles  = true;
   lifetimeMS       = 100;
   particles = "PlasmaSplashParticle";
};


datablock SplashData(PlasmaSplash)
{
   numSegments = 15;
   ejectionFreq = 0.0001;
   ejectionAngle = 45;
   ringLifetime = 0.5;
   lifetimeMS = 400;
   velocity = 5.0;
   startRadius = 0.0;
   acceleration = -3.0;
   texWrap = 5.0;

   texture = "special/water2";

   emitter[0] = PlasmaSplashEmitter;
   emitter[1] = PlasmaSplashEmitter2;
   emitter[2] = PlasmaMistEmitter;

   colors[0] = "0.7 0.8 1.0 0.0";
   colors[1] = "0.7 0.8 1.0 1.0";
   colors[2] = "0.7 0.8 1.0 0.0";
   colors[3] = "0.7 0.8 1.0 0.0";
   times[0] = 0.0;
   times[1] = 0.4;
   times[2] = 0.8;
   times[3] = 1.0;
};

//--------------------------------------------------------------------------
// Force-Feedback Effects
//--------------------------------------
datablock EffectProfile(ChaingunSwitchEffect)
{
   effectname = "weapons/chaingun_activate";
   minDistance = 2.5;
   maxDistance = 2.5;
};

datablock EffectProfile(ChaingunFireEffect)
{
   effectname = "weapons/chaingun_fire";
   minDistance = 2.5;
   maxDistance = 2.5;
};

datablock EffectProfile(ChaingunSpinUpEffect)
{
   effectname = "weapons/chaingun_spinup";
   minDistance = 2.5;
   maxDistance = 2.5;
};

datablock EffectProfile(ChaingunSpinDownEffect)
{
   effectname = "weapons/chaingun_spindown";
   minDistance = 2.5;
   maxDistance = 2.5;
};

datablock EffectProfile(ChaingunDryFire)
{
   effectname = "weapons/chaingun_dryfire";
   minDistance = 2.5;
   maxDistance = 2.5;
};

//--------------------------------------------------------------------------
// Sounds
//--------------------------------------
datablock AudioProfile(ChaingunSwitchSound)
{
   filename    = "fx/weapons/chaingun_activate.wav";
   description = AudioClosest3d;
   preload = true;
   effect = ChaingunSwitchEffect;
};

datablock AudioProfile(ChaingunFireSound)
{
   filename    = "fx/weapons/chaingun_fire.wav";
   description = AudioDefaultLooping3d;
   preload = true;
   effect = ChaingunFireEffect;
};

datablock AudioProfile(ChaingunProjectile)
{
   filename    = "fx/weapons/chaingun_projectile.wav";
   description = ProjectileLooping3d;
   preload = true;
};

datablock AudioProfile(ChaingunImpact)
{
   filename    = "fx/weapons/chaingun_impact.WAV";
   description = AudioClosest3d;
   preload = true;
};

datablock AudioProfile(ChaingunSpinDownSound)
{
   filename    = "fx/weapons/chaingun_spindown.wav";
   description = AudioClosest3d;
   preload = true;
   effect = ChaingunSpinDownEffect;
};

datablock AudioProfile(ChaingunSpinUpSound)
{
   filename    = "fx/weapons/chaingun_spinup.wav";
   description = AudioClosest3d;
   preload = true;
   effect = ChaingunSpinUpEffect;
};

datablock AudioProfile(ChaingunDryFireSound)
{
   filename    = "fx/weapons/chaingun_dryfire.wav";
   description = AudioClose3d;
   preload = true;
   effect = ChaingunDryFire;
};

datablock AudioProfile(ShrikeBlasterProjectileSound)
{
   filename    = "fx/vehicles/shrike_blaster_projectile.wav";
   description = ProjectileLooping3d;
   preload = true;
};

//--------------------------------------------------------------------------
// Splash
//--------------------------------------------------------------------------

datablock ParticleData( ChaingunSplashParticle )
{
   dragCoefficient      = 1;
   gravityCoefficient   = 0.0;
   inheritedVelFactor   = 0.2;
   constantAcceleration = -1.4;
   lifetimeMS           = 300;
   lifetimeVarianceMS   = 0;
   textureName          = "special/droplet";
   colors[0]     = "0.7 0.8 1.0 1.0";
   colors[1]     = "0.7 0.8 1.0 0.5";
   colors[2]     = "0.7 0.8 1.0 0.0";
   sizes[0]      = 0.05;
   sizes[1]      = 0.2;
   sizes[2]      = 0.2;
   times[0]      = 0.0;
   times[1]      = 0.5;
   times[2]      = 1.0;
};

datablock ParticleEmitterData( ChaingunSplashEmitter )
{
   ejectionPeriodMS = 4;
   periodVarianceMS = 0;
   ejectionVelocity = 3;
   velocityVariance = 1.0;
   ejectionOffset   = 0.0;
   thetaMin         = 0;
   thetaMax         = 50;
   phiReferenceVel  = 0;
   phiVariance      = 360;
   overrideAdvances = false;
   orientParticles  = true;
   lifetimeMS       = 100;
   particles = "ChaingunSplashParticle";
};

datablock SplashData(ChaingunSplash)
{
   numSegments = 10;
   ejectionFreq = 10;
   ejectionAngle = 20;
   ringLifetime = 0.4;
   lifetimeMS = 400;
   velocity = 3.0;
   startRadius = 0.0;
   acceleration = -3.0;
   texWrap = 5.0;

   texture = "special/water2";

   emitter[0] = ChaingunSplashEmitter;

   colors[0] = "0.7 0.8 1.0 0.0";
   colors[1] = "0.7 0.8 1.0 1.0";
   colors[2] = "0.7 0.8 1.0 0.0";
   colors[3] = "0.7 0.8 1.0 0.0";
   times[0] = 0.0;
   times[1] = 0.4;
   times[2] = 0.8;
   times[3] = 1.0;
};

//--------------------------------------------------------------------------
// Particle Effects
//--------------------------------------
datablock ParticleData(ChaingunFireParticle)
{
   dragCoefficient      = 2.75;
   gravityCoefficient   = 0.1;
   inheritedVelFactor   = 0.0;
   constantAcceleration = 0.0;
   lifetimeMS           = 550;
   lifetimeVarianceMS   = 0;
   textureName          = "particleTest";
   colors[0]     = "0.46 0.36 0.26 1.0";
   colors[1]     = "0.46 0.36 0.26 0.0";
   sizes[0]      = 0.25;
   sizes[1]      = 0.20;
};

datablock ParticleEmitterData(ChaingunFireEmitter)
{
   ejectionPeriodMS = 6;
   periodVarianceMS = 0;
   ejectionVelocity = 10;
   velocityVariance = 1.0;
   ejectionOffset   = 0.0;
   thetaMin         = 0;
   thetaMax         = 12;
   phiReferenceVel  = 0;
   phiVariance      = 360;
   overrideAdvance  = true;
   particles = "ChaingunFireParticle";
};

//--------------------------------------------------------------------------
// Explosions
//--------------------------------------
datablock ParticleData(ChaingunExplosionParticle1)
{
   dragCoefficient      = 0.65;
   gravityCoefficient   = 0.3;
   inheritedVelFactor   = 0.0;
   constantAcceleration = 0.0;
   lifetimeMS           = 500;
   lifetimeVarianceMS   = 150;
   textureName          = "particleTest";
   colors[0]     = "0.56 0.36 0.26 1.0";
   colors[1]     = "0.56 0.36 0.26 0.0";
   sizes[0]      = 0.0625;
   sizes[1]      = 0.2;
};

datablock ParticleEmitterData(ChaingunExplosionEmitter)
{
   ejectionPeriodMS = 10;
   periodVarianceMS = 0;
   ejectionVelocity = 0.75;
   velocityVariance = 0.25;
   ejectionOffset   = 0.0;
   thetaMin         = 0;
   thetaMax         = 60;
   phiReferenceVel  = 0;
   phiVariance      = 360;
   overrideAdvances = false;
   particles = "ChaingunExplosionParticle1";
};

datablock ParticleData(ChaingunImpactSmokeParticle)
{
   dragCoefficient      = 0.0;
   gravityCoefficient   = -0.2;
   inheritedVelFactor   = 0.0;
   constantAcceleration = 0.0;
   lifetimeMS           = 1000;
   lifetimeVarianceMS   = 200;
   useInvAlpha          = true;
   spinRandomMin        = -90.0;
   spinRandomMax        = 90.0;
   textureName          = "particleTest";
   colors[0]     = "0.7 0.7 0.7 0.0";
   colors[1]     = "0.7 0.7 0.7 0.4";
   colors[2]     = "0.7 0.7 0.7 0.0";
   sizes[0]      = 0.5;
   sizes[1]      = 0.5;
   sizes[2]      = 1.0;
   times[0]      = 0.0;
   times[1]      = 0.5;
   times[2]      = 1.0;
};

datablock ParticleEmitterData(ChaingunImpactSmoke)
{
   ejectionPeriodMS = 8;
   periodVarianceMS = 1;
   ejectionVelocity = 1.0;
   velocityVariance = 0.5;
   ejectionOffset   = 0.0;
   thetaMin         = 0;
   thetaMax         = 35;
   overrideAdvances = false;
   particles = "ChaingunImpactSmokeParticle";
   lifetimeMS       = 50;
};

datablock ParticleData(ChaingunSparks)
{
   dragCoefficient      = 1;
   gravityCoefficient   = 0.0;
   inheritedVelFactor   = 0.2;
   constantAcceleration = 0.0;
   lifetimeMS           = 300;
   lifetimeVarianceMS   = 0;
   textureName          = "special/spark00";
   colors[0]     = "0.56 0.36 0.26 1.0";
   colors[1]     = "0.56 0.36 0.26 1.0";
   colors[2]     = "1.0 0.36 0.26 0.0";
   sizes[0]      = 0.6;
   sizes[1]      = 0.2;
   sizes[2]      = 0.05;
   times[0]      = 0.0;
   times[1]      = 0.2;
   times[2]      = 1.0;

};

datablock ParticleEmitterData(ChaingunSparkEmitter)
{
   ejectionPeriodMS = 4;
   periodVarianceMS = 0;
   ejectionVelocity = 4;
   velocityVariance = 2.0;
   ejectionOffset   = 0.0;
   thetaMin         = 0;
   thetaMax         = 50;
   phiReferenceVel  = 0;
   phiVariance      = 360;
   overrideAdvances = false;
   orientParticles  = true;
   lifetimeMS       = 100;
   particles = "ChaingunSparks";
};

datablock ExplosionData(ChaingunExplosion)
{
//   soundProfile   = ChaingunImpact;

   emitter[0] = ChaingunImpactSmoke;
   emitter[1] = ChaingunSparkEmitter;

   faceViewer           = false;
};

datablock ShockwaveData(ScoutChaingunHit)
{
   width = 0.5;
   numSegments = 13;
   numVertSegments = 1;
   velocity = 0.5;
   acceleration = 2.0;
   lifetimeMS = 900;
   height = 0.1;
   verticalCurve = 0.5;

   mapToTerrain = false;
   renderBottom = false;
   orientToNormal = true;

   texture[0] = "special/shockwave5";
   texture[1] = "special/gradient";
   texWrap = 3.0;

   times[0] = 0.0;
   times[1] = 0.5;
   times[2] = 1.0;

   colors[0] = "0.6 0.6 1.0 1.0";
   colors[1] = "0.6 0.3 1.0 0.5";
   colors[2] = "0.0 0.0 1.0 0.0";
};

datablock ParticleData(ScoutChaingunExplosionParticle1)
{
   dragCoefficient      = 2;
   gravityCoefficient   = 0.0;
   inheritedVelFactor   = 0.2;
   constantAcceleration = -0.0;
   lifetimeMS           = 600;
   lifetimeVarianceMS   = 000;
   textureName          = "special/crescent4";
   colors[0] = "0.6 0.6 1.0 1.0";
   colors[1] = "0.6 0.3 1.0 1.0";
   colors[2] = "0.0 0.0 1.0 0.0";
   sizes[0]      = 0.25;
   sizes[1]      = 0.5;
   sizes[2]      = 1.0;
   times[0]      = 0.0;
   times[1]      = 0.5;
   times[2]      = 1.0;
};

datablock ParticleEmitterData(ScoutChaingunExplosionEmitter)
{
   ejectionPeriodMS = 10;
   periodVarianceMS = 0;
   ejectionVelocity = 2;
   velocityVariance = 1.5;
   ejectionOffset   = 0.0;
   thetaMin         = 80;
   thetaMax         = 90;
   phiReferenceVel  = 0;
   phiVariance      = 360;
   overrideAdvances = false;
   orientParticles  = true;
   lifetimeMS       = 200;
   particles = "ScoutChaingunExplosionParticle1";
};

datablock ExplosionData(ScoutChaingunExplosion)
{
   soundProfile   = blasterExpSound;
   shockwave = ScoutChaingunHit;
   emitter[0] = ScoutChaingunExplosionEmitter;
};

//--------------------------------------------------------------------------
// Particle effects
//--------------------------------------

datablock DebrisData( ShellDebris )
{
   shapeName = "weapon_chaingun_ammocasing.dts";

   lifetime = 3.0;

   minSpinSpeed = 300.0;
   maxSpinSpeed = 400.0;

   elasticity = 0.5;
   friction = 0.2;

   numBounces = 3;

   fade = true;
   staticOnMaxBounce = true;
   snapOnMaxBounce = true;
};

//--------------------------------------------------------------------------
// Projectile
//--------------------------------------
datablock DecalData(ChaingunDecal1)
{
   sizeX       = 0.05;
   sizeY       = 0.05;
   textureName = "special/bullethole1";
};

datablock DecalData(ChaingunDecal2) : ChaingunDecal1
{
   textureName = "special/bullethole2";
};

datablock DecalData(ChaingunDecal3) : ChaingunDecal1
{
   textureName = "special/bullethole3";
};

datablock DecalData(ChaingunDecal4) : ChaingunDecal1
{
   textureName = "special/bullethole4";
};

datablock DecalData(ChaingunDecal5) : ChaingunDecal1
{
   textureName = "special/bullethole5";
};

datablock DecalData(ChaingunDecal6) : ChaingunDecal1
{
   textureName = "special/bullethole6";
};


datablock AudioProfile(DeployablesExplosionSound)
{
   filename = "fx/explosions/deployables_explosion.wav";
   description = AudioExplosion3d;
   preload = true;
};

datablock AudioProfile(MeteorExplosionSound)
{
   filename    = "fx/Bonuses/upward_passback1_bomb.wav";
   description = AudioBIGExplosion3d;
   preload = true;
};

datablock AudioProfile(HeavyPlasmaExpSound)
{
   filename    = "fx/powered/turret_plasma_explode.wav";
   description = AudioDefault3d;
   preload = true;
};

datablock ExplosionData(SmallMiscExplosion)
{
   explosionShape = "effect_plasma_explosion.dts";
   playSpeed      = 0.5;
   soundProfile   = DeployablesExplosionSound;
   faceViewer     = true;

   emitter[0] = MineExplosionSmokeEmitter;
   emitter[1] = MineCrescentEmitter;

   shakeCamera = false;
   camShakeFreq = "8.0 7.0 9.0";
   camShakeAmp = "10.0 10.0 10.0";
   camShakeDuration = 1.0;
   camShakeRadius = 15.0;
};

datablock ExplosionData(UnderwaterSmallMiscExplosion)
{
   explosionShape = "disc_explosion.dts";
   playSpeed      = 1.0;
   sizes[0] = "0.4 0.4 0.4";
   sizes[1] = "0.4 0.4 0.4";
   soundProfile   = DeployablesExplosionSound;
   faceViewer     = true;

   emitter[0] = UnderwaterMineExplosionSmokeEmitter;
   emitter[1] = UnderwaterMineCrescentEmitter;
   emitter[2] = MineExplosionBubbleEmitter;

   shakeCamera = false;
   camShakeFreq = "8.0 7.0 9.0";
   camShakeAmp = "10.0 10.0 10.0";
   camShakeDuration = 1.0;
   camShakeRadius = 10.0;
};

datablock ShockwaveData(MagIonOLShockwave)
{
   width = 8.0;
   numSegments = 32;
   numVertSegments = 6;
   velocity = 30;
   acceleration = 50.0;
   lifetimeMS = 1250;
   height = 1.0;
   verticalCurve = 0.5;
   is2D = false;

   texture[0] = "special/shockwave4";
   texture[1] = "special/gradient";
   texWrap = 6.0;

   times[0] = 0.0;
   times[1] = 0.5;
   times[2] = 1.0;

   colors[0] = "1 1 1 0.50";
   colors[1] = "1 1 1 0.25";
   colors[2] = "1 1 1 0.0";

   mapToTerrain = false;
   orientToNormal = true;
   renderBottom = true;
};

datablock ExplosionData(MagIonOLExplosion)
{
   soundProfile   = DreadnoughtExplodeSound;

   shockwave = MagIonOLShockwave;

   emitter[0] = MortarExplosionSmokeEmitter;
   emitter[1] = MortarCrescentEmitter;

   subExplosion[0] = ImpactSubExplosion1;
   subExplosion[1] = ImpactSubExplosion2;
   subExplosion[2] = ImpactSubExplosion3;

   debris = MortarDebris;
   debrisThetaMin = 10;
   debrisThetaMax = 50;
   debrisNum = 8;
   debrisVelocity = 26.0;
   debrisVelocityVariance = 7.0;

   shakeCamera = true;
   camShakeFreq = "8.0 9.0 7.0";
   camShakeAmp = "100.0 100.0 100.0";
   camShakeDuration = 1.3;
   camShakeRadius = 25.0;
};

datablock ExplosionData(MagIonOLUnderwaterExplosion)
{
   soundProfile   = DreadnoughtExplodeSound;

   shockwave = UnderwaterMortarShockwave;
   shockwaveOnTerrain = true;

   subExplosion[0] = UnderwaterMortarSubExplosion1;
   subExplosion[1] = UnderwaterMortarSubExplosion2;
   subExplosion[2] = UnderwaterMortarSubExplosion3;

   emitter[0] = MortarExplosionBubbleEmitter;
   emitter[1] = UnderwaterExplosionSparksEmitter;

   shakeCamera = true;
   camShakeFreq = "8.0 9.0 7.0";
   camShakeAmp = "100.0 100.0 100.0";
   camShakeDuration = 1.3;
   camShakeRadius = 25.0;
};

datablock ExplosionData(TurbochargerOLExplosion)
{
   soundProfile   = TurbochargerExplodeSound;

   shockwave = MagIonOLShockwave;

   emitter[0] = MortarExplosionSmokeEmitter;
   emitter[1] = MortarCrescentEmitter;

   subExplosion[0] = ImpactSubExplosion1;
   subExplosion[1] = ImpactSubExplosion2;
   subExplosion[2] = ImpactSubExplosion3;

   debris = MortarDebris;
   debrisThetaMin = 10;
   debrisThetaMax = 50;
   debrisNum = 8;
   debrisVelocity = 26.0;
   debrisVelocityVariance = 7.0;

   shakeCamera = true;
   camShakeFreq = "8.0 9.0 7.0";
   camShakeAmp = "100.0 100.0 100.0";
   camShakeDuration = 1.3;
   camShakeRadius = 25.0;
};

datablock LinearFlareProjectileData(MagIonOLAreaCharge)
{
   projectileShapeName = "turret_muzzlepoint.dts";
   faceViewer          = false;
   directDamage        = 0.0;
   hasDamageRadius     = true;
   indirectDamage      = 0.3;
   damageRadius        = 6.0;
   radiusDamageType    = $DamageType::DreadnoughtOL;
   kickBackStrength    = 0;

   flags               = $Projectile::PlayerFragment;
   ticking             = false;
   headshotMultiplier  = 1.25;

   explosion           = "SmallMiscExplosion";
   underwaterExplosion = "UnderwaterSmallMiscExplosion";

   splash              = MissileSplash;
   velInheritFactor    = 0;

   dryVelocity       = 0.1;
   wetVelocity       = 0.1;
   fizzleTimeMS      = 35;
   lifetimeMS        = 35;
   explodeOnDeath    = true;
   reflectOnWaterImpactAngle = 90;
   explodeOnWaterImpact      = false;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 35;

   activateDelayMS = -1;

   numFlares         = 0;
   flareColor        = "0 0 0";
   flareModTexture   = "flaremod";
   flareBaseTexture  = "flarebase";
};

datablock LinearFlareProjectileData(MagIonOLDeathCharge)
{
   projectileShapeName = "turret_muzzlepoint.dts";
   faceViewer          = false;
   directDamage        = 0.0;
   hasDamageRadius     = true;
   indirectDamage      = 4.0;
   damageRadius        = 20.0;
   radiusDamageType    = $DamageType::Reactor;
   kickBackStrength    = 5000;

   flags               = $Projectile::DreadnoughtOL;
   ticking             = false;
   headshotMultiplier  = 1.25;

   explosion           = "MagIonOLExplosion";
   underwaterExplosion = "MagIonOLUnderwaterExplosion";

   splash              = MissileSplash;
   velInheritFactor    = 0;

   dryVelocity       = 0.1;
   wetVelocity       = 0.1;
   fizzleTimeMS      = 35;
   lifetimeMS        = 35;
   explodeOnDeath    = true;
   reflectOnWaterImpactAngle = 90;
   explodeOnWaterImpact      = false;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 35;

   activateDelayMS = -1;

   numFlares         = 0;
   flareColor        = "0 0 0";
   flareModTexture   = "flaremod";
   flareBaseTexture  = "flarebase";
};

datablock LinearFlareProjectileData(TurbochargerDeathCharge)
{
   projectileShapeName = "turret_muzzlepoint.dts";
   faceViewer          = false;
   directDamage        = 0.0;
   hasDamageRadius     = true;
   indirectDamage      = 2.0;
   damageRadius        = 15.0;
   radiusDamageType    = $DamageType::TurbochargerOL;
   kickBackStrength    = 1000;

   flags               = $Projectile::PlayerFragment;
   ticking             = false;
   headshotMultiplier  = 1.0;

   explosion           = "TurbochargerOLExplosion";
   underwaterExplosion = "TurbochargerOLExplosion";

   radialStatusEffect  = "EMPulseEffect";
   statusEffectChance  = 0.9;
   statusEffectMask    = $TypeMasks::PlayerObjectType | $TypeMasks::VehicleObjectType | $TypeMasks::StationObjectType | $TypeMasks::GeneratorObjectType | $TypeMasks::SensorObjectType | $TypeMasks::TurretObjectType;

   splash              = MissileSplash;
   velInheritFactor    = 0;

   dryVelocity       = 0.1;
   wetVelocity       = 0.1;
   fizzleTimeMS      = 35;
   lifetimeMS        = 35;
   explodeOnDeath    = true;
   reflectOnWaterImpactAngle = 90;
   explodeOnWaterImpact      = false;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 35;

   activateDelayMS = -1;

   numFlares         = 0;
   flareColor        = "0 0 0";
   flareModTexture   = "flaremod";
   flareBaseTexture  = "flarebase";
};

datablock LinearFlareProjectileData(StarHammerAreaCharge)
{
   projectileShapeName = "turret_muzzlepoint.dts";
   faceViewer          = false;
   directDamage        = 0.0;
   hasDamageRadius     = true;
   indirectDamage      = 0.2;
   damageRadius        = 8.0;
   radiusDamageType    = $DamageType::StarHammer;
   kickBackStrength    = 0;

   mdEnable            = true;
   mdDeathMessageSet   = $DamageType::StarHammer;
   mdDamageTypeCount   = 1;
   mdDamageType[0]     = $DamageGroupMask::Explosive;
   mdDamageAmount[0]   = 25;
   mdDamageRadius[0]   = true;

   flags               = $Projectile::PlayerFragment | $Projectile::PlaysHitSound;
   ticking             = false;
   headshotMultiplier  = 1.0;

   explosion           = "SmallMiscExplosion";
   underwaterExplosion = "UnderwaterSmallMiscExplosion";

   splash              = MissileSplash;
   velInheritFactor    = 0;

   dryVelocity       = 0.1;
   wetVelocity       = 0.1;
   fizzleTimeMS      = 35;
   lifetimeMS        = 35;
   explodeOnDeath    = true;
   reflectOnWaterImpactAngle = 90;
   explodeOnWaterImpact      = false;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 35;

   activateDelayMS = -1;

   numFlares         = 0;
   flareColor        = "0 0 0";
   flareModTexture   = "flaremod";
   flareBaseTexture  = "flarebase";
};

datablock ParticleData(ExtendedAAExplosionParticle1)
{
   dragCoefficient      = 2;
   gravityCoefficient   = 0.0;
   inheritedVelFactor   = 0.2;
   constantAcceleration = -0.0;
   lifetimeMS           = 600;
   lifetimeVarianceMS   = 000;
   textureName          = "special/crescent4";
   olors[0]     = "0.8 0.5 0.5 0.5";
   colors[1]     = "0.7 0.4 0.4 0.8";
   colors[2]     = "0.6 0.3 0.3 0.0";
   sizes[0]      = 1.0;
   sizes[1]      = 2.0;
   sizes[2]      = 4.5;
   times[0]      = 0.0;
   times[1]      = 0.5;
   times[2]      = 1.0;
};

datablock ParticleEmitterData(ExtendedAAExplosionEmitter)
{
   ejectionPeriodMS = 7;
   periodVarianceMS = 0;
   ejectionVelocity = 2;
   velocityVariance = 1.5;
   ejectionOffset   = 0.0;
   thetaMin         = 70;
   thetaMax         = 80;
   phiReferenceVel  = 0;
   phiVariance      = 360;
   overrideAdvances = false;
   orientParticles  = true;
   lifetimeMS       = 200;
   particles = "ExtendedAAExplosionParticle1";
};

datablock ParticleData(ExtendedAAExplosionParticle2)
{
   dragCoefficient      = 2;
   gravityCoefficient   = 0.0;
   inheritedVelFactor   = 0.2;
   constantAcceleration = -0.0;
   lifetimeMS           = 600;
   lifetimeVarianceMS   = 000;
   textureName          = "special/blasterHit";
   olors[0]     = "0.8 0.5 0.5 0.5";
   colors[1]     = "0.7 0.4 0.4 0.8";
   colors[2]     = "0.6 0.3 0.3 0.0";
   sizes[0]      = 1.0;
   sizes[1]      = 2.0;
   sizes[2]      = 4.5;
   times[0]      = 0.0;
   times[1]      = 0.5;
   times[2]      = 1.0;
};

datablock ParticleEmitterData(ExtendedAAExplosionEmitter2)
{
   ejectionPeriodMS = 30;
   periodVarianceMS = 0;
   ejectionVelocity = 1;
   velocityVariance = 0.0;
   ejectionOffset   = 0.0;
   thetaMin         = 0;
   thetaMax         = 80;
   phiReferenceVel  = 0;
   phiVariance      = 360;
   overrideAdvances = false;
   orientParticles  = false;
   lifetimeMS       = 200;
   particles = "ExtendedAAExplosionParticle2";
};

datablock ExplosionData(ExtendedAAExplosion)
{
   soundProfile   = TurboBlasterExpSound;
   emitter[0]     = ExtendedAAExplosionEmitter;
   emitter[1]     = ExtendedAAExplosionEmitter2;
};

datablock ShockwaveData(MeteorTrailShockwave)
{
   width = 2.5;
   numSegments = 16;
   numVertSegments = 6;
   velocity = 10;
   acceleration = -20;
   lifetimeMS = 500;
   height = 1.0;
   verticalCurve = 0.5;
//   is2D = true;

   texture[0] = "special/shockwave4";
   texture[1] = "special/gradient";
   texWrap = 6.0;

   times[0] = 0.0;
   times[1] = 0.5;
   times[2] = 1.0;

   colors[0] = "1.0 1.0 1.0 0.75";
   colors[1] = "1.0 1.0 0.5 0.5";
   colors[2] = "1.0 1.0 0.1 0.0";

   mapToTerrain = false;
   orientToNormal = true;
   renderBottom = true;
};

datablock ExplosionData(MeteorTrailExplosion)
{
   shockwave = MeteorTrailShockwave;

   faceViewer = true;
};

datablock LinearFlareProjectileData(MeteorTrailCharge)
{
   projectileShapeName = "turret_muzzlePoint.dts";
   faceViewer          = false;
   directDamage        = 0;
   hasDamageRadius     = true;
   indirectDamage      = 0.0001;
   damageRadius        = 1;
   radiusDamageType    = $DamageType::Default;
   kickBackStrength    = 750;

   explosion           = "MeteorTrailExplosion";

   splash              = MissileSplash;
   velInheritFactor    = 0;

   dryVelocity       = 0.1;
   wetVelocity       = 0.1;
   fizzleTimeMS      = 35;
   lifetimeMS        = 35;
   explodeOnDeath    = true;
   reflectOnWaterImpactAngle = 90;
   explodeOnWaterImpact      = false;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 35;

   activateDelayMS = -1;

   numFlares         = 0;
   flareColor        = "0 0 0";
   flareModTexture   = "flaremod";
   flareBaseTexture  = "flarebase";
};

datablock ShockwaveData(BluePowerShockwave)
{
   width = 5.0;
   numSegments = 32;
   numVertSegments = 6;
   velocity = 15;
   acceleration = -30;
   lifetimeMS = 500;
   height = 0.75;
   verticalCurve = 0.4;
//   is2D = true;

   texture[0] = "special/shockwave4";
   texture[1] = "special/gradient";
   texWrap = 6.0;

   times[0] = 0.0;
   times[1] = 0.5;
   times[2] = 1.0;

   colors[0] = "0.3 0.8 1.0 0.75";
   colors[1] = "0.3 0.4 1.0 0.5";
   colors[2] = "0.3 0.2 1.0 0.0";

   mapToTerrain = false;
   orientToNormal = true;
   renderBottom = true;
};

datablock ExplosionData(BluePowerDisplayExplosion)
{
//   soundProfile   = MeteorShockwaveSound;
   shockwave = BluePowerShockwave;

   faceViewer = true;
};

datablock LinearFlareProjectileData(BluePowerDisplayCharge)
{
   projectileShapeName = "turret_muzzlePoint.dts";
   faceViewer          = false;
   directDamage        = 0;
   hasDamageRadius     = true;
   indirectDamage      = 0.0001;
   damageRadius        = 5;
   radiusDamageType    = $DamageType::Default;
   kickBackStrength    = 1250;

   explosion           = "BluePowerDisplayExplosion";

   splash              = MissileSplash;
   velInheritFactor    = 0;

   dryVelocity       = 0.1;
   wetVelocity       = 0.1;
   fizzleTimeMS      = 35;
   lifetimeMS        = 35;
   explodeOnDeath    = true;
   reflectOnWaterImpactAngle = 90;
   explodeOnWaterImpact      = false;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 35;

   activateDelayMS = -1;

   numFlares         = 0;
   flareColor        = "0 0 0";
   flareModTexture   = "flaremod";
   flareBaseTexture  = "flarebase";
};

datablock ParticleData(MeteorExplosionParticle)
{
   dragCoefficient      = 2;
   gravityCoefficient   = 0.0;
   inheritedVelFactor   = 0.2;
   constantAcceleration = 0.0;
   lifetimeMS           = 750;
   lifetimeVarianceMS   = 150;
   textureName          = "particleTest";
   colors[0]     = "0.15 0.2 1.0 1.0";
   colors[1]     = "0.6 0.1 1.0 0.0";
   sizes[0]      = 2;
   sizes[1]      = 5;
};

datablock ParticleEmitterData(MeteorExplosionEmitter)
{
   ejectionPeriodMS = 7;
   periodVarianceMS = 0;
   ejectionVelocity = 5;
   velocityVariance = 1.0;
   ejectionOffset   = 0.0;
   thetaMin         = 0;
   thetaMax         = 60;
   phiReferenceVel  = 0;
   phiVariance      = 360;
   overrideAdvances = false;
   particles = "MeteorExplosionParticle";
};

datablock ParticleData(MeteorTrailParticle)
{
   dragCoefficient      = 2.75;
   gravityCoefficient   = 0.1;
   inheritedVelFactor   = 0.2;
   constantAcceleration = 0.0;
   lifetimeMS           = 900;
   lifetimeVarianceMS   = 0;
   textureName          = "particleTest";
   colors[0]     = "0.15 0.2 1.0 1.0";
   colors[1]     = "0.6 0.1 1.0 0.0";
   sizes[0]      = 2.0;
   sizes[1]      = 1.5;
};

datablock ParticleEmitterData(MeteorTrailEmitter)
{
   ejectionPeriodMS = 10;
   periodVarianceMS = 0;
   ejectionVelocity = 10;
   velocityVariance = 1.0;
   ejectionOffset   = 0.0;
   thetaMin         = 0;
   thetaMax         = 12;
   phiReferenceVel  = 0;
   phiVariance      = 360;
   overrideAdvance  = true;
   particles = "MeteorTrailParticle";
};

datablock ExplosionData(MeteorExplosion)
{
   explosionShape = "disc_explosion.dts";
   soundProfile   = MeteorExplosionSound;

   shakeCamera = true;
   camShakeFreq = "8.0 9.0 7.0";
   camShakeAmp = "25.0 25.0 25.0";
   camShakeDuration = 1.3;
   camShakeRadius = 25.0;

   particleEmitter = MeteorExplosionEmitter;
   particleDensity = 150;
   particleRadius = 3.5;
   faceViewer = true;
};

datablock ShockwaveData(PowerShockwave)
{
   width = 5.0;
   numSegments = 32;
   numVertSegments = 6;
   velocity = 15;
   acceleration = -30;
   lifetimeMS = 500;
   height = 0.75;
   verticalCurve = 0.4;
//   is2D = true;

   texture[0] = "special/shockwave4";
   texture[1] = "special/gradient";
   texWrap = 6.0;

   times[0] = 0.0;
   times[1] = 0.5;
   times[2] = 1.0;

   colors[0] = "1.0 1.0 1.0 0.75";
   colors[1] = "1.0 1.0 0.5 0.5";
   colors[2] = "1.0 1.0 0.1 0.0";

   mapToTerrain = false;
   orientToNormal = true;
   renderBottom = true;
};

datablock ExplosionData(PowerDisplayExplosion)
{
   soundProfile   = plasmaExpSound;
   shockwave = PowerShockwave;

   faceViewer = true;
};

datablock LinearFlareProjectileData(PowerDisplayCharge)
{
   projectileShapeName = "turret_muzzlePoint.dts";
   faceViewer          = false;
   directDamage        = 0;
   hasDamageRadius     = true;
   indirectDamage      = 0.0001;
   damageRadius        = 5;
   radiusDamageType    = $DamageType::Default;
   kickBackStrength    = 1250;

   explosion           = "PowerDisplayExplosion";

   splash              = MissileSplash;
   velInheritFactor    = 0;

   dryVelocity       = 0.1;
   wetVelocity       = 0.1;
   fizzleTimeMS      = 35;
   lifetimeMS        = 35;
   explodeOnDeath    = true;
   reflectOnWaterImpactAngle = 90;
   explodeOnWaterImpact      = false;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 35;

   activateDelayMS = -1;

   numFlares         = 0;
   flareColor        = "0 0 0";
   flareModTexture   = "flaremod";
   flareBaseTexture  = "flarebase";
};

datablock LinearFlareProjectileData(MeteorCannonBlast)
{
   scale               = "4.5 4.5 4.5";
   faceViewer          = true;
   directDamage        = 0;
   hasDamageRadius     = true;
   indirectDamage      = 2.5;
   damageRadius        = 6.0;
   kickBackStrength    = 3000;
   radiusDamageType    = $DamageType::MeteorCannon;

   mdEnable            = true;
   mdDeathMessageSet   = $DamageType::MeteorCannon;
   mdDamageTypeCount   = 2;
   mdDamageType[0]     = $DamageGroupMask::Energy;
   mdDamageAmount[0]   = 250;
   mdDamageRadius[0]   = false;
   mdDamageType[1]     = $DamageGroupMask::Kinetic;
   mdDamageAmount[1]   = 100;
   mdDamageRadius[1]   = false;

   flags               = $Projectile::CanHeadshot | $Projectile::CountMAs | $Projectile::PlaysHitSound;
   ticking             = true;
   headshotMultiplier  = 1.5;

   explosion           = "MeteorExplosion";
   underwaterExplosion = "MeteorExplosion";
   splash              = PlasmaSplash;
   baseEmitter         = MeteorTrailEmitter;

   dryVelocity       = 237.0;
   wetVelocity       = 237.0;
   velInheritFactor  = 1.0;
   fizzleTimeMS      = 2000;
   lifetimeMS        = 2000;
   explodeOnDeath    = false;
   reflectOnWaterImpactAngle = 0.0;
   explodeOnWaterImpact      = false;
   deflectionOnWaterImpact   = 0.0;
   fizzleUnderwaterMS        = 15000;

   activateDelayMS = -1;
   numFlares         = 35;
   flareColor        = "0.15 0.2 1";
   flareModTexture   = "flaremod";
   flareBaseTexture  = "flarebase";

	sound       = PlasmaProjectileSound;
   fireSound    = PlasmaFireSound;
   wetFireSound = PlasmaFireWetSound;

   hasLight    = true;
   lightRadius = 8.0;
   lightColor  = "0.15 0.2 1";
};

function MeteorCannonBlast::onTick(%this, %proj)
{
    Parent::onTick(%this, %proj);

    if(%proj.tickCount % 8 == 0)
        createRemoteProjectile("LinearFlareProjectile", "MeteorTrailCharge", %proj.initialDirection, %proj.position, 0, %proj.instigator);
}

datablock ParticleData(StreakTrailParticle)
{
   dragCoefficient      = 1.5;
   gravityCoefficient   = 0;
   inheritedVelFactor   = 0.0;
   constantAcceleration = 0.0;
   lifetimeMS           = 800;
   lifetimeVarianceMS   = 0;
   textureName          = "particleTest";
   colors[0]     = "0.6 0.6 0.6 0.5";
   colors[1]     = "0.2 0.2 0.2 0";
   sizes[0]      = 0.6;
   sizes[1]      = 0.8;
};

datablock ParticleEmitterData(StreakTrailEmitter)
{
   ejectionPeriodMS = 5;
   periodVarianceMS = 0;
   ejectionVelocity = 1;
   velocityVariance = 1.0;
   ejectionOffset   = 0.0;
   thetaMin         = 0;
   thetaMax         = 10;
   phiReferenceVel  = 0;
   phiVariance      = 360;
   overrideAdvances = false;
   particles = "StreakTrailParticle";
};

datablock ExplosionData(HeavyPlasmaExplosion)
{
   explosionShape = "effect_plasma_explosion.dts";
   soundProfile   = HeavyPlasmaExpSound;
   particleEmitter = PlasmaExplosionEmitter;
   particleDensity = 150;
   particleRadius = 3.25;
   faceViewer = true;

   sizes[0] = "2.0 2.0 2.0";
   sizes[1] = "2.0 2.0 2.0";
   times[0] = 0.0;
   times[1] = 1.0;
};
